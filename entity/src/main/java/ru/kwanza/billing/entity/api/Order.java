package ru.kwanza.billing.entity.api;

import ru.kwanza.console.security.api.AuthField;
import ru.kwanza.console.security.api.AuthIdField;
import ru.kwanza.dbtool.orm.annotations.Association;
import ru.kwanza.dbtool.orm.annotations.Field;
import ru.kwanza.dbtool.orm.annotations.IdField;
import ru.kwanza.dbtool.orm.annotations.ManyToOne;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Vasily Vorobyov
 */
public abstract class Order implements Serializable {

    @IdField("id")
    @AuthIdField
    @AuthField(name = "ID")
    private Long id;

    @Field("session_record_id")
    private Long sessionRecordId;

    @Field("exchange_id")
    private Long exchangeId;

    @Field("processed_at")
    @AuthField(name = "Processed at")
    private Date processedAt;

    @Association(property = "id", relationProperty = "id")
    private RejectedReport rejectedReport;

    @ManyToOne(property = "sessionRecordId")
    private SessionHistoryRecord sessionHistoryRecord;

    @ManyToOne(property = "exchangeId")
    private RESTExchange exchange;

    protected int type ;

    protected Order() {
    }

    protected Order(Long id, Long sessionRecordId, Long exchangeId, Date processedAt, OrderType orderType) {
        this.id = id;
        this.exchangeId = exchangeId;
        this.sessionRecordId = sessionRecordId;
        this.processedAt = processedAt;
        this.type = orderType.getCode();
    }

    public Long getId() {
        return id;
    }

    public Long getSessionRecordId() {
        return sessionRecordId;
    }

    public SessionHistoryRecord getSessionHistoryRecord() {
        return sessionHistoryRecord;
    }

    public Date getProcessedAt() {
        return processedAt;
    }

    public RejectedReport getRejectedReport() {
        return rejectedReport;
    }

    public Long getExchangeId() {
        return exchangeId;
    }
    public RESTExchange getExchange() {
        return exchange;
    }


    public abstract void initType();
}
