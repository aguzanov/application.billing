package ru.kwanza.billing.processor.signer;

import ru.kwanza.billing.api.ISigned;

/**
 * @author henadiy
 */
public interface ISignatureGenerator {

    String getSignature(byte[] data);

}
