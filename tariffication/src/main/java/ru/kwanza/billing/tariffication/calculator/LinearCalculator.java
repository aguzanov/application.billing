package ru.kwanza.billing.tariffication.calculator;

import ru.kwanza.billing.tariffication.*;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

/**
 * @author henadiy
 */
public class LinearCalculator implements ICalculator<LinearCalculator.Arguments, AmountResult<LinearCalculator.Arguments>> {

    private final BigDecimal argumentScale;
    private final BigDecimal resultScale;
    private final BigDecimal resultConst;

    public LinearCalculator(BigDecimal argumentScale, BigDecimal resultScale, BigDecimal resultConst) {
        this.argumentScale = argumentScale;
        this.resultScale = resultScale;
        this.resultConst = resultConst;
    }

    public AmountResult<Arguments> calculate(Arguments arguments) {
        BigDecimal res = resultScale
                .divide(argumentScale, MathContext.DECIMAL128)
                .multiply(arguments.getVariableValue())
                .add(resultConst)
                .setScale(4, RoundingMode.HALF_UP);

        long amount = res.setScale(0, RoundingMode.FLOOR).longValue();
        if (0 != BigDecimal.ZERO.compareTo(res.remainder(BigDecimal.ONE).abs())) {
            if (0L == amount) amount = 1L;
            return new AmountResult<Arguments>(amount, new Arguments(getArgumentForAmount(amount)),
                    ResultCode.SUCCESS_WITH_CORRECTIONS);
        } else {
            return new AmountResult<Arguments>(amount, arguments, ResultCode.SUCCESS);
        }
    }

    protected BigDecimal getArgumentForAmount(Long amount) {
        return BigDecimal.valueOf(amount)
                .subtract(resultConst)
                .multiply(argumentScale)
                .divide(resultScale, MathContext.DECIMAL128);
    }

    public static class Arguments implements IArguments {
        private final BigDecimal variableValue;

        public Arguments(Long variable) {
            this.variableValue = BigDecimal.valueOf(variable);
        }
        public Arguments(BigDecimal variable) {
            this.variableValue = variable;
        }

        public BigDecimal getVariableValue() {
            return variableValue;
        }
    }

}
