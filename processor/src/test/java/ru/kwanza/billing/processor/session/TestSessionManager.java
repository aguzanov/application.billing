package ru.kwanza.billing.processor.session;

import ru.kwanza.billing.api.Session;
import ru.kwanza.billing.api.SessionId;
import ru.kwanza.billing.api.order.OpenSessionOrder;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @author henadiy
 */
public class TestSessionManager extends NopSessionManager {

	private final Map<SessionId, Session> sessions = new HashMap<SessionId, Session>();

	public Map<Long, Session> open(Collection<OpenSessionOrder> openSessionOrders) {
		Map<Long, Session> res = super.open(openSessionOrders);
		for(Session ses : res.values()) {
			sessions.put(ses.getId(), ses);
		}
		return res;
	}

	public Map<SessionId, Session> close(Collection<SessionId> sessionIdsToClose) {
		Map<SessionId, Session> res = super.close(sessionIdsToClose);
		for(Session ses : res.values()) {
			sessions.remove(ses.getId());
		}
		return res;
	}

	public Map<SessionId, Session> get(Collection<SessionId> sessionIds) {
		Map<SessionId, Session> res = new HashMap<SessionId, Session>(sessionIds.size());
		for(SessionId sessionId : sessionIds) {
			if (sessions.containsKey(sessionId)) res.put(sessionId, sessions.get(sessionId));
		}
		return res;
	}

	public void putSession(Session session) {
		sessions.put(session.getId(), session);
	}
}
