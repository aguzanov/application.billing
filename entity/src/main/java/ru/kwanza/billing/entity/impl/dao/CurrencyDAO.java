package ru.kwanza.billing.entity.impl.dao;

import ru.kwanza.billing.entity.api.Currency;
import ru.kwanza.billing.entity.api.dao.ICurrencyDAO;
import ru.kwanza.dbtool.core.UpdateException;
import ru.kwanza.dbtool.orm.api.IEntityManager;
import ru.kwanza.dbtool.orm.api.IQuery;
import ru.kwanza.dbtool.orm.api.OrderBy;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.Map;

/**
 * @author Vasily Vorobyov
 */
public class CurrencyDAO implements ICurrencyDAO {

    @Resource(name = "dbtool.IEntityManager")
    private IEntityManager em;

    private IQuery<Currency> currenciesQuery;

    void init() {
        currenciesQuery = em.queryBuilder(Currency.class).orderBy(OrderBy.ASC("id")).create();
    }

    public Map<Integer, Currency> readByKeys(Collection<Integer> ids) {
        return em.readMapByKeys(Currency.class, ids);
    }

    public Map<Integer, Currency> readAll() {
        return currenciesQuery.prepare().selectMap("id");
    }

    public Currency prepare(Integer id, String alphaCode, Integer exponent, String title) {
        return new Currency(id, alphaCode, title, exponent);
    }

    public Collection<Currency> create(Collection<Currency> currencies) throws UpdateException {
        return em.create(Currency.class, currencies);
    }

    public Collection<Currency> update(Collection<Currency> currencies) throws UpdateException {
        return em.update(Currency.class, currencies);
    }

    public Collection<Currency> delete(Collection<Currency> currencies) throws UpdateException {
        return em.delete(Currency.class, currencies);
    }

}
