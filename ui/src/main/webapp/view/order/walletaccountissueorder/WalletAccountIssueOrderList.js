/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.view.order.walletaccountissueorder.WalletAccountIssueOrderList', {
    extend: 'Billing.view.order.OrderList',

    requires: [
        'Billing.store.WalletAccountIssueOrderStore'
    ],

    alias: ['widget.wallet-account-issue-order-list', 'widget.walletaccountissueorderlist'],
    tabTitle: 'Add accounts to wallet orders',
    caption: 'Add accounts to wallet orders',

    model: 'Billing.model.WalletAccountIssueOrder',
    store: 'Billing.store.WalletAccountIssueOrderStore',
    iconCls: 'i-checklist',

    constructor: function () {
        var me = this;

        me.columns = [
            {
                xtype: 'quicklinkcolumn',
                detailView: 'Billing.view.wallet.WalletCard',
                valueField: 'walletId',
                text: 'Wallet',
                dataIndex: 'walletId',
                width: 200,
                filter: true
            },
            {
                xtype: 'quicklinkcolumn',
                detailView: 'Billing.view.account.AccountCard',
                valueField: 'accountId',
                text: 'Account',
                dataIndex: 'accountId',
                width: 200,
                filter: true
            },
            {
                text: 'Name',
                dataIndex: 'title',
                width: 200,
                filter: true
            }
        ];

        me.callParent(arguments);
    }

});
