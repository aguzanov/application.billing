package ru.kwanza.billing.clearing.entity;

import ru.kwanza.dbtool.orm.annotations.Entity;
import ru.kwanza.dbtool.orm.annotations.Field;
import ru.kwanza.dbtool.orm.annotations.IdField;
import ru.kwanza.dbtool.orm.annotations.ManyToOne;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author henadiy
 */

@Entity(name = "ClearingMember", table = "clearing_member")
public class ClearingMember implements Serializable {

	@IdField("id")
	private Long id;

	@Field("clearing_group_id")
	private Long clearingGroupId;

	@Field("netto_account_id")
	private BigDecimal nettoAccountId;

	@Field("name")
	private String name;

	@ManyToOne(property = "clearingGroupId")
	private ClearingGroup clearingGroup;

	protected ClearingMember() {
	}

	public ClearingMember(Long id, Long clearingGroupId, BigDecimal nettoAccountId, String name) {
		this.id = id;
		this.clearingGroupId = clearingGroupId;
		this.nettoAccountId = nettoAccountId;
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public Long getClearingGroupId() {
		return clearingGroupId;
	}

	public BigDecimal getNettoAccountId() {
		return nettoAccountId;
	}

	public String getName() {
		return name;
	}
}
