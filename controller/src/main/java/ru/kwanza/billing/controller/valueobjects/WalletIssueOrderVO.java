package ru.kwanza.billing.controller.valueobjects;

import ru.kwanza.billing.entity.api.Wallet;

import java.util.Date;

/**
 * @author Dmitry Zhukov
 */
public class WalletIssueOrderVO extends OrderVO {


    private Wallet wallet;

    public WalletIssueOrderVO(Long id, Long sessionRecordId, Date processedAt, Integer rejectCode, Wallet wallet) {
        super(id, sessionRecordId, processedAt, rejectCode);
        this.wallet = wallet;
    }


    public Wallet getWallet() {
        return wallet;
    }
}
