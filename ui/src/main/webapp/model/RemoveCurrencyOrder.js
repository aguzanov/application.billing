/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.model.RemoveCurrencyOrder', {
    extend: 'Billing.model.Order',
    fields: [
        'currencyId',
        {
            name: 'currency',
            persist: false
        }
    ],

    detailView: 'Billing.view.order.removecurrencyorder.RemoveCurrencyOrderCard',

    permissions: {
        read: "RemoveCurrencyOrder:read",
        create: false,
        update: false,
        destroy: false
    },

    proxy: {
        type: 'ajax',
        apiUrl: 'billing-controller/removecurrencyorder/'

    },

    statics: {
        currencyRenderer: function (value, meta, record) {
            var status = record.get('rejectCode');
            var currency = record.get('currency');
            if (status || !currency) {
                return record.get('currencyId');
            }
            return Console.application.QuickLink.createLink(record.get('currencyId'),
                'Billing.view.currency.CurrencyCard', value);
        }
    }
});
