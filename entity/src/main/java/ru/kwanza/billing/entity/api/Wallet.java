package ru.kwanza.billing.entity.api;

import ru.kwanza.billing.api.WalletStatus;
import ru.kwanza.billing.api.order.*;
import ru.kwanza.console.security.api.AuthEntity;
import ru.kwanza.console.security.api.AuthField;
import ru.kwanza.console.security.api.AuthIdField;
import ru.kwanza.dbtool.orm.annotations.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Types;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Vasily Vorobyov
 */
@Entity(table = "wallet")
@AuthEntity(name = "Wallet", description = "Wallet")
public class Wallet implements Serializable {

    @IdField(value="id", type = Types.DECIMAL)
    @AuthIdField
    @AuthField(name = "ID")
    private Long id;

    @VersionField("version")
    private Long version;

    @Field("created_at")
    @AuthField(name = "Created at")
    private Date createdAt;


    @Field( "order_id")
    @AuthField(name = "ID of the order")
    private Long orderId;

    @Field("holder_id")
    @AuthField(name = "Holder ID")
    private String holderId;

    @Field("title")
    @AuthField(name = "Name")
    private String title;

    @Field("status_code")
    @AuthField(name = "Status")
    private Integer statusCode;

    @Field("wallet_account_counter")
    @AuthField(name = "Number of accounts")
    private Integer walletAccountCounter;

    @ManyToOne(property = "orderId")
    private WalletIssueOrder order;


    public Wallet() {
    }

    public Wallet(Integer id, Long orderId, Date createdAt, String holderId, String title) {
        this.id = buildId(id);
        this.orderId = orderId;
        this.createdAt = createdAt;
        this.statusCode = WalletStatus.ACTIVE.getCode();
        this.holderId = holderId;
        this.title = title;
        this.walletAccountCounter = 0;
    }

    public Long getId() {
        return id;
    }

    public String getIdAsString() {
        return String.format("%015d", id);
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Long getOrderId() {
        return orderId;
    }

    public String getHolderId() {
        return holderId;
    }

    public void setHolderId(String holderId) {
        this.holderId = holderId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getWalletAccountCounter() {
        return walletAccountCounter;
    }

    public void setWalletAccountCounter(Integer walletAccountCounter) {
        this.walletAccountCounter = walletAccountCounter;
    }

    public WalletStatus getStatus() {
        return WalletStatus.findByCode(statusCode);
    }

    public void setStatus(WalletStatus status) {
        this.statusCode = status.getCode();
    }

    public static boolean checkId(Long id) {
        return CheckDigitUtil.verify(BigDecimal.valueOf(id));
    }

    public static Long buildId(Integer seq) {
        Calendar ts = Calendar.getInstance();
        Long id = ((ts.get(Calendar.YEAR) % 100) * 100 + ts.get(Calendar.WEEK_OF_YEAR)) * (long)1E10 + seq;
        int checkDigit = CheckDigitUtil.calculate(BigDecimal.valueOf(id));
        id = id * 10 + checkDigit;
        return id;
    }


    @Override
    public String toString() {
       return title;
    }
}
