package ru.kwanza.billing.processor;

import ru.kwanza.billing.entity.api.AccountRecord;
import ru.kwanza.billing.entity.api.ConversionInstruction;
import ru.kwanza.billing.entity.api.PaymentInstruction;
import ru.kwanza.billing.entity.api.RejectedPaymentInstruction;
import ru.kwanza.billing.entity.api.dao.IPaymentInstructionDAO;
import ru.kwanza.billing.mock.SyncMaps;
import ru.kwanza.dbtool.core.UpdateException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Random;

/**
 * @author Dmitry Zhukov
 */
public class MockPaymentInstructionDAO implements IPaymentInstructionDAO {

    private Collection<BigDecimal> accountIds;

    private SyncMaps<BigDecimal, AccountRecord> withdrawals =
            new SyncMaps<BigDecimal, AccountRecord>(new SyncMaps.KeyExtractor<BigDecimal, AccountRecord>() {
                public BigDecimal extractKey(AccountRecord rec) {
                    return rec.getId();
                }
            });
    private SyncMaps<BigDecimal, AccountRecord> deposits =
            new SyncMaps<BigDecimal, AccountRecord>(new SyncMaps.KeyExtractor<BigDecimal, AccountRecord>() {
                public BigDecimal extractKey(AccountRecord rec) {
                    return rec.getId();
                }
            });

    public MockPaymentInstructionDAO() {
        accountIds = new ArrayList<BigDecimal>();
        withdrawals.addNonUniqueIndex("accountId", new SyncMaps.IndexExtractor<AccountRecord>() {
            public Object extractKey(AccountRecord rec) {
                return rec.getSourceAccountId();
            }
        });
        deposits.addNonUniqueIndex("accountId", new SyncMaps.IndexExtractor<AccountRecord>() {
            public Object extractKey(AccountRecord rec) {
                return rec.getTargetAccountId();
            }
        });
        init();
    }

    //dzhukov: a method for generating entries
    private void init() {
        initAccountIds();
        initCreditsDebits();
    }

    //dzhukov: add id accounts
    private void initAccountIds() {
        accountIds.add(BigDecimal.valueOf(170943000L));
    }

    //dzhukov: generated debits and credits.
    private void initCreditsDebits() {
        long i = 0;
        for (BigDecimal accountId : accountIds) {
            Random random = new Random();
            random.nextLong();
            withdrawals.createUpdate(new AccountRecord(
                    new BigDecimal(String.valueOf(i++)), 2001L, accountId, 2, 90L, 100L, null, null, 90L, 100L, new Date(2014, 4, 7, 17, 36), "test"));
            withdrawals.createUpdate(new AccountRecord(
                    new BigDecimal(String.valueOf(i++)), 2001L, accountId, 2, 90L, 100L, null, null, 90L, 100L, new Date(2014, 4, 7, 17, 36), "test"));
            withdrawals.createUpdate(new AccountRecord(
                    new BigDecimal(String.valueOf(i++)), 2001L, accountId, 2, 90L, 100L, null, null, 90L, 100L, new Date(2014, 4, 7, 17, 36), "test"));
            withdrawals.createUpdate(new AccountRecord(
                    new BigDecimal(String.valueOf(i++)), 2001L, accountId, 2, 90L, 100L, null, null, 90L, 100L, new Date(2014, 4, 7, 17, 36), "test"));
            deposits.createUpdate(new AccountRecord(
                    new BigDecimal(String.valueOf(i++)), 2001L, null, null, 90L, 100L, accountId, 2, 90L, 100L, new Date(2014, 4, 7, 17, 36), "test"));
            deposits.createUpdate(new AccountRecord(
                    new BigDecimal(String.valueOf(i++)), 2001L, null, null, 90L, 100L, accountId, 2, 90L, 100L, new Date(2014, 4, 7, 17, 36), "test"));
            deposits.createUpdate(new AccountRecord(
                    new BigDecimal(String.valueOf(i++)), 2001L, null, null, 90L, 100L, accountId, 2, 90L, 100L, new Date(2014, 4, 7, 17, 36), "test"));
            deposits.createUpdate(new AccountRecord(
                    new BigDecimal(String.valueOf(i++)), 2001L, null, null, 90L, 100L, accountId, 2, 90L, 100L, new Date(2014, 4, 7, 17, 36), "test"));
        }
    }

    public PaymentInstruction preparePaymentInstruction(BigDecimal id, Long orderId, Integer indexInOrder,
                                                        BigDecimal sourceAccountId, BigDecimal sourceWalletAccountId, Long sourceAmount,
                                                        BigDecimal targetAccountId, BigDecimal targetWalletAccountId, Long targetAmount,
                                                        Date recordedAt, String description) {
        return new PaymentInstruction(id, orderId, indexInOrder,
                sourceAccountId, sourceWalletAccountId, sourceAmount,
                targetAccountId, targetWalletAccountId, targetAmount,
                recordedAt, description);
    }

    public Collection<PaymentInstruction> createPaymentInstructions(Collection<PaymentInstruction> instructions) throws UpdateException {
        return instructions;
    }

    public ConversionInstruction prepareConversionInstruction(BigDecimal id,
                                                              Date issuedAt, Date expiresIn,
                                                              Integer sourceCurrencyId, BigDecimal sourceScale,
                                                              Integer targetCurrencyId, BigDecimal targetScale) {
        return new ConversionInstruction(id, issuedAt, expiresIn,
                sourceCurrencyId, sourceScale, targetCurrencyId, targetScale);
    }

    public Collection<ConversionInstruction> createConversionInstructions(Collection<ConversionInstruction> instructions) throws UpdateException {
        return instructions;
    }

    public RejectedPaymentInstruction prepareRejectedPaymentInstruction(BigDecimal id, Long sourceIncomingBalance, Long targetIncomingBalance) {
        return new RejectedPaymentInstruction(id, sourceIncomingBalance, targetIncomingBalance);
    }

    public Collection<RejectedPaymentInstruction> createRejectedPaymentInstructions(Collection<RejectedPaymentInstruction> rejectedPaymentInstructions) throws UpdateException {
        return rejectedPaymentInstructions;
    }

    public AccountRecord prepareAccountRecord(BigDecimal id, Long orderId,
                                              BigDecimal sourceAccountId, Integer sourceRecordNumber, Long sourceAmount, Long sourceIncomingBalance,
                                              BigDecimal targetAccountId, Integer targetRecordNumber, Long targetAmount, Long targetIncomingBalance,
                                              Date processedAt, String description) {
        return new AccountRecord(id, orderId,
                sourceAccountId, sourceRecordNumber, sourceAmount, sourceIncomingBalance,
                targetAccountId, targetRecordNumber, targetAmount, targetIncomingBalance,
                processedAt, description);
    }

    public Collection<AccountRecord> createAccountRecords(Collection<AccountRecord> records) throws UpdateException {
        return records;
    }


    public Collection<AccountRecord> selectDepositRecordsByAccountId(BigDecimal accountId, Date recorderedAfter, Date recorderedBefore) {
        Collection<AccountRecord> outRecords = new ArrayList<AccountRecord>();
        Collection<AccountRecord> depositRecords = deposits.readByNonUniqueIndex("accountId", accountId);


        for (AccountRecord depositRecord : depositRecords) {
            if (((recorderedAfter == null || recorderedAfter.before(depositRecord.getProcessedAt()))) && ((recorderedBefore == null
                    || recorderedBefore.after(depositRecord.getProcessedAt())))) {
                outRecords.add(depositRecord);
            }
        }

        return outRecords;
    }

    public Collection<AccountRecord> selectWithdrawalRecordsByAccountId(BigDecimal accountId, Date recorderedAfter, Date recorderedBefore) {
        Collection<AccountRecord> outRecords = new ArrayList<AccountRecord>();
        Collection<AccountRecord> withdrawRecords = withdrawals.readByNonUniqueIndex("accountId", accountId);


        for (AccountRecord withdrawRecord : withdrawRecords) {
            if (((recorderedAfter == null || recorderedAfter.before(withdrawRecord.getProcessedAt()))) && ((recorderedBefore == null
                    || recorderedBefore.after(withdrawRecord.getProcessedAt())))) {
                outRecords.add(withdrawRecord);
            }
        }

        return outRecords;
    }

    public Collection<AccountRecord> selectDepositRecordsByAccountId(Collection<BigDecimal> ids, Date recorderedAfter, Date recorderedBefore) {
        Collection<AccountRecord> outDepositRecords = new ArrayList<AccountRecord>();
        for (BigDecimal id : ids) {
            outDepositRecords.addAll(selectDepositRecordsByAccountId(id, recorderedAfter, recorderedBefore));
        }
        return outDepositRecords;
    }

    public Collection<AccountRecord> selectWithdrawalRecordsByAccountId(Collection<BigDecimal> ids, Date recorderedAfter,
                                                                             Date recorderedBefore) {
        Collection<AccountRecord> outCreditRecords = new ArrayList<AccountRecord>();
        for (BigDecimal id : ids) {
            outCreditRecords.addAll(selectWithdrawalRecordsByAccountId(id, recorderedAfter, recorderedBefore));
        }
        return outCreditRecords;
    }

}
