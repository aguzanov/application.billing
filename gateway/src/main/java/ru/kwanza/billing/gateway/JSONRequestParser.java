package ru.kwanza.billing.gateway;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.json.UTF8StreamJsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.type.ArrayType;
import ru.kwanza.billing.api.Request;

import java.io.IOException;
import java.text.SimpleDateFormat;

/**
 * @author Vasily Vorobyov
 */
public class JSONRequestParser<R extends Request> implements IRequestParser<R> {

    private static ObjectReader reader;

    static {
        reader = new ObjectMapper()
                .configure(JsonGenerator.Feature.WRITE_BIGDECIMAL_AS_PLAIN, true)
                .configure(JsonGenerator.Feature.WRITE_NUMBERS_AS_STRINGS, true)
                .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
                .setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'"))
                .reader();
    }

    private Class<R> clazz;
    private ArrayType arrayType;

    public JSONRequestParser(Class<R> clazz) {
        this.clazz = clazz;
        this.arrayType = reader.getTypeFactory().constructArrayType(clazz);
    }

    public R[] parse(String content) throws IOException {

        final JsonNode jsonNode = reader.readTree(content);
        R[] requests;

        if (jsonNode.isArray()) {
            requests = reader.readValue(jsonNode.traverse(reader), arrayType);
        } else {
            requests = (R[]) new Request[]{jsonNode.traverse(reader).readValueAs(clazz)};
        }
        return requests;
    }
}
