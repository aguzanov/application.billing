/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.view.order.registerissuerorder.RegisterIssuerOrderList', {
    extend: 'Billing.view.order.OrderList',

    requires: [
        'Billing.store.RegisterIssuerOrderStore'
    ],

    alias: ['widget.register-issuer-order-list', 'widget.registerissueorderlist'],
    tabTitle: 'Creating and editing issuers orders',
    caption: 'Creating and editing issuers orders',

    model: 'Billing.model.RegisterIssuerOrder',
    store: 'Billing.store.RegisterIssuerOrderStore',
    iconCls: 'i-document-survey',

    constructor: function () {
        var me = this;

        me.columns = [
            {
                text: 'Type of operation',
                width: 200,
                renderer: function (value, meta, record) {
                    var issuerId = record.get('issuerId');
                    if (issuerId) {
                        meta.iconCls = 'i-action-edit';
                        return 'Edit';
                    }
                    meta.iconCls = 'i-action-add';
                    return 'Add';
                }
            },{

              text: "Order params",
                columns:[
                    {
                        text: 'Bank ID',
                        dataIndex: 'bin',
                        width: 80,
                        filter: true,
                        renderer: Billing.model.RegisterIssuerOrder.issuerRenderer
                    },
                    {
                        text: 'Title',
                        dataIndex: 'title',
                        width: 100,
                        filter: true
                    }
                ]
            }
        ];

        me.callParent(arguments);
    }
});
