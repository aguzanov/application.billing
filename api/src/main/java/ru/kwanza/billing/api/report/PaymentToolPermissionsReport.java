package ru.kwanza.billing.api.report;

import com.fasterxml.jackson.annotation.JsonTypeName;
import ru.kwanza.billing.api.PaymentToolId;
import ru.kwanza.billing.api.PaymentToolPermission;

import java.util.Date;
import java.util.Map;

/**
 * @author henadiy
 */
@JsonTypeName(value = "PAYMENT_TOOL_PERMISSIONS_REPORT")
public class PaymentToolPermissionsReport extends Report {

    private PaymentToolId paymentToolId;
    private Map<String, PaymentToolPermission[]> userPermissions;

    public PaymentToolPermissionsReport() {
    }

    public PaymentToolPermissionsReport(Long id, Date processedAt,
                                        PaymentToolId paymentToolId, Map<String, PaymentToolPermission[]> userPermissions) {
        super(id, processedAt);
        this.paymentToolId = paymentToolId;
        this.userPermissions = userPermissions;
    }

    public PaymentToolId getPaymentToolId() {
        return paymentToolId;
    }

    public Map<String, PaymentToolPermission[]> getUserPermissions() {
        return userPermissions;
    }
}
