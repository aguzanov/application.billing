package ru.kwanza.billing.api;

/**
 * Type dispo
 *
 * @author Vasily Vorobyov
 */
public enum RequestType {

    O_WALLET_ISSUE("O:WALLET:ISSUE", "Wallet emission"),
    O_WALLET_SUSPEND("O:WALLET:SUSPEND", "Suspend wallet serving"),
    O_WALLET_RESUME("O:WALLET:RESUME", "Resume wallet serving"),
    O_ACCOUNT_ISSUE("O:ACCOUNT:ISSUE", "Open account"),
    O_ACCOUNT_SUSPEND("O:ACCOUNT:SUSPEND", "Suspend account serving"),
    O_ACCOUNT_RESUME("O:ACCOUNT:RESUME", "Resume account serving"),
    O_PAYMENT("O:PAYMENT", "Payment"),
    O_WALLET_ACCOUNT_ISSUE("O:WALLET_ACCOUNT:ISSUE", "Inclusion of accounts in the wallet"),
    O_CURRENCY_REGISTER("O:CURRENCY:REGISTER", "Registration  of currency"),
    O_ISSUER_REGISTER("O:ISSUER:REGISTER", "Registration of the issuer"),
    O_CURRENCY_REMOVE("O:CURRENCY:REMOVE", "Removing currency from the system"),
    O_WALLET_CLOSE("O:WALLET:CLOSE", "Close wallet"),
    O_WALLET_ACTIVATE("O:WALLET:ACTIVATE", "Wallet activation"),
	O_SESSION_OPEN("O:SESSION:OPEN", "Open session"),
	O_SESSION_CLOSE("O:SESSION:CLOSE", "Slose  session"),
    O_PAYMENT_TOOL_GRANT_PERMISSIONS("O:PAYMENT_TOOL:GRANT_PERMISSIONS", "Grant permissions for account"),

	Q_ACCOUNT_STATEMENT("Q:ACCOUNT_STATEMENT", "Account statement"),
	Q_ACCOUNT("Q:ACCOUNT", "Certificate of account status"),
	Q_CURRENCIES("Q:CURRENCIES", "Currencies help"),
	Q_CONVERSION_INSTRUCTION("Q:CONVERSION_INSTRUCTION", "Guidance on the conversion");

    /**
     * Allocated a numeric code for the type of orders
     */
    private String code;
    /**
     * Name the type of orders
     */
    private String title;

    private RequestType(String code, String title) {
        this.code = code;
        this.title = title;
    }

    public String getCode() {
        return code;
    }

    public String getTitle() {
        return title;
    }

    public static RequestType findByCode(String code) {

        for (RequestType o: values()) {
           if (o.code.equals(code)) return o;
        }

        throw new IllegalArgumentException("Unknown code: " + code);
    }
}
