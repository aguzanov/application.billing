package ru.kwanza.billing.processor.session;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.model.authentication.UserAuthenticationContext;
import com.atlassian.crowd.model.authentication.ValidationFactor;
import com.atlassian.crowd.service.client.CrowdClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import ru.kwanza.billing.api.Session;
import ru.kwanza.billing.api.SessionId;
import ru.kwanza.billing.api.order.OpenSessionOrder;
import ru.kwanza.billing.entity.impl.SessionHistory;

import java.util.*;

/**
 * @author henadiy
 *
 * Session Manager for Atlassian CROWD
 *
 * Read more:
 * https://docs.atlassian.com/crowd/current/com/atlassian/crowd/service/client/CrowdClient.html
 * https://developer.atlassian.com/display/CROWDDEV/Crowd+2.1+REST+Java+Client+Migration+Guide
 *
 */
public class CrowdSessionManager implements ISessionManager {

	private static final Logger logger = LoggerFactory.getLogger(CrowdSessionManager.class);

    private CrowdClient crowdClient;
    private SessionHistory sessionHistory;

	public void setCrowdClient(CrowdClient crowdClient) {
		this.crowdClient = crowdClient;
	}

    public void setSessionHistory(SessionHistory sessionHistory) {
        this.sessionHistory = sessionHistory;
    }

    public Map<Long, Session> open(Collection<OpenSessionOrder> openSessionOrders) {
		Map<Long, Session> res = new HashMap<Long, Session>(openSessionOrders.size());

		for(OpenSessionOrder order : openSessionOrders) {
			// get the user name
			String username = (String)order.getArguments().get(OpenSessionOrder.ARG_USERNAME);

			try {
                // the resulting verification field
//                Object validationFactorsSource = order.getArguments().get(OpenSessionOrder.ARG_VALIDATION_FACTORS);
//                ValidationFactor[] validationFactors = getValidationFactors(validationFactorsSource);

				// take a token from the arguments or if it is not trying to authenticate
                String token = (String)order.getArguments().get(OpenSessionOrder.ARG_TOKEN);
                if (null == token) {
                    token = crowdClient.authenticateSSOUser(
                            new UserAuthenticationContext(username,
                                    new PasswordCredential((String)order.getArguments().get(OpenSessionOrder.ARG_PASSWORD)),
                                    null, "ru.kwanza.billing"));
                }

				// if authentication is performed, a data session
                SessionId sessionId = new SessionId((token));
//                if (validationFactors.length > 0) {
//                    sessionId.putDetail(SessionId.DETAIL_VALIDATION_FACTORS, validationFactorsSource);
//                }
				res.put(order.getId(), getSession(sessionId));
			} catch (Exception e) {
				logger.error("Unable to authenticate user {} because {}", new Object[]{username, e});
			}
		}

		return res;
	}

	public Map<SessionId, Session> close(Collection<SessionId> sessionIdsToClose) {
		Map<SessionId, Session> res = new HashMap<SessionId, Session>(sessionIdsToClose.size());
		Date processingDate = new Date();

		for(SessionId sessionId : sessionIdsToClose) {
			try {
				// invalidiem session
				crowdClient.invalidateSSOToken(sessionId.getToken());

				// the trigger reset session
				invalidateSession(sessionId);

				// return session from current expiration date
				res.put(sessionId, new Session(sessionId, processingDate, null, Collections.EMPTY_SET));
			} catch (Exception e) {
				logger.error("Unable to invalidate sessionId {} because {}", new Object[]{sessionId, e});
			}
		}

        // registered closed session in history
        sessionHistory.registerClosed(res.values());

		return res;
	}

	public Map<SessionId, Session> get(Collection<SessionId> sessionIds) {
		Map<SessionId, Session> res = new HashMap<SessionId, Session>(sessionIds.size());
		for(SessionId sessionId : sessionIds) {
			res.put(sessionId, getSession(sessionId));
		}
		return res;
	}

	@Cacheable(value = "crowdSessions")
	private Session getSession(SessionId sessionId) {
		// get detailed information about the session
		com.atlassian.crowd.model.authentication.Session crowdSession;
		try {
			crowdSession = crowdClient.validateSSOAuthenticationAndGetSession(sessionId.getToken(),
                    Arrays.asList(getValidationFactors(sessionId.getDetails().get(SessionId.DETAIL_VALIDATION_FACTORS))));
		}catch (Exception e) {
			logger.error("Unable to get session by token {} because {}", new Object[]{sessionId, e});
			return null;
		}

		// get detailed information about customer
		com.atlassian.crowd.model.user.User crowdUser;
		try {
			crowdUser = crowdClient.findUserFromSSOToken(sessionId.getToken());
		}catch (Exception e) {
			logger.error("Unable to get user by token {} because {}", new Object[]{sessionId, e});
			return null;
		}

		Set<String> groups = new HashSet<String>();

		// get customer group
		try {
			groups.addAll(crowdClient.getNamesOfGroupsForUser(crowdUser.getName(), 0, 1000));
		}catch (Exception e) {
			logger.error("Unable to get groups of user {} because {}", new Object[]{crowdUser.getName(), e});
		}

		// get nested groups client
		try {
			groups.addAll(crowdClient.getNamesOfGroupsForNestedUser(crowdUser.getName(), 0, 1000));
		}catch (Exception e) {
			logger.error("Unable to get nested groups of user {} because {}", new Object[]{crowdUser.getName(), e});
		}

        // prepare session
        Session session = new Session(sessionId, crowdSession.getExpiryDate(), crowdUser.getName(), groups);

        // the recorded session in history
        sessionHistory.register(Arrays.asList(session));

		return session;
	}

	@CacheEvict(value = "crowdSessions")
	private void invalidateSession(SessionId sessionId) {
	}

    private ValidationFactor[] getValidationFactors(Object source) {
        ValidationFactor[] validationFactors = new ValidationFactor[0];
        if (null != source && source instanceof Map && !((Map)source).isEmpty()) {
            validationFactors = new ValidationFactor[((Map)source).size()];
            int index = 0;
            for(Map.Entry<?,?> f : ((Map<?, ?>)source).entrySet()) {
                validationFactors[index++] = new ValidationFactor(
                        f.getKey().toString(), f.getValue().toString());
            }
        }
        return validationFactors;
    }
}
