package ru.kwanza.billing.entity.api;

import ru.kwanza.console.security.api.AuthEntity;
import ru.kwanza.console.security.api.AuthField;
import ru.kwanza.dbtool.orm.annotations.Entity;
import ru.kwanza.dbtool.orm.annotations.Field;
import ru.kwanza.dbtool.orm.annotations.ManyToOne;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Vasily Vorobyov
 */

@Entity(table = "wallet_account_issue_order")
@AuthEntity(name = "WalletAccountIssueOrder", description = "Order for join wallet with account")
public class WalletAccountIssueOrder extends Order {

    @Field("wallet_id")
    @AuthField(name = "Wallet")
    private Long walletId;

    @Field("account_id")
    @AuthField(name = "Account")
    private BigDecimal accountId;

    @Field("title")
    @AuthField(name = "Name")
    private String title;

    @ManyToOne(property = "accountId")
    private Account account;

    @ManyToOne(property = "walletId")
    private Wallet wallet;

    public WalletAccountIssueOrder() {
    }

    public WalletAccountIssueOrder(Long id, Long sessionRecordId, Long exchangeId, Date processedAt, Long walletId, BigDecimal accountId, String title) {
        super(id, sessionRecordId, exchangeId, processedAt, OrderType.REGISTER_WALLET_ACCOUNT);
        this.walletId = walletId;
        this.accountId = accountId;
        this.title = title;
    }

    public Long getWalletId() {
        return walletId;
    }

    public BigDecimal getAccountId() {
        return accountId;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public void initType() {
        type = OrderType.REGISTER_WALLET_ACCOUNT.getCode();
    }
}
