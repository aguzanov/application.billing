/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.controller.OrderController', {
    extend: 'Ext.app.Controller',
    models: [
        'Billing.model.AccountIssueOrder',
        'Billing.model.DetailOrder',
        'Billing.model.RegisterCurrencyOrder',
        'Billing.model.PaymentOrder',
        'Billing.model.RegisterIssuerOrder',
        'Billing.model.WalletAccountIssueOrder',
        'Billing.model.RemoveCurrencyOrder',
        'Billing.model.AccountRecord',
        'Billing.model.SessionHistory',
        'Billing.model.RESTExchange'
    ],
    stores: [
        'Billing.store.AccountIssueOrderStore',
        'Billing.store.RegisterCurrencyOrderStore',
        'Billing.store.PaymentOrderStore',
        'Billing.store.RegisterIssuerOrderStore',
        'Billing.store.WalletIssueOrderStore',
        'Billing.store.WalletAccountIssueOrderStore',
        'Billing.store.RemoveCurrencyOrderStore',
        'Billing.store.AccountRecordStore',
        'Billing.store.PaymentInstructionStore',
        'Billing.store.DetailOrderStore',
        'Billing.store.SessionHistoryStore',
        'Billing.store.RESTExchangeStore'
    ],
    views: [
        'Billing.view.order.accountissueorder.AccountIssueOrderList',
        'Billing.view.order.accountissueorder.AccountIssueOrderCard',

        'Billing.view.order.registercurrencyorder.RegisterCurrencyOrderList',
        'Billing.view.order.registercurrencyorder.RegisterCurrencyOrderCard',

        'Billing.view.order.paymentorder.PaymentOrderList',
        'Billing.view.order.paymentorder.PaymentOrderCard',

        'Billing.view.order.registerissuerorder.RegisterIssuerOrderList',
        'Billing.view.order.registerissuerorder.RegisterIssuerOrderCard',

        'Billing.view.order.walletissueorder.WalletIssueOrderList',
        'Billing.view.order.walletissueorder.WalletIssueOrderCard',

        'Billing.view.order.walletaccountissueorder.WalletAccountIssueOrderList',
        'Billing.view.order.walletaccountissueorder.WalletAccountIssueOrderCard',

        'Billing.view.order.removecurrencyorder.RemoveCurrencyOrderList',
        'Billing.view.order.removecurrencyorder.RemoveCurrencyOrderCard',

        'Billing.view.order.OrderCard',
        'Billing.view.order.OrderList',
        'Billing.view.order.DetailOrderList',

        'Billing.view.sessionhistory.SessionHistoryList',
        'Billing.view.sessionhistory.SessionHistoryCard',

        'Billing.view.restexchange.RESTExchangeList',
        'Billing.view.restexchange.RESTExchangeCard'
    ],

    init: function () {
        var me = this;
        this.control({
            'paymentorderdetails #paymentInstructionsButton': {
                click: me.paymentInstructionsClick
            },
            'paymentorderdetails #accountRecordsButton': {
                click: me.accountRecordsClick
            }
        });

        Billing.model.DetailOrder.registerMetaData(1, 'REGISTER_ACCOUNT', 'Billing.view.order.accountissueorder.AccountIssueOrderCard');
        Billing.model.DetailOrder.registerMetaData(2, 'REGISTER_WALLET', 'Billing.view.order.walletissueorder.WalletIssueOrderCard');
        Billing.model.DetailOrder.registerMetaData(3, 'REGISTER_WALLET_ACCOUNT', 'Billing.view.order.walletaccountissueorder.WalletAccountIssueOrderCard');
        Billing.model.DetailOrder.registerMetaData(4, 'REGISTER_ISSUER', 'Billing.view.order.registercurrencyorder.RegisterCurrencyOrderCard');
        Billing.model.DetailOrder.registerMetaData(5, 'REGISTER_CURRENCY', 'Billing.view.order.registerissuerorder.RegisterIssuerOrderCard');
        Billing.model.DetailOrder.registerMetaData(6, 'PAYMENT_ORDER', 'Billing.view.order.paymentorder.PaymentOrderCard');
        Billing.model.DetailOrder.registerMetaData(7, 'SUSPEND_WALLET_ORDER', 'Billing.view.order.OrderCard');
        Billing.model.DetailOrder.registerMetaData(8, 'ACTIVATE_WALLET_ORDER', 'Billing.view.order.OrderCard');
        Billing.model.DetailOrder.registerMetaData(9, 'CLOSE_WALLET_ORDER', 'Billing.view.order.OrderCard');
        Billing.model.DetailOrder.registerMetaData(10, 'PERMISSION_ORDER', 'Billing.view.order.OrderCard');
        Billing.model.DetailOrder.registerMetaData(11, 'REMOVE_CURRENCY_ORDER', 'Billing.view.order.removecurrencyorder.RemoveCurrencyOrderCard');
    },
    paymentInstructionsClick: function (button, pressed) {
        var details = button.up('paymentorderdetails');
        var paymentInstructions = details.items.get('paymentInstructionsGroup');
        var accountRecords = details.items.get('accountRecordsGroup');
        paymentInstructions.setVisible(true);
        accountRecords.setVisible(false);
    },
    accountRecordsClick: function (button, pressed) {
        var details = button.up('paymentorderdetails');
        var paymentInstructions = details.items.get('paymentInstructionsGroup');
        var accountRecords = details.items.get('accountRecordsGroup');
        paymentInstructions.setVisible(false);
        accountRecords.setVisible(true);
    }
});
