package ru.kwanza.billing.api.report;

import com.fasterxml.jackson.annotation.JsonTypeName;

import java.util.Date;

/**
 * @author Vasily Vorobyov
 */
@JsonTypeName(value = "CURRENCIES_REPORT")
public class CurrenciesReport extends Report {

    public static class Currency {
        private Integer id;
        private Integer exponent;
        private String alphaCode;
        private String title;

        protected Currency() {
        }

        public Currency(Integer id, Integer exponent, String alphaCode, String title) {
            this.id = id;
            this.exponent = exponent;
            this.alphaCode = alphaCode;
            this.title = title;
        }

        public Integer getId() {
            return id;
        }

        public Integer getExponent() {
            return exponent;
        }

        public String getAlphaCode() {
            return alphaCode;
        }

        public String getTitle() {
            return title;
        }
    }

    private Currency[] currencies;

    protected CurrenciesReport() {
    }

    public CurrenciesReport(Long id, Date processedAt, Currency[] currencies) {
        super(id, processedAt);
        this.currencies = currencies;
    }

    public Currency[] getCurrencies() {
        return currencies;
    }
}
