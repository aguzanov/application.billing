/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.store.WalletStore', {
    extend: 'Ext.data.Store',
    model: 'Billing.model.Wallet',
    autoLoad: true,
    remoteFilter: true,
    remoteSort: false,
    autoSync: false
});