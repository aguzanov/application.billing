/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.store.WalletAccountStore', {
    extend: 'Ext.data.Store',
    model: 'Billing.model.WalletAccount',
    autoLoad: true,
    remoteFilter: true,
    remoteSort: false,
    autoSync: false
});