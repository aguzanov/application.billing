package ru.kwanza.billing.api.order;

import com.fasterxml.jackson.annotation.JsonTypeName;
import ru.kwanza.billing.api.RequestType;
import ru.kwanza.billing.api.SessionId;

import java.math.BigDecimal;

/**
 * @author Vasily Vorobyov
 */
@JsonTypeName(value = "O_WALLET_ACCOUNT_ISSUE")
public class WalletAccountIssueOrder extends Order {

    private Long walletId;
    private BigDecimal accountId;
    private String title;

    protected WalletAccountIssueOrder() {
    }

    public WalletAccountIssueOrder(Long id, SessionId sessionId, Long walletId, BigDecimal accountId, String title) {
        super(id, sessionId);
        this.walletId = walletId;
        this.accountId = accountId;
        this.title = title;
    }

    @Override
    public RequestType getRequestType() {
        return RequestType.O_WALLET_ACCOUNT_ISSUE;
    }

    public Long getWalletId() {
        return walletId;
    }

    public BigDecimal getAccountId() {
        return accountId;
    }

    public String getTitle() {
        return title;
    }
}
