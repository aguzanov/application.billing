package ru.kwanza.billing.controller.valueobjects;

import ru.kwanza.billing.api.report.RejectCode;

/**
 * @author Dmitry Zhukov
 */
public class WalletAccountException extends Exception {
    public WalletAccountException(String message) {
        super(message);
    }

    public static class WalletAccountCreateException extends WalletAccountException {

        public WalletAccountCreateException(RejectCode rejectCode) {
            super("Failed to associate the wallet and the account. " + rejectCode.getTitle());
        }
    }

    public static class WalletAccountNotFoundException extends WalletAccountException {

        public WalletAccountNotFoundException() {
            super("Wallet account doesn't exist!");
        }
    }
}
