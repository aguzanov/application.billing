package ru.kwanza.billing.entity.api.dao;

import ru.kwanza.billing.entity.api.Wallet;
import ru.kwanza.dbtool.core.UpdateException;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Vasily Vorobyov
 */
public interface IWalletDAO {

    List<Wallet> readAll();

    Map<Long, Wallet> readByKeys(Collection<Long> ids);

    Map<Long, Wallet> readByOrderIds(Collection<Long> orderIds);

    Wallet prepare(Integer counter, Long orderId, Date createdAt, String holderId, String title);

    Collection<Wallet> create(Collection<Wallet> wallets) throws UpdateException;

    void update(Collection<Wallet> wallets) throws UpdateException;
}
