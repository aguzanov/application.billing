package ru.kwanza.billing.entity.api;


import com.google.gson.JsonObject;
import org.glassfish.grizzly.http.HttpContent;
import org.glassfish.grizzly.http.HttpHeader;
import org.glassfish.grizzly.http.HttpPacket;
import org.glassfish.grizzly.http.HttpResponsePacket;
import org.glassfish.grizzly.http.util.MimeHeaders;
import org.springframework.security.core.userdetails.UserDetails;
import ru.kwanza.console.security.api.AuthEntity;
import ru.kwanza.console.security.api.AuthField;
import ru.kwanza.console.security.api.AuthIdField;
import ru.kwanza.dbtool.orm.annotations.Entity;
import ru.kwanza.dbtool.orm.annotations.Field;
import ru.kwanza.dbtool.orm.annotations.IdField;
import ru.kwanza.jeda.nio.server.http.IHttpRequest;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Entity(table = "rest_exchange")
@AuthEntity(name = "RESTExchange", description = "REST exchange")
public class RESTExchange {
    @IdField("id")
    @AuthIdField
    private Long id;
    @Field("uri")
    @AuthField(name = "URI")
    private String uri;
    @Field("request_headers")
    @AuthField(name = "Request headers")
    private String requestHeaders;
    @Field("response_headers")
    @AuthField(name = "Response headers")
    private String responseHeaders;
    @Field("request")
    @AuthField(name = "Request")
    private String request;
    @Field("response")
    @AuthField(name = "Response")
    private String response;
    @Field("remote_address")
    @AuthField(name = "Remote address")
    private String remoteAddress;
    @Field("ts")
    @AuthField(name = "TimeStamp")
    private Date ts;
    @Field("is_success")
    private Boolean isSuccess;

    private transient UserDetails user;

    public RESTExchange( IHttpRequest request, String remoteAddress) {
        this.uri = request.getUri();
        this.requestHeaders = createHeadersDescription(request.getContent());
        this.request = request.getContent().getContent().toStringContent();
        this.remoteAddress = remoteAddress;
        this.ts = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id){
        this.id = id;
    }

    public String getRequestHeaders() {
        return requestHeaders;
    }


    public UserDetails getUser() {
        return user;
    }
    public void setUser(UserDetails user) {
        this.user = user;
    }
    public String getRequest() {
        return request;
    }

    public String getResponse() {
        return response;
    }

    public Boolean getIsSuccess() {
        return isSuccess;
    }
    public void setIsSuccess(Boolean isSuccess) {
        this.isSuccess = isSuccess;
    }
    public String getUri() {
        return uri;
    }
    public String getResponseHeaders() {
        return responseHeaders;
    }

    public RESTExchange error(HttpPacket packet) {
        buildResponse(packet);
        this.isSuccess = false;

        return this;
    }
    private void buildResponse(HttpPacket packet) {
        if (packet instanceof HttpContent) {
            HttpContent content = (HttpContent) packet;
            this.responseHeaders = createHeadersDescription(content);
            this.response = content.getContent().toStringContent();
        } else {
            HttpResponsePacket responsePacket = (HttpResponsePacket) packet;
            this.responseHeaders = createHeadersDescription(responsePacket);

        }
    }

    private static String createHeadersDescription(HttpContent request) {
        final Map<String, String> map = new HashMap<String, String>();
        final HttpHeader httpHeader = request.getHttpHeader();
        map.put("protocol", httpHeader.getProtocolString());
        if(httpHeader instanceof HttpResponsePacket){
            HttpResponsePacket responsePacket = (HttpResponsePacket) httpHeader;
            map.put("status", String.valueOf(responsePacket.getStatus()));
            map.put("reason", responsePacket.getReasonPhrase());
        }
        final MimeHeaders mimeHeaders = httpHeader.getHeaders();
        for (final String s : mimeHeaders.names()) {
            if (!"demo-user-password".equals(s)) {
                map.put(s, mimeHeaders.getHeader(s));
            }
        }
        return createHeadersDescription(map);
    }

    private static String createHeadersDescription(HttpResponsePacket response) {
        final Map<String, String> map = new HashMap<String, String>();
        map.put("status", String.valueOf(response.getStatus()));
        map.put("reason", response.getReasonPhrase());
        final MimeHeaders mimeHeaders = response.getHeaders();
        for (final String s : mimeHeaders.names()) {
            if (!"demo-user-password".equals(s)) {
                map.put(s, mimeHeaders.getHeader(s));
            }
        }
        return createHeadersDescription(map);
    }

    private static String createHeadersDescription(Map<String, String> headerMap) {
        final JsonObject result = new JsonObject();

        for (Map.Entry<String, String> e : headerMap.entrySet()) {
            result.addProperty(e.getKey(), e.getValue());
        }
        return result.toString();
    }


    public RESTExchange success(HttpPacket packet) {
        buildResponse(packet);
        this.isSuccess = true;

        return this;
    }


    public String getRemoteAddress() {
        return remoteAddress;
    }
    public void setRemoteAddress(String remoteAddress) {
        this.remoteAddress = remoteAddress;
    }
    public Date getTs() {
        return ts;
    }
}
