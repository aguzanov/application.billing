package ru.kwanza.billing.controller.exceptions;

import ru.kwanza.billing.api.report.RejectCode;

/**
 * @author Dmitry Zhukov
 */
public class AccountException extends Exception {
    public AccountException(String message) {
        super(message);
    }

    public static class AccountNotFoundException extends AccountException {

        public AccountNotFoundException() {
            super("Account doesn't exist");
        }
    }

    public static class AccountCreateException extends AccountException {

        public AccountCreateException(RejectCode rejectCode) {
            super("It is impossible to open account. " + rejectCode.getTitle());
        }
    }
}
