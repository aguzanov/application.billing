package ru.kwanza.billing.entity.impl;

import ru.kwanza.billing.api.Request;
import ru.kwanza.billing.api.SessionId;
import ru.kwanza.billing.entity.api.IRequestRegistrator;
import ru.kwanza.billing.entity.api.SessionHistoryRecord;
import ru.kwanza.dbtool.orm.api.EntityUpdateException;
import ru.kwanza.dbtool.orm.api.IEntityBatcher;
import ru.kwanza.dbtool.orm.api.IEntityManager;

import javax.annotation.Resource;
import java.util.*;

/**
 * @author Vasily Vorobyov
 */
public abstract class RequestRegistrator<API_REQUEST extends Request, ENTITY> implements IRequestRegistrator<API_REQUEST> {

    @Resource(name = "dbtool.IEntityManager")
    private IEntityManager em;

    private SessionHistory sessionHistory;

    @Resource(name = "billing.entity.SessionHistory")
    public void setSessionHistory(SessionHistory sessionHistory) {
        this.sessionHistory = sessionHistory;
    }

    protected SessionHistory getSessionHistory() {
        return sessionHistory;
    }

    private Class<ENTITY> entityClass;

    protected RequestRegistrator(Class<ENTITY> entityClass) {
        this.entityClass = entityClass;
    }

    public Map<Long, API_REQUEST> registerRequests(Collection<API_REQUEST> apiRequests, Date processedAt) {

        final Map<Long, API_REQUEST> duplicateRequests = new HashMap<Long, API_REQUEST>();
        final Map<Long, API_REQUEST> requestByIds = new HashMap<Long, API_REQUEST>(apiRequests.size());
        final Map<SessionId, SessionHistoryRecord> sessionHistoryRecords = getRequestSessionRecords(apiRequests);

        final IEntityBatcher entityBatcher = em.createEntityBatcher();

        for (API_REQUEST apiRequest: apiRequests) {
            requestByIds.put(apiRequest.getId(), apiRequest);
            entityBatcher.create(createEntity(apiRequest, processedAt,
                    sessionHistoryRecords.get(apiRequest.getSessionId()).getId()));
        }

        try {
            entityBatcher.flush();
        } catch (EntityUpdateException e) {
            List<ENTITY> constrained = e.getCreateConstrained(entityClass);
            if (null != constrained) {
                for (ENTITY entity: constrained) {
                    final Long entityId = getEntityId(entity);
                    duplicateRequests.put(entityId, requestByIds.get(entityId));
                }
            } else {
                throw new RuntimeException("Unable to register " + entityClass.getName(), e);
            }
        }

        return duplicateRequests;
    }

    protected abstract ENTITY createEntity(API_REQUEST apiRequest, Date processedAt, Long sessionRecordId);

    protected abstract Long getEntityId(ENTITY entity);

    protected Map<SessionId, SessionHistoryRecord> getRequestSessionRecords(Collection<API_REQUEST> apiRequests) {
        Set<SessionId> sessionIds = new HashSet<SessionId>(apiRequests.size());
        for(API_REQUEST req : apiRequests) {
            sessionIds.add(req.getSessionId());
        }
        return sessionHistory.getHistoryRecords(sessionIds);
    }
}
