package ru.kwanza.billing.entity.impl;

import ru.kwanza.billing.api.order.RemoveCurrencyOrder;

import java.util.Date;

/**
 * @author Dmitry Zhukov
 */
public class RemoveCurrencyOrderRegistrator
        extends RequestRegistrator<RemoveCurrencyOrder, ru.kwanza.billing.entity.api.RemoveCurrencyOrder> {

    public RemoveCurrencyOrderRegistrator() {
        super(ru.kwanza.billing.entity.api.RemoveCurrencyOrder.class);
    }

    @Override
    protected ru.kwanza.billing.entity.api.RemoveCurrencyOrder createEntity(RemoveCurrencyOrder order, Date processedAt, Long sessionRecordId) {
        return new ru.kwanza.billing.entity.api.RemoveCurrencyOrder(order.getId(), sessionRecordId,order.getExchangeId(), processedAt, order.getCurrencyId());
    }

    @Override
    protected Long getEntityId(ru.kwanza.billing.entity.api.RemoveCurrencyOrder entity) {
        return entity.getId();
    }
}
