package ru.kwanza.billing.gateway;

import java.io.IOException;
import java.util.Collection;

/**
 * @author Vasily Vorobyov
 */
public interface IListBuilder {

    String getMimeType();

    String getEncoding();

    byte[] build(Collection collection) throws IOException;
}
