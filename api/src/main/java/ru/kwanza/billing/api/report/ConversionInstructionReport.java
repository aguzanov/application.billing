package ru.kwanza.billing.api.report;

import com.fasterxml.jackson.annotation.JsonTypeName;
import ru.kwanza.billing.api.order.ConversionInstruction;

import java.util.Date;

/**
 * @author henadiy
 */
@JsonTypeName(value = "CONVERSION_INSTRUCTION_REPORT")
public class ConversionInstructionReport extends Report {

    private ConversionInstruction instruction;

    protected ConversionInstructionReport() {

    }

    public ConversionInstructionReport(Long id, Date processedAt, ConversionInstruction instruction) {
        super(id, processedAt);
        this.instruction = instruction;
    }

    public ConversionInstruction getInstruction() {
        return instruction;
    }
}
