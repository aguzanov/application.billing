/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.view.issuer.IssuerList', {
    requires: [
        'Console.grid.FiltersFeature',
        'Console.application.QuickLink',
        'Billing.store.IssuerStore'
    ],
    extend: 'Console.grid.BaseGrid',
    alias: ['widget.issuer-list', 'widget.issuerlist'],
    tabTitle: 'Issuers',
    caption: 'Issuers',

    model: 'Billing.model.Issuer',
    store: 'Billing.store.IssuerStore',
    iconCls: 'i-person-male',

    liveSearch: true,

    columns: [
        {
            xtype: 'quicklinkcolumn',
            text: 'ID',
            dataIndex: 'id',
            width: 100,
            filter: true
        },
        {
            text: 'Order',
            dataIndex: 'orderId',
            width: 250,
            renderer: Billing.model.Issuer.orderRenderer,
            valueField: 'orderId'
        },
        {
            text: 'Bank ID',
            dataIndex: 'bin',
            width: 200,
            filter: true
        },
        {
            text: 'Name',
            dataIndex: 'title',
            width: 200,
            filter: true
        },
        {
            text: 'Accounts',
            dataIndex: 'accountCounter',
            width: 200
        }

    ]
});
