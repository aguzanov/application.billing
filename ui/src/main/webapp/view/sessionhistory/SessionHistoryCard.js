/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.view.sessionhistory.SessionHistoryCard', {
    extend: 'Console.panel.CardPanel',
    requires: ['Billing.view.order.DetailOrderList'],

    alias: ['widget.session-history', 'widget.sessionhistory'],

    title: 'Session history',
    iconCls: 'i-person-male',

    defaultType: 'fieldset',
    defaults: {
        columnWidth: 1,
        margin: '5 5 4 5',
        defaultType: 'textfield',
        minWidth: 560,
        defaults: {
            labelWidth: 180,
            anchor: '100%'
        }
    },

    layout: {
        type: 'vbox',
        align: 'stretch',
        pack: 'start'
    },

    model: 'Billing.model.SessionHistory',
    store: 'Billing.store.SessionHistoryStore',

    initComponent: function () {
        var me = this;
        me.items = [
            {
                title: 'Main parameters',
                itemId: 'main',
                items: [
                    {
                        fieldLabel: 'ID',
                        itemId: 'id',
                        name: 'id',
                        readOnly: true
                    },
                    {
                        fieldLabel: 'Token',
                        itemId: 'token',
                        name: 'sessionKey',
                        readOnly: true
                    },
                    {
                        fieldLabel: 'User ID',
                        itemId: 'userId',
                        name: 'userId',
                        readOnly: true
                    },
                    {
                        fieldLabel: 'Opened at',
                        itemId: 'openedAt',
                        name: 'openedAt',
                        readOnly: true
                    },
                    {
                        fieldLabel: 'Closed at',
                        itemId: 'closedAt',
                        name: 'closedAt',
                        readOnly: true
                    }
                ]
            },
            {
                xtype: "tabpanel",
                layout: 'fit',
                flex: 1,
                items: [
                    {
                        title: "Session orders",
                        xtype: "orderlist",
                        height: 400,
                        itemId: "orderlist",
                        columnWidth: 1,
                        store: Ext.create('Billing.store.DetailOrderStore', {
                            autoLoad: false
                        }),
                        excludeFields: ["sessionId"]
                    }
                ]
            }
        ];

        me.callParent(arguments);
    },

    updateRecord: function (record) {
        var me = this;
        if (record) {
            me.title = 'Session ' + record.get('id');
        }
        me.callParent(arguments);

        var orderlist = me.down('#orderlist');
        if (record) {
            orderlist.store.addFilter([new Ext.util.Filter({
                id: 'sessionId',
                property: 'sessionId',
                value: me.record.get('id')
            })], false);
            orderlist.store.load();
        }
    }

});
