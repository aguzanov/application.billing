package ru.kwanza.billing.gateway.http;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.glassfish.grizzly.http.HttpContent;
import org.glassfish.grizzly.http.HttpPacket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import ru.kwanza.billing.api.IRequestIdGenerator;
import ru.kwanza.billing.gateway.IListBuilder;
import ru.kwanza.console.demo.DemoRegistry;
import ru.kwanza.console.demo.DemoUser;
import ru.kwanza.jeda.api.IEventProcessor;
import ru.kwanza.jeda.nio.server.http.IHttpEvent;
import ru.kwanza.jeda.nio.server.http.IHttpRequest;
import ru.kwanza.jeda.nio.utils.HttpUtil;
import ru.kwanza.toolbox.fieldhelper.FieldHelper;
import ru.kwanza.toolbox.splitter.Splitter;

import java.io.IOException;
import java.util.*;

/**
 * @author Vasily Vorobyov
 */
public class HttpIdEventProcessor implements IEventProcessor<IHttpEvent> {
    public static final Splitter<IdRequest> SPLITTER = new Splitter<IdRequest>(FieldHelper.construct(IdRequest.class, "userDetails"));
    private static Logger logger  = LoggerFactory.getLogger(HttpIdEventProcessor.class);

    private static class IdRequest {
        private Integer count;
        private transient UserDetails userDetails;

        public Integer getCount() {
            return count==null?1:count;
        }

    }

    private IRequestIdGenerator requestIdGenerator;
    private IListBuilder listBuilder;
    @Autowired
    private DemoRegistry demo;

    public void setRequestIdGenerator(IRequestIdGenerator requestIdGenerator) {
        this.requestIdGenerator = requestIdGenerator;
    }

    public void setListBuilder(IListBuilder listBuilder) {
        this.listBuilder = listBuilder;
    }

    public void process(Collection<IHttpEvent> events) {

        if (events.isEmpty()) return;

        Map<IdRequest,IHttpRequest> requests = new HashMap<IdRequest,IHttpRequest>(events.size());

        for (IHttpEvent event: events) {
            IdRequest idRequest = null;
            final IHttpRequest httpRequest = event.getHttpRequest();
            final UserDetails userDetails = checkDemoUser(httpRequest);
            if(userDetails!=null) {
                HttpContent httpContent = httpRequest.getContent();
                if (null != httpContent && null != httpContent.getContent() && httpContent.getContent().remaining() > 0) {
                    ObjectMapper mapper = new ObjectMapper();
                    try {
                        idRequest = mapper.readValue(httpContent.getContent().toStringContent(), IdRequest.class);
                        idRequest.userDetails = userDetails;
                        requests.put(idRequest,httpRequest);
                    } catch (IOException e) {
                        httpRequest.finish(HttpUtil.create500(httpRequest, e));
                    }

                }
            }
        }

        final Map<UserDetails, Collection<IdRequest>> map = SPLITTER.oneToMany(requests.keySet());
        for (Map.Entry<UserDetails, Collection<IdRequest>> entry : map.entrySet()) {
            try {
                demo.setCurrentUser(entry.getKey());
                int totalCount = 0;
                for (IdRequest idReq : entry.getValue()) {
                    totalCount += idReq.getCount();
                }

                Collection<Long> ids = requestIdGenerator.generateIds(totalCount);
                Iterator<Long> idIterator = ids.iterator();

                for (Map.Entry<IdRequest, IHttpRequest> e: requests.entrySet()) {
                    IHttpRequest httpRequest = e.getValue();
                    IdRequest idRequest = e.getKey();
                    List<Long> eventIds = new ArrayList<Long>(idRequest.getCount());
                    for (int i = 0; (i < idRequest.getCount()) && idIterator.hasNext(); ++i) {
                        eventIds.add(idIterator.next());
                    }

                    HttpPacket httpResponse;
                    try {
                        httpResponse = HttpUtil.createResponse(httpRequest, listBuilder.getMimeType(), listBuilder.getEncoding(),
                                listBuilder.build(eventIds));
                    } catch (IOException ex) {
                        httpResponse = HttpUtil.create500(httpRequest, "Unable to build json");
                    }
                    httpRequest.finish(httpResponse);
                }
            }finally {
                demo.setCurrentUser(null);
            }
        }

    }

    private UserDetails checkDemoUser(IHttpRequest httpRequest) {
        final String user = httpRequest.getHeader("demo-user");
        final String userPassword = httpRequest.getHeader("demo-user-password");
        final UserDetails userDetails = demo.loadUserByUsername(user);
        if (userDetails == null || !userDetails.getPassword().equals(userPassword)) {
            final String message = "User notFound or password is wrong!";
            logger.error(message);
            final HttpPacket result = HttpUtil.createResponse(httpRequest, 403, message);
            httpRequest.finish(result);
        }

        return userDetails;
    }
}
