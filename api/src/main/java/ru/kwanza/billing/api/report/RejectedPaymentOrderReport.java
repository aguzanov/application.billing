package ru.kwanza.billing.api.report;

import com.fasterxml.jackson.annotation.JsonTypeName;

import java.util.Date;

/**
 * @author Vasily Vorobyov
 */
@JsonTypeName(value = "REJECTED_PAYMENT_ORDER_REPORT")
public class RejectedPaymentOrderReport extends RejectedRequestReport {

    private Integer paymentInstructionIndex;

    protected RejectedPaymentOrderReport() {
    }

    public RejectedPaymentOrderReport(Long id, Date processedAt, RejectCode rejectCode, Integer paymentInstructionIndex) {
        super(id, processedAt, rejectCode);
        this.paymentInstructionIndex = paymentInstructionIndex;
    }

    public Integer getPaymentInstructionIndex() {
        return paymentInstructionIndex;
    }
}
