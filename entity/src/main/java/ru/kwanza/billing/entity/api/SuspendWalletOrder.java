package ru.kwanza.billing.entity.api;

import ru.kwanza.console.security.api.AuthEntity;
import ru.kwanza.dbtool.orm.annotations.Entity;

import java.util.Date;

/**
 * @author Dmitry Zhukov
 */
@Entity(table = "suspend_wallet_order")
@AuthEntity(name = "SuspendWalletOrder", description = "Order for transfer of wallet to LOCKED status")
public class SuspendWalletOrder extends WalletOrder {
    public SuspendWalletOrder(Long id, Long sessionRecordId, Long exchangeId, Date processedAt, Long walletId) {
        super(id, sessionRecordId, exchangeId, processedAt, walletId, OrderType.SUSPEND_WALLET_ORDER);
    }

    @Override
    public void initType() {
        type = OrderType.SUSPEND_WALLET_ORDER.getCode();
    }
}
