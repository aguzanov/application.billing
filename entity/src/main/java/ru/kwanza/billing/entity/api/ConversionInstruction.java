package ru.kwanza.billing.entity.api;

import ru.kwanza.dbtool.orm.annotations.Entity;
import ru.kwanza.dbtool.orm.annotations.Field;
import ru.kwanza.dbtool.orm.annotations.IdField;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author henadiy
 */
@Entity(name = "ConversionInstruction", table = "conversion_instruction")
public class ConversionInstruction implements Serializable {

    @IdField("id")
    protected BigDecimal id;

    @Field("issued_at")
    protected Date issuedAt;
    @Field("expires_in")
    protected Date expiresIn;


    @Field("source_currency_id")
    protected Integer sourceCurrencyId;
    @Field("source_scale")
    protected BigDecimal sourceScale;

    @Field("target_currency_id")
    protected Integer targetCurrencyId;
    @Field("target_scale")
    protected BigDecimal targetScale;


    protected ConversionInstruction() {
    }

    public ConversionInstruction(BigDecimal id,
                                 Date issuedAt, Date expiresIn,
                                 Integer sourceCurrencyId, BigDecimal sourceScale,
                                 Integer targetCurrencyId, BigDecimal targetScale) {
        this.id = id;
        this.issuedAt = issuedAt;
        this.expiresIn = expiresIn;
        this.sourceCurrencyId = sourceCurrencyId;
        this.sourceScale = sourceScale;
        this.targetCurrencyId = targetCurrencyId;
        this.targetScale = targetScale;
    }

    public BigDecimal getId() {
        return id;
    }

    public Date getIssuedAt() {
        return issuedAt;
    }

    public Date getExpiresIn() {
        return expiresIn;
    }

    public Integer getSourceCurrencyId() {
        return sourceCurrencyId;
    }

    public BigDecimal getSourceScale() {
        return sourceScale;
    }

    public Integer getTargetCurrencyId() {
        return targetCurrencyId;
    }

    public BigDecimal getTargetScale() {
        return targetScale;
    }
}
