package ru.kwanza.billing.api.query;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ru.kwanza.billing.api.Request;
import ru.kwanza.billing.api.RequestType;
import ru.kwanza.billing.api.SessionId;

/**
 *
 * @author Dmitry Zhukov
 */
public abstract class Query extends Request {

    protected Query() {
    }

    public Query(Long id, SessionId sessionId) {
        super(id, sessionId);
    }

}
