package ru.kwanza.billing.entity.id;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import ru.kwanza.billing.entity.api.id.AccountIdBuilder;
import ru.kwanza.billing.entity.api.id.WalletAccountIdBuilder;

import java.math.BigDecimal;
import java.util.Calendar;

import static org.junit.Assert.assertEquals;

/**
 * @author henadiy
 */
@RunWith(JUnit4.class)
public class WalletAccountIdBuilderTest {

    @Test
    public void testPack() throws Exception {

        WalletAccountIdBuilder b = new WalletAccountIdBuilder(1230L, 2, 3);
        assertEquals(Long.valueOf(1230), b.getWalletId());
        assertEquals(Integer.valueOf(2), b.getCurrencyId());
        assertEquals(new BigDecimal("1230002000030"), b.getId());

    }

    @Test
    public void testUnpack() throws Exception {

        WalletAccountIdBuilder b = new WalletAccountIdBuilder(new BigDecimal("1231234000030"));
        assertEquals(new BigDecimal("1231234000030"), b.getId());
        assertEquals(Long.valueOf(1230), b.getWalletId());
        assertEquals(Integer.valueOf(1234), b.getCurrencyId());

    }
}
