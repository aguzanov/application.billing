/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.view.order.registerissuerorder.RegisterIssuerOrderCard', {
    extend: 'Billing.view.order.OrderCard',

    alias: 'widget.registerissuerorderdetails',

    title: 'Issuer order',
    iconCls: 'i-document-survey',

    model: 'Billing.model.RegisterIssuerOrder',
    store: 'Billing.store.RegisterIssuerOrderStore',

    defaults: {
        columnWidth: 0.5,
        margin: '5 5 4 5',
        defaultType: 'textfield',
        minWidth: 560,
        defaults: {
            labelWidth: 190,
            anchor: '100%'
        }
    },

    constructor: function (config) {
        var me = this;
        me.items = [
            {
                title: 'Additional parameters of the order',
                columnWidth: 1,
                items: [
                    {
                        fieldLabel: 'The type of operation',
                        renderer: function (value, meta, record) {
                            var issuerId = record.get('issuerId');
                            if (issuerId) {
                                meta.iconCls = 'i-action-edit';
                                return 'Edit';
                            }
                            meta.iconCls = 'i-action-add';
                            return 'Add';
                        }
                    },
                    {
                        fieldLabel: 'Issuer',
                        renderer: function (value, meta, record) {
                            var issuer = record.get('issuer');
                            var rejectCode = record.get('rejectCode');
                            if (issuer) {
                                return Console.application.QuickLink.createLink(issuer.title,
                                    'Billing.view.issuer.IssuerCard', issuer.id);
                            }
                            return record.get('title');
                        }
                    },
                    {
                        fieldLabel: 'Bank ID',
                        name: 'bin'
                    },
                    {
                        fieldLabel: 'Title',
                        name: 'title'
                    }
                ]
            }
        ];
        me.callParent(arguments);
        me.items.get('main').columnWidth = 1;
    }
});
