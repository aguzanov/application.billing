package ru.kwanza.billing.processor;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.kwanza.billing.api.IRequestProcessor;
import ru.kwanza.billing.api.SessionId;
import ru.kwanza.billing.api.query.AccountStatementQuery;
import ru.kwanza.billing.api.report.Report;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author Dmitry Zhukov
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:query-processors-test.xml")
@Ignore
public class QueryProcessorTest extends AbstractJUnit4SpringContextTests {

    @Resource(name = "billing.processor.AccountStatementQueryProcessor")
    private IRequestProcessor<AccountStatementQuery> queryProcessor;

    @Test
    public void testSuccess() {
        long t0 = System.currentTimeMillis(), t;

        List<AccountStatementQuery> records = new ArrayList<AccountStatementQuery>();

        records.add(new AccountStatementQuery(1L, new SessionId("SID"), BigDecimal.valueOf(170943000L), null, null));
        Map<Long, Report> result = queryProcessor.process(records);

        t = System.currentTimeMillis();

        System.out.format("All time = %dms \n", t - t0);
    }
}
