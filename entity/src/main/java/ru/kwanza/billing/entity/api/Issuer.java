package ru.kwanza.billing.entity.api;

import ru.kwanza.console.security.api.AuthEntity;
import ru.kwanza.console.security.api.AuthField;
import ru.kwanza.console.security.api.AuthIdField;
import ru.kwanza.dbtool.orm.annotations.*;

import java.io.Serializable;

/**
 * @author Vasily Vorobyov
 */
@Entity(table = "issuer")
@AuthEntity(name = "Issuer", description = "Issuer")
public class Issuer implements Serializable {

    @Override
    public String toString() {
        return title;
    }

    @IdField("id")
    @AuthIdField
    @AuthField(name = "ID")
    private Integer id;

    @VersionField("version")
    private Long version;

    @Field("order_id")
    @AuthField(name = "ID of the order")
    private Long orderId;

    @Field("bin")
    @AuthField(name = "Bank identification number")
    private Integer bin;

    @Field("title")
    @AuthField(name = "Name")
    private String title;

    @Field("account_counter")
    @AuthField(name = "Count of open accounts")
    private Integer accountCounter;

    @ManyToOne(property = "orderId")
    private RegisterIssuerOrder order;

    public Issuer() {
    }

    public Issuer(Integer id, Long orderId, Integer bin, String title) {
        this.id = id;
        this.orderId = orderId;
        this.bin = bin;
        this.title = title;
        this.accountCounter = 0;
    }

    public Integer getId() {
        return id;
    }

    public Long getOrderId() {
        return orderId;
    }

    public Integer getBin() {
        return bin;
    }

    public void setBin(Integer bin) {
        this.bin = bin;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getAccountCounter() {
        return accountCounter;
    }

    public void setAccountCounter(Integer accountCounter) {
        this.accountCounter = accountCounter;
    }

    public RegisterIssuerOrder getOrder() {
        return order;
    }
}
