/**
 * @author Dmitry Zhukov
 */

Ext.define('Billing.controller.AccountController', {
    extend: 'Ext.app.Controller',
    models: [
        'Billing.model.Account'
    ],
    stores: [
        'Billing.store.AccountStore',
        'Billing.store.AccountStatusStore'
    ],
    views: [
        'Billing.view.account.AccountList',
        'Billing.view.account.AccountCard'
    ],

    init: function () {
        var me = this;
        this.control({
            'accountlist #closeAccount': {
                click: me.closeAccountFromList
            },

            'accountdetails #closeAccount': {
                click: me.closeAccountFromCard
            },

            'accountdetails #walletAccountsButton':{
                click: me.walletAccountsClick
            },

            'accountdetails #accountRecordsButton':{
                click: me.accountRecordsClick
            }


        });
    },

    walletAccountsClick: function (button) {
        var paymentorderdetails = button.up('accountdetails');
        var walletaccount = paymentorderdetails.items.get('walletaccount');
        var accountrecords = paymentorderdetails.items.get('accountrecords');
        walletaccount.setVisible(true);
        accountrecords.setVisible(false);
    },

    accountRecordsClick: function (button) {
        var paymentorderdetails = button.up('accountdetails');
        var walletaccount = paymentorderdetails.items.get('walletaccount');
        var accountrecords = paymentorderdetails.items.get('accountrecords');
        walletaccount.setVisible(false);
        accountrecords.setVisible(true);
    },

    closeAccountFromList: function (button) {
        var accountlist = button.up('accountlist'),
            record = accountlist.getSelectionModel().getSelection()[0];
        this.docloseAccount(record, button);
    },

    closeAccountFromCard: function (button) {
        var accountdetails = button.up('accountdetails'),
            record = accountdetails.record;
        this.docloseAccount(record, button);
    },


    docloseAccount: function (record, button) {
        var me = this;
        me.onRemove({
            accept: function () {
                var store = Ext.StoreManager.get('Billing.store.AccountStore');
                Ext.Ajax.request(
                    {
                        url: 'billing-controller/account/close',
                        method: "POST",
                        async: true,
                        params: {
                            id: record.get('id')
                        },
                        success: function (response) {
                            var result = Ext.decode(response.responseText);
                            if (result.success) {

                                store.reload();
                                Ext.MessageBox.show({
                                    title: 'Success',
                                    msg: 'The account is closed',
                                    buttons: Ext.Msg.OK,
                                    closable: false,
                                    icon: Ext.Msg.INFO
                                });
                            } else {
                                Ext.MessageBox.show({
                                    title: 'Error:' + result.messageTitle,
                                    msg: 'The account is not closed:' + ' ' + result.message,
                                    buttons: Ext.Msg.OK,
                                    closable: false,
                                    icon: Ext.Msg.ERROR
                                });
                            }
                        },
                        failure: function (response) {
                            button.up('.window').close();
                            Ext.MessageBox.show({
                                title: 'Error:' + result.messageTitle,
                                msg: failureMessage + ' ' + result.message,
                                buttons: Ext.Msg.OK,
                                closable: false,
                                icon: Ext.Msg.ERROR
                            });
                        }
                    });

            },
            reject: function () {
            }
        }, record);
    },

    onRemove: function (handler, record) {
        handler.wait = true;
        var me = this;

        Ext.Msg.show(
            {title: 'Confirmation',
                msg: 'Do you want to close the account?',
                width: 300,
                buttons: Ext.Msg.YESNO,
                buttonText: {
                    yes: 'Yes',
                    no: 'No'
                },
                fn: function (buttonId) {
                    if ('yes' === buttonId) {
                        handler.accept();
                    } else {
                        handler.reject();
                    }
                },
                icon: Ext.Msg.QUESTION
            });
    }
});
