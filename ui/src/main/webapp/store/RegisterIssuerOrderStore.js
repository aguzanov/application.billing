/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.store.RegisterIssuerOrderStore', {
    extend: 'Ext.data.Store',
    model: 'Billing.model.RegisterIssuerOrder',
    autoLoad: true,
    remoteFilter: true,
    remoteSort: false,
    autoSync: false
});