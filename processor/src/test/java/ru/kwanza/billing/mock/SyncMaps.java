package ru.kwanza.billing.mock;


import java.util.*;

/**
 * Naotkamegwanning assistant implementation DAO Mock
 *
 * @author Vasily Vorobyov
 */
public class SyncMaps<K, V> {

    public static interface KeyExtractor<K, V> {
        K extractKey(V v);
    }

    public static interface IndexExtractor<V> extends KeyExtractor<Object, V> {}


    private final KeyExtractor<K, V> pkExtractor;
    private final Map<K, V> data = new HashMap<K, V>();
    private final Map<String, Map<Object, Set<K>>> nonUniqueIndexes = new HashMap<String, Map<Object, Set<K>>>();
    private final Map<String, Map<Object, K>> uniqueIndexes = new HashMap<String, Map<Object, K>>();
    private final Map<String, IndexExtractor<V>> indexExtractors = new HashMap<String, IndexExtractor<V>>();

    public SyncMaps(KeyExtractor<K, V> pkExtractor) {
        this.pkExtractor = pkExtractor;
    }

    public void addNonUniqueIndex(String name, IndexExtractor<V> indexExtractor) {

        nonUniqueIndexes.put(name, new HashMap<Object, Set<K>>());
        indexExtractors.put(name, indexExtractor);
    }

    public void addUniqueIndex(String name, IndexExtractor<V> indexExtractor) {
        uniqueIndexes.put(name, new HashMap<Object, K>());
        indexExtractors.put(name, indexExtractor);
    }

    private void removeFromIndexes(K pk) {
        for (Map.Entry<String, Map<Object, K>> o: uniqueIndexes.entrySet()) {
            o.getValue().values().remove(pk);
        }
        for (Map.Entry<String, Map<Object, Set<K>>> o: nonUniqueIndexes.entrySet()) {
            for (Set<K> p: o.getValue().values()) {
                p.remove(pk);
            }
        }
    }

    public V createUpdate(V v) {

        K pk = pkExtractor.extractKey(v);

        removeFromIndexes(pk);

        for (Map.Entry<String, Map<Object, K>> o: uniqueIndexes.entrySet()) {
            String name = o.getKey();
            Map<Object, K> index = o.getValue();
            KeyExtractor<Object, V> keyExtractor = indexExtractors.get(name);
            Object indexValue = keyExtractor.extractKey(v);

            if (null == indexValue) {
                throw new IllegalArgumentException("Index field is null: indexName = " + name);
            }

            index.put(indexValue, pk);
        }

        for (Map.Entry<String, Map<Object, Set<K>>> o: nonUniqueIndexes.entrySet()) {
            String name = o.getKey();
            Map<Object, Set<K>> index = o.getValue();
            KeyExtractor<Object, V> keyExtractor = indexExtractors.get(name);
            Object indexValue = keyExtractor.extractKey(v);

            Set<K> indexEntry = index.get(indexValue);
            if (null == indexEntry) {
                indexEntry = new HashSet<K>();
                index.put(indexValue, indexEntry);
            }
            if (!indexEntry.contains(pk)) {
                indexEntry.add(pk);
            }
        }

        data.put(pk, v);

        return v;
    }

    public Collection<V> createUpdate(Collection<V> vCollection) {

        if (null != vCollection) {
            for (V v: vCollection) {
                createUpdate(v);
            }
        }

        return vCollection;
    }

    public V delete(V v) {
        K pk = pkExtractor.extractKey(v);
        V removed = data.remove(pk);
        if (null != removed) {
            removeFromIndexes(pk);
        }
        return removed;
    }

    public Collection<V> delete(Collection<V> vCollection) {
        ArrayList<V> removed = new ArrayList<V>();
        if (null != vCollection) {
            for (V v: vCollection) {
                V r = delete(v);
                if (null != r) {
                    removed.add(v);
                }
            }
        }
        return removed;
    }

    public V readByKey(K key) {
        return data.get(key);
    }

    public Map<K, V> readByKey(Collection<K> keys) {
        Map<K, V> result = new HashMap<K, V>();
        for (K key: keys) {
            V v = readByKey(key);
            if (null != v) {
                result.put(key, v);
            }
        }
        return result;
    }

    public V readByUniqueIndex(String name, Object key) {
        Map<Object, K> index = uniqueIndexes.get(name);
        if (null == index) throw new IllegalArgumentException("No such index: " + name);
        K pk = index.get(key);
        return data.get(pk);
    }

    public <I> Map<I, V> readByUniqueIndex(String name, Collection<I> keys) {
        Map<I, V> result = new HashMap<I, V>();
        for (I key: keys) {
            V value = readByUniqueIndex(name, key);
            if (null != value) {
                result.put(key, value);
            }
        }
        return result;
    }

    public List<V> readByNonUniqueIndex(String name, Object key) {

        Map<Object, Set<K>> index = nonUniqueIndexes.get(name);
        if (null == index) throw new IllegalArgumentException("No such index: " + name);
        ArrayList<V> result = new ArrayList<V>();
        Set<K> indexEntry = index.get(key);
        if (null != indexEntry) {
            for (K pk: indexEntry) {
                result.add(data.get(pk));
            }
        }

        return result;
    }

    public <I> Map<I, List<V>> readByNonUniqueIndex(String name, Collection<I> keys) {
        Map<I, List<V>> result = new HashMap<I, List<V>>();
        for (I key: keys) {
            result.put(key, readByNonUniqueIndex(name, key));
        }
        return result;
    }

    public Map<K, V> remapToPk(Collection<V> vCollection) {
        Map<K, V> result = new HashMap<K, V>();
        for (V v: vCollection) {
            K pk = pkExtractor.extractKey(v);
            result.put(pk, v);
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    public <I> Map<I, List<V>> remapToIndex(String name, Collection<V> vCollection) {

        if (null == vCollection) return null;

        Map<Object, Set<K>> index = nonUniqueIndexes.get(name);
        if (null == index) throw new IllegalArgumentException("No such index: " + name);

        Map<I, List<V>> result = new HashMap<I, List<V>>();

        for (V v: vCollection) {
            K pk = pkExtractor.extractKey(v);
            I indexValue = null;
            for (Map.Entry<Object, Set<K>> idx: index.entrySet()) {
                if (idx.getValue().contains(pk)) {
                    indexValue = (I) idx.getKey();
                    break;
                }
            }
            if (null == indexValue) {
                throw new IllegalArgumentException("No such object: pk = " + pk);
            }
            List<V> resultEntry = result.get(indexValue);
            if (null == resultEntry) {
                resultEntry = new ArrayList<V>();
                result.put(indexValue, resultEntry);
            }
            resultEntry.add(v);
        }
        return result;
    }
}
