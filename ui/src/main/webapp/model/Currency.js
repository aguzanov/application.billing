/**
 * @author Dmitry Zhukov
 */

Ext.define('Billing.model.Currency', {
    extend: 'Ext.data.Model',
    fields:[
        'id',
        {
            name: 'realId',
            convert:function(v, record){
                return record.get('id');
            }
        },
        'cachedId',
        'version',
        'alphaCode',
        'title',
        'exponent'
    ],
    entityClassName: 'Currency',

    detailView: 'Billing.view.currency.CurrencyCard',

    permissions: {
        read: "Currency:read",
        create: "Currency:create",
        update: "Currency:update",
        destroy: "Currency:delete"
    },

    proxy: {
        type: 'ajax',
        apiUrl: 'billing-controller/currency/'

    }
});