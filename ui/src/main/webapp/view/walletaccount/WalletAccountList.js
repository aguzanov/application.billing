/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.view.walletaccount.WalletAccountList', {
    requires: [
        'Console.grid.FiltersFeature',
        'Console.application.QuickLink',
        'Billing.store.WalletAccountStore',
        'Billing.model.WalletAccount'

    ],
    extend: 'Console.grid.BaseGrid',
    alias: ['widget.wallet-account-list', 'widget.walletaccountlist'],
    tabTitle: 'List of wallet accounts',
    caption: 'List of wallet accounts',

    model: 'Billing.model.WalletAccount',
    store: 'Billing.store.WalletAccountStore',
    iconCls: 'i-briefcase',

    columns: [
        {
            text: 'Issue date',
            dataIndex: 'createdAt',
            width: 200,
            xtype: 'datecolumn',
            format: 'd.m.Y H:i:s',
            filter: true
        },
        {
            text: 'Order',
            dataIndex: 'orderId',
            width: 200,
            renderer: Billing.model.WalletAccount.orderRenderer,
            readOnly: true,
            valueField: 'orderId'
        },
        {
            text: 'Account',
            dataIndex: 'accountId',
            width: 200,
            xtype: "quicklinkcolumn",
            store: "Billing.store.AccountStore",
            detailView: 'Billing.view.account.AccountCard',
            valueField: 'accountId'
        },
        {
            text: 'Wallet',
            dataIndex: 'walletId',
            width: 200,
            xtype: 'quicklinkcolumn',
            detailView: 'Billing.view.wallet.WalletCard',
            valueField: 'walletId'
        },
        {
            text: 'Name',
            dataIndex: 'title',
            width: 200,
            filter: true
        }
    ]
});
