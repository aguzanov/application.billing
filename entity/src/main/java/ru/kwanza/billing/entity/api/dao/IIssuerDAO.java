package ru.kwanza.billing.entity.api.dao;

import ru.kwanza.billing.entity.api.Issuer;
import ru.kwanza.dbtool.core.UpdateException;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author Vasily Vorobyov
 */
public interface IIssuerDAO {

    List<Issuer> readAll();

    Issuer prepare(Long orderId, Integer bin, String title);

    Collection<Issuer> create(Collection<Issuer> issuers) throws UpdateException;

    Map<Integer, Issuer> readByKeys(Collection<Integer> ids);

    void update(Collection<Issuer> issuers) throws UpdateException;
}
