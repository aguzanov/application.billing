package ru.kwanza.billing.gateway;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Collection;

/**
 * @author Vasily Vorobyov
 */
public class JSONListBuilder implements IListBuilder {

    private ObjectMapper mapper = new ObjectMapper();

    public String getMimeType() {
        return "application/json";
    }

    public String getEncoding() {
        return "utf-8";
    }

    public byte[] build(Collection collection) throws IOException {

        return mapper.writeValueAsBytes(collection);
    }
}
