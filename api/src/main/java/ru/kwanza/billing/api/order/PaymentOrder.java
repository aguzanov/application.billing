package ru.kwanza.billing.api.order;

import com.fasterxml.jackson.annotation.JsonTypeName;
import ru.kwanza.billing.api.RequestType;
import ru.kwanza.billing.api.SessionId;

/**
 * @author Vasily Vorobyov
 */
@JsonTypeName(value = "O_PAYMENT")
public class PaymentOrder extends Order {

    private PaymentInstruction[] paymentInstructions;
    private String description;

    public PaymentOrder() {
    }

    public PaymentOrder(Long id, SessionId sessionId, String description, PaymentInstruction[] paymentInstructions) {
        super(id, sessionId);
        this.description = description;
        this.paymentInstructions = paymentInstructions;
    }

    @Override
    public RequestType getRequestType() {
        return RequestType.O_PAYMENT;
    }

    public String getDescription() {
        return description;
    }

    public PaymentInstruction[] getPaymentInstructions() {
        return paymentInstructions;
    }
}
