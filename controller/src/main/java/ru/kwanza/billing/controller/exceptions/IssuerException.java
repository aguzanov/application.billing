package ru.kwanza.billing.controller.exceptions;

import ru.kwanza.billing.api.report.RejectCode;

/**
 * @author Dmitry Zhukov
 */
public class IssuerException extends Exception {
    public IssuerException(String message) {
        super(message);
    }

    public static class IssuerCreateException extends IssuerException {
        public IssuerCreateException(RejectCode rejectCode) {
            super("You cannot create the Issuer. " + rejectCode.getTitle());
        }
    }

    public static class IssuerNotFoundException extends IssuerException {
        public IssuerNotFoundException() {
            super("The Issuer is not found.");
        }
    }

    public static class IssuerUpdateException extends IssuerException {
        public IssuerUpdateException(RejectCode rejectCode) {
            super("You cannot update the Issuer. " + rejectCode.getTitle());
        }
    }

    public static class AccountAlreadyExistException extends IssuerException {
        public AccountAlreadyExistException() {
            super("The Issuer has opened the account. Removal is not possible.");
        }
    }
}
