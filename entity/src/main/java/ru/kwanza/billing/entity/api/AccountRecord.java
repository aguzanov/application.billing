package ru.kwanza.billing.entity.api;

import ru.kwanza.console.security.api.AuthEntity;
import ru.kwanza.console.security.api.AuthField;
import ru.kwanza.console.security.api.AuthIdField;
import ru.kwanza.dbtool.orm.annotations.Entity;
import ru.kwanza.dbtool.orm.annotations.Field;
import ru.kwanza.dbtool.orm.annotations.IdField;
import ru.kwanza.dbtool.orm.annotations.ManyToOne;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Types;
import java.util.Date;

/**
 * @author henadiy
 */
@Entity(name = "AccountRecord", table = "account_record")
@AuthEntity(name = "AccountRecord", description = "Accoutn transaction")
public class AccountRecord implements Serializable {

    @IdField(value="id", type = Types.DECIMAL)
    @AuthIdField
    @AuthField(name = "ID")
    protected BigDecimal id;

    @Field("order_id")
    @AuthField(name = "ID of the order")
    protected Long orderId;

    @Field("source_account_id")
    @AuthField(name = "Sender account ID")
    protected BigDecimal sourceAccountId;

    @Field("source_record_number")
    @AuthField(name = "Sequence writing number by account sender")
    protected Integer sourceRecordNumber;

    @Field("source_amount")
    @AuthField(name ="Amount of the write-off")
    protected Long sourceAmount;

    @Field("source_incoming_balance")
    @AuthField(name ="Balance of sender account")
    protected Long sourceIncomingBalance;

    @Field("target_account_id")
    @AuthField(name ="Receiver account ID")
    protected BigDecimal targetAccountId;

    @Field("target_record_number")
    @AuthField(name ="Sequenve writing number by receiver account")
    protected Integer targetRecordNumber;

    @Field("target_amount")
    @AuthField(name ="Deposit amount")
    protected Long targetAmount;

    @Field("target_incoming_balance")
    @AuthField(name ="Balance of the receiver account")
    protected Long targetIncomingBalance;

    @Field("processed_at")
    @AuthField(name ="Processed at")
    protected Date processedAt;

    @Field("description")
    @AuthField(name ="Description")
    protected String description;

    @ManyToOne(property = "orderId")
    private PaymentOrder paymentOrder;

    @ManyToOne(property = "sourceAccountId")
    private Account sourceAccount;

    @ManyToOne(property = "targetAccountId")
    private Account targetAccount;


    protected AccountRecord() {
    }

    public AccountRecord(BigDecimal id, Long orderId,
                         BigDecimal sourceAccountId, Integer sourceRecordNumber, Long sourceAmount, Long sourceIncomingBalance,
                         BigDecimal targetAccountId, Integer targetRecordNumber, Long targetAmount, Long targetIncomingBalance,
                         Date processedAt, String description) {
        this.id = id;
        this.orderId = orderId;
        this.sourceAccountId = sourceAccountId;
        this.sourceAmount = sourceAmount;
        this.sourceRecordNumber = sourceRecordNumber;
        this.sourceIncomingBalance = sourceIncomingBalance;
        this.targetAccountId = targetAccountId;
        this.targetAmount = targetAmount;
        this.targetRecordNumber = targetRecordNumber;
        this.targetIncomingBalance = targetIncomingBalance;
        this.processedAt = processedAt;
        this.description = description;
    }

    public BigDecimal getId() {
        return id;
    }

    public Long getOrderId() {
        return orderId;
    }

    public BigDecimal getSourceAccountId() {
        return sourceAccountId;
    }

    public Long getSourceAmount() {
        return sourceAmount;
    }

    public Integer getSourceRecordNumber() {
        return sourceRecordNumber;
    }

    public Long getSourceIncomingBalance() {
        return sourceIncomingBalance;
    }

    public BigDecimal getTargetAccountId() {
        return targetAccountId;
    }

    public Long getTargetAmount() {
        return targetAmount;
    }

    public Integer getTargetRecordNumber() {
        return targetRecordNumber;
    }

    public Long getTargetIncomingBalance() {
        return targetIncomingBalance;
    }

    public Date getProcessedAt() {
        return processedAt;
    }

    public String getDescription() {
        return description;
    }

    public Account getSourceAccount() {
        return sourceAccount;
    }

    public Account getTargetAccount() {
        return targetAccount;
    }

    public PaymentOrder getPaymentOrder() {
        return paymentOrder;
    }
}
