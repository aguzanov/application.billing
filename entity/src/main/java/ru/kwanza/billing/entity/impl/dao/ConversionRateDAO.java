package ru.kwanza.billing.entity.impl.dao;

import ru.kwanza.billing.entity.api.ConversionRate;
import ru.kwanza.billing.entity.api.dao.IConversionRateDAO;
import ru.kwanza.dbtool.core.UpdateException;
import ru.kwanza.dbtool.orm.api.IEntityManager;
import ru.kwanza.dbtool.orm.api.IQuery;
import ru.kwanza.dbtool.orm.api.If;
import ru.kwanza.dbtool.orm.api.OrderBy;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author henadiy
 */
public class ConversionRateDAO implements IConversionRateDAO {

    @Resource(name = "dbtool.IEntityManager")
    private IEntityManager em;

    private IQuery<ConversionRate> queryByDateRange;
    private IQuery<ConversionRate> queryByCurrencyPairAndDate;
//    private Pattern patternToExtractSql;

    public void init() {
        queryByDateRange = em.queryBuilder(ConversionRate.class)
                .where(If.and(
                        If.isGreaterOrEqual("effectiveFrom"),
                        If.or(If.isNull("effectiveTo"), If.isLessOrEqual("effectiveTo"))
                )).create();

        queryByCurrencyPairAndDate = em.queryBuilder(ConversionRate.class).where(
                If.and(
                        If.isEqual("currencyPair"),
                        If.isLessOrEqual("effectiveFrom"),
                        If.or(
                                If.isNull("effectiveTo"),
                                If.isGreaterOrEqual("effectiveTo")
                        )
                )).orderBy(OrderBy.DESC("effectiveFrom")).create();

//        patternToExtractSql = Pattern.compile("query='(.+?)'");
    }

    public ConversionRate prepare(Long id, Date effectiveFrom, Date effectiveTo,
                                  Integer sourceCurrencyId, Integer targetCurrencyId,
                                  BigDecimal sourceScale, BigDecimal targetScale) {
        return new ConversionRate(id, effectiveFrom, effectiveTo,
                sourceCurrencyId, targetCurrencyId, sourceScale, targetScale);
    }

    public Collection<ConversionRate> create(Collection<ConversionRate> conversionRates)
            throws UpdateException {
        return em.create(ConversionRate.class, conversionRates);
    }

    public Map<Integer, ConversionRate> readByDateRange(Date fromDate, Date toDate) {
        return queryByDateRange.prepare()
                .setParameter(1, fromDate).setParameter(2, toDate)
                .selectMap("currencyPair");
    }

    public Map<Integer, ConversionRate> readConversionRates(Collection<Integer> currencyPairs, Date date) {

        Map<Integer, ConversionRate> result = new HashMap<Integer, ConversionRate>(currencyPairs.size());
        for(Integer pair : currencyPairs) {
            ConversionRate r = queryByCurrencyPairAndDate.prepare()
                    .setParameter(1, pair)
                    .setParameter(2, date)
                    .setParameter(3, date)
                    .paging(0, 1).select();
            if (null != r) result.put(pair, r);
        }

        return result;

//        System.out.println(queryByCurrencyPairAndDate.toString());
//        Matcher m = patternToExtractSql.matcher(queryByCurrencyPairAndDate.toString());
//        if (!m.find()) {
//            throw new RuntimeException("Unable to build query");
//        }
//        final String sql = m.group(1);
//
//        // combine multiple queries
//        StringBuilder b = new StringBuilder();
//        for (Iterator<Integer> it = currencyPairs.iterator(); it.hasNext(); ) {
//            it.next();
//            b.append("(").append(sql).append(")");
//            if (it.hasNext()) {
//                b.append(" UNION ");
//            }
//        }
//
//        // preparation of statement and filling parameters
//        int index = 1;
//        IStatement<ConversionRate> qb =
//                em.queryBuilder(ConversionRate.class).createNative(b.toString()).prepare();
//        for (Integer pair : currencyPairs) {
//            qb.setParameter(index++, pair);
//            qb.setParameter(index++, date);
//            qb.setParameter(index++, date);
//        }
//
//        // the result
//        return qb.selectMap("currencyPair");
    }
}
