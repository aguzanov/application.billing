package ru.kwanza.billing.entity.impl.dao;

import ru.kwanza.billing.entity.api.Account;
import ru.kwanza.billing.entity.api.Currency;
import ru.kwanza.billing.entity.api.Issuer;
import ru.kwanza.billing.entity.api.dao.IAccountDAO;
import ru.kwanza.dbtool.core.UpdateException;
import ru.kwanza.dbtool.orm.api.*;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Vasily Vorobyov
 */
public class AccountDAO implements IAccountDAO {

    @Resource(name = "dbtool.IEntityManager")
    private IEntityManager em;

    private IQuery<Account> queryByOrderId;
    private IQuery<Account> queryByLimits;
    private IQuery<Account> queryAll;


    public void init() {
        queryByOrderId = em.queryBuilder(Account.class).where(If.in("orderId", "orderIds")).create();
        queryByLimits = em.queryBuilder(Account.class).where(If.and(If.isGreater("id", "lowerLimit"), If.isLess("id", "upperLimit"))).create();
        queryAll = em.queryBuilder(Account.class).create();
    }

    public List<Account> readAll() {
        return queryAll.prepare().selectList();
    }

    public Boolean isExistAccountInIdLimits(BigDecimal lowerLimit, BigDecimal upperLimit) {
        List<Account> accounts = queryByLimits.prepare().setParameter("lowerLimit", lowerLimit).setParameter("upperLimit", upperLimit).paging(0,1).selectList();
        return !accounts.isEmpty();
    }

    public Map<BigDecimal, Account> readByKeys(Collection<BigDecimal> ids) {
        return em.readMapByKeys(Account.class, ids);
    }

    public Map<Long, Account> readByOrderIds(Collection<Long> orderIds) {
        return queryByOrderId.prepare().setParameter("orderIds", orderIds).selectMap("orderId");
    }

    public Account prepare(Date createdAt, Issuer issuer, Currency currency, Integer accountCounter, Long orderId, Long minBalance, Long maxBalance) {

        return new Account(createdAt, issuer, currency, accountCounter, orderId, minBalance, maxBalance);
    }

    public Collection<Account> create(Collection<Account> accounts) throws UpdateException {

        return em.create(Account.class, accounts);
    }

    public Collection<Account> update(Collection<Account> accounts) throws UpdateException {
        return em.update(Account.class, accounts);
    }
}
