package ru.kwanza.billing.gateway.sandbox;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.type.ArrayType;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import ru.kwanza.billing.api.SessionId;
import ru.kwanza.billing.api.order.AccountIssueOrder;
import ru.kwanza.billing.api.order.WalletIssueOrder;
import ru.kwanza.billing.api.report.WalletReport;
import ru.kwanza.billing.api.WalletStatus;
import ru.kwanza.billing.gateway.JSONReportBuilder;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Vasily Vorobyov
 */
public class JSONTest {

    public static void main(String[] args) throws IOException {
        ObjectMapper mapper = new ObjectMapper().configure(JsonGenerator.Feature.WRITE_BIGDECIMAL_AS_PLAIN, true)
                .configure(JsonGenerator.Feature.WRITE_NUMBERS_AS_STRINGS, true)
                .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
                .setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'"));
        ObjectReader reader = mapper.reader();

//        JSONRequestParser<AccountIssueOrder> parser = new JSONRequestParser<AccountIssueOrder>(AccountIssueOrder.class);
        final String JSON =
                "[" +
                        "{\"type\":\"ACCOUNT_ISSUE_ORDER\",\"id\":\"28850\",\"sessionId\":\"SID\",\"issuerId\":\"1\",\"currencyId\":\"943\",\"minBalance\":\"0\",\"maxBalance\":null}" + "," +
                        "{\"type\":\"ACCOUNT_ISSUE_ORDER\",\"id\":\"28850\",\"sessionId\":\"SID\",\"issuerId\":\"1\",\"currencyId\":\"943\",\"minBalance\":\"0\",\"maxBalance\":null}" + "]";
        long t0 = System.currentTimeMillis(), t;
        for (int i = 0; i < 1000000; ++i) {
            JsonNode tree = reader.readTree(JSON);
            if (!tree.isArray()) {
                tree.traverse(reader).readValueAs(AccountIssueOrder.class);
            } else {
                final ArrayType arrayType = mapper.getTypeFactory().constructArrayType(AccountIssueOrder.class);
                reader.readValue(tree.traverse(), arrayType);
            }
        }
        t = System.currentTimeMillis();
        System.out.println("time = " + (t - t0));
    }

    public static void __main(String[] args) {
        Gson gson = new Gson();
        final String JSON =
                "[" +
                "{\"type\":\"ACCOUNT_ISSUE_ORDER\",\"id\":\"28850\",\"sessionId\":\"SID\",\"issuerId\":\"1\",\"currencyId\":\"943\",\"minBalance\":\"0\",\"maxBalance\":null}" + "," +
                        "{\"type\":\"ACCOUNT_ISSUE_ORDER\",\"id\":\"28850\",\"sessionId\":\"SID\",\"issuerId\":\"1\",\"currencyId\":\"943\",\"minBalance\":\"0\",\"maxBalance\":null}" + "]";
        long t0 = System.currentTimeMillis(), t;
        for (int i = 0; i < 100000; ++i) {
            final JsonElement tree = gson.toJsonTree(JSON);
            if (tree.isJsonArray()) {
                gson.fromJson(tree, AccountIssueOrder[].class);
            } else {
                gson.fromJson(tree, AccountIssueOrder.class);
            }
        }
        t = System.currentTimeMillis();
        System.out.println("time = " + (t - t0));
    }

    public static void _main(String[] args) throws IOException, ClassNotFoundException {
        JSONReportBuilder jsonReportBuilder = new JSONReportBuilder();
        byte[] json = jsonReportBuilder.build(new WalletReport(new WalletIssueOrder(1L, new SessionId("SID"), null, null), new Date(), 1L, 1L, new Date(), WalletStatus.ACTIVE, "holderId", "title", null));

//        Gson gson = new Gson();
//
//        JsonParser jsonParser = new JsonParser();
//        JsonObject root = jsonParser.parse(new InputStreamReader(new ByteArrayInputStream(json), "UTF-8")).getAsJsonObject();
//        String className = root.get("reportType").getAsString();
//        Object v = gson.fromJson(root.get("report"), Class.forName(className));

        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(json);
        String className = root.get("reportType").asText();
        Class<?> type = Class.forName(className);
        Object v = mapper.readValue(root.get("report").traverse(), type);

        System.out.format("class = %s, value = %s\n", className, v);
    }
}
