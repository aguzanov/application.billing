package ru.kwanza.billing.controller.billing;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import ru.kwanza.billing.api.Request;
import ru.kwanza.billing.api.SessionId;
import ru.kwanza.billing.api.order.CloseSessionOrder;
import ru.kwanza.billing.api.order.OpenSessionOrder;
import ru.kwanza.billing.api.report.Report;
import ru.kwanza.billing.api.report.SessionReport;
import ru.kwanza.billing.client.IBillingClient;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Map;

/**
 * @author henadiy
 */
public class BillingHelper {

    public static final String BILLING_SESSION_ID = "billing_session_id";
    private static final Logger log = LoggerFactory.getLogger(BillingHelper.class);

    @Value("${console.demo.appId}")
    private String cookieName;

    private IBillingClient billingClient;

    public void setBillingClient(IBillingClient billingClient) {
        this.billingClient = billingClient;
    }


    public Report processRequest(Request request) {
        return billingClient.processRequests(Arrays.asList(request)).get(0);
    }

    public Long generateRequestId() {
        return billingClient.generateRequestId();
    }

    public SessionId getSessionId(HttpServletRequest request) {
        for (Cookie cookie : request.getCookies()) {
            if (cookie.getName().equals(cookieName)) return new SessionId(cookie.getValue());
        }
        return null;
    }

    public boolean isLoggedIn(HttpServletRequest request) {
        return null != getSessionId(request);
    }

    public SessionId login(Map<String, Object> sessionArgs, HttpServletRequest request) {
        resetSessionId(request);

        // the requested session
        try {
            Report report = processRequest(new OpenSessionOrder(billingClient.generateRequestId(), sessionArgs));
            if (report instanceof SessionReport && ((SessionReport) report).getSession().isValid()) {
            } else {
                log.error(String.format("Unable to obtain billing session id, because %s", report));
            }
        } catch (Exception e) {
            log.error("Unable to obtain billing session id", e);
        }

        return getSessionId(request);
    }

    public void logout(HttpServletRequest request) {
        // if the session is open Billingham
        if (isLoggedIn(request)) {
            // reset the session
            resetSessionId(request);

            // try eё close to the billing
            try {
                processRequest((Request) new CloseSessionOrder(generateRequestId(), getSessionId(request)));
            } catch (Exception e) {
                log.error("Unable to clear billing session id", e);
            }
        }
    }

    protected void setSessionId(SessionId sessionId, HttpServletRequest request) {
        request.getSession().setAttribute(BILLING_SESSION_ID, sessionId);
    }

    protected void resetSessionId(HttpServletRequest request) {
        request.getSession().removeAttribute(BILLING_SESSION_ID);
    }

}
