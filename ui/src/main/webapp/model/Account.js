/**
 * @author Dmitry Zhukov
 */

Ext.define('Billing.model.Account', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'string'},
        'version',
        'cachedId',
        {name: 'createdAt', type: 'date', dateFormat: 'd.m.Y H:i:s.u'},
        'orderId',
        {name: 'order', persist: false},
        'balance',
        'minBalance',
        'maxBalance',
        {name: 'lastRecordAt', type: 'date', dateFormat: 'd.m.Y H:i:s.u'},

        {
            name: 'issuerTitle',
            persist: false,
            convert: function (v, r) {
                var order = r.get('order');
                var issuer = order ? order.issuer : undefined
                return issuer ? issuer.title : '';
            }
        },
        {
            name: 'issuerId',
            convert: function (v, r) {
                var order = r.get('order');
                var issuer = order ? order.issuer : undefined
                return issuer ? issuer.id : v;
            }
        },
        {
            name: 'currencyTitle',
            persist: false,
            convert: function (v, r) {
                var order = r.get('order');
                var currency = order ? order.currency : undefined
                return currency ? currency.title : '';
            }
        },
        {
            name: 'currencyId',
            convert: function (v, r) {
                var order = r.get('order');
                var currency = order ? order.currency : undefined
                return currency ? currency.id : v;
            }
        },
        'lastRecordNumber',
        'lastRecordId',
        'lastRecordAmount',
        'statusCode'
    ],
    entityClassName: 'Account',

    detailView: 'Billing.view.account.AccountCard',

    permissions: {
        read: "Account:read",
        create: "Account:create",
        update: "Account:update",
        destroy: false
    },

    proxy: {
        type: 'ajax',
        apiUrl: 'billing-controller/account/'
    },

    hasOne: [
        {
            model: 'Billing.model.AccountIssueOrder',
            foreignKey: 'orderId',
            getterName: 'getOrder'
        }
    ],

    statics: {
        orderRenderer: function (value, meta, record) {
            var order = record.get('order');
            return Console.application.QuickLink.createLink("ID: " + order.id + ", processed at: " + order.processedAt,
                'Billing.view.order.accountissueorder.AccountIssueOrderCard', {recordId: order.id});
        }
    }
});
