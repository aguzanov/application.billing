package ru.kwanza.billing.processor.order;

import ru.kwanza.billing.api.order.RegisterCurrencyOrder;
import ru.kwanza.billing.api.report.CurrenciesReport;
import ru.kwanza.billing.api.report.RejectCode;
import ru.kwanza.billing.api.report.RejectedRequestReport;
import ru.kwanza.billing.api.report.Report;
import ru.kwanza.billing.entity.api.Currency;
import ru.kwanza.billing.entity.api.dao.ICurrencyDAO;
import ru.kwanza.billing.processor.RequestProcessor;
import ru.kwanza.dbtool.core.UpdateException;

import javax.annotation.Resource;
import java.util.*;

/**
 * @author Vasily Vorobyov
 */
public class RegisterCurrencyOrderProcessor extends RequestProcessor<RegisterCurrencyOrder> {

    @Resource(name = "billing.entity.dao.CurrencyDAO")
    private ICurrencyDAO currencyDAO;

    @Override
    protected Map<Long, Report> handleRequests(Date processedAt, Collection<RegisterCurrencyOrder> requests) {

        Map<Long, Report> reports = new HashMap<Long, Report>(requests.size());

        Set<Integer> currencyIds = new HashSet<Integer>(requests.size());

        Iterator<RegisterCurrencyOrder> orderIt = requests.iterator();
        while (orderIt.hasNext()){
            RegisterCurrencyOrder order = orderIt.next();
            RejectCode rejectCode = null;
            if (null == order.getCurencyId() || null == order.getExponent() ||
                    null == order.getAlphaCode() || null == order.getTitle()) {
                rejectCode = RejectCode.EMPTY_REQUIRED_FIELD;
            } else if (order.getCurencyId() <= 0 || order.getCurencyId() > Currency.MAX_ID) {
                rejectCode = RejectCode.ILLEGAL_CURRENCY_ID;
            } else if (order.getAlphaCode().length() > 4) {
                rejectCode = RejectCode.CURRENCY_ALPHA_CODE_TOO_LONG;
            } else if (order.getTitle().length() > 255) {
                rejectCode = RejectCode.TITLE_TOO_LONG;
            }

            if (null != rejectCode) {
                reports.put(order.getId(), new RejectedRequestReport(order.getId(), processedAt, rejectCode));
                orderIt.remove();
            } else {
                currencyIds.add(order.getCurencyId());
            }
        }

        Map<Integer, Currency> currencies = currencyDAO.readByKeys(currencyIds);
        List<Currency> created = new ArrayList<Currency>(requests.size() - currencyIds.size());
        for (RegisterCurrencyOrder order: requests) {
            Currency currency = currencies.get(order.getCurencyId());
            if (null != currency) {
                currency.setExponent(order.getExponent());
                currency.setAlphaCode(order.getAlphaCode());
                currency.setTitle(order.getTitle());
            } else {
                created.add(currencyDAO.prepare(order.getCurencyId(), order.getAlphaCode(), order.getExponent(), order.getTitle()));
            }
        }

        try {
            currencyDAO.update(currencies.values());
            currencyDAO.create(created);
        } catch (UpdateException e) {
            throw new RuntimeException("Unable to create/update currency table", e);
        }

        buildSuccessReports(processedAt, requests, reports);

        return reports;
    }

    @Override
    protected Map<Long, Report> answerDuplicateRequests(Map<Long, RegisterCurrencyOrder> duplicateRequests) {
        // TODO: Unlikely to correctly return the current registry, or need to make a reservation API
        Map<Long, Report> reports = new HashMap<Long, Report>(duplicateRequests.size());

        buildSuccessReports(new Date(), duplicateRequests.values(), reports);

        return reports;
    }

    private void buildSuccessReports(Date processedAt, Collection<RegisterCurrencyOrder> requests, Map<Long, Report> reports) {
        Map<Integer, Currency> allCurrencies = currencyDAO.readAll();
        CurrenciesReport.Currency[] reportCurrencies = new CurrenciesReport.Currency[allCurrencies.size()];

        int i = 0;
        for (Currency currency: allCurrencies.values()) {
            reportCurrencies[i++] = new CurrenciesReport.Currency(
                    currency.getId(), currency.getExponent(), currency.getAlphaCode(), currency.getTitle());
        }

        for (RegisterCurrencyOrder order: requests) {
            reports.put(order.getId(), new CurrenciesReport(order.getId(), processedAt, reportCurrencies));
        }
    }

}
