/**
 * @author Dmitry Zhukov
 */

Ext.define('Billing.controller.CurrencyController',{
    extend: 'Ext.app.Controller',
    models: ['Billing.model.Currency'],
    stores: ['Billing.store.CurrencyStore'],
    views: [
        'Billing.view.currency.CurrencyList',
        'Billing.view.currency.CurrencyCard'
    ],

    init: function () {
        var me = this;
        this.control({

        });
    }

});