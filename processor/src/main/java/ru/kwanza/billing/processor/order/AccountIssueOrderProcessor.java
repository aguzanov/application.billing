package ru.kwanza.billing.processor.order;

import ru.kwanza.billing.api.report.*;
import ru.kwanza.billing.api.order.*;
import ru.kwanza.billing.entity.api.Account;
import ru.kwanza.billing.entity.api.Currency;
import ru.kwanza.billing.entity.api.Issuer;
import ru.kwanza.billing.entity.api.dao.IAccountDAO;
import ru.kwanza.billing.entity.api.dao.ICurrencyDAO;
import ru.kwanza.billing.entity.api.dao.IIssuerDAO;
import ru.kwanza.billing.processor.RequestProcessor;
import ru.kwanza.dbtool.core.UpdateException;

import javax.annotation.Resource;
import java.util.*;

/**
 * @author Vasily Vorobyov
 */
public class AccountIssueOrderProcessor extends RequestProcessor<AccountIssueOrder> {

    @Resource(name = "billing.entity.dao.IssuerDAO")
    private IIssuerDAO issuerDAO;
    @Resource(name = "billing.entity.dao.CurrencyDAO")
    private ICurrencyDAO currencyDAO;
    @Resource(name = "billing.entity.dao.AccountDAO")
    private IAccountDAO accountDAO;

    protected Map<Long, Report> handleRequests(Date processedAt, Collection<AccountIssueOrder> requests) {

        HashMap<Long, Report> orderReports = new HashMap<Long, Report>(requests.size());

        Set<Integer> issuerIds = new HashSet<Integer>(requests.size());
        Set<Integer> currencyIds = new HashSet<Integer>();

        for (AccountIssueOrder order : requests) {
            issuerIds.add(order.getIssuerId());
            currencyIds.add(order.getCurrencyId());
        }

        Map<Integer, Issuer> issuers = issuerDAO.readByKeys(issuerIds);

        try {
            issuerDAO.update(issuers.values());
        } catch (UpdateException e) {
            throw new RuntimeException("Unable to lock issuers by update");
        }

        Map<Integer, Currency> currencies = currencyDAO.readByKeys(currencyIds);

        ArrayList<Account> accounts = new ArrayList<Account>(requests.size());

        for (AccountIssueOrder order : requests) {
            Issuer issuer = issuers.get(order.getIssuerId());
            if (null == issuer) {
                orderReports.put(order.getId(), new RejectedRequestReport(order.getId(), processedAt, RejectCode.ISSUER_NOT_FOUND));
            } else {
                Currency currency = currencies.get(order.getCurrencyId());
                if (null == currency) {
                    orderReports.put(order.getId(), new RejectedRequestReport(order.getId(), processedAt, RejectCode.CURRENCY_NOT_FOUND));
                } else {
                    Integer lastCounter = issuer.getAccountCounter();
                    if (null == lastCounter) {
                        lastCounter = 1;
                    } else {
                        lastCounter = lastCounter + 1;
                    }
                    issuer.setAccountCounter(lastCounter);

                    Account account = accountDAO.prepare(
                            processedAt, issuer, currency, lastCounter, order.getId(), order.getMinBalance(), order.getMaxBalance());
                    orderReports.put(order.getId(),
                            new AccountReport(order.getId(), processedAt, account.getId(), account.getCurrencyId(),
                                    account.getCreatedAt(), order.getId(), account.getBalance(),
                                    account.getMinBalance(), account.getMaxBalance(), account.getStatus(),
                                    account.getLastRecordNumber(), null));
                    accounts.add(account);
                }
            }
        }

        try {
            issuerDAO.update(issuers.values());
            accountDAO.create(accounts);
        } catch (UpdateException e) {
            throw new RuntimeException("Unable to create accounts");
        }

        return orderReports;
    }

    @Override
    protected Map<Long, Report> answerDuplicateRequests(Map<Long, AccountIssueOrder> duplicateRequests) {

        Map<Long, Report> answers = new HashMap<Long, Report>();
        Map<Long, Account> accountByOrderIds = accountDAO.readByOrderIds(duplicateRequests.keySet());

        for (Map.Entry<Long, Account> accountByOrderId : accountByOrderIds.entrySet()) {
            Account account = accountByOrderId.getValue();
            answers.put(accountByOrderId.getKey(),
                    new AccountReport(accountByOrderId.getKey(), account.getCreatedAt(),
                            account.getId(), account.getCurrencyId(), account.getCreatedAt(), account.getOrderId(),
                            account.getBalance(), account.getMinBalance(), account.getMaxBalance(), account.getStatus(),
                            account.getLastRecordNumber() , account.getLastRecordAt()));
        }

        return answers;
    }
}
