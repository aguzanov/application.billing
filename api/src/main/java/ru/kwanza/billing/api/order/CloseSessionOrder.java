package ru.kwanza.billing.api.order;

import com.fasterxml.jackson.annotation.JsonTypeName;
import ru.kwanza.billing.api.RequestType;
import ru.kwanza.billing.api.SessionId;

/**
 * @author henadiy
 */
@JsonTypeName(value = "O_SESSION_CLOSE")
public class CloseSessionOrder extends Order {

	protected CloseSessionOrder() {
	}

	public CloseSessionOrder(Long id, SessionId sessionId) {
		super(id, sessionId);
	}

	@Override
	public RequestType getRequestType() {
		return RequestType.O_SESSION_CLOSE;
	}
}
