package ru.kwanza.billing.processor;

import ru.kwanza.billing.api.Request;
import ru.kwanza.billing.api.Session;
import ru.kwanza.billing.api.SessionId;
import ru.kwanza.billing.processor.session.ISessionManager;
import ru.kwanza.console.security.impl.access.AccessMatrix;

import java.util.*;

/**
 * @author henadiy
 */
public abstract class AccessMatrixCompetenceFilter<R extends Request> implements ICompetenceFilter<R> {

    private ISessionManager sessionManager;
    private AccessMatrix accessMatrix;

    public void setSessionManager(ISessionManager sessionManager) {
        this.sessionManager = sessionManager;
    }

    protected ISessionManager getSessionManager() {
        return sessionManager;
    }

    public void setAccessMatrix(AccessMatrix accessMatrix) {
        this.accessMatrix = accessMatrix;
    }

    protected AccessMatrix getAccessMatrix() {
        return accessMatrix;
    }

    protected Map<SessionId, Session> getSessions(Collection<R> requests) {
        // preparation of session IDs
        List<SessionId> sessionIds = new ArrayList<SessionId>(requests.size());
        for(R req  : requests) {
            sessionIds.add(req.getSessionId());
        }

        // get the list of sessions
        return sessionManager.get(sessionIds);
    }

    protected boolean isSessionValid(Session session, Date processedAt) {
        return null != session && (null == session.getExpiresIn() || !session.getExpiresIn().before(processedAt));
    }

    protected String buildPermission(R req, String code) {
        return new StringBuilder().append(req.getClass().getSimpleName()).append(":").append(code).toString();
    }

}
