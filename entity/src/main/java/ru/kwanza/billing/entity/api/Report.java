package ru.kwanza.billing.entity.api;

import ru.kwanza.console.security.api.AuthField;
import ru.kwanza.console.security.api.AuthIdField;
import ru.kwanza.dbtool.orm.annotations.Field;
import ru.kwanza.dbtool.orm.annotations.IdField;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Vasily Vorobyov
 */
public abstract class Report implements Serializable {


    @IdField("id")
    @AuthIdField
    @AuthField(name = "ID")
    private Long id;

    @AuthField(name = "Date of processing")
    @Field("processed_at")
    private Date processedAt;

    protected Report() {
    }

    protected Report(Long id, Date processedAt) {
        this.id = id;
        this.processedAt = processedAt;
    }

    public Long getId() {
        return id;
    }

    public Date getProcessedAt() {
        return processedAt;
    }
}
