package ru.kwanza.billing.gateway;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.kwanza.billing.api.PaymentToolId;
import ru.kwanza.billing.api.PaymentToolPermission;
import ru.kwanza.billing.api.Request;
import ru.kwanza.billing.api.SessionId;
import ru.kwanza.billing.api.order.*;
import ru.kwanza.billing.api.query.AccountReportQuery;
import ru.kwanza.billing.api.query.ConversionInstructionQuery;
import ru.kwanza.billing.api.report.*;
import ru.kwanza.billing.client.IBillingClient;

import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author Vasily Vorobyov
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:billing-gateway-tests.xml")
//@Ignore
public class HttpGatewayTest extends AbstractJUnit4SpringContextTests {

	@Autowired
    private IBillingClient client;

    @Test
    public void testSuccessPayment() throws URISyntaxException {

        Collection<Long> id = client.generateRequestIds(11);
        Iterator<Long> idIt = id.iterator();

		OpenSessionOrder sessionOrder = new OpenSessionOrder(idIt.next());
		sessionOrder.addArgument("sessionId", "1234567");
		SessionReport sessionReport = (SessionReport)sendRequests(sessionOrder)[0];

		sendRequests(new RegisterCurrencyOrder(idIt.next(), sessionReport.getSession().getId(), 943, 2, "EUR", "Euro"));
		sendRequests(new RegisterCurrencyOrder(idIt.next(), sessionReport.getSession().getId(), 974, 2, "BYR", "Belarusian ruble"));

		sendRequests(new RegisterIssuerOrder(idIt.next(), sessionReport.getSession().getId(), 1, 447300, "Test issuer"));

        final AccountIssueOrder accountIssueOrder = new AccountIssueOrder(idIt.next(), sessionReport.getSession().getId(), 1, 943, 0L, null);
        AccountReport accountReport = (AccountReport) sendRequests(accountIssueOrder)[0];

        PaymentToolPermissionsReport permissionsReport = (PaymentToolPermissionsReport)sendRequests(
                new PaymentToolPermissionsOrder(idIt.next(), sessionReport.getSession().getId(),
                        new PaymentToolId(accountReport.getAccountId(), PaymentToolId.Type.ACCOUNT),
                        sessionReport.getSession().getUserId(), PaymentToolPermission.values()))[0];

        PaymentInstruction[] instructions = new PaymentInstruction[] {
                new PaymentInstruction(new PaymentToolId(accountReport.getAccountId(), PaymentToolId.Type.ACCOUNT), -10000L)
        };
        PaymentOrder paymentOrder = new PaymentOrder(idIt.next(), sessionReport.getSession().getId(), null, instructions);

        RejectedRequestReport paymentReport = (RejectedRequestReport) sendRequests(paymentOrder)[0];

        assertEquals(RejectCode.MIN_BALANCE_EXCEEDED, paymentReport.getRejectCode());

        instructions = new PaymentInstruction[] {
                new PaymentInstruction(new PaymentToolId(accountReport.getAccountId(), PaymentToolId.Type.ACCOUNT), 50L)
        };
        paymentOrder = new PaymentOrder(idIt.next(), sessionReport.getSession().getId(), null, instructions);

        assertEquals(PaymentOrderReport.class, sendRequests(paymentOrder)[0].getClass());

        ConversionInstruction i =
                new ConversionInstruction(sessionReport.getSession().getId(), new Date(114, 3, 12, 15, 35, 22), new Date(114, 3, 12, 16, 35, 22),
                        PaymentToolId.BLANK, 555L, 222, new BigDecimal("11.12"),
                        new PaymentToolId(accountReport.getAccountId(), PaymentToolId.Type.ACCOUNT), 15L, 943, new BigDecimal("0.4321"));
        instructions = new PaymentInstruction[] {i};
        paymentOrder = new PaymentOrder(idIt.next(), sessionReport.getSession().getId(), null, instructions);
        paymentReport = (RejectedRequestReport) sendRequests(paymentOrder)[0];
        assertEquals(RejectCode.ILLEGAL_SIGNATURE, paymentReport.getRejectCode());


        ConversionInstructionQuery convQuery = new ConversionInstructionQuery(
                idIt.next(), sessionReport.getSession().getId(),
				974, 50000L, new PaymentToolId(accountReport.getAccountId(), PaymentToolId.Type.ACCOUNT), null);
        Report convRep = sendRequests(convQuery)[0];
//        assertEquals(ConversionInstructionReport.class, convRep.getClass());
//        assertEquals(Long.valueOf(41940), ((ConversionInstructionReport)convRep).getInstruction().getSourceAmount());
//        assertEquals(Long.valueOf(3), ((ConversionInstructionReport)convRep).getInstruction().getTargetAmount());
        assertEquals(RejectedRequestReport.class, convRep.getClass());
        assertEquals(RejectCode.CONVERSION_RATE_NOT_FOUND, ((RejectedRequestReport)convRep).getRejectCode());


		sessionReport = (SessionReport)sendRequests(
				new CloseSessionOrder(idIt.next(), sessionReport.getSession().getId()))[0];
		assertEquals("1234567", sessionReport.getSession().getId().getToken());

	}

//    @Test
    public void testBatch() throws URISyntaxException {

        Collection<Long> ids = client.generateRequestIds(1000);
        Iterator<Long> idIt = ids.iterator();

        AccountIssueOrder[] accountIssueOrders = new AccountIssueOrder[1000];
        for (int i = 0; i < 1000; ++i) {
            accountIssueOrders[i] = new AccountIssueOrder(idIt.next(), new SessionId("SID"), 1, 943, null, null);
        }
        Report[] reports = sendRequests(accountIssueOrders);

        assertEquals(1000, reports.length);

        ids = client.generateRequestIds(5000 + 1000);
        idIt = ids.iterator();

        long totalRemote = 0L;
        for (int j = 0; j < 5; j++) {
            PaymentOrder[] paymentOrders = new PaymentOrder[1000];
            Random random = new Random();
            for (int i = 0; i < reports.length; ++i) {
                assertTrue(reports[i] instanceof AccountReport);
                AccountReport accountReport = (AccountReport) reports[i];

                final long amount = random.nextLong();
                paymentOrders[i] = new PaymentOrder(idIt.next(), new SessionId("SID"), "recharge on " + amount,
                        new PaymentInstruction[]{
                                new PaymentInstruction(new PaymentToolId(accountReport.getAccountId(), PaymentToolId.Type.ACCOUNT), amount),
                                new PaymentInstruction(new PaymentToolId(accountReport.getAccountId(), PaymentToolId.Type.ACCOUNT), -amount)});
            }

            long t0 = System.currentTimeMillis(), t;
            Report[] paymentReports = sendRequests(paymentOrders);
            t = System.currentTimeMillis();

            totalRemote += t - t0;

            assertEquals(1000, paymentReports.length);

            for (Report pr: paymentReports) {
                assertTrue(pr instanceof PaymentOrderReport);
            }
        }


        AccountReportQuery[] accountReportQueries = new AccountReportQuery[1000];
        for (int i = 0; i < reports.length; ++i) {
            AccountReport accountReport = (AccountReport) reports[i];
            accountReportQueries[i] = new AccountReportQuery(idIt.next(), new SessionId("SID"), accountReport.getAccountId());
        }


        Report[] accountReports = sendRequests(accountReportQueries);

        for (Report ar: accountReports) {
            assertTrue(ar instanceof AccountReport);
            AccountReport accountReport = (AccountReport) ar;
            System.out.format("%s: %d\n", accountReport.getAccountId().toPlainString(), accountReport.getBalance());
        }

        System.out.println("total remote payment time = " + totalRemote);
    }


	protected Report[] sendRequests(Request...requests) {
		return client.processRequests(Arrays.asList(requests)).toArray(new Report[requests.length]);
	}

}
