package ru.kwanza.billing.processor;

import ru.kwanza.autokey.api.AutoKeyValueSequence;
import ru.kwanza.autokey.api.IAutoKey;
import ru.kwanza.billing.api.IRequestIdGenerator;
import ru.kwanza.billing.api.Request;
import ru.kwanza.txn.api.Transactional;
import ru.kwanza.txn.api.TransactionalType;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Vasily Vorobyov
 */
public class RequestIdGenerator implements IRequestIdGenerator {

    @Resource(name = "autokey.IAutoKey")
    protected IAutoKey autoKey;

    @Transactional(TransactionalType.REQUIRED)
    public Collection<Long> generateIds(int count) {
        // todo - Mix the date in the beginning
        AutoKeyValueSequence seq = autoKey.getValueSequence(Request.class.getName(), count);
        List<Long> orderIds = new ArrayList<Long>(count);
        while (seq.hasNext()) {
            orderIds.add(seq.next());
        }
        return orderIds;
    }
}
