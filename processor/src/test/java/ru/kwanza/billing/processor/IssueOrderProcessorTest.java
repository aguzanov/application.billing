package ru.kwanza.billing.processor;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.kwanza.billing.api.IRequestProcessor;
import ru.kwanza.billing.api.PaymentToolId;
import ru.kwanza.billing.api.SessionId;
import ru.kwanza.billing.api.order.*;
import ru.kwanza.billing.api.report.AccountReport;
import ru.kwanza.billing.api.report.RejectedRequestReport;
import ru.kwanza.billing.api.report.Report;
import ru.kwanza.billing.api.report.WalletReport;
import ru.kwanza.billing.entity.api.Currency;
import ru.kwanza.billing.entity.api.Issuer;
import ru.kwanza.billing.entity.api.dao.ICurrencyDAO;
import ru.kwanza.billing.entity.api.dao.IIssuerDAO;
import ru.kwanza.dbtool.core.UpdateException;

import javax.annotation.Resource;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * @author Vasily Vorobyov
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:order-processors-test.xml")
//@Ignore
public class IssueOrderProcessorTest extends AbstractJUnit4SpringContextTests {

    @Resource(name = "billing.entity.dao.IssuerDAO")
    private IIssuerDAO issuerDAO;

    @Resource(name = "billing.entity.dao.CurrencyDAO")
    private ICurrencyDAO currencyDAO;

    @Resource(name = "billing.processor.WalletIssueOrderProcessor")
    private IRequestProcessor<WalletIssueOrder> walleIssueProcessor;

    @Resource(name = "billing.processor.AccountIssueOrderProcessor")
    private IRequestProcessor<AccountIssueOrder> accountIssueProcessor;

    @Resource(name = "billing.processor.WalletAccountIssueOrderProcessor")
    private IRequestProcessor<WalletAccountIssueOrder> walletAccountIssueProcessor;

    @Resource(name = "billing.processor.PaymentOrderProcessor")
    private IRequestProcessor<PaymentOrder> paymentOrderProcessor;

    @Test
    public void testSuccess() throws UpdateException {

        long t0 = System.currentTimeMillis(), t1, t1_1, t;

        Map<Integer, Currency> currencies = currencyDAO.readByKeys(Collections.singleton(943));
        Currency currency;
        if (null == currencies || currencies.isEmpty()) {
            currency = currencyDAO.prepare(943, "EUR", 2, "Euro");
            currencyDAO.create(Collections.singleton(currency));
        } else {
            currency = currencies.get(943);
        }

        Map<Integer, Issuer> issuers = issuerDAO.readByKeys(Collections.singleton(1));
        Issuer issuer;
        if (null == issuers || issuers.isEmpty()) {
            issuer = issuerDAO.prepare(1L, 447300, "Test Issuer");
            issuerDAO.create(Collections.singleton(issuer));
        } else {
            issuer = issuers.get(1);
        }

        final int WALLETS_COUNT = 1000;
        final int ACCOUNTS_IN_WALLET_COUNT = 1;

        Collection<Long> walletOrderIds = walleIssueProcessor.generateRequestIds(WALLETS_COUNT);
        ArrayList<WalletIssueOrder> walletIssueOrders = new ArrayList<WalletIssueOrder>(walletOrderIds.size());
        for (Long id: walletOrderIds) {
            walletIssueOrders.add(new WalletIssueOrder(id, new SessionId("SID"), "Wallet#" + id, "Group №" + id));
        }
        Map<Long, Report> walletReports = walleIssueProcessor.process(walletIssueOrders);

        Collection<Long> accountOrderIds = accountIssueProcessor.generateRequestIds(WALLETS_COUNT * ACCOUNTS_IN_WALLET_COUNT);
        Iterator<Long> accountOrderId = accountOrderIds.iterator();
        ArrayList<AccountIssueOrder> accountOrders = new ArrayList<AccountIssueOrder>(accountOrderIds.size());
        for (Map.Entry<Long, Report> r: walletReports.entrySet()) {
            assertFalse(r.getValue() instanceof RejectedRequestReport);
            for (int i = 0; i < ACCOUNTS_IN_WALLET_COUNT; ++i) {
                accountOrders.add(
                        new AccountIssueOrder(accountOrderId.next(), new SessionId("SID"), issuer.getId(), currency.getId(), -10000L, null));
            }
        }
        Map<Long, Report> accountReports = accountIssueProcessor.process(accountOrders);

        Collection<Long> walletAccountOrderIds = walletAccountIssueProcessor.generateRequestIds(WALLETS_COUNT);
        Iterator<Map.Entry<Long, Report>> itAccountReport = accountReports.entrySet().iterator();
        Iterator<Map.Entry<Long, Report>> itWalletReport = walletReports.entrySet().iterator();
        ArrayList<WalletAccountIssueOrder> walletAccountIssueOrders = new ArrayList<WalletAccountIssueOrder>(WALLETS_COUNT);

        for (Long id: walletAccountOrderIds) {
            WalletReport walletReport = (WalletReport) itWalletReport.next().getValue();
            AccountReport accountReport = (AccountReport) itAccountReport.next().getValue();
            assertEquals(943, accountReport.getCurrencyId().intValue());
            walletAccountIssueOrders.add(new WalletAccountIssueOrder(id, new SessionId("SID"), walletReport.getWalletId(), accountReport.getAccountId(),
                    walletReport.getWalletId().toString() + '#' + accountReport.getAccountId().toString()));
        }
        Map<Long, Report> walletAccountReports = walletAccountIssueProcessor.process(walletAccountIssueOrders);

        t1 = System.currentTimeMillis();

        ArrayList<PaymentOrder> paymentOrders = new ArrayList<PaymentOrder>(accountOrderIds.size());
        Collection<Long> paymentOrderIds = paymentOrderProcessor.generateRequestIds(accountOrderIds.size());

        for (Map.Entry<Long, Report> r: accountReports.entrySet()) {
            assertFalse(r.getValue() instanceof RejectedRequestReport);
            AccountReport accountIssueOrderReport = (AccountReport) r.getValue();
            Iterator<Long> idIt = paymentOrderIds.iterator();
            PaymentInstruction[] paymentInstructions = new PaymentInstruction[] {
                    new PaymentInstruction(new PaymentToolId(accountIssueOrderReport.getAccountId(), PaymentToolId.Type.ACCOUNT), +100L),
                    new PaymentInstruction(new PaymentToolId(accountIssueOrderReport.getAccountId(), PaymentToolId.Type.ACCOUNT), -90L),
                    new PaymentInstruction(new PaymentToolId(accountIssueOrderReport.getAccountId(), PaymentToolId.Type.ACCOUNT), +20L),
                    new PaymentInstruction(new PaymentToolId(accountIssueOrderReport.getAccountId(), PaymentToolId.Type.ACCOUNT), -30L)
            };
            paymentOrders.add(new PaymentOrder(idIt.next(), new SessionId("SID"), "test", paymentInstructions));
            idIt.remove();
        }

        t1_1 = System.currentTimeMillis();
        paymentOrderProcessor.process(paymentOrders);
        t = System.currentTimeMillis();

        System.out.format("issue = %dms, payment = %dms", t1 - t0, t - t1_1);
    }

}
