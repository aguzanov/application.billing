package ru.kwanza.billing.controller.valueobjects;

import ru.kwanza.billing.api.AccountStatus;
import ru.kwanza.billing.entity.api.Account;
import ru.kwanza.billing.entity.api.AccountIssueOrder;
import ru.kwanza.billing.entity.api.Currency;
import ru.kwanza.billing.entity.api.Issuer;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Dmitry Zhukov
 */
public class AccountVO {

    private BigDecimal id;
    private Long version;
    private Date createdAt;
    private Long orderId;
    private Long balance;
    private Long minBalance;
    private Long maxBalance;
    private Date lastRecordAt;
    private Integer lastRecordNumber;
    private BigDecimal lastRecordId;
    private Long lastRecordAmount;
    private Integer statusCode;
    private Integer issuerId;
    private Integer currencyId;
    private AccountIssueOrder accountIssueOrder;
    private Currency currency;
    private Issuer issuer;

    public AccountVO() {
    }

    public AccountVO(Account account) {
        this.id = account.getId();
        this.createdAt = account.getCreatedAt();
        this.orderId = account.getOrderId();
        this.version = account.getVersion();
        this.balance = 0L;
        this.minBalance = account.getMinBalance();
        this.maxBalance = account.getMaxBalance();
        this.statusCode = account.getStatus().getCode();
        this.lastRecordNumber = 0;
        this.lastRecordId = null;
        this.lastRecordAmount = 0L;
        this.lastRecordAt = null;
        this.issuerId = account.getIssuerId();
        this.currencyId = account.getCurrencyId();
        this.accountIssueOrder = account.getOrder();
        this.issuer = accountIssueOrder.getIssuer();
        this.currency = accountIssueOrder.getCurrency();
    }

    public BigDecimal getId() {
        return id;
    }

    public String getIdAsString() {
        return String.format("%025s", id.toString());
    }

    public Long getVersion() {
        return version;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Long getOrderId() {
        return orderId;
    }

    public Long getBalance() {
        return balance;
    }

    public void setBalance(Long balance) {
        this.balance = balance;
    }

    public Integer getLastRecordNumber() {
        return lastRecordNumber;
    }

    public Long getMinBalance() {
        return minBalance;
    }

    public void setMinBalance(Long minBalance) {
        this.minBalance = minBalance;
    }

    public Long getMaxBalance() {
        return maxBalance;
    }

    public void setMaxBalance(Long maxBalance) {
        this.maxBalance = maxBalance;
    }

    public Date getLastRecordAt() {
        return lastRecordAt;
    }

    public void setLastRecordAt(Date lastRecordAt) {
        this.lastRecordAt = lastRecordAt;
    }

    public BigDecimal getLastRecordId() {
        return lastRecordId;
    }

    public void setLastRecordId(BigDecimal lastRecordId) {
        this.lastRecordId = lastRecordId;
    }

    public Long getLastRecordAmount() {
        return lastRecordAmount;
    }

    public void setLastRecordAmount(Long lastRecordAmount) {
        this.lastRecordAmount = lastRecordAmount;
    }

    public Integer nextRecordNumber() {
        return ++lastRecordNumber;
    }

    public AccountStatus getStatus() {
        return AccountStatus.findByCode(this.statusCode);
    }

    public void setStatus(AccountStatus status) {
        this.statusCode = status.getCode();
    }

    public AccountIssueOrder getAccountIssueOrder() {
        return accountIssueOrder;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public Integer getIssuerId() {
        return issuerId;
    }

    public Integer getCurrencyId() {
        return currencyId;
    }

    public Currency getCurrency() {
        return currency;
    }
    public Issuer getIssuer() {
        return issuer;
    }
}
