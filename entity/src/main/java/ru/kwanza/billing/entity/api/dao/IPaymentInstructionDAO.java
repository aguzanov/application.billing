package ru.kwanza.billing.entity.api.dao;

import ru.kwanza.billing.entity.api.AccountRecord;
import ru.kwanza.billing.entity.api.ConversionInstruction;
import ru.kwanza.billing.entity.api.PaymentInstruction;
import ru.kwanza.billing.entity.api.RejectedPaymentInstruction;
import ru.kwanza.dbtool.core.UpdateException;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

/**
 * @author Vasily Vorobyov
 */
public interface IPaymentInstructionDAO {

    PaymentInstruction preparePaymentInstruction(BigDecimal id, Long orderId, Integer indexInOrder,
                                                 BigDecimal sourceAccountId, BigDecimal sourceWalletAccountId, Long sourceAmount,
                                                 BigDecimal targetAccountId, BigDecimal targetWalletAccountId, Long targetAmount,
                                                 Date recordedAt, String description);


    Collection<PaymentInstruction> createPaymentInstructions(Collection<PaymentInstruction> instructions) throws UpdateException;

    ConversionInstruction prepareConversionInstruction(BigDecimal id, Date issuedAt, Date expiresIn,
                                                       Integer sourceCurrencyId, BigDecimal sourceScale,
                                                       Integer targetCurrencyId, BigDecimal targetScale);


    Collection<ConversionInstruction> createConversionInstructions(
            Collection<ConversionInstruction> instructions) throws UpdateException;



    RejectedPaymentInstruction prepareRejectedPaymentInstruction(BigDecimal id,
                                                                 Long sourceIncomingBalance, Long targetIncomingBalance);

    Collection<RejectedPaymentInstruction> createRejectedPaymentInstructions(
            Collection<RejectedPaymentInstruction> rejectedPaymentInstructions) throws UpdateException;


    AccountRecord prepareAccountRecord(BigDecimal id, Long orderId,
                                       BigDecimal sourceAccountId, Integer sourceRecordNumber, Long sourceAmount, Long sourceIncomingBalance,
                                       BigDecimal targetAccountId, Integer targetRecordNumber, Long targetAmount, Long targetIncomingBalance,
                                       Date processedAt, String description);

    Collection<AccountRecord> createAccountRecords(Collection<AccountRecord> records) throws UpdateException;


    Collection<AccountRecord> selectDepositRecordsByAccountId(Collection<BigDecimal> ids, Date recorderedAfter, Date recorderedBefore);

    Collection<AccountRecord> selectWithdrawalRecordsByAccountId(Collection<BigDecimal> ids, Date recorderedAfter, Date recorderedBefore);


}
