package ru.kwanza.billing.controller;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.kwanza.billing.entity.api.RegisterIssuerOrder;
import ru.kwanza.console.security.api.AuthOperation;
import ru.kwanza.dbtool.orm.api.Filter;
import ru.kwanza.dbtool.orm.api.IEntityManager;
import ru.kwanza.dbtool.orm.api.If;
import ru.kwanza.dbtool.orm.api.OrderBy;
import ru.kwanza.txn.api.Transactional;
import ru.kwanza.txn.api.TransactionalType;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @author Dmitry Zhukov
 */

@Controller
@RequestMapping("registerissuerorder")
public class RegisterIssuerOrderController {
    @Resource(name = "dbtool.IEntityManager")
    private IEntityManager manager;

    @RequestMapping(value = "read", method = RequestMethod.GET)
    @ModelAttribute("data")
    @Transactional(TransactionalType.REQUIRES_NEW)
    @AuthOperation(name = "View issuers creation orders", check = "hasPermission('RegisterIssuerOrder:read')",
            auditable = false)
    public List<RegisterIssuerOrder> getList(@RequestParam(value = "start", required = false) Integer start,
                                               @RequestParam(value = "limit", required = false) Integer limit,
                                               @RequestParam(value = "id", required = false) Long id,
                                               @RequestParam(value = "filter.id", required = false) Long filterId,
                                               @RequestParam(value = "filter.sessionId", required = false) String sessionId,
                                               @RequestParam(value = "filter.processedAt.from", required = false) @DateTimeFormat(
                                                       pattern = "dd.MM.yyyy HH:mm:ss") Date processedAtFrom,
                                               @RequestParam(value = "filter.processedAt.to", required = false) @DateTimeFormat(
                                                       pattern = "dd.MM.yyyy HH:mm:ss") Date processedAtTo,
                                               @RequestParam(value = "filter.bin", required = false) Integer bin,
                                               @RequestParam(value = "filter.title", required = false) String title,
                                               @RequestParam(value = "filter.rejectCode", required = false) Integer rejectCode) {

        Filter rejectCodeFilter;
        if (rejectCode != null && rejectCode == -1) {
            rejectCodeFilter = new Filter(rejectCode != null, If.isNull("rejectedReport.id"));
        } else {
            rejectCodeFilter =
                    new Filter(rejectCode != null, If.isEqual("rejectedReport.rejectCode"),
                            rejectCode);
        }

        return manager.filtering(RegisterIssuerOrder.class)
                .filter(id != null, If.isEqual("id"), id)
                .filter(filterId != null, If.isEqual("id"), filterId)
                .filter(processedAtFrom != null, If.isGreaterOrEqual("processedAt"), processedAtFrom)
                .filter(processedAtTo != null, If.isLessOrEqual("processedAt"), processedAtTo)
                .filter(sessionId != null, If.isEqual("sessionHistoryRecord.id"), sessionId)
                .filter(bin != null, If.isEqual("bin"), bin)
                .filter(title != null, If.like("title"), "%" + (title == null ? title : title.toUpperCase()) + "%")
                .filter(rejectCodeFilter)
                .join("rejectedReport, sessionHistoryRecord, exchange, issuer")
                .orderBy(OrderBy.DESC("id"))
                .paging(start, limit)
                .selectList();
    }

}
