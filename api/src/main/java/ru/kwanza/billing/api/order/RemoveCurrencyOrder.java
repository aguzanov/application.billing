package ru.kwanza.billing.api.order;

import com.fasterxml.jackson.annotation.JsonTypeName;
import ru.kwanza.billing.api.RequestType;
import ru.kwanza.billing.api.SessionId;

/**
 * @author Dmitry Zhukov
 */
@JsonTypeName(value = "O_CURRENCY_REMOVE")
public class RemoveCurrencyOrder extends Order {

    private Integer currencyId;

    public RemoveCurrencyOrder() {
    }

    public RemoveCurrencyOrder(Long id, SessionId sessionId, Integer currencyId) {
        super(id, sessionId);
        this.currencyId = currencyId;
    }

    @Override
    public RequestType getRequestType() {
        return RequestType.O_CURRENCY_REMOVE;
    }

    public Integer getCurrencyId() {
        return currencyId;
    }
}
