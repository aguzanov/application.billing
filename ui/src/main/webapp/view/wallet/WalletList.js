/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.view.wallet.WalletList', {
    requires: [
        'Console.grid.FiltersFeature',
        'Console.application.QuickLink'
    ],
    extend: 'Console.grid.BaseGrid',
    alias: ['widget.wallet-list', 'widget.walletlist'],
    tabTitle: 'Wallet list',
    caption: 'Wallet list',

    model: 'Billing.model.Wallet',
    store: 'Billing.store.WalletStore',

    iconCls: 'i-briefcase',

    buttons: [
        {
            xtype: 'button',
            text: 'Activate',
            itemId: 'activateWallet',
            iconCls: 'i-action-accept',
            disabled: true,
            permission: 'Wallet:activate'
        },
        {
            xtype: 'button',
            text: 'Block',
            itemId: 'suspendWallet',
            iconCls: 'i-msg-blocked',
            disabled: true,
            permission: 'Wallet:suspend'
        },
        {
            xtype: 'button',
            text: 'Close',
            itemId: 'closeWallet',
            iconCls: 'i-action-delete',
            disabled: true,
            permission: 'Wallet:close'
        }
    ],

    columns: [
        {
            xtype: 'quicklinkcolumn',
            text: 'Number of walllet',
            dataIndex: 'id',
            width: 180,
            filter: true
        },
        {
            text: 'Issue date',
            dataIndex: 'createdAt',
            width: 180,
            xtype: 'datecolumn',
            format: 'd.m.Y H:i:s',
            filter: true
        },
        {
            text: 'Order',
            dataIndex: 'orderId',
            width: 250,
            renderer: Billing.model.Wallet.orderRenderer,
            readOnly: true,
            valueField: 'orderId'
        },
        {
            text: 'Holder ID',
            dataIndex: 'holderId',
            width: 200,
            filter: true
        },
        {
            text: 'Name',
            dataIndex: 'title',
            width: 200
        },
        {
            xtype: 'storecolumn',
            store: 'Billing.store.WalletStatusStore',
            text: 'Status',
            dataIndex: 'statusCode',
            width: 100,
            filter: true,
            showIcons: true
        }

    ],

    initComponent: function () {
        var me = this;

        me.callParent(arguments);
        me.getSelectionModel().on('selectionchange', me.onSelectChange, me);
    },

    onSelectChange: function (selModel, selections) {
        var isClosed = selections.length != 0 ? (selections[0].get('statusCode') == 3 || selections[0].get('statusCode') == -1) : true;
        var isSuspended = selections.length != 0 ? (selections[0].get('statusCode') == 2 || selections[0].get('statusCode') == -1) : true;
        var isActivated = selections.length != 0 ? (selections[0].get('statusCode') == 1 || selections[0].get('statusCode') == -1) : true;
        this.down('#activateWallet').setDisabled(selections.length === 0 || isActivated);
        this.down('#suspendWallet').setDisabled(selections.length === 0 || isSuspended || isClosed);
        this.down('#closeWallet').setDisabled(selections.length === 0 || isClosed);
        this.down('#edit').setDisabled(selections.length === 0 || isClosed);
    }
});
