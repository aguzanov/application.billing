package ru.kwanza.billing.processor.query;

import ru.kwanza.billing.api.query.CurrenciesQuery;
import ru.kwanza.billing.api.report.CurrenciesReport;
import ru.kwanza.billing.api.report.Report;
import ru.kwanza.billing.entity.api.Currency;
import ru.kwanza.billing.entity.api.dao.ICurrencyDAO;
import ru.kwanza.billing.processor.RequestProcessor;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Vasily Vorobyov
 */
public class CurrenciesQueryProcessor extends RequestProcessor<CurrenciesQuery> {

    @Resource(name = "billing.entity.dao.CurrencyDAO")
    private ICurrencyDAO currencyDAO;

    protected Map<Long, Report> handleRequests(Date processedAt, Collection<CurrenciesQuery> requests) {

        Map<Long, Report> reports = new HashMap<Long, Report>(requests.size());

        Map<Integer, Currency> allCurrencies = currencyDAO.readAll();
        CurrenciesReport.Currency[] reportCurrencies = new CurrenciesReport.Currency[allCurrencies.size()];

        int i = 0;
        for (Currency currency: allCurrencies.values()) {
            reportCurrencies[i++] = new CurrenciesReport.Currency(
                    currency.getId(), currency.getExponent(), currency.getAlphaCode(), currency.getTitle());
        }

        for (CurrenciesQuery query: requests) {
            reports.put(query.getId(), new CurrenciesReport(query.getId(), processedAt, reportCurrencies));
        }

        return reports;
    }

    @Override
    protected Map<Long, Report> answerDuplicateRequests(Map<Long, CurrenciesQuery> duplicateRequests) {
        return handleRequests(new Date(), duplicateRequests.values());
    }
}
