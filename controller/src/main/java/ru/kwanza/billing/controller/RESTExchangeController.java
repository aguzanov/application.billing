package ru.kwanza.billing.controller;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.kwanza.billing.entity.api.RESTExchange;
import ru.kwanza.console.security.api.AuthOperation;
import ru.kwanza.dbtool.orm.api.IEntityManager;
import ru.kwanza.dbtool.orm.api.If;
import ru.kwanza.dbtool.orm.api.OrderBy;
import ru.kwanza.txn.api.Transactional;
import ru.kwanza.txn.api.TransactionalType;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @author Dmitry Zhukov
 */

@Controller
@RequestMapping("restexchange")
public class RESTExchangeController {
    @Resource(name = "dbtool.IEntityManager")
    private IEntityManager manager;

    @RequestMapping(value = "read", method = RequestMethod.GET)
    @ModelAttribute("data")
    @Transactional(TransactionalType.REQUIRES_NEW)
    @AuthOperation(name = "View REST exchange journal", check = "hasPermission('RESTExchange:read')", auditable = false)
    public List<RESTExchange> getList(@RequestParam(value = "start", required = false) Integer start,
                                      @RequestParam(value = "limit", required = false) Integer limit,
                                      @RequestParam(value = "id", required = false) Long id,
                                      @RequestParam(value = "filter.isSuccess", required = false) Boolean isSuccess,
                                      @RequestParam(value = "filter.uri", required = false) String uri,
                                      @RequestParam(value = "filter.ts.from", required = false) @DateTimeFormat(pattern = "dd.MM.yyyy HH:mm:ss") Date tsFrom,
                                      @RequestParam(value = "filter.ts.to", required = false) @DateTimeFormat(pattern = "dd.MM.yyyy HH:mm:ss") Date tsTo) {


        return manager.filtering(RESTExchange.class)
                .filter(id != null, If.isEqual("id"), id)
                .filter(uri != null, If.like("uri"), "%" + uri + "%")
                .filter(isSuccess != null, If.isEqual("isSuccess"), isSuccess)
                .filter(tsFrom != null, If.isGreaterOrEqual("ts"), tsFrom)
                .filter(tsTo != null, If.isLessOrEqual("ts"), tsTo)
                .orderBy(OrderBy.DESC("ts"))
                .paging(start, limit)
                .selectList();
    }
}
