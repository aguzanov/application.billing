package ru.kwanza.billing.entity.impl.dao;

import ru.kwanza.billing.api.PaymentToolId;
import ru.kwanza.billing.entity.api.PaymentToolACE;
import ru.kwanza.billing.entity.api.dao.IAccessControlListDAO;
import ru.kwanza.dbtool.core.UpdateException;
import ru.kwanza.dbtool.orm.api.IEntityManager;

import javax.annotation.Resource;
import java.util.*;

/**
 * @author henadiy
 */
public class AccessControlListDAO implements IAccessControlListDAO {

	@Resource(name = "dbtool.IEntityManager")
	private IEntityManager em;

	public void init() {
	}


	public PaymentToolACE preparePaymentToolACE(PaymentToolId paymentToolId, String userId, Integer permissions) {
		return new PaymentToolACE(paymentToolId, userId, permissions);
	}

	public Map<String, PaymentToolACE> readPaymentToolACEByIds(Collection<String> ids) {
		return em.readMapByKeys(PaymentToolACE.class, ids);
	}

	public Collection<PaymentToolACE> createPaymentToolACE(Collection<PaymentToolACE> aces) throws UpdateException {
		return em.create(PaymentToolACE.class, aces);
	}

	public Collection<PaymentToolACE> updatePaymentToolACE(Collection<PaymentToolACE> aces) throws UpdateException {
		return em.update(PaymentToolACE.class, aces);
	}

    public Collection<PaymentToolACE> deletePaymentToolACE(Collection<PaymentToolACE> aces) throws UpdateException {
        return em.delete(PaymentToolACE.class, aces);
    }


}
