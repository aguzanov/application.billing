package ru.kwanza.billing.api.report;

import com.fasterxml.jackson.annotation.JsonTypeName;

import java.util.Date;

/**
 * @author Dmitry Zhukov
 */
@JsonTypeName(value = "REJECTED_REQUEST_REPORT")
public class RejectedRequestReport extends Report {

    private RejectCode rejectCode;

    protected RejectedRequestReport() {
    }

    public RejectedRequestReport(Long id, Date processedAt, RejectCode rejectCode) {
        super(id, processedAt);
        this.rejectCode = rejectCode;
    }

    public RejectCode getRejectCode() {
        return rejectCode;
    }

	@Override
	public String toString() {
		return String.format("RejectedRequestReport{id=%s, processedAt=%s, rejectCode=%s}",
				getId(), getProcessedAt(), getRejectCode());
	}
}
