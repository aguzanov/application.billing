package ru.kwanza.billing.processor.order;

import ru.kwanza.billing.api.PaymentToolId;
import ru.kwanza.billing.api.PaymentToolPermission;
import ru.kwanza.billing.api.order.PaymentToolPermissionsOrder;
import ru.kwanza.billing.api.report.PaymentToolPermissionsReport;
import ru.kwanza.billing.api.report.RejectCode;
import ru.kwanza.billing.api.report.RejectedRequestReport;
import ru.kwanza.billing.api.report.Report;
import ru.kwanza.billing.entity.api.Account;
import ru.kwanza.billing.entity.api.PaymentToolACE;
import ru.kwanza.billing.entity.api.WalletAccount;
import ru.kwanza.billing.entity.api.dao.IAccessControlListDAO;
import ru.kwanza.billing.entity.api.dao.IAccountDAO;
import ru.kwanza.billing.entity.api.dao.IWalletAccountDAO;
import ru.kwanza.billing.entity.api.id.PaymentToolACEIdBuilder;
import ru.kwanza.billing.processor.RequestProcessor;
import ru.kwanza.dbtool.core.UpdateException;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author henadiy
 */
public class PaymentToolPermissionsOrderProcessor extends RequestProcessor<PaymentToolPermissionsOrder> {

    @Resource(name = "billing.entity.dao.WalletAccountDAO")
    private IWalletAccountDAO walletAccountDAO;
    @Resource(name = "billing.entity.dao.AccountDAO")
    private IAccountDAO accountDAO;
    @Resource(name = "billing.entity.dao.AccessControlListDAO")
    private IAccessControlListDAO accessControlListDAO;

    @Override
    protected Map<Long, Report> handleRequests(Date processedAt, Collection<PaymentToolPermissionsOrder> requests) {
        HashMap<Long, Report> orderReports = new HashMap<Long, Report>(requests.size());

        Set<BigDecimal> accountIds = new HashSet<BigDecimal>();
        Set<BigDecimal> walletAccountIds = new HashSet<BigDecimal>();

        Collection<PaymentToolPermissionsOrder> acceptedOrderRequests =
                new ArrayList<PaymentToolPermissionsOrder>(requests);

        // collectin account
        Iterator<PaymentToolPermissionsOrder> orderIterator = acceptedOrderRequests.iterator();
        while (orderIterator.hasNext()) {
            PaymentToolPermissionsOrder order = orderIterator.next();
            if (PaymentToolId.Type.ACCOUNT.equals(order.getPaymentToolId().getType())) {
                accountIds.add(order.getPaymentToolId().getId());
            } else if (PaymentToolId.Type.WALLET_ACCOUNT.equals(order.getPaymentToolId().getType())) {
                walletAccountIds.add(order.getPaymentToolId().getId());
            } else {
                orderReports.put(order.getId(),
                        new RejectedRequestReport(order.getId(), processedAt, RejectCode.NO_ACCESS_OR_ENTITY));
                orderIterator.remove();
            }
        }

        // of the requested account
        Map<BigDecimal, Account> accounts = accountDAO.readByKeys(accountIds);
        Map<BigDecimal, WalletAccount> walletAccounts = walletAccountDAO.readByKeys(walletAccountIds);

        Set<String> requestAceIds = new HashSet<String>(requests.size());

        // check the requests
        orderIterator = acceptedOrderRequests.iterator();
        while (orderIterator.hasNext()) {
            PaymentToolPermissionsOrder order = orderIterator.next();
            RejectCode rejectCode = null;
            if (PaymentToolId.Type.ACCOUNT.equals(order.getPaymentToolId().getType()) &&
                    !accounts.containsKey(order.getPaymentToolId().getId())) {
                rejectCode = RejectCode.ACCOUNT_NOT_FOUND;
            } else if (PaymentToolId.Type.WALLET_ACCOUNT.equals(order.getPaymentToolId().getType()) &&
                    !walletAccounts.containsKey(order.getPaymentToolId().getId())) {
                rejectCode = RejectCode.WALLET_ACCOUNT_NOT_FOUND;
            } else {
                requestAceIds.add(
                        new PaymentToolACEIdBuilder(order.getPaymentToolId(), order.getUserId()).getId());
            }
            if (null != rejectCode) {
                orderReports.put(order.getId(), new RejectedRequestReport(order.getId(), processedAt, rejectCode));
                orderIterator.remove();
            }
        }

        // the requested record access control
        Map<String, PaymentToolACE> existingAces = accessControlListDAO.readPaymentToolACEByIds(requestAceIds);

        List<PaymentToolACE> acesForCreate = new ArrayList<PaymentToolACE>(requests.size());
        List<PaymentToolACE> acesForUpdate = new ArrayList<PaymentToolACE>(requests.size());
        List<PaymentToolACE> acesForDelete = new ArrayList<PaymentToolACE>(requests.size());

        // prepared by the records access control
        orderIterator = acceptedOrderRequests.iterator();
        while (orderIterator.hasNext()) {
            PaymentToolPermissionsOrder order = orderIterator.next();

            int permissions = PaymentToolPermission.pack(order.getPermissions());
            PaymentToolACE ace = new PaymentToolACE(order.getPaymentToolId(), order.getUserId(), permissions);
            if (existingAces.containsKey(ace.getId())) {
                if (existingAces.containsKey(ace.getId()) && 0 == permissions) {
                    acesForDelete.add(ace);
                } else {
                    acesForUpdate.add(ace);
                }
            } else if (0 != permissions) {
                acesForCreate.add(ace);
            }

            Map<String, PaymentToolPermission[]> userPermissions = new HashMap<String, PaymentToolPermission[]>(1);
            userPermissions.put(order.getUserId(), order.getPermissions());
            orderReports.put(order.getId(),
                    new PaymentToolPermissionsReport(order.getId(), processedAt, order.getPaymentToolId(), userPermissions));
        }

        // save the changes
        try {
            accessControlListDAO.createPaymentToolACE(acesForCreate);
        } catch (UpdateException e) {
            throw new RuntimeException("Unable to create payment tool access control records", e);
        }

        try {
            accessControlListDAO.updatePaymentToolACE(acesForUpdate);
        } catch (UpdateException e) {
            throw new RuntimeException("Unable to update payment tool access control records", e);
        }

        try {
            accessControlListDAO.deletePaymentToolACE(acesForDelete);
        } catch (UpdateException e) {
            throw new RuntimeException("Unable to delete payment tool access control records", e);
        }

        return orderReports;
    }


    @Override
    protected Map<Long, Report> answerDuplicateRequests(Map<Long, PaymentToolPermissionsOrder> duplicateRequests) {
        return Collections.emptyMap();
    }

}
