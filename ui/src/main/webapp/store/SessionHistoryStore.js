Ext.define('Billing.store.SessionHistoryStore',{
    extend: 'Ext.data.Store',
    model: 'Billing.model.SessionHistory',
    autoLoad: true,
    remoteFilter: true,
    remoteSort: false,
    autoSync: false
});
