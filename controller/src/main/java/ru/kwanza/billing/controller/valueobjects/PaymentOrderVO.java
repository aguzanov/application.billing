package ru.kwanza.billing.controller.valueobjects;

import ru.kwanza.billing.entity.api.AccountRecord;
import ru.kwanza.billing.entity.api.PaymentInstruction;

import java.util.Collection;
import java.util.Date;

/**
 * @author Dmitry Zhukov
 */
public class PaymentOrderVO extends OrderVO {

    private Integer instructionCount;

    private String description;

    private Collection<PaymentInstruction> paymentInstructions;

    private Collection<AccountRecord> accountRecords;

    public PaymentOrderVO(Long id, Long sessionRecordId, Date processedAt, Integer rejectCode, Integer instructionCount, String description,
                          Collection<PaymentInstruction> paymentInstructions, Collection<AccountRecord> accountRecords) {
        super(id, sessionRecordId, processedAt, rejectCode);
        this.instructionCount = instructionCount;
        this.description = description;
        this.paymentInstructions = paymentInstructions;
        this.accountRecords = accountRecords;
    }

    public Integer getInstructionCount() {
        return instructionCount;
    }

    public String getDescription() {
        return description;
    }

    public Collection<PaymentInstruction> getPaymentInstructions() {
        return paymentInstructions;
    }

    public Collection<AccountRecord> getAccountRecords() {
        return accountRecords;
    }
}
