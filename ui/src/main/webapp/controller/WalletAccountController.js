/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.controller.WalletAccountController', {
    extend: 'Ext.app.Controller',
    models: ['Billing.model.WalletAccount'],
    stores: ['Billing.store.WalletAccountStore'],
    views: [
        'Billing.view.walletaccount.WalletAccountList',
        'Billing.view.walletaccount.WalletAccountCard'
    ],

    init: function () {
        var me = this;
        this.control({

        });
    }
});