package ru.kwanza.billing.clearing.entity;

import ru.kwanza.dbtool.orm.annotations.Entity;
import ru.kwanza.dbtool.orm.annotations.Field;
import ru.kwanza.dbtool.orm.annotations.IdField;
import ru.kwanza.dbtool.orm.annotations.ManyToOne;

import java.math.BigDecimal;
import java.sql.Types;

/**
 * @author henadiy
 */
@Entity(name = "ClearingAccount", table = "clearing_account")
public class ClearingAccount {

	@IdField(value = "account_id", type = Types.DECIMAL)
	private BigDecimal accountId;

	@Field("clearing_member_id")
	private Long clearingMemberId;

    @Field("last_cleared_record_number")
    private Integer lastClearedRecordNumber;


	@ManyToOne(property = "clearingMemberId")
	private ClearingMember clearingMember;


	public ClearingAccount() {
	}

    public ClearingAccount(BigDecimal accountId, Long clearingMemberId, Integer lastClearedRecordNumber) {
        this.accountId = accountId;
        this.clearingMemberId = clearingMemberId;
        this.lastClearedRecordNumber = lastClearedRecordNumber;
    }

    public BigDecimal getAccountId() {
        return accountId;
    }

    public Long getClearingMemberId() {
        return clearingMemberId;
    }

    public Integer getLastClearedRecordNumber() {
        return lastClearedRecordNumber;
    }

    public void setLastClearedRecordNumber(Integer lastClearedRecordNumber) {
        this.lastClearedRecordNumber = lastClearedRecordNumber;
    }

    public ClearingMember getClearingMember() {
        return clearingMember;
    }
}
