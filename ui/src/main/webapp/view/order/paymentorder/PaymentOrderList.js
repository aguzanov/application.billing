/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.view.order.paymentorder.PaymentOrderList', {
    extend: 'Billing.view.order.OrderList',

    requires: [
        'Billing.store.PaymentOrderStore',
        'Billing.view.order.paymentorder.PaymentOrderCard'
    ],

    alias: ['widget.payment-order-list', 'widget.paymentorderlist'],
    tabTitle: 'Payment orders',
    caption: 'Payment orders',

    model: 'Billing.model.PaymentOrder',
    store: 'Billing.store.PaymentOrderStore',
    iconCls: 'i-document-license',

    constructor: function () {
        var me = this;

        me.columns = [
            {
                text: 'Number of instructions',
                dataIndex: 'instructionCount',
                width: 200
//                ,
//                renderer:function(v,m,r){
//                    return Console.application.QuickLink.createLink(r.get('id'),
//                        'Billing.view.order.paymentorder.PaymentOrderCard', {recordId: r.data.id, tab: {name: 'paymentorder'}});
//                }
            },
            {
                text: 'Description',
                dataIndex: 'description',
                width: 200
            }
        ];

        me.callParent(arguments);
    }
});
