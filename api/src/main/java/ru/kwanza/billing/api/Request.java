package ru.kwanza.billing.api;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * @author Vasily Vorobyov
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
public abstract class Request {

    private Long id;
    private SessionId sessionId;
    private transient UserDetails userData;
    private Long exchangeId;


    protected Request() {

    }

    protected Request(Long id, SessionId sessionId) {
        this.id = id;
        this.sessionId = sessionId;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public SessionId getSessionId() {
        return sessionId;
    }


    public UserDetails getUserData() {
        return userData;
    }
    public void setUserData(UserDetails userData) {
        this.userData = userData;
    }
    public Long getExchangeId() {
        return exchangeId;
    }
    public void setExchangeId(Long exchangeId) {
        this.exchangeId = exchangeId;
    }
    @JsonIgnore
    public abstract RequestType getRequestType();

    @Override
    public String toString() {
        return "Request{" +
                "id=" + id +
                ", sessionId=" + sessionId +
                ", requestType=" + getRequestType() +
                '}';
    }
}
