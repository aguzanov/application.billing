/**
 * @author Dmitry Zhukov
 */

Ext.define('Billing.view.account.AccountList', {
    requires: [
        'Console.grid.FiltersFeature',
        'Console.application.QuickLink',
        'Billing.store.AccountStore'
    ],
    extend: 'Console.grid.BaseGrid',
    alias: ['widget.account-list', 'widget.accountlist'],
    tabTitle: 'Accounts',
    caption: 'Accounts',

    model: 'Billing.model.Account',
    store: 'Billing.store.AccountStore',
    iconCls: 'i-budget',

    buttons: [
        {
            xtype: 'button',
            text: 'Close account',
            itemId: 'closeAccount',
            disabled: true,
            iconCls: 'i-action-close'
        }
    ],

    columns: [
        {
            xtype: 'quicklinkcolumn',
            text: 'Account number',
            dataIndex: 'id',
            width: 200,
            filter: true,
            renderer: function (value, meta, record) {
                console.log(value);
                return value;
            }
        },
        {
            text: 'Issue date',
            dataIndex: 'createdAt',
            width: 200,
            xtype: 'datecolumn',
            format: 'd.m.Y H:i:s.u',
            filter: true
        },
        {
            text: 'Issuer',
            displayField: 'issuerTitle',
            valueField: 'issuerId',
            dataIndex: 'issuerId',
            width: 200,
            filter: {
                store: 'Billing.store.IssuerStore'
            },
            xtype: 'quicklinkcolumn',
            detailView: 'Billing.view.issuer.IssuerCard'
        },
        {
            xtype: 'quicklinkcolumn',
            detailView: 'Billing.view.currency.CurrencyCard',
            displayField: "currencyTitle",
            valueField: 'currencyId',
            dataIndex: 'currencyId',
            text: 'Currency',
            width: 200,
            filter: {
                store: 'Billing.store.CurrencyStore'
            },

        },
        {
            text: 'Balance',
            dataIndex: 'balance',
            width: 200
        },
        {
            xtype: 'storecolumn',
            store: 'Billing.store.AccountStatusStore',
            text: 'Status',
            dataIndex: 'statusCode',
            width: 200,
            filter: true,
            showIcons: true
        }

    ],

    initComponent: function () {
        var me = this;

        me.callParent(arguments);
        me.getSelectionModel().on('selectionchange', me.onSelectChange, me);
    },

    onSelectChange: function (selModel, selections) {
        var isClosed = selections.length != 0 ? (selections[0].get('statusCode') == 3 || selections[0].get('statusCode') == -1) : true;

        //dzhukov: closed, if the account is not closed and status is not an indeterminate
        this.down('#closeAccount').setDisabled(selections.length === 0 || isClosed);
        //dzhukov: the edit checks that the account is active
        this.down('#edit').setDisabled(selections.length === 0 || isClosed);
    }


});
