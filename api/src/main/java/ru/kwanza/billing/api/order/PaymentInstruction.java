package ru.kwanza.billing.api.order;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import ru.kwanza.billing.api.PaymentToolId;

/**
 * @author Vasily Vorobyov
 */
@JsonTypeName(value = "PAYMENT_INSTRUCTION")
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = PaymentInstruction.class),
        @JsonSubTypes.Type(value = ConversionInstruction.class)
})
public class PaymentInstruction {

    private PaymentToolId sourceAccountId;
    private PaymentToolId targetAccountId;

    private Long sourceAmount;
    private Long targetAmount;

    protected PaymentInstruction() {
    }

    protected PaymentInstruction(PaymentToolId sourceAccountId, Long sourceAmount, PaymentToolId targetAccountId, Long targetAmount) {
        this.sourceAccountId = sourceAccountId;
        this.targetAccountId = targetAccountId;
        this.sourceAmount = sourceAmount;
        this.targetAmount = targetAmount;
    }

    public PaymentInstruction(PaymentToolId sourceAccountId, PaymentToolId targetAccountId, Long amount) {
        this(sourceAccountId, amount, targetAccountId, amount);
    }

    public PaymentInstruction(PaymentToolId accountId, Long amount) {
        this((amount > 0 ? PaymentToolId.BLANK : accountId), Math.abs(amount),
                (amount > 0 ? accountId : PaymentToolId.BLANK), Math.abs(amount));
    }

    public PaymentToolId getSourceAccountId() {
        return sourceAccountId;
    }

    public PaymentToolId getTargetAccountId() {
        return targetAccountId;
    }

    public Long getSourceAmount() {
        return sourceAmount;
    }

    public Long getTargetAmount() {
        return targetAmount;
    }
}
