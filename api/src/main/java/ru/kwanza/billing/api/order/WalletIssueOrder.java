package ru.kwanza.billing.api.order;

import com.fasterxml.jackson.annotation.JsonTypeName;
import ru.kwanza.billing.api.RequestType;
import ru.kwanza.billing.api.SessionId;

/**
 * @author Vasily Vorobyov
 */
@JsonTypeName(value = "O_WALLET_ISSUE")
public class WalletIssueOrder extends Order {

    private String holderId;
    private String title;

    protected WalletIssueOrder() {
    }

    public WalletIssueOrder(Long id, SessionId sessionId, String holderId, String title) {
        super(id, sessionId);
        this.holderId = holderId;
        this.title = title;
    }

    @Override
    public RequestType getRequestType() {
        return RequestType.O_WALLET_ISSUE;
    }

    public String getHolderId() {
        return holderId;
    }

    public String getTitle() {
        return title;
    }
}
