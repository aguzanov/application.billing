package ru.kwanza.billing.processor.xr;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collection;
import java.util.Date;
import java.util.Set;

/**
 * @author henadiy
 */
public abstract class Feed {
    private Date date;
    private Set<Integer> requiredCodes;

    public Feed() {
        setDate(new Date());
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Set<Integer> getRequiredCodes() {
        return requiredCodes;
    }

    public void setRequiredCodes(Set<Integer> requiredCodes) {
        this.requiredCodes = requiredCodes;
    }

    public abstract Collection<Entry> getAllEntries();

    protected abstract Collection<Entry> fetch();

    protected abstract Collection<Entry> parse(InputStream is);

    protected Collection<Entry> fetchURL(String url) {
        InputStream is = null;
        try {
            URLConnection conn =  new URL(url).openConnection();
            return parse((is = conn.getInputStream()));
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (null != is) {
                try {
                    is.close();
                } catch (IOException e) {
                }
            }
        }
    }

    public static class Entry {
        private final Integer sourceCode, targetCode;
        private final BigDecimal sourceScale, targetScale;

        public Entry(BigDecimal sourceScale, Integer sourceCode, BigDecimal targetScale, Integer targetCode) {
            this.sourceCode = sourceCode;
            this.targetCode = targetCode;
            this.sourceScale = sourceScale;
            this.targetScale = targetScale;
        }

        public Integer getSourceCode() {
            return sourceCode;
        }

        public Integer getTargetCode() {
            return targetCode;
        }

        public BigDecimal getSourceScale() {
            return sourceScale;
        }

        public BigDecimal getTargetScale() {
            return targetScale;
        }

        public boolean isValid() {
            return (null != sourceCode && sourceCode > 0) &&
                    (null != targetCode && targetCode > 0) &&
                    (null != sourceScale && sourceScale.floatValue() > 0) &&
                    (null != targetScale && targetScale.floatValue() > 0);
        }

        public String toString() {
            return String.format("%.4f(%s) -> %.4f(%s)", sourceScale, sourceCode, targetScale, targetCode);
        }

        public Entry getReversive() {
            return new Entry(getTargetScale(), getTargetCode(), getSourceScale(), getSourceCode());
        }

        public boolean equals(Object ob) {
            if (this == ob) {
                return true;
            } else if (null == ob || !(ob instanceof Entry)) {
                return false;
            } else {
                Entry e = (Entry) ob;
                return (sourceCode == e.sourceCode ||
                        null != sourceCode && sourceCode.equals(e.sourceCode) ) &&
                    (targetCode == e.targetCode ||
                        null != targetCode && targetCode.equals(e.targetCode) ) &&
                    (sourceScale == e.sourceScale ||
                        null != sourceScale && sourceScale.equals(e.sourceScale) ) &&
                    (targetScale == e.targetScale ||
                        null != targetScale && targetScale.equals(e.targetScale) );
            }
        }
    }
}
