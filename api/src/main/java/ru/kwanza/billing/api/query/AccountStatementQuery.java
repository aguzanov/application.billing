package ru.kwanza.billing.api.query;

import com.fasterxml.jackson.annotation.JsonTypeName;
import ru.kwanza.billing.api.RequestType;
import ru.kwanza.billing.api.SessionId;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Dmitry Zhukov
 */
@JsonTypeName(value = "Q_ACCOUNT_STATEMENT")
public class AccountStatementQuery extends Query implements IAccountInfoQuery {
    private BigDecimal accountId;
    private Date recorderedBefore;
    private Date recorderedAfter;

    protected AccountStatementQuery() {
    }

    public AccountStatementQuery(Long id, SessionId sessionId, BigDecimal accountId, Date recorderedBefore, Date recorderedAfter) {
        super(id, sessionId);
        this.accountId = accountId;
        this.recorderedBefore = recorderedBefore;
        this.recorderedAfter = recorderedAfter;
    }

    @Override
    public RequestType getRequestType() {
        return RequestType.Q_ACCOUNT_STATEMENT;
    }

    public BigDecimal getAccountId() {
        return accountId;
    }

    public Date getRecorderedBefore() {
        return recorderedBefore;
    }

    public Date getRecorderedAfter() {
        return recorderedAfter;
    }
}
