package ru.kwanza.billing.processor;

import ru.kwanza.console.security.impl.access.AccessMatrix;

import java.util.*;

/**
 * @author henadiy
 */
public class MockAccessMatrix extends AccessMatrix {

    private Map<String, Set<String>> rolePermissions = new HashMap<String, Set<String>>();

    @Override
    public Set<String> getPermissions(String role) {
        return rolePermissions.containsKey(role) ? rolePermissions.get(role) : Collections.EMPTY_SET;
    }

    @Override
    public Set<String> getPermissions(Collection<String> roles) {
        Set<String> res = new HashSet<String>();
        for(String role : roles) {
            res.addAll(getPermissions(role));
        }
        return res;
    }

    public void addRolePermissions(String role, String...permissions) {
        Set<String> perms = rolePermissions.get(role);
        if (null == perms) {
            perms = new HashSet<String>();
            rolePermissions.put(role, perms);
        }
        perms.addAll(Arrays.asList(permissions));
    }


}
