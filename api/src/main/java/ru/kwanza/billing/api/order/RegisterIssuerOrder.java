package ru.kwanza.billing.api.order;

import com.fasterxml.jackson.annotation.JsonTypeName;
import ru.kwanza.billing.api.RequestType;
import ru.kwanza.billing.api.SessionId;

/**
 * @author Vasily Vorobyov
 */
@JsonTypeName(value = "O_ISSUER_REGISTER")
public class RegisterIssuerOrder extends Order {

    private Integer issuerId;
    private Integer issuerBin;
    private String title;

    public RegisterIssuerOrder() {
    }

    public RegisterIssuerOrder(Long id, SessionId sessionId, Integer issuerId, Integer issuerBin, String title) {
        super(id, sessionId);
        this.issuerId = issuerId;
        this.issuerBin = issuerBin;
        this.title = title;
    }

    @Override
    public RequestType getRequestType() {
        return RequestType.O_ISSUER_REGISTER;
    }

    public Integer getIssuerId() {
        return issuerId;
    }

    public Integer getIssuerBin() {
        return issuerBin;
    }

    public String getTitle() {
        return title;
    }
}
