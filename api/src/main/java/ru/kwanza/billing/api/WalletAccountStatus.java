package ru.kwanza.billing.api;

/**
 * @author Vasily Vorobyov
 */
public enum WalletAccountStatus {

    ACTIVE(1, "Active"),
    SUSPENDED(2, "Blocked"),
    UNDEFINED(-1, "Not defined");

    private int code;
    private String title;

    WalletAccountStatus(int code, String title) {
        this.code = code;
        this.title = title;
    }

    public int getCode() {
        return code;
    }

    public String getTitle() {
        return title;
    }

    public static WalletAccountStatus findByCode(Integer code) {
        if (null != code) {
            for (WalletAccountStatus o: WalletAccountStatus.values()) {
                if (o.getCode() == code) {
                    return o;
                }
            }
        }
        return UNDEFINED;
    }
}
