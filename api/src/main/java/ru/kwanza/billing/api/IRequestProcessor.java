package ru.kwanza.billing.api;

import ru.kwanza.billing.api.report.Report;

import java.util.Collection;
import java.util.Map;

/**
 * @author Vasily Vorobyov
 */
public interface IRequestProcessor<R extends Request> {

    Collection<Long> generateRequestIds(int count);

    Map<Long, Report> process(Collection<R> requests);

}
