/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.view.order.walletaccountissueorder.WalletAccountIssueOrderCard', {
    extend: 'Billing.view.order.OrderCard',

    alias: 'widget.walletaccountissueorderdetails',

    title: 'Add account to wallet',
    iconCls: 'i-checklist',

    model: 'Billing.model.WalletAccountIssueOrder',
    store: 'Billing.store.WalletAccountIssueOrderStore',

    constructor: function (config) {
        var me = this;
        me.items = [
            {
                title: 'Additional parameters of the order',
                columnWidth: 1,
                items: [
                    {
                        xtype: 'infofield',
                        detailView: 'Billing.view.wallet.WalletCard',
                        valueField: 'walletId',
                        fieldLabel: 'Wallet',
                        name: 'walletId'
                    },
                    {
                        xtype: 'infofield',
                        detailView: 'Billing.view.account.AccountCard',
                        valueField: 'accountId',
                        fieldLabel: 'Account',
                        name: 'accountId'
                    },
                    {
                        fieldLabel: 'Name',
                        name: 'title'
                    }
                ]
            }
        ];
        me.callParent(arguments);
        me.items.get('main').columnWidth = 1;
    }
});
