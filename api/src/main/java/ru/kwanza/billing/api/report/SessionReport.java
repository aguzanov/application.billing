package ru.kwanza.billing.api.report;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;
import ru.kwanza.billing.api.Session;

import java.util.Date;

/**
 * @author henadiy
 */
@JsonTypeName(value = "SESSION_REPORT")
public class SessionReport extends Report {

	private Session session;

	protected SessionReport() {

	}

	public SessionReport(Long id, Date processedAt, Session session) {
		super(id, processedAt);
		this.session = session;
	}

	public Session getSession() {
		return session;
	}

}
