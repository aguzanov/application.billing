/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.model.AccountRecord', {
    extend: 'Ext.data.Model',
    fields: [
        'id',
        'orderId',
        'sourceAccountId',
        'sourceRecordNumber',
        'sourceAmount',
        'sourceIncomingBalance',
        'targetAccountId',
        'targetRecordNumber',
        'targetAmount',
        'targetIncomingBalance',
        { name: 'processedAt', type: 'date', dateFormat: 'd.m.Y H:i:s.u' },
        'description'
    ],
    permissions: {
        read: "AccountRecord:read",
        create: false,
        update: false,
        destroy: false
    },

    proxy: {
        type: 'ajax',
        apiUrl: 'billing-controller/accountrecord/'

    }
});