package ru.kwanza.billing.entity.api;

/**
 * Created by barboza on 22.12.15.
 */
public enum OrderType {
    REGISTER_ACCOUNT(1, "Register account"),
    REGISTER_WALLET(2, "Register wallet"),
    REGISTER_WALLET_ACCOUNT(3, "Add wallet to account"),
    REGISTER_CURRENCY(4, "Creating and editing of currencies"),
    REGISTER_ISSUER(5, "Creating and editing of issuers"),
    PAYMENT_ORDER(6, "Payment"),
    SUSPEND_WALLET_ORDER(7, "Suspend wallet"),
    ACTIVATE_WALLET_ORDER(8, "Activate wallet"),
    CLOSE_WALLET_ORDER(9, "Close wallet"),
    PERMISSION_ORDER(10, "Permission tool order"),
    REMOVE_CURRENCY_ORDER(11, "Removing currencies");

    private int code;
    private String title;

    OrderType(int code, String title) {
        this.code = code;
        this.title = title;
    }

    public int getCode() {
        return code;
    }

    public String getTitle() {
        return title;
    }
}
