package ru.kwanza.billing.api;

/**
 * @author henadiy
 */
public interface ISigned {

    String getSignature();

    void sign(String signature);
}
