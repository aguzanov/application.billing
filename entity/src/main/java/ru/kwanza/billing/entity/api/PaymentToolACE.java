package ru.kwanza.billing.entity.api;

import ru.kwanza.billing.api.PaymentToolId;
import ru.kwanza.billing.entity.api.id.PaymentToolACEIdBuilder;
import ru.kwanza.dbtool.orm.annotations.Entity;
import ru.kwanza.dbtool.orm.annotations.Field;
import ru.kwanza.dbtool.orm.annotations.IdField;

import java.io.Serializable;

/**
 * @author henadiy
 */
@Entity(table = "payment_tool_ace")
public class PaymentToolACE implements Serializable {

	private PaymentToolACEIdBuilder idBuilder;

	@IdField(value = "id")
	private String id;

	@Field("permissions")
	private Integer permissions;

	protected PaymentToolACE() {
	}

	public PaymentToolACE(PaymentToolId paymentToolId, String userId, Integer permissions) {
		this.idBuilder = new PaymentToolACEIdBuilder(paymentToolId, userId);
		this.id = this.idBuilder.getId();
		this.permissions = permissions;
	}

	public String getId() {
		return id;
	}

	public PaymentToolId getPaymentToolId() {
		return getIdBuilder().getPaymentToolId();
	}

	public String getUserId() {
		return getIdBuilder().getUserId();
	}

	public Integer getPermissions() {
		return permissions;
	}

	protected PaymentToolACEIdBuilder getIdBuilder() {
		if (null == idBuilder) {
			idBuilder = new PaymentToolACEIdBuilder(getId());
		}
		return idBuilder;
	}

}
