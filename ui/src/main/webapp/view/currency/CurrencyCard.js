/**
 * @author Dmitry Zhukov
 */

Ext.define('Billing.view.currency.CurrencyCard',{
    extend: 'Console.panel.CardPanel',
    requires: [],

    alias: 'widget.currencydetails',

    title: 'Card currency',
    iconCls: 'i-money',

    defaultType: 'fieldset',
    defaults: {
        columnWidth: 1,
        margin: '5 5 4 5',
        defaultType: 'textfield',
        minWidth: 560,
        defaults: {
            labelWidth: 140,
            anchor: '100%'
        }
    },

    model: 'Billing.model.Currency',
    store: 'Billing.store.CurrencyStore',

    items:[
        {
            title: 'Main parameters',
            itemId: 'main',
            items: [
                {
                    fieldLabel: 'Currency code', name: 'id', itemId: 'currencyId', maxLength: 3, allowBlank: false
                },
                {
                    fieldLabel: 'Alpha code', name: 'alphaCode', maxLength: 3, allowBlank: false
                },
                {
                    fieldLabel: 'Name', name: 'title', maxLength: 255, allowBlank: false
                },
                {
                    fieldLabel: 'Exponent', name: 'exponent', xtype: 'numberfield', minValue: 0, maxValue: 99, allowBlank: false
                }
            ]
        }
    ],
    listeners:{
        changemode: function(mode){
            var currencyIdField = this.items.get('main').items.get('currencyId');
            currencyIdField.setReadOnly(mode == 'edit');
        }
    }
});
