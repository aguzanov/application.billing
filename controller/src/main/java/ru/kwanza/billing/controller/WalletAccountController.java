package ru.kwanza.billing.controller;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.kwanza.billing.api.order.WalletAccountIssueOrder;
import ru.kwanza.billing.api.report.RejectedRequestReport;
import ru.kwanza.billing.api.report.Report;
import ru.kwanza.billing.api.report.WalletAccountReport;
import ru.kwanza.billing.controller.billing.BillingHelper;
import ru.kwanza.billing.controller.exceptions.AccountException;
import ru.kwanza.billing.controller.exceptions.WalletException;
import ru.kwanza.billing.controller.valueobjects.WalletAccountException;
import ru.kwanza.billing.controller.valueobjects.WalletAccountVO;
import ru.kwanza.billing.entity.api.Account;
import ru.kwanza.billing.entity.api.Wallet;
import ru.kwanza.billing.entity.api.WalletAccount;
import ru.kwanza.billing.entity.api.id.WalletAccountIdBuilder;
import ru.kwanza.console.security.api.AuthOperation;
import ru.kwanza.console.security.api.EntityAuditor;
import ru.kwanza.dbtool.core.UpdateException;
import ru.kwanza.dbtool.orm.api.IEntityManager;
import ru.kwanza.dbtool.orm.api.If;
import ru.kwanza.dbtool.orm.api.OrderBy;
import ru.kwanza.txn.api.Transactional;
import ru.kwanza.txn.api.TransactionalType;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author Dmitry Zhukov
 */

@Controller
@RequestMapping("walletaccount")
public class WalletAccountController {

    @Resource(name = "dbtool.IEntityManager")
    private IEntityManager manager;

//    @Resource(name = "billing.processor.WalletAccountIssueOrderProcessor")
//    private WalletAccountIssueOrderProcessor walletAccountIssueOrderProcessor;

    @Resource(name = "billing.BillingHelper")
    private BillingHelper billingHelper;


    @RequestMapping(value = "read", method = RequestMethod.GET)
    @ModelAttribute("data")
    @Transactional(TransactionalType.REQUIRES_NEW)
    @AuthOperation(name = "View account wallets", check = "hasPermission('WalletAccount:read')", auditable = false)
    public List<WalletAccount> getList(@RequestParam(value = "id", required = false) BigDecimal id,
                                         @RequestParam(value = "filter.accountId", required = false) BigDecimal accountId,
                                         @RequestParam(value = "filter.walletId", required = false) Long walletId,
                                         @RequestParam(value = "filter.createdAt.from", required = false) @DateTimeFormat(
                                                 pattern = "dd.MM.yyyy HH:mm:ss") Date createdAtFrom,
                                         @RequestParam(value = "filter.createdAt.to", required = false) @DateTimeFormat(
                                                 pattern = "dd.MM.yyyy HH:mm:ss") Date createdAtTo,
                                         @RequestParam(value = "filter.orderId", required = false) Long orderId,
                                         @RequestParam(value = "filter.title", required = false) String title

    ) {
        BigDecimal lowerWalletIdLimit = new BigDecimal(0);
        BigDecimal upperWalletIdLimit = new BigDecimal(0);
        if (walletId != null) {
            //dzhukov: add 10 to id account to receive "the upper boundary of the" filter
            lowerWalletIdLimit = new WalletAccountIdBuilder(walletId, 0, 0).getId();
            upperWalletIdLimit = new WalletAccountIdBuilder(walletId + 10l, 0, 0).getId();

        }

       return  manager.filtering(WalletAccount.class)
                .filter(id != null, If.isEqual("id"), id)
                .filter(accountId != null, If.isEqual("accountId"), accountId)
                .filter(walletId != null, If.isGreater("id"), lowerWalletIdLimit)
                .filter(walletId != null, If.isLess("id"), upperWalletIdLimit)
                .filter(createdAtFrom != null, If.isGreaterOrEqual("createdAt"), createdAtFrom)
                .filter(createdAtTo != null, If.isLessOrEqual("createdAt"), createdAtTo)
                .filter(orderId != null, If.isEqual("orderId"), orderId)
                .filter(title != null, If.like("title"), "%" + (title == null ? title : title + "%"))
                .join("order{account,wallet}")
                .orderBy(OrderBy.ASC("id"))
                .selectList();
    }

    @RequestMapping(value = "create", method = RequestMethod.POST)
    @ModelAttribute("data")
    @Transactional(TransactionalType.REQUIRES_NEW)
    @AuthOperation(name = "Create account wallet", check = "hasPermission('WalletAccount:create')")
    public WalletAccount create(@RequestBody WalletAccountVO data, HttpServletRequest request)
            throws WalletException, AccountException, WalletAccountException {
        Wallet wallet = manager.readByKey(Wallet.class, data.getWalletId());

        if (wallet == null) {
            throw new WalletException.WalletNotFoundException();
        }

        Account account = manager.readByKey(Account.class, data.getAccountId());
        if (account == null) {
            throw new AccountException.AccountNotFoundException();
        }

        Report report = billingHelper.processRequest(
                new WalletAccountIssueOrder(billingHelper.generateRequestId(), billingHelper.getSessionId(request),
                        data.getWalletId(), data.getAccountId(), data.getTitle())
        );
        if (report instanceof RejectedRequestReport) {
            throw new WalletAccountException.WalletAccountCreateException(((RejectedRequestReport) report).getRejectCode());
        }

        final WalletAccount walletAccount = manager.readByKey(WalletAccount.class, ((WalletAccountReport) report).getWalletAccountId());
        manager.fetch(walletAccount, "order{account,wallet}");
        return EntityAuditor.logCreate(walletAccount);
    }

    @RequestMapping(value = "destroy", method = RequestMethod.POST)
    @ModelAttribute("data")
    @Transactional(TransactionalType.REQUIRES_NEW)
    @AuthOperation(name = "Delete account wallet", check = "hasPermission('WalletAccount:delete')")
    public WalletAccount delete(@RequestBody WalletAccountVO data) throws WalletAccountException, UpdateException {
        WalletAccount walletAccount = manager.readByKey(WalletAccount.class, data.getId());

        if (walletAccount == null) {
            throw new WalletAccountException.WalletAccountNotFoundException();
        }

        return EntityAuditor.logDelete(manager.delete(walletAccount));
    }

}
