package ru.kwanza.billing.api.report;

import com.fasterxml.jackson.annotation.JsonTypeName;
import ru.kwanza.billing.api.AccountStatus;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Vasily Vorobyov
 */
@JsonTypeName(value = "ACCOUNT_REPORT")
public class AccountReport extends Report {

    private BigDecimal accountId;
    private Integer currencyId;
    private Date createdAt;
    private Long orderId;
    private Long balance;
    private Long minBalance;
    private Long maxBalance;
    private AccountStatus status;
    private Integer maxRecordNumber;
    private Date lastRecordAt;

    public AccountReport() {
    }

    public AccountReport(Long id, Date processedAt, BigDecimal accountId, Integer currencyId, Date createdAt, Long orderId, Long balance,
                         Long minBalance, Long maxBalance, AccountStatus status, Integer maxRecordNumber, Date lastRecordAt) {
        super(id, processedAt);
        this.accountId = accountId;
        this.currencyId = currencyId;
        this.createdAt = createdAt;
        this.orderId = orderId;
        this.balance = balance;
        this.minBalance = minBalance;
        this.maxBalance = maxBalance;
        this.status = status;
        this.maxRecordNumber = maxRecordNumber;
        this.lastRecordAt = lastRecordAt;
    }

    public BigDecimal getAccountId() {
        return accountId;
    }

    public Integer getCurrencyId() {
        return currencyId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Long getOrderId() {
        return orderId;
    }

    public Long getBalance() {
        return balance;
    }

    public Long getMinBalance() {
        return minBalance;
    }

    public Long getMaxBalance() {
        return maxBalance;
    }

    public AccountStatus getStatus() {
        return status;
    }

    public Integer getMaxRecordNumber() {
        return maxRecordNumber;
    }

    public Date getLastRecordAt() {
        return lastRecordAt;
    }
}
