/**
 * @author Dmitry Zhukov
 */

Ext.define('Billing.store.CurrencyStore',{
    extend: 'Ext.data.Store',
    model: 'Billing.model.Currency',
    autoLoad: true,
    remoteFilter: true,
    remoteSort: false,
    autoSync: false
});