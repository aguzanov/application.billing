package ru.kwanza.billing.processor.order;

import ru.kwanza.billing.api.order.RemoveCurrencyOrder;
import ru.kwanza.billing.api.report.RejectCode;
import ru.kwanza.billing.api.report.RejectedRequestReport;
import ru.kwanza.billing.api.report.RemoveCurrenciesReport;
import ru.kwanza.billing.api.report.Report;
import ru.kwanza.billing.entity.api.Account;
import ru.kwanza.billing.entity.api.Issuer;
import ru.kwanza.billing.entity.api.dao.IAccountDAO;
import ru.kwanza.billing.entity.api.dao.ICurrencyDAO;
import ru.kwanza.billing.entity.api.dao.IIssuerDAO;
import ru.kwanza.billing.processor.RequestProcessor;
import ru.kwanza.dbtool.core.UpdateException;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author Dmitry Zhukov
 */
public class RemoveCurrencyOrderProcessor extends RequestProcessor<RemoveCurrencyOrder> {

    @Resource(name = "billing.entity.dao.CurrencyDAO")
    private ICurrencyDAO currencyDAO;

    @Resource(name = "billing.entity.dao.IssuerDAO")
    private IIssuerDAO issuerDAO;

    @Resource(name = "billing.entity.dao.AccountDAO")
    private IAccountDAO accountDAO;

    @Override
    protected Map<Long, Report> handleRequests(Date processedAt, Collection<RemoveCurrencyOrder> requests) {
        Map<Long, Report> reports = new HashMap<Long, Report>(requests.size());

        Set<Integer> currencyIds = new HashSet<Integer>(requests.size());

        Iterator<RemoveCurrencyOrder> orderIt = requests.iterator();
        while (orderIt.hasNext()) {
            RemoveCurrencyOrder order = orderIt.next();
            RejectCode rejectCode = null;
            if (null == order.getCurrencyId()) {
                rejectCode = RejectCode.EMPTY_REQUIRED_FIELD;
            }
            if (null != rejectCode) {
                reports.put(order.getId(), new RejectedRequestReport(order.getId(), processedAt, rejectCode));
                orderIt.remove();
            } else {
                currencyIds.add(order.getCurrencyId());
            }
        }

        Map<Integer, ru.kwanza.billing.entity.api.Currency> currencies = currencyDAO.readByKeys(currencyIds);
        List<ru.kwanza.billing.entity.api.Currency> currenciesForDelete =
                new ArrayList<ru.kwanza.billing.entity.api.Currency>(currencies.size());
        List<ru.kwanza.billing.entity.api.Issuer> allIssuers = issuerDAO.readAll();

        //dzhukov: "sieve" the list of currencies to be removed non-existent currency, eliminating the remaining orders
        orderIt = requests.iterator();
        while (orderIt.hasNext()) {
            RemoveCurrencyOrder order = orderIt.next();
            ru.kwanza.billing.entity.api.Currency currency = currencies.get(order.getCurrencyId());
            RejectCode rejectCode = null;
            if (null == currency) {
                rejectCode = RejectCode.CURRENCY_NOT_FOUND;
            } else {
                for (Issuer issuer : allIssuers) {
                    Integer currencyId = currency.getId();
                    Integer issuerBin = issuer.getBin();
                    BigDecimal lowerLimit = Account.calculateLowerLimit(issuerBin, currencyId);
                    BigDecimal upperLimit = Account.calculateUpperLimit(issuerBin, currencyId);

                    if (accountDAO.isExistAccountInIdLimits(lowerLimit, upperLimit)) {
                        rejectCode = RejectCode.HAS_RELATED_ACCOUNTS;
                        //dzhukov: removed from card currency, because it has open invoices
                        currencies.remove(order.getCurrencyId());
                    }
                }
            }
            //dzhukov: check the initialization code cancel.
            // If the code is - add the rejected report, no - add currency to the list to be deleted
            if (null != rejectCode) {
                reports.put(order.getId(), new RejectedRequestReport(order.getId(), processedAt, rejectCode));
                orderIt.remove();
            } else {
                currenciesForDelete.add(currency);
            }
        }

        try {
            currencyDAO.delete(currenciesForDelete);
        } catch (UpdateException e) {
            throw new RuntimeException("Unable to delete currency", e);
        }

        buildSuccessReports(processedAt, requests, currencies, reports);

        return reports;
    }

    @Override
    protected Map<Long, Report> answerDuplicateRequests(Map<Long, RemoveCurrencyOrder> duplicateRequests) {
        return Collections.emptyMap();
    }

    //dzhukov: the method returns the records of only the deleted currency
    private void buildSuccessReports(Date processedAt, Collection<RemoveCurrencyOrder> requests,
                                     Map<Integer, ru.kwanza.billing.entity.api.Currency> deleted, Map<Long, Report> reports) {

        for (RemoveCurrencyOrder order : requests) {
            ru.kwanza.billing.entity.api.Currency currency = deleted.get(order.getCurrencyId());
            RemoveCurrenciesReport.Currency[] reportCurrency =
                    {new RemoveCurrenciesReport.Currency(currency.getId(), currency.getExponent(), currency.getAlphaCode(),
                            currency.getTitle())};

            reports.put(order.getId(), new RemoveCurrenciesReport(order.getId(), processedAt, reportCurrency));
        }
    }
}
