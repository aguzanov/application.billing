package ru.kwanza.billing.tariffication.calculator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import ru.kwanza.billing.tariffication.AmountResult;
import ru.kwanza.billing.tariffication.ResultCode;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

/**
 * @author henadiy
 */
@RunWith(JUnit4.class)
public class LinearCalculatorTest {

    @Test
    public void testCalcualte() throws Exception {

        LinearCalculator calc = new LinearCalculator(BigDecimal.valueOf(10100), BigDecimal.ONE, BigDecimal.ZERO);
        AmountResult<LinearCalculator.Arguments> res;

        res = calc.calculate(new LinearCalculator.Arguments(10100L));
        assertEquals(ResultCode.SUCCESS, res.getResultCode());
        assertEquals(Long.valueOf(1), res.getAmount());
        assertEquals(0, BigDecimal.valueOf(10100).compareTo(res.getArguments().getVariableValue()));

        res = calc.calculate(new LinearCalculator.Arguments(20200L));
        assertEquals(ResultCode.SUCCESS, res.getResultCode());
        assertEquals(Long.valueOf(2), res.getAmount());
        assertEquals(0, BigDecimal.valueOf(20200).compareTo(res.getArguments().getVariableValue()));

        res = calc.calculate(new LinearCalculator.Arguments(100L));
        assertEquals(ResultCode.SUCCESS_WITH_CORRECTIONS, res.getResultCode());
        assertEquals(Long.valueOf(1), res.getAmount());
        assertEquals(0, BigDecimal.valueOf(10100).compareTo(res.getArguments().getVariableValue()));

        res = calc.calculate(new LinearCalculator.Arguments(30400L));
        assertEquals(ResultCode.SUCCESS_WITH_CORRECTIONS, res.getResultCode());
        assertEquals(Long.valueOf(3), res.getAmount());
        assertEquals(0, BigDecimal.valueOf(30300).compareTo(res.getArguments().getVariableValue()));

        res = calc.calculate(new LinearCalculator.Arguments(40100L));
        assertEquals(ResultCode.SUCCESS_WITH_CORRECTIONS, res.getResultCode());
        assertEquals(Long.valueOf(3), res.getAmount());
        assertEquals(0, BigDecimal.valueOf(30300).compareTo(res.getArguments().getVariableValue()));

    }
}
