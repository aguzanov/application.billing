/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.store.IssuerStore', {
    extend: 'Ext.data.Store',
    model: 'Billing.model.Issuer',
    autoLoad: true,
    remoteFilter: true,
    remoteSort: false,
    autoSync: false
});