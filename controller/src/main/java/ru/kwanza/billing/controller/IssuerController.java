package ru.kwanza.billing.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.kwanza.billing.api.order.RegisterIssuerOrder;
import ru.kwanza.billing.api.report.IssuerReport;
import ru.kwanza.billing.api.report.RejectedRequestReport;
import ru.kwanza.billing.api.report.Report;
import ru.kwanza.billing.controller.billing.BillingHelper;
import ru.kwanza.billing.controller.exceptions.IssuerException;
import ru.kwanza.billing.entity.api.Account;
import ru.kwanza.billing.entity.api.Issuer;
import ru.kwanza.console.core.IMerger;
import ru.kwanza.console.security.api.AuthOperation;
import ru.kwanza.console.security.api.EntityAuditor;
import ru.kwanza.dbtool.core.UpdateException;
import ru.kwanza.dbtool.orm.api.Filter;
import ru.kwanza.dbtool.orm.api.IEntityManager;
import ru.kwanza.dbtool.orm.api.If;
import ru.kwanza.dbtool.orm.api.OrderBy;
import ru.kwanza.txn.api.Transactional;
import ru.kwanza.txn.api.TransactionalType;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Dmitry Zhukov
 */

@Controller
@RequestMapping("issuer")
public class IssuerController {
    @Resource(name = "dbtool.IEntityManager")
    private IEntityManager manager;

    @Resource(name = "console.IMerger")
    private IMerger merger;

//    @Resource(name = "billing.processor.RegisterIssuerOrderProcessor")
//    private RegisterIssuerOrderProcessor registerIssuerOrderProcessor;

    @Resource(name = "billing.BillingHelper")
    private BillingHelper billingHelper;


    @RequestMapping(value = "read", method = RequestMethod.GET)
    @ModelAttribute("data")
    @Transactional(TransactionalType.REQUIRES_NEW)
    @AuthOperation(name = "View issuers", check = "hasPermission('Issuer:read')", auditable = false)
    public List<Issuer> getList(@RequestParam(value = "id", required = false) Integer id,
                                @RequestParam(value = "filter.id", required = false) Integer filterId,
                                @RequestParam(value = "filter.bin", required = false) Integer bin,
                                @RequestParam(value = "filter.title", required = false) String title) {

        return manager.filtering(Issuer.class)
                .filter(id != null, If.isEqual("id"), id)
                .filter(filterId != null, If.isEqual("id"), filterId)
                .filter(bin != null, If.isEqual("bin"), bin)
                .filter(title != null, If.like("title"), "%" + (title == null ? title : title.toUpperCase()) + "%")
                .join("order")
                .orderBy(OrderBy.ASC("id"))
                .selectList();
    }

    @RequestMapping(value = "create", method = RequestMethod.POST)
    @ModelAttribute("data")
    @Transactional(TransactionalType.REQUIRES_NEW)
    @AuthOperation(name = "Create issuer", check = "hasPermission('Issuer:create')")
    public Issuer create(@RequestBody Issuer data, HttpServletRequest request)
            throws IllegalAccessException, UpdateException, IssuerException {

//        Long id = registerIssuerOrderProcessor.generateRequestIds(1).iterator().next();
//        RegisterIssuerOrder registerIssuerOrder = new RegisterIssuerOrder(id, new SessionId("BILLING_SESSION_ID"), null, data.getBin(), data.getTitle());
//        Map<Long, Report> result = registerIssuerOrderProcessor.process(
//                new ArrayList<RegisterIssuerOrder>(Arrays.asList(registerIssuerOrder)));
//
//        Report report = result.get(registerIssuerOrder.getId());

        Report report = billingHelper.processRequest(
                new RegisterIssuerOrder(billingHelper.generateRequestId(), billingHelper.getSessionId(request),
                        null, data.getBin(), data.getTitle())
        );
        if (report instanceof RejectedRequestReport) {
            throw new IssuerException.IssuerCreateException(((RejectedRequestReport) report).getRejectCode());
        }

        final Issuer issuer = manager.readByKey(Issuer.class, ((IssuerReport) report).getIssuerId());
        manager.fetch(issuer,"order");
        return EntityAuditor.logCreate(issuer);
    }

    @RequestMapping(value = "update", method = RequestMethod.POST)
    @ModelAttribute("data")
    @Transactional(TransactionalType.REQUIRES_NEW)
    @AuthOperation(name = "Update issuer", check = "hasPermission('Issuer:update')")
    public Issuer update(@RequestBody Issuer data, HttpServletRequest request)
            throws UpdateException, IllegalAccessException, IssuerException {

        Issuer issuer = manager.readByKey(Issuer.class, data.getId());

        if (issuer == null) {
            throw new IssuerException.IssuerNotFoundException();
        }

        EntityAuditor.logBeforeUpdate(issuer);

//        Long id = registerIssuerOrderProcessor.generateRequestIds(1).iterator().next();
//
//        RegisterIssuerOrder registerIssuerOrder = new RegisterIssuerOrder(id, new SessionId("BILLING_SESSION_ID"), data.getId(), data.getBin(), data.getTitle());
//
//        Map<Long, Report> result = registerIssuerOrderProcessor.process(
//                new ArrayList<RegisterIssuerOrder>(Arrays.asList(registerIssuerOrder)));
//
//        Report report = result.get(registerIssuerOrder.getId());

        Report report = billingHelper.processRequest(
                new RegisterIssuerOrder(billingHelper.generateRequestId(), billingHelper.getSessionId(request),
                        data.getId(), data.getBin(), data.getTitle())
        );
        if (report instanceof RejectedRequestReport) {
            throw new IssuerException.IssuerUpdateException(((RejectedRequestReport) report).getRejectCode());
        }
        merger.merge(issuer, data);
        manager.fetch(issuer,"order");
        return EntityAuditor.logAfterUpdate(issuer);
    }

    @RequestMapping(value = "destroy", method = RequestMethod.POST)
    @ModelAttribute("data")
    @Transactional(TransactionalType.REQUIRES_NEW)
    @AuthOperation(name = "Delete issuer", check = "hasPermission('Issuer:delete')")
    public Issuer destroy(@RequestBody Issuer data) throws UpdateException, IllegalAccessException, IssuerException {

        Issuer issuer = manager.readByKey(Issuer.class, data.getId());

        if (issuer == null) {
            throw new IssuerException.IssuerNotFoundException();
        }

        List<Account> issuerAccounts = manager.filtering(Account.class)
                .filter(new Filter(true, If.isGreater("id"), Account.calculateLowerLimit(issuer.getBin())),
                        new Filter(true, If.isLess("id"), Account.calculateUpperLimit(issuer.getBin()))).selectList();

        if (!issuerAccounts.isEmpty()) {
            throw new IssuerException.AccountAlreadyExistException();
        }

        manager.fetch(issuer,"order");
        return EntityAuditor.logDelete(manager.delete(issuer));
    }
}
