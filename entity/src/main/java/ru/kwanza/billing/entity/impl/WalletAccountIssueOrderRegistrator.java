package ru.kwanza.billing.entity.impl;

import ru.kwanza.billing.api.order.WalletAccountIssueOrder;

import java.util.*;

/**
 * @author Vasily Vorobyov
 */
public class WalletAccountIssueOrderRegistrator extends RequestRegistrator<
        WalletAccountIssueOrder, ru.kwanza.billing.entity.api.WalletAccountIssueOrder> {

    public WalletAccountIssueOrderRegistrator() {
        super(ru.kwanza.billing.entity.api.WalletAccountIssueOrder.class);
    }

    @Override
    protected ru.kwanza.billing.entity.api.WalletAccountIssueOrder createEntity(WalletAccountIssueOrder apiOrder,
                                                                                Date processedAt, Long sessionRecordId) {
        return new ru.kwanza.billing.entity.api.WalletAccountIssueOrder(
                apiOrder.getId(), sessionRecordId, apiOrder.getExchangeId(), processedAt, apiOrder.getWalletId(), apiOrder.getAccountId(), apiOrder.getTitle());
    }

    @Override
    protected Long getEntityId(ru.kwanza.billing.entity.api.WalletAccountIssueOrder entity) {
        return entity.getId();
    }
}
