package ru.kwanza.billing.gateway.http;

import org.glassfish.grizzly.http.HttpPacket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import ru.kwanza.autokey.api.AutoKeyValueSequence;
import ru.kwanza.autokey.api.IAutoKey;
import ru.kwanza.billing.api.IRequestProcessor;
import ru.kwanza.billing.api.Request;
import ru.kwanza.billing.api.report.Report;
import ru.kwanza.billing.entity.api.RESTExchange;
import ru.kwanza.billing.gateway.IReportBuilder;
import ru.kwanza.billing.gateway.IRequestParser;
import ru.kwanza.console.demo.DemoRegistry;
import ru.kwanza.dbtool.core.UpdateException;
import ru.kwanza.dbtool.orm.api.IEntityManager;
import ru.kwanza.jeda.api.IEventProcessor;
import ru.kwanza.jeda.nio.server.http.IHttpEvent;
import ru.kwanza.jeda.nio.server.http.IHttpRequest;
import ru.kwanza.jeda.nio.server.http.RequestID;
import ru.kwanza.jeda.nio.utils.HttpUtil;
import ru.kwanza.toolbox.fieldhelper.FieldHelper;
import ru.kwanza.toolbox.splitter.Splitter;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.*;

/**
 * @author Vasily Vorobyov
 */
public class HttpEventProcessor<R extends Request> implements IEventProcessor<IHttpEvent> {

    public static final Splitter<RESTExchange> EXCHANGE_SPLITTER = new Splitter<RESTExchange>(FieldHelper.<RESTExchange, UserDetails>construct(RESTExchange.class, "user"));
    public static final Splitter<Request> REQUEST_SPLITTER = new Splitter<Request>(FieldHelper.construct(Request.class, "userData"));
    private Logger logger = LoggerFactory.getLogger(HttpEventProcessor.class);

    private IRequestParser<R> requestParser;
    private IRequestProcessor<R> orderProcessor;
    private IReportBuilder reportBuilder;
    @Autowired
    private DemoRegistry demo;
    @Autowired
    private IEntityManager em;
    @Autowired
    private IAutoKey autoKey;

    private Class<R> clazz;

    public HttpEventProcessor(Class<R> clazz) {
        this.clazz = clazz;
    }

    public HttpEventProcessor<R> getInstance() {
        return this;
    }

    public void setRequestParser(IRequestParser<R> requestParser) {
        this.requestParser = requestParser;
    }

    public void setOrderProcessor(IRequestProcessor<R> orderProcessor) {
        this.orderProcessor = orderProcessor;
    }

    public void setReportBuilder(IReportBuilder reportBuilder) {
        this.reportBuilder = reportBuilder;
    }

    public void process(Collection<IHttpEvent> events) {
        Map<Long, RequestID> httpIDByRequestIds = new HashMap<Long, RequestID>(events.size());
        Map<RequestID, IHttpRequest> httpRequestByIds = new HashMap<RequestID, IHttpRequest>(events.size());
        Map<RequestID, RESTExchange> exchageIdByhttpID = new HashMap<RequestID, RESTExchange>(events.size());
        List<R> requestBatch = new ArrayList<R>(events.size());

        for (IHttpEvent event : events) {
            final IHttpRequest httpRequest = event.getHttpRequest();
            final UserDetails userDetails = checkDemoUser(httpRequest);
            if (userDetails != null) {
                final RESTExchange exchange = new RESTExchange(httpRequest, httpRequest.getRemoteAddress());
                exchange.setUser(userDetails);
                exchageIdByhttpID.put(httpRequest.getID(), exchange);
                try {
                    final RequestID httpRequestId = httpRequest.getID();
                    httpRequestByIds.put(httpRequestId, httpRequest);

                    R[] requests = requestParser.parse(exchange.getRequest());
                    for (R req : requests) {
                        httpIDByRequestIds.put(req.getId(), httpRequestId);
                        requestBatch.add(req);
                        req.setUserData(userDetails);
                    }

                } catch (IOException e) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("Request parse error:\nContent:\n{}\nError:\n{}\n",
                                new String(httpRequest.getBody(), Charset.forName("utf-8")),
                                e.getMessage());
                    }
                    final HttpPacket result = HttpUtil.create500(httpRequest, e.getMessage());
                    httpRequest.finish(result);
                    exchange.error(result);
                } catch (Throwable e) {
                    logger.error("Request error:\nContent:\n{}\nError:\n{}\n",
                            new String(httpRequest.getBody(), Charset.forName("utf-8")), e.getMessage());
                    final HttpPacket result = HttpUtil.create500(httpRequest, e.getMessage());
                    httpRequest.finish(result);
                    exchange.error(result);
                }
            }
        }


        final Map<UserDetails, Collection<Request>> requestByUserData = REQUEST_SPLITTER.oneToMany((Collection<Request>) requestBatch);

        try {
            for (Map.Entry<UserDetails, Collection<Request>> entry : requestByUserData.entrySet()) {
                final UserDetails userData = entry.getKey();
                final Collection<Request> requests = entry.getValue();
                if (userData != null) {
                    demo.setCurrentUser(userData);
                    try {
                        processRequests(httpIDByRequestIds, httpRequestByIds, (Collection<R>) requests, exchageIdByhttpID);
                    } finally {
                        demo.setCurrentUser(null);
                    }
                } else {
                    for (Request request : requests) {
                        final RequestID requestID = httpIDByRequestIds.get(request.getId());
                        final IHttpRequest httpRequest = httpRequestByIds.get(requestID);
                        final HttpPacket result = HttpUtil.create500(httpRequest, "Demo user is not found or password is wrong!");
                        exchageIdByhttpID.get(requestID).error(result);
                        httpRequest.finish(result);
                    }
                }
            }
        } finally {
            saveExchanges(exchageIdByhttpID);
        }
    }

    private UserDetails checkDemoUser(IHttpRequest httpRequest) {
        final String user = httpRequest.getHeader("demo-user");
        final String userPassword = httpRequest.getHeader("demo-user-password");
        final UserDetails userDetails = demo.loadUserByUsername(user);
        if (userDetails == null || !userDetails.getPassword().equals(userPassword)) {
            final String message = "User notFound or password is wrong!";
            logger.error(message);
            final HttpPacket result = HttpUtil.createResponse(httpRequest, 403, message);
            httpRequest.finish(result);
        }

        return userDetails;
    }

    private void saveExchanges(Map<RequestID, RESTExchange> exchageIdByhttpID) {
        final Map<UserDetails, Collection<RESTExchange>> exchanges = EXCHANGE_SPLITTER.oneToMany(exchageIdByhttpID.values());

        for (Map.Entry<UserDetails, Collection<RESTExchange>> e : exchanges.entrySet()) {
            demo.setCurrentUser(e.getKey());
            try {
                em.create(RESTExchange.class, e.getValue());
            } catch (UpdateException e1) {
                e1.printStackTrace();
            } finally {

                demo.setCurrentUser(null);
            }
        }
    }
    private void processRequests(Map<Long, RequestID> httpIDByRequestIds, Map<RequestID, IHttpRequest> httpRequestByIds, Collection<R> requestBatch, Map<RequestID, RESTExchange> exchangeMap) {
        if (!requestBatch.isEmpty()) {
            final AutoKeyValueSequence sequence = autoKey.getValueSequence(RESTExchange.class.getName(), exchangeMap.size());
            for (R r : requestBatch) {
                final RequestID requestID = httpIDByRequestIds.get(r.getId());
                final RESTExchange restExchange = exchangeMap.get(requestID);
                restExchange.setId(sequence.next());
                r.setExchangeId(restExchange.getId());
            }


            Map<Long, Report> batchReports = orderProcessor.process(requestBatch);
            Map<RequestID, List<Report>> reportsByHttpIds = new HashMap<RequestID, List<Report>>(httpRequestByIds.size());

            for (Map.Entry<Long, Report> o : batchReports.entrySet()) {
                final RequestID httpRequestId = httpIDByRequestIds.get(o.getKey());
                if (null == httpIDByRequestIds) {
                    logger.error("Skipped unknown requestId={}: {}", o.getKey(), o.getValue());
                    continue;
                }
                List<Report> httpReports = reportsByHttpIds.get(httpRequestId);
                if (null == httpReports) {
                    httpReports = new ArrayList<Report>(32);
                    reportsByHttpIds.put(httpRequestId, httpReports);
                }
                httpReports.add(o.getValue());
            }

            for (Map.Entry<RequestID, List<Report>> reportsByHttpId : reportsByHttpIds.entrySet()) {
                final RequestID key = reportsByHttpId.getKey();
                IHttpRequest httpRequest = httpRequestByIds.get(key);
                HttpPacket httpResponse;
                List<Report> reports = reportsByHttpId.getValue();
                try {
                    final byte[] body = reportBuilder.build(reports.toArray(new Report[reports.size()]));
                    httpResponse = HttpUtil.createResponse(httpRequest, reportBuilder.getMimeType(), reportBuilder.getEncoding(), body);
                    exchangeMap.get(key).success(httpResponse);
                } catch (IOException e) {
                    logger.error("Unable to build json: requestId = " + httpRequest.getID() + ", report = " + reports, e);
                    httpResponse = HttpUtil.create500(httpRequest, "Unable to build json");
                    exchangeMap.get(key).error(httpResponse);
                }
                httpRequest.finish(httpResponse);
            }
        }
    }
}

