package ru.kwanza.billing.clearing;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import ru.kwanza.billing.api.PaymentToolId;
import ru.kwanza.billing.api.Request;
import ru.kwanza.billing.api.Session;
import ru.kwanza.billing.api.SessionId;
import ru.kwanza.billing.api.order.OpenSessionOrder;
import ru.kwanza.billing.api.order.PaymentInstruction;
import ru.kwanza.billing.api.order.PaymentOrder;
import ru.kwanza.billing.api.report.*;
import ru.kwanza.billing.clearing.entity.ClearingAccount;
import ru.kwanza.billing.clearing.entity.ClearingGroup;
import ru.kwanza.billing.clearing.entity.ClearingMember;
import ru.kwanza.billing.client.IBillingClient;
import ru.kwanza.billing.entity.api.AccountRecord;

import java.math.BigDecimal;
import java.util.*;

import static junit.framework.Assert.assertEquals;

/**
 * @author henadiy
 */
@RunWith(JUnit4.class)
public class ClearingCalculatorTest {

	public static final BigDecimal ACC1 = new BigDecimal("1");
	public static final BigDecimal ACC2 = new BigDecimal("2");
	public static final BigDecimal ACC3 = new BigDecimal("3");
	public static final BigDecimal ACC4 = new BigDecimal("4");
	public static final BigDecimal ACC5 = new BigDecimal("5");
	public static final BigDecimal ACC6 = new BigDecimal("6");


	class TestBillingClient implements IBillingClient {

		private long reqId = 0;
		private Map<BigDecimal, Long> amounts = new HashMap<BigDecimal, Long>();

		public Long generateRequestId() {
			return ++reqId;
		}

		public List<Long> generateRequestIds(int count) {
			List<Long> res = new ArrayList<Long>(count);
			for(int i = 0; i < count; i++) {
				res.add(++reqId);
			}
			return res;
		}

		public List<Report> processRequests(List<Request> requests) {
			Date processedAt = new Date();
			List<Report> res = new ArrayList<Report>(requests.size());
			for(Request r : requests) {
				if (r instanceof OpenSessionOrder) {
					if ("valid".equals(((OpenSessionOrder)r).getArguments().get("username"))) {
						res.add(new SessionReport(r.getId(), processedAt, new Session(new SessionId("SID"),
                                new Date(), (String)((OpenSessionOrder)r).getArguments().get("username"),
                                Collections.EMPTY_SET)));
					} else {
						res.add(new RejectedRequestReport(r.getId(), processedAt, RejectCode.LACKS_OF_AUTHORITY));
					}
				} else if (r instanceof PaymentOrder) {
					for(PaymentInstruction pi : ((PaymentOrder) r).getPaymentInstructions()) {
						if (!PaymentToolId.BLANK.equals(pi.getTargetAccountId())) {
							Long amount = amounts.get(pi.getTargetAccountId().getId());
							amounts.put(pi.getTargetAccountId().getId(),
									(null == amount ? 0L : amount) + pi.getTargetAmount());
						} else if (!PaymentToolId.BLANK.equals(pi.getSourceAccountId())) {
							Long amount = amounts.get(pi.getSourceAccountId().getId());
							amounts.put(pi.getSourceAccountId().getId(),
									(null == amount ? 0L : amount) - pi.getSourceAmount());
						}
					}
					res.add(new PaymentOrderReport(r.getId(), processedAt));
				} else {
					res.add(new RejectedRequestReport(r.getId(), processedAt, RejectCode.LACKS_OF_AUTHORITY));
				}
			}
			return res;
		}

		public Map<BigDecimal, Long> getAmounts() {
			return amounts;
		}
	}

	class TestClearingAccount extends ClearingAccount {
		private ClearingMember member;

		public TestClearingAccount(BigDecimal accountId, ClearingMember member) {
			super(accountId, member.getId(), null);
			this.member= member;
		}

		@Override
		public ClearingMember getClearingMember() {
			return member;
		}
	}

	@Test
	public void testCalculateOneMember() throws Exception {

        Date dt1 = new Date(System.currentTimeMillis() - 10000);
        Date dt2 = new Date(System.currentTimeMillis());

		ClearingMember member = new ClearingMember(1L, 1L, ACC5, "");

		MockClearingDAO dao = new MockClearingDAO();
		TestBillingClient client = new TestBillingClient();

		Map<String, Object> sessionArgs = new HashMap<String, Object>();
		sessionArgs.put("username", "valid");

		ClearingCalculator calc = new ClearingCalculator();
		calc.setClearingDAO(dao);
		calc.setBillingClient(client);
		calc.setSessionArguments(sessionArgs);


        dao.addAccountRecords(
                new AccountRecord(BigDecimal.valueOf(1), 1L,
                        ACC1, 1, 100L, 1000L, ACC2, 1, 100L, 0L, dt1, null),
                new AccountRecord(BigDecimal.valueOf(2), 1L,
                        ACC2, 2, 15L, 100L, ACC3, 5, 15L, 0L, dt1, null)
        );

		dao.createClearingAccounts(
                Arrays.asList((ClearingAccount)new TestClearingAccount(ACC1, member), new TestClearingAccount(ACC3, member)));

        assertEquals(null, dao.getClearingAccounts().get(ACC1).getLastClearedRecordNumber());
        assertEquals(null, dao.getClearingAccounts().get(ACC3).getLastClearedRecordNumber());
		assertEquals(0, client.getAmounts().size());
		calc.calculate(new ClearingGroup(1L, 1, 0, new Date()));

		assertEquals(1, client.getAmounts().size());
		assertEquals(Long.valueOf(85), client.getAmounts().get(ACC5));

        Thread.sleep(1000);

        assertEquals(Integer.valueOf(1), dao.getClearingAccounts().get(ACC1).getLastClearedRecordNumber());
        assertEquals(Integer.valueOf(5), dao.getClearingAccounts().get(ACC3).getLastClearedRecordNumber());

        dao.addAccountRecords(
                new AccountRecord(BigDecimal.valueOf(3), 1L,
                        ACC2, 3, 2L, 85L, ACC3, 6, 2L, 15L, dt2, null),
                new AccountRecord(BigDecimal.valueOf(4), 1L,
                        ACC2, 4, 3L, 83L, ACC1, 2, 3L, 900L, dt2, null)
        );

        calc.calculate(new ClearingGroup(1L, 1, 0, new Date()));
        assertEquals(1, client.getAmounts().size());
        assertEquals(Long.valueOf(80), client.getAmounts().get(ACC5));

        assertEquals(Integer.valueOf(2), dao.getClearingAccounts().get(ACC1).getLastClearedRecordNumber());
        assertEquals(Integer.valueOf(6), dao.getClearingAccounts().get(ACC3).getLastClearedRecordNumber());
	}
}
