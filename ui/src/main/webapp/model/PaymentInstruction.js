/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.model.PaymentInstruction', {
    extend: 'Ext.data.Model',
    fields: [
        'id',
        'orderId',
        'indexInOrder',
        'sourceAccountId',
        'sourceWalletAccountId',
        'sourceAmount',
        'targetAccountId',
        'targetWalletAccountId',
        'targetAmount',
        'recordedAt',
        'description'
    ],
    permissions: {
        read: "PaymentInstruction:read",
        create: false,
        update: false,
        destroy: false
    },

    proxy: {
        type: 'ajax',
        apiUrl: 'billing-controller/paymentinstruction/'

    }
});