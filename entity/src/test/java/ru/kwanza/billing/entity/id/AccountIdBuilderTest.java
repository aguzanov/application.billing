package ru.kwanza.billing.entity.id;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import ru.kwanza.billing.entity.api.id.AccountIdBuilder;

import java.math.BigDecimal;
import java.util.Calendar;

import static org.junit.Assert.assertEquals;

/**
 * @author henadiy
 */
@RunWith(JUnit4.class)
public class AccountIdBuilderTest {

    @Test
    public void testPack() throws Exception {

        Calendar cal = Calendar.getInstance();

        AccountIdBuilder b = new AccountIdBuilder(1, 2, 3);
        assertEquals(Integer.valueOf(1), b.getBin());
        assertEquals(Integer.valueOf(2), b.getCurrencyId());
        assertEquals(new BigDecimal(String.format("0000010002%02d%02d00000000030",
                cal.get(Calendar.YEAR) % 100, cal.get(Calendar.WEEK_OF_YEAR))), b.getId());

    }

    @Test
    public void testUnpack() throws Exception {

        AccountIdBuilder b = new AccountIdBuilder(new BigDecimal("10002143200000000030"));
        assertEquals(new BigDecimal("10002143200000000030"), b.getId());
        assertEquals(Integer.valueOf(1), b.getBin());
        assertEquals(Integer.valueOf(2), b.getCurrencyId());

    }
}
