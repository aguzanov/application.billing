package ru.kwanza.billing.tariffication;

/**
 * @author henadiy
 */
public enum ResultCode {

    SUCCESS, SUCCESS_WITH_CORRECTIONS;
}
