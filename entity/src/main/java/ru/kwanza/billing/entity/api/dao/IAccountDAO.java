package ru.kwanza.billing.entity.api.dao;

import ru.kwanza.billing.entity.api.Account;
import ru.kwanza.billing.entity.api.Currency;
import ru.kwanza.billing.entity.api.Issuer;
import ru.kwanza.billing.entity.api.Wallet;
import ru.kwanza.dbtool.core.UpdateException;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Vasily Vorobyov
 */
public interface IAccountDAO {

    List<Account> readAll();

    Boolean isExistAccountInIdLimits(BigDecimal lowerLimit, BigDecimal upperLimit);

    Map<BigDecimal, Account> readByKeys(Collection<BigDecimal> ids);

    Map<Long, Account> readByOrderIds(Collection<Long> orderIds);

    Account prepare(Date createdAt, Issuer issuer, Currency currency, Integer accountCounter, Long orderId, Long minBalance, Long maxBalance);

    Collection<Account> create(Collection<Account> accounts) throws UpdateException;

    Collection<Account> update(Collection<Account> accounts) throws UpdateException;

}
