package ru.kwanza.billing.processor.order;

import ru.kwanza.autokey.api.AutoKeyValueSequence;
import ru.kwanza.billing.api.AccountStatus;
import ru.kwanza.billing.api.PaymentToolId;
import ru.kwanza.billing.api.order.ConversionInstruction;
import ru.kwanza.billing.api.order.PaymentInstruction;
import ru.kwanza.billing.api.order.PaymentOrder;
import ru.kwanza.billing.api.report.*;
import ru.kwanza.billing.entity.api.*;
import ru.kwanza.billing.api.report.Report;
import ru.kwanza.billing.entity.api.RejectedPaymentInstruction;
import ru.kwanza.billing.entity.api.dao.*;
import ru.kwanza.billing.processor.RequestProcessor;
import ru.kwanza.billing.processor.signer.Signer;
import ru.kwanza.dbtool.core.UpdateException;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author Vasily Vorobyov
 */
public class PaymentOrderProcessor extends RequestProcessor<PaymentOrder> {

    @Resource(name = "billing.entity.dao.WalletAccountDAO")
    private IWalletAccountDAO walletAccountDAO;
    @Resource(name = "billing.entity.dao.AccountDAO")
    private IAccountDAO accountDAO;
    @Resource(name = "billing.entity.dao.PaymentInstructionDAO")
    private IPaymentInstructionDAO paymentInstructionDAO;
    @Resource(name = "billing.processor.signer.ConversionInstructionSigner")
    private Signer<ConversionInstruction> conversionInstructionSigner;

    @Override
    protected Map<Long, Report> handleRequests(Date processedAt, Collection<PaymentOrder> requests) {

        long ti = System.currentTimeMillis(), t0, t1, t2, t3, t4, t5, t6, t7, t;

        Collection<PaymentOrder> acceptedOrderRequests = new ArrayList<PaymentOrder>(requests);
        Map<Long, Report> orderReports = new HashMap<Long, Report>(acceptedOrderRequests.size());

        Set<BigDecimal> accountIds = new HashSet<BigDecimal>();
        Set<BigDecimal> walletAccountIds = new HashSet<BigDecimal>();

        // registered incoming payment instructions
        Map<Long, List<ru.kwanza.billing.entity.api.PaymentInstruction>> registeredPaymentInstructions =
                registerPaymentInstructions(processedAt, requests);

        t0 = System.currentTimeMillis();

        // Determination of the numbers of the required accounts, wallets and currencies. Filtering empty orders.
        Iterator<PaymentOrder> orderIterator = acceptedOrderRequests.iterator();
        while (orderIterator.hasNext()) {
            PaymentOrder order = orderIterator.next();
            if (null == order.getPaymentInstructions() || 0 == order.getPaymentInstructions().length) {
                orderReports.put(order.getId(),
                        new RejectedPaymentOrderReport(order.getId(), processedAt, RejectCode.NO_PAYMENT_INSTRUCTIONS, null));
                orderIterator.remove();
            } else {
                // verification of payment instructions
                RejectedPaymentOrderReport rejected = null;
                int posInOrder = 0;
                for (PaymentInstruction pi : order.getPaymentInstructions()) {
                    if ((null == pi.getSourceAccountId() || PaymentToolId.BLANK.equals(pi.getSourceAccountId())) &&
                            (null == pi.getTargetAccountId() || PaymentToolId.BLANK.equals(pi.getTargetAccountId()))) {
                        rejected = new RejectedPaymentOrderReport(order.getId(), processedAt,
                                        RejectCode.NO_ACCOUNTS, posInOrder);
                    } else if (null == pi.getSourceAmount() || pi.getSourceAmount() <= 0 ||
                            null == pi.getTargetAmount() || pi.getTargetAmount() <= 0 ) {
                        rejected = new RejectedPaymentOrderReport(order.getId(), processedAt,
                                RejectCode.ILLEGAL_AMOUNT, posInOrder);
                    } else if (pi instanceof ConversionInstruction) {
                        if (!conversionInstructionSigner.check((ConversionInstruction)pi)) {
                            rejected = new RejectedPaymentOrderReport(order.getId(), processedAt,
                                    RejectCode.ILLEGAL_SIGNATURE, posInOrder);
                        } else if (processedAt.compareTo(((ConversionInstruction)pi).getIssuedAt()) < 0 ||
                                processedAt.compareTo(((ConversionInstruction)pi).getExpiresIn()) > 0)  {
                            rejected = new RejectedPaymentOrderReport(order.getId(), processedAt,
                                    RejectCode.EXPIRED, posInOrder);
                        }
                    }

                    // skip the order if it contains the rejected instructions
                    if (null != rejected) {
                        orderReports.put(order.getId(), rejected);
                        orderIterator.remove();
                        break;
                    }

                    // collect the identifiers of means of payment
                    for(PaymentToolId paymentToolId : new PaymentToolId[]{pi.getSourceAccountId(), pi.getTargetAccountId()}) {
                        if (PaymentToolId.Type.ACCOUNT.equals(paymentToolId.getType())) {
                            accountIds.add(paymentToolId.getId());
                        } else if (PaymentToolId.Type.WALLET_ACCOUNT.equals(paymentToolId.getType())) {
                            walletAccountIds.add(paymentToolId.getId());
                        }
                    }

                    // increase the number of instructions in the order
                    posInOrder++;
                }
            }
        }

        t1 = System.currentTimeMillis();

        // The request of the required accounts of purses from the store
        Map<BigDecimal, WalletAccount> walletAccounts = walletAccountDAO.readByKeys(walletAccountIds);

        t2 = System.currentTimeMillis();

        // The definition of account numbers for PI accounts on wallet. Filtering missing.
        orderIterator = acceptedOrderRequests.iterator();
        while (orderIterator.hasNext()) {
            PaymentOrder order = orderIterator.next();
            int posInOrder = 0;
            for (PaymentInstruction pi : order.getPaymentInstructions()) {
                RejectedPaymentOrderReport rejected = null;

                // collect account numbers
                BigDecimal[] piAccountIds = new BigDecimal[]{null, null};
                PaymentToolId[] ids = new PaymentToolId[]{pi.getSourceAccountId(), pi.getTargetAccountId()};
                for(int i =0; i < ids.length; i++) {
                    if (PaymentToolId.Type.ACCOUNT.equals(ids[i].getType())) {
                        piAccountIds[i] = ids[i].getId();
                    } else if (PaymentToolId.Type.WALLET_ACCOUNT.equals(ids[i].getType())) {
                        WalletAccount a = walletAccounts.get(ids[i].getId());
                        piAccountIds[i] = null == a ? null : a.getAccountId();
                    }
                }

                // the rejected instructions if one of the accounts of the purse is wrong
                if (!PaymentToolId.BLANK.equals(pi.getSourceAccountId()) && null == piAccountIds[0] ||
                        !PaymentToolId.BLANK.equals(pi.getTargetAccountId()) && null == piAccountIds[1]) {
                    rejected = new RejectedPaymentOrderReport(order.getId(), processedAt,
                            RejectCode.WALLET_NOT_FOUND, posInOrder);
                } else {
                    if (null != piAccountIds[0]) accountIds.add(piAccountIds[0]);
                    if (null != piAccountIds[1]) accountIds.add(piAccountIds[1]);
                }

                // rejected raspolaganje containing the rejected instructions
                if (null != rejected) {
                    orderReports.put(order.getId(), rejected);
                    orderIterator.remove();
                    break;
                }

                // counter instructions
                posInOrder++;
            }
        }

        t3 = System.currentTimeMillis();

        // The request of the required accounts from the store
        Map<BigDecimal, Account> accounts = accountDAO.readByKeys(accountIds);

        t4 = System.currentTimeMillis();

        // Here we will aggregate the account balances that result after execution
        Map<BigDecimal, Long> accountSummaryAmounts = new HashMap<BigDecimal, Long>(accounts.size());

        // Filtering account balances unchanged balances, at the same time we count the number of write-offs and enrollments
        int instructionsCounter = 0;
        orderIterator = acceptedOrderRequests.iterator();
        while (orderIterator.hasNext()) {
            PaymentOrder order = orderIterator.next();
            RejectCode rejectCode = null;
            int posInOrder = 0;
            for (PaymentInstruction pi : order.getPaymentInstructions()) {

                // prepare paired data instructions
                PaymentToolId[] piIds = new PaymentToolId[]{pi.getSourceAccountId(), pi.getTargetAccountId()};
                Account[] piAccounts = new Account[]{
                        searchAccount(walletAccounts, accounts, pi.getSourceAccountId()),
                        searchAccount(walletAccounts, accounts, pi.getTargetAccountId())};
                Long[] piAmounts = new Long[]{-1L* pi.getSourceAmount(), pi.getTargetAmount()};
                Long[] piBalances = new Long[]{null, null};

                if (null != piAccounts[0] && piAccounts[0].equals(piAccounts[1])) {
                    rejectCode = RejectCode.IDENTICAL_ACCOUNTS;
                } else {
                    for(int i = 0; i < piIds.length; i++) {
                        if (PaymentToolId.BLANK.equals(piIds[i])) {
                            // skip empty accounts
                            continue;
                        } else if (null == piAccounts[i]) {
                            rejectCode = RejectCode.ACCOUNT_NOT_FOUND;
                        } else {
                            if (!AccountStatus.ACTIVE.equals(piAccounts[i].getStatus())) {
                                rejectCode = RejectCode.ACCOUNT_SUSPENDED;
                            } else {
                                Long accountBalance = accountSummaryAmounts.get(piAccounts[i].getId());
                                if (null == accountBalance) {
                                    accountBalance = piAccounts[i].getBalance();
                                }
                                final long newBalance = accountBalance + piAmounts[i];
                                if (null != piAccounts[i].getMinBalance() && newBalance < piAccounts[i].getMinBalance()) {
                                    rejectCode = RejectCode.MIN_BALANCE_EXCEEDED;
                                } else if (null != piAccounts[i].getMaxBalance() && newBalance > piAccounts[i].getMaxBalance()) {
                                    rejectCode = RejectCode.MAX_BALANCE_EXCEEDED;
                                } else {
                                    // maintain the instantaneous balance
                                    piBalances[i] = newBalance;
                                }
                            }
                        }
                    }
                }

                // rejected the order containing the rejected instructions
                if (null != rejectCode) {
                    orderReports.put(order.getId(), new RejectedPaymentOrderReport(
                            order.getId(), processedAt, rejectCode, posInOrder));
                    orderIterator.remove();
                    break;
                }

                // updated estimates on the balances
                for(int i = 0; i < piIds.length; i++) {
                    if (null != piAccounts[i] && null != piBalances[i]) {
                        accountSummaryAmounts.put(piAccounts[i].getId(), piBalances[i]);
                    }
                }

                // count the number of instructions
                instructionsCounter++;
                // the number of instructions in the order
                posInOrder++;
            }
        }

        t5 = System.currentTimeMillis();

        // There remained only the orders that can be fulfilled here and perform
        ArrayList<AccountRecord> accountRecords = new ArrayList<AccountRecord>(instructionsCounter);
        AutoKeyValueSequence recordSeq = autoKey.getValueSequence(AccountRecord.class.getName(), instructionsCounter);

        Set<Account> updatedAccounts = new HashSet<Account>(instructionsCounter*2);
        Set<RejectedPaymentInstruction> rejectedPaymentInstructions =
                new HashSet<RejectedPaymentInstruction>(orderReports.size());

        orderIterator = requests.iterator();
        while (orderIterator.hasNext()) {
            PaymentOrder order = orderIterator.next();

            // prepared rejected instructions to register
            if (orderReports.get(order.getId()) instanceof RejectedPaymentOrderReport) {
                int piIndex = ((RejectedPaymentOrderReport)orderReports.get(order.getId())).getPaymentInstructionIndex();
                PaymentInstruction pi = order.getPaymentInstructions()[piIndex];
                Account sourceAccount = searchAccount(walletAccounts, accounts, pi.getSourceAccountId());
                Account targetAccount = searchAccount(walletAccounts, accounts, pi.getTargetAccountId());

                rejectedPaymentInstructions.add(paymentInstructionDAO.prepareRejectedPaymentInstruction(
                        registeredPaymentInstructions.get(order.getId()).get(piIndex).getId(),
                        null != sourceAccount ? sourceAccount.getBalance() : null,
                        null != targetAccount ? targetAccount.getBalance() : null));

                continue;
            }

            // we prepare the accepted instructions to the registration
            for (PaymentInstruction pi : order.getPaymentInstructions()) {
                Account sourceAccount = searchAccount(walletAccounts, accounts, pi.getSourceAccountId());
                Account targetAccount = searchAccount(walletAccounts, accounts, pi.getTargetAccountId());

                final AccountRecord record = paymentInstructionDAO.prepareAccountRecord(
                        BigDecimal.valueOf(recordSeq.next()), order.getId(),
                        null == sourceAccount ? null : sourceAccount.getId(),
                        null == sourceAccount ? null : sourceAccount.nextRecordNumber(),
                        pi.getSourceAmount(),
                        null == sourceAccount ? null : sourceAccount.getBalance(),
                        null == targetAccount ? null : targetAccount.getId(),
                        null == targetAccount ? null : targetAccount.nextRecordNumber(),
                        pi.getTargetAmount(),
                        null == targetAccount ? null : targetAccount.getBalance(),
                        processedAt, order.getDescription()
                );

                accountRecords.add(record);
                if (null != sourceAccount) {
                    sourceAccount.setBalance(sourceAccount.getBalance() - pi.getSourceAmount());
                    sourceAccount.setLastRecordAmount(-1L * pi.getSourceAmount());
                    sourceAccount.setLastRecordAt(processedAt);
                    sourceAccount.setLastRecordId(record.getId());
                    updatedAccounts.add(sourceAccount);
                }
                if (null != targetAccount) {
                    targetAccount.setBalance(targetAccount.getBalance() + pi.getTargetAmount());
                    targetAccount.setLastRecordAmount(pi.getTargetAmount());
                    targetAccount.setLastRecordAt(processedAt);
                    targetAccount.setLastRecordId(record.getId());
                    updatedAccounts.add(targetAccount);
                }
            }
            orderReports.put(order.getId(), new PaymentOrderReport(order.getId(), processedAt));
        }

        t6 = System.currentTimeMillis();

        // Fix change in stocks and instructions in the repository
        try {
            accountDAO.update(updatedAccounts);
        } catch (UpdateException e) {
            throw new RuntimeException("Unable to update accounts", e);
        }

        t7 = System.currentTimeMillis();

        try {
            paymentInstructionDAO.createAccountRecords(accountRecords);
        } catch (UpdateException e) {
            System.out.println("Problems here:");
            for(AccountRecord accountRecord : accountRecords){
                System.out.println(accountRecord);
            }
            throw new RuntimeException("Unable to create account records", e);
        }

        try {
            paymentInstructionDAO.createRejectedPaymentInstructions(rejectedPaymentInstructions);
        } catch (UpdateException e) {
            throw new RuntimeException("Unable to create rejected payment instructions", e);
        }

        t = System.currentTimeMillis();

        System.out.format("payment: registerPaymentInstructions = %dms, collectWalletAccountIds = %dms, readWalletAccounts = %dms, resolveAccountIds = %dms, readAccounts = %dms, checkBalances = %dms, prepareRecords = %dms, updateAccounts = %dms, createRecords = %dms\n",
                t0 - ti, t1 - t0, t2 - t1, t3 - t2, t4 - t3, t5 - t4, t6 - t5, t7 - t6, t - t7);

        return orderReports;
    }

    private Account searchAccount(Map<BigDecimal, WalletAccount> walletAccounts, Map<BigDecimal, Account> accounts, PaymentToolId id) {
        BigDecimal accountId = null;
        if (PaymentToolId.Type.ACCOUNT.equals(id.getType())) {
            accountId = id.getId();
        } else if (PaymentToolId.Type.WALLET_ACCOUNT.equals(id.getType())) {
            WalletAccount walletAccount = walletAccounts.get(id.getId());
            accountId = walletAccount.getAccountId();
        }
        return accounts.get(accountId);
    }

    private Map<Long, List<ru.kwanza.billing.entity.api.PaymentInstruction>> registerPaymentInstructions(
            Date processedAt, Collection<PaymentOrder> requests) {

        List<ru.kwanza.billing.entity.api.PaymentInstruction> piToRegister =
                new LinkedList<ru.kwanza.billing.entity.api.PaymentInstruction>();
        Map<Long, Set<Integer>> conversionIndexes = new HashMap<Long, Set<Integer>>();

        for(PaymentOrder order : requests) {
            // prepare IDs
            AutoKeyValueSequence piSeq = autoKey.getValueSequence(
                    ru.kwanza.billing.entity.api.PaymentInstruction.class.getName(),
                            order.getPaymentInstructions().length);

            // training records
            int indexInOrder = 0;
            for(PaymentInstruction pi : order.getPaymentInstructions()) {
                PaymentToolId srcAccId = pi.getSourceAccountId(), targAccId = pi.getTargetAccountId();
                piToRegister.add(paymentInstructionDAO.preparePaymentInstruction(
                        BigDecimal.valueOf(piSeq.next()), order.getId(), indexInOrder,
                        null == srcAccId || PaymentToolId.BLANK.equals(srcAccId) || PaymentToolId.Type.WALLET_ACCOUNT.equals(srcAccId.getType()) ?
                                null : pi.getSourceAccountId().getId(),
                        null == srcAccId || PaymentToolId.BLANK.equals(srcAccId) || PaymentToolId.Type.ACCOUNT.equals(srcAccId.getType()) ?
                                null : pi.getSourceAccountId().getId(),
                        pi.getSourceAmount(),
                        null == targAccId || PaymentToolId.BLANK.equals(targAccId) || PaymentToolId.Type.WALLET_ACCOUNT.equals(targAccId.getType()) ?
                                null : pi.getTargetAccountId().getId(),
                        null == targAccId || PaymentToolId.BLANK.equals(targAccId) || PaymentToolId.Type.ACCOUNT.equals(targAccId.getType()) ?
                                null : pi.getTargetAccountId().getId(),
                        pi.getTargetAmount(), processedAt, null
                ));

                // remember the indexes of conversion operations
                if (pi instanceof ConversionInstruction) {
                    if (!conversionIndexes.containsKey(order.getId())) {
                        conversionIndexes.put(order.getId(), new HashSet<Integer>());
                    }
                    conversionIndexes.get(order.getId()).add(indexInOrder);
                }

                // nidex instructions available
                indexInOrder++;
            }

        }

        // registration payment instructions
        Map<Long, List<ru.kwanza.billing.entity.api.PaymentInstruction>> result =
                new HashMap<Long, List<ru.kwanza.billing.entity.api.PaymentInstruction>>(requests.size());
        try {
            // registered operations
            List<ru.kwanza.billing.entity.api.PaymentInstruction> all =
                new ArrayList<ru.kwanza.billing.entity.api.PaymentInstruction>(
                    paymentInstructionDAO.createPaymentInstructions(piToRegister));

            // sort transactions by number in the order
            Collections.sort(all, new Comparator<ru.kwanza.billing.entity.api.PaymentInstruction>() {
                public int compare(ru.kwanza.billing.entity.api.PaymentInstruction o1, ru.kwanza.billing.entity.api.PaymentInstruction o2) {
                    return o1.getIndexInOrder().compareTo(o2.getIndexInOrder());
                }
            });

            // scatter on orders
            for(ru.kwanza.billing.entity.api.PaymentInstruction pi : all) {
                if (!result.containsKey(pi.getOrderId())) {
                    result.put(pi.getOrderId(),
                            new LinkedList<ru.kwanza.billing.entity.api.PaymentInstruction>());
                }
                result.get(pi.getOrderId()).add(pi);
            }
        } catch (UpdateException e) {
            throw new RuntimeException("Unable to register payment instructions", e);
        }

        if (!conversionIndexes.isEmpty()) {

            List<ru.kwanza.billing.entity.api.ConversionInstruction> ciToRegister = new
                    LinkedList<ru.kwanza.billing.entity.api.ConversionInstruction>();

            // preparation conversioni operations
            for(PaymentOrder order : requests) {
                // skipped if the order does not contain the conversion
                if (!conversionIndexes.containsKey(order.getId())) {
                    continue;
                }

                // looking for instructions on indexes and prepare for registration
                for(Integer indexInOrder : conversionIndexes.get(order.getId())) {
                    ConversionInstruction ci = (ConversionInstruction)order.getPaymentInstructions()[indexInOrder];
                    ciToRegister.add(paymentInstructionDAO.prepareConversionInstruction(
                            result.get(order.getId()).get(indexInOrder).getId(),
                            ci.getIssuedAt(), ci.getExpiresIn(), ci.getSourceCurrencyId(), ci.getSourceScale(),
                            ci.getTargetCurrencyId(), ci.getTargetScale()));
                }
            }

            // registered orders
            try {
                paymentInstructionDAO.createConversionInstructions(ciToRegister);
            } catch (UpdateException e) {
                throw new RuntimeException("Unable to register conversion instructions", e);
            }
        }

        return result;
    }

    @Override
    protected Map<Long, Report> answerDuplicateRequests(Map<Long, PaymentOrder> duplicateRequests) {

        return Collections.emptyMap();
    }
}
