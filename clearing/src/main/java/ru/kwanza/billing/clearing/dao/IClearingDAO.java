package ru.kwanza.billing.clearing.dao;

import ru.kwanza.billing.clearing.entity.ClearingAccount;
import ru.kwanza.billing.clearing.entity.ClearingGroup;
import ru.kwanza.billing.entity.api.AccountRecord;
import ru.kwanza.dbtool.core.UpdateException;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

/**
 * @author henadiy
 */
public interface IClearingDAO {


    Collection<ClearingAccount> createClearingAccounts(Collection<ClearingAccount> clearingAccounts)
            throws UpdateException;

    Collection<ClearingAccount> updateClearingAccounts(Collection<ClearingAccount> clearingAccounts)
            throws UpdateException;

    Collection<ClearingAccount> readClearingGroupAccounts(ClearingGroup clearingGroup);

	Collection<AccountRecord> readAccountRecordsByClearedRecordNumberAndPeriod(
			BigDecimal accountId, Integer recordNumber, Date to);


    Collection<ClearingGroup> readClearingGroups();

    Collection<ClearingGroup> createClearingGroups(Collection<ClearingGroup> clearingGroups) throws UpdateException;

    Collection<ClearingGroup> updateClearingGroups(Collection<ClearingGroup> clearingGroups) throws UpdateException;

}
