package ru.kwanza.billing.controller.valueobjects;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Dmitry Zhukov
 */
public class WalletAccountVO {

    private BigDecimal id;

    private Long version;

    private BigDecimal accountId;

    private Long walletId;

    private Date createdAt;

    private Long orderId;

    private String title;

    public WalletAccountVO() {
    }

    public WalletAccountVO(BigDecimal id, Long version, BigDecimal accountId, Long walletId, Date createdAt, Long orderId, String title) {
        this.id = id;
        this.version = version;
        this.accountId = accountId;
        this.walletId = walletId;
        this.createdAt = createdAt;
        this.orderId = orderId;
        this.title = title;
    }

    public BigDecimal getId() {
        return id;
    }

    public Long getVersion() {
        return version;
    }

    public BigDecimal getAccountId() {
        return accountId;
    }

    public Long getWalletId() {
        return walletId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Long getOrderId() {
        return orderId;
    }

    public String getTitle() {
        return title;
    }
}
