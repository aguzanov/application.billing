package ru.kwanza.billing.api.report;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.Date;

/**
 * @author Dmitry Zhukov
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = AccountReport.class),
        @JsonSubTypes.Type(value = WalletReport.class),
        @JsonSubTypes.Type(value = WalletAccountReport.class),
        @JsonSubTypes.Type(value = PaymentOrderReport.class),
        @JsonSubTypes.Type(value = RejectedRequestReport.class),
        @JsonSubTypes.Type(value = RejectedPaymentOrderReport.class),
        @JsonSubTypes.Type(value = AccountStatementReport.class),
        @JsonSubTypes.Type(value = CurrenciesReport.class),
        @JsonSubTypes.Type(value = IssuerReport.class),
        @JsonSubTypes.Type(value = ConversionInstructionReport.class),
		@JsonSubTypes.Type(value = SessionReport.class),
        @JsonSubTypes.Type(value = WalletChangeStatusReport.class),
        @JsonSubTypes.Type(value = PaymentToolPermissionsReport.class)
})
public abstract class Report {

    private Long id;
    private Date processedAt;

    protected Report() {
    }

    public Report(Long id, Date processedAt) {
        this.id = id;
        this.processedAt = processedAt;
    }

    public Long getId() {
        return id;
    }

    public Date getProcessedAt() {
        return processedAt;
    }

}
