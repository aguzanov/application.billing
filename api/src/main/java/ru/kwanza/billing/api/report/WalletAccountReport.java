package ru.kwanza.billing.api.report;

import com.fasterxml.jackson.annotation.JsonTypeName;
import ru.kwanza.billing.api.order.Order;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

/**
 * @author Vasily Vorobyov
 */
@JsonTypeName(value = "WALLET_ACCOUNT_REPORT")
public class WalletAccountReport extends Report {

    private BigDecimal walletAccountId;
    private Long walletId;
    private BigDecimal accountId;
    private Integer currencyId;
    private Date createdAt;
    private Long orderId;

    protected WalletAccountReport() {
    }

    public WalletAccountReport(Order order, Date processedAt, BigDecimal walletAccountId, Long walletId, BigDecimal accountId,
                               Integer currencyId, Date createdAt, Long orderId) {
        super(order.getId(), processedAt);
        this.walletAccountId = walletAccountId;
        this.walletId = walletId;
        this.accountId = accountId;
        this.currencyId = currencyId;
        this.createdAt = createdAt;
        this.orderId = orderId;
    }

    public BigDecimal getWalletAccountId() {
        return walletAccountId;
    }

    public Long getWalletId() {
        return walletId;
    }

    public BigDecimal getAccountId() {
        return accountId;
    }

    public Integer getCurrencyId() {
        return currencyId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Long getOrderId() {
        return orderId;
    }

}
