package ru.kwanza.billing.entity.impl;

import ru.kwanza.billing.api.order.WalletIssueOrder;

import java.util.Date;

/**
 * @author Vasily Vorobyov
 */
public class WalletIssueOrderRegistrator extends RequestRegistrator<
        WalletIssueOrder, ru.kwanza.billing.entity.api.WalletIssueOrder> {

    public WalletIssueOrderRegistrator() {
        super(ru.kwanza.billing.entity.api.WalletIssueOrder.class);
    }

    @Override
    protected ru.kwanza.billing.entity.api.WalletIssueOrder createEntity(WalletIssueOrder apiOrder, Date processedAt, Long sessionRecordId) {
        return new ru.kwanza.billing.entity.api.WalletIssueOrder(apiOrder.getId(), apiOrder.getExchangeId(), sessionRecordId, processedAt);
    }

    @Override
    protected Long getEntityId(ru.kwanza.billing.entity.api.WalletIssueOrder entity) {
        return entity.getId();
    }
}
