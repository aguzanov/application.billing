/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.model.PaymentOrder', {
    extend: 'Billing.model.Order',
    requires: [
        'Billing.model.PaymentInstruction',
        'Billing.model.AccountRecord'
    ],
    fields: [
        'instructionCount',
        'description'
    ],

    detailView: 'Billing.view.order.paymentorder.PaymentOrderCard',

    permissions: {
        read: "PaymentOrder:read",
        create: false,
        update: false,
        destroy: false
    },

    proxy: {
        type: 'ajax',
        apiUrl: 'billing-controller/payments/'

    },
    hasMany: [
        {
            model: 'Billing.model.PaymentInstruction',
            foreignKey: 'orderId',
            name: 'paymentInstructions',
            storeConfig: {
                sortOnLoad: true,
                sorters: [
                    {
                        sorterFn: function (o1, o2) {
                            return o1.get('indexInOrder') - o2.get('indexInOrder');
                        }
                    }
                ]
            }
        },
        {
            model: 'Billing.model.AccountRecord',
            foreignKey: 'orderId',
            name: 'accountRecords',
            storeConfig: {
                sortOnLoad: true,
                sorters: [
                    {
                        sorterFn: function (o1, o2) {
                            return o1.get('id') - o2.get('id');
                        }
                    }
                ]
            }
        }
    ]
});