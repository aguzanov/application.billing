package ru.kwanza.billing.processor;

import ru.kwanza.billing.api.Request;
import ru.kwanza.billing.api.Session;
import ru.kwanza.billing.api.SessionId;
import ru.kwanza.billing.api.report.RejectCode;
import ru.kwanza.billing.api.report.RejectedRequestReport;

import java.util.*;

/**
 * @author henadiy
 */
public class GeneralCompetenceFilter<R extends Request> extends AccessMatrixCompetenceFilter<R> {

    public Map<Long, RejectedRequestReport> filter(Date processedAt, Collection<R> requests) {

        // the results of the access check
        Map<Long, RejectedRequestReport> res = new HashMap<Long, RejectedRequestReport>(requests.size());

        // get the list of sessions
        Map<SessionId, Session> sessions = getSessions(requests);

        Iterator<R> it = requests.iterator();
        while (it.hasNext()) {
            R req = it.next();

            // access checking
            Session session = sessions.get(req.getSessionId());
            if (!isSessionValid(session, processedAt) ||
                    !getAccessMatrix().getPermissions(session.getGroupIds()).contains(buildPermission(req, "create"))) {
                res.put(req.getId(),
                        new RejectedRequestReport(req.getId(), processedAt, RejectCode.LACKS_OF_AUTHORITY));
                it.remove();
            }
        }

        return res;
    }
}
