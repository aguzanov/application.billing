package ru.kwanza.billing.api;

import com.fasterxml.jackson.annotation.JsonTypeName;

import java.math.BigDecimal;

/**
 * @author henadiy
 */
@JsonTypeName(value = "PAYMENT_TOOL_ID")
public class PaymentToolId {

    public static final PaymentToolId BLANK = new PaymentToolId((BigDecimal)null, Type.BLANK);

    public enum Type {
        BLANK, ACCOUNT, WALLET_ACCOUNT
    }

    private BigDecimal id;
    private Type type;

    protected PaymentToolId() {
    }

    public PaymentToolId(BigDecimal id, Type type) {
        this.id = id;
        this.type = type;
    }

    public BigDecimal getId() {
        return id;
    }

    public Type getType() {
        return type;
    }

    @Override
    public boolean equals(Object obj) {
        return this == obj || obj instanceof PaymentToolId &&
                (getId() == ((PaymentToolId) obj).getId() || null != getId() && getId().equals(((PaymentToolId) obj).getId())) &&
                getType().equals(((PaymentToolId) obj).getType());
    }
}
