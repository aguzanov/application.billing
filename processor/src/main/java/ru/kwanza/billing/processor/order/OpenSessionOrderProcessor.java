package ru.kwanza.billing.processor.order;

import ru.kwanza.billing.api.Session;
import ru.kwanza.billing.api.order.OpenSessionOrder;
import ru.kwanza.billing.api.report.RejectCode;
import ru.kwanza.billing.api.report.RejectedRequestReport;
import ru.kwanza.billing.api.report.Report;
import ru.kwanza.billing.api.report.SessionReport;
import ru.kwanza.billing.processor.RequestProcessor;
import ru.kwanza.billing.processor.session.ISessionManager;

import java.util.*;

/**
 * @author henadiy
 */
public class OpenSessionOrderProcessor extends RequestProcessor<OpenSessionOrder> {

	private ISessionManager sessionManager;

	public void setSessionManager(ISessionManager sessionManager) {
		this.sessionManager = sessionManager;
	}

	@Override
	protected Map<Long, Report> handleRequests(Date processedAt, Collection<OpenSessionOrder> requests) {
		HashMap<Long, Report> orderReports = new HashMap<Long, Report>(requests.size());

		Map<Long, Session> sessionInfoMap = sessionManager.open(requests);
		for(OpenSessionOrder order : requests) {
            System.out.println("PROCESS open session:"  + order);
            Session session = sessionInfoMap.get(order.getId());
			if (null != session) {
				orderReports.put(order.getId(),
						new SessionReport(order.getId(), processedAt, session));
			} else {
				orderReports.put(order.getId(),
						new RejectedRequestReport(order.getId(), processedAt, RejectCode.LACKS_OF_AUTHORITY));
			}
		}

		return orderReports;
	}

	@Override
	protected Map<Long, Report> answerDuplicateRequests(Map<Long, OpenSessionOrder> duplicateRequests) {
		return Collections.emptyMap();
	}
}
