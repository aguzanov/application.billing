package ru.kwanza.billing.entity.api;

import ru.kwanza.console.security.api.AuthEntity;
import ru.kwanza.console.security.api.AuthField;
import ru.kwanza.dbtool.orm.annotations.Entity;
import ru.kwanza.dbtool.orm.annotations.Field;

import java.util.Date;

/**
 * @author Vasily Vorobyov
 */
@Entity(table = "rejected_report")
@AuthEntity(name = "RejectedReport", description = "Reject report order")
public class RejectedReport extends Report {

    @Field("reject_code")
    @AuthField(name = "Reject code")
    private Integer rejectCode;

    public RejectedReport() {
        super();
    }

    public RejectedReport(Long id, Date processedAt, Integer rejectCode) {
        super(id, processedAt);
        this.rejectCode = rejectCode;
    }

    public Integer getRejectCode() {
        return rejectCode;
    }
}
