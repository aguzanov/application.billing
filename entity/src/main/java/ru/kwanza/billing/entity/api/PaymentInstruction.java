package ru.kwanza.billing.entity.api;

import ru.kwanza.console.security.api.AuthEntity;
import ru.kwanza.console.security.api.AuthField;
import ru.kwanza.console.security.api.AuthIdField;
import ru.kwanza.dbtool.orm.annotations.Entity;
import ru.kwanza.dbtool.orm.annotations.Field;
import ru.kwanza.dbtool.orm.annotations.IdField;
import ru.kwanza.dbtool.orm.annotations.ManyToOne;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Types;
import java.util.Date;

/**
 * @author henadiy
 */
@Entity(table = "payment_instruction")
@AuthEntity(name = "PaymentInstruction", description = "Payment instruction")
public class PaymentInstruction implements Serializable {

    @IdField(value = "id", type = Types.DECIMAL)
    @AuthIdField
    @AuthField(name = "ID")
    protected BigDecimal id;

    @Field("order_id")
    @AuthField(name = "Order")
    protected Long orderId;

    @Field("index_in_order")
    @AuthField(name = "Available number")
    protected Integer indexInOrder;

    @Field("source_account_id")
    @AuthField(name = "Sender account")
    protected BigDecimal sourceAccountId;

    @Field("source_wallet_account_id")
    @AuthField(name = "Sender wallet")
    protected BigDecimal sourceWalletAccountId;
    @Field("source_amount")
    @AuthField(name = "Source amount")
    protected Long sourceAmount;

    @Field("target_account_id")
    @AuthField(name = "Receiver account")
    protected BigDecimal targetAccountId;
    @Field("target_wallet_account_id")
    @AuthField(name = "Receiver wallet")
    protected BigDecimal targetWalletAccountId;
    @Field("target_amount")
    @AuthField(name = "Target amount")
    protected Long targetAmount;

    @Field("recorded_at")
    @AuthField(name = "Date of processing")
    protected Date recordedAt;
    @Field("description")
    @AuthField(name = "Short description")
    protected String description;

    @ManyToOne(property = "orderId")
    protected PaymentOrder paymentOrder;

    protected PaymentInstruction() {
    }

    public PaymentInstruction(BigDecimal id, Long orderId, Integer indexInOrder,
                              BigDecimal sourceAccountId, BigDecimal sourceWalletAccountId, Long sourceAmount,
                              BigDecimal targetAccountId, BigDecimal targetWalletAccountId, Long targetAmount,
                              Date recordedAt, String description) {
        this.id = id;
        this.orderId = orderId;
        this.indexInOrder = indexInOrder;
        this.sourceAccountId = sourceAccountId;
        this.sourceWalletAccountId = sourceWalletAccountId;
        this.sourceAmount = sourceAmount;
        this.targetAccountId = targetAccountId;
        this.targetWalletAccountId = targetWalletAccountId;
        this.targetAmount = targetAmount;
        this.recordedAt = recordedAt;
        this.description = description;
    }

    public BigDecimal getId() {
        return id;
    }

    public Long getOrderId() {
        return orderId;
    }

    public Integer getIndexInOrder() {
        return indexInOrder;
    }

    public BigDecimal getSourceAccountId() {
        return sourceAccountId;
    }

    public BigDecimal getSourceWalletAccountId() {
        return sourceWalletAccountId;
    }

    public Long getSourceAmount() {
        return sourceAmount;
    }

    public BigDecimal getTargetAccountId() {
        return targetAccountId;
    }

    public BigDecimal getTargetWalletAccountId() {
        return targetWalletAccountId;
    }

    public Long getTargetAmount() {
        return targetAmount;
    }

    public Date getRecordedAt() {
        return recordedAt;
    }

    public String getDescription() {
        return description;
    }

    public PaymentOrder getPaymentOrder() {
        return paymentOrder;
    }
}
