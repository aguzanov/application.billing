package ru.kwanza.billing.processor.signer;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import ru.kwanza.billing.api.PaymentToolId;
import ru.kwanza.billing.api.SessionId;
import ru.kwanza.billing.api.order.ConversionInstruction;

import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.util.Date;

import static org.junit.Assert.*;

/**
 * @author henadiy
 */
@RunWith(JUnit4.class)
public class ConversionInstructionSignerTest {

    private ConversionInstructionSigner signer;

    @Before
    public void setUp() throws Exception {
        MD5SignatureGenerator gen = new MD5SignatureGenerator();
        gen.setSecretKey("1q2w3e4r5t6y7u8i");

        signer = new ConversionInstructionSigner();
        signer.setSignatureGenerator(gen);
    }

    @Test
    public void testGetData() throws Exception {

        byte[] data = signer.getData(new ConversionInstruction(new SessionId("123"),
                        new Date(14, 3, 12, 15, 35, 22), new Date(14, 3, 12, 16, 35, 22),
                        new PaymentToolId(new BigDecimal("12345"), PaymentToolId.Type.ACCOUNT), 555L, 222, new BigDecimal("11.12"),
                        new PaymentToolId(new BigDecimal("54321"), PaymentToolId.Type.ACCOUNT), 15L, 9876, new BigDecimal("0.4321")));

        assertEquals("123,-1758449678000,-1758446078000,12345,ACCOUNT,555,222,11.12,54321,ACCOUNT,15,9876,0.4321",
                new String(data, Charset.defaultCharset()));

        data = signer.getData(new ConversionInstruction(new SessionId("123"),
                new Date(14, 3, 12, 15, 35, 22), new Date(14, 3, 12, 16, 35, 22),
                new PaymentToolId(null, PaymentToolId.Type.BLANK), 555L, 222, new BigDecimal("11.12"),
                new PaymentToolId(null, PaymentToolId.Type.BLANK), 15L, 9876, new BigDecimal("0.4321")));

        assertEquals("123,-1758449678000,-1758446078000,null,BLANK,555,222,11.12,null,BLANK,15,9876,0.4321",
                new String(data, Charset.defaultCharset()));

    }

    @Test
    public void testSign() throws Exception {

        ConversionInstruction i = signer.sign(new ConversionInstruction(new SessionId("123"),
                new Date(14, 3, 12, 15, 35, 22), new Date(14, 3, 12, 16, 35, 22),
                new PaymentToolId(new BigDecimal("12345"), PaymentToolId.Type.ACCOUNT), 555L, 222, new BigDecimal("11.12"),
                new PaymentToolId(new BigDecimal("54321"), PaymentToolId.Type.ACCOUNT), 15L, 9876, new BigDecimal("0.4321")));

        assertEquals("EB38B79A8099B6002490F36A26EEC9A4", i.getSignature());

    }

    @Test
    public void testCheck() throws Exception {
        ConversionInstruction i = new ConversionInstruction(new SessionId("123"),
                new Date(14, 3, 12, 15, 35, 22), new Date(14, 3, 12, 16, 35, 22),
                new PaymentToolId(new BigDecimal("12345"), PaymentToolId.Type.ACCOUNT), 555L, 222, new BigDecimal("11.12"),
                new PaymentToolId(new BigDecimal("54321"), PaymentToolId.Type.ACCOUNT), 15L, 9876, new BigDecimal("0.4321"));
        i.sign("EB38B79A8099B6002490F36A26EEC9A4");
        assertTrue(signer.check(i));


        i = new ConversionInstruction(new SessionId("123"),
                new Date(14, 3, 12, 15, 35, 22), new Date(14, 3, 12, 16, 35, 22),
                new PaymentToolId(new BigDecimal("12345"), PaymentToolId.Type.ACCOUNT), 555L, 222, new BigDecimal("11.12"),
                new PaymentToolId(new BigDecimal("54321"), PaymentToolId.Type.ACCOUNT), 15L, 9876, new BigDecimal("0.4321"));
        assertFalse(signer.check(i));
    }
}
