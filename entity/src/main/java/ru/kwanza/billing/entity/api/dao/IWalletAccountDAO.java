package ru.kwanza.billing.entity.api.dao;

import ru.kwanza.billing.entity.api.Account;
import ru.kwanza.billing.entity.api.Wallet;
import ru.kwanza.billing.entity.api.WalletAccount;
import ru.kwanza.dbtool.core.UpdateException;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

/**
 * @author Vasily Vorobyov
 */
public interface IWalletAccountDAO {

    WalletAccount prepare(Wallet wallet, Account account, Integer walletAccountCounter, Long orderId, Date createdAt, String title);

    Collection<WalletAccount> create(Collection<WalletAccount> walletAccounts) throws UpdateException;

    Map<BigDecimal, WalletAccount> readByKeys(Collection<BigDecimal> ids);

    Map<Long, WalletAccount> readByOrderIds(Collection<Long> orderIds);

}
