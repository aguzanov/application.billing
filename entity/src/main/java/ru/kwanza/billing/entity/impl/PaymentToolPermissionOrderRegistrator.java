package ru.kwanza.billing.entity.impl;

import ru.kwanza.billing.api.PaymentToolId;
import ru.kwanza.billing.api.PaymentToolPermission;
import ru.kwanza.billing.api.order.PaymentToolPermissionsOrder;

import java.util.Date;

/**
 * @author henadiy
 */
public class PaymentToolPermissionOrderRegistrator extends RequestRegistrator<
        PaymentToolPermissionsOrder, ru.kwanza.billing.entity.api.PaymentToolPermissionsOrder> {

    public PaymentToolPermissionOrderRegistrator() {
        super(ru.kwanza.billing.entity.api.PaymentToolPermissionsOrder.class);
    }

    @Override
    protected ru.kwanza.billing.entity.api.PaymentToolPermissionsOrder createEntity(
            PaymentToolPermissionsOrder order, Date processedAt, Long sessionRecordId) {
        return new ru.kwanza.billing.entity.api.PaymentToolPermissionsOrder(order.getId(), sessionRecordId,
                order.getExchangeId(), processedAt, order.getUserId(), PaymentToolPermission.pack(order.getPermissions()),
                PaymentToolId.Type.ACCOUNT.equals(order.getPaymentToolId().getType()) ?
                        order.getPaymentToolId().getId() : null,
                PaymentToolId.Type.WALLET_ACCOUNT.equals(order.getPaymentToolId().getType()) ?
                        order.getPaymentToolId().getId() : null);
    }

    @Override
    protected Long getEntityId(ru.kwanza.billing.entity.api.PaymentToolPermissionsOrder order) {
        return order.getId();
    }
}
