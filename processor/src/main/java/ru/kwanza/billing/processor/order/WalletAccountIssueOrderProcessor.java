package ru.kwanza.billing.processor.order;

import ru.kwanza.billing.api.order.WalletAccountIssueOrder;
import ru.kwanza.billing.api.report.RejectCode;
import ru.kwanza.billing.api.report.RejectedRequestReport;
import ru.kwanza.billing.api.report.Report;
import ru.kwanza.billing.api.report.WalletAccountReport;
import ru.kwanza.billing.entity.api.Account;
import ru.kwanza.billing.entity.api.Wallet;
import ru.kwanza.billing.entity.api.WalletAccount;
import ru.kwanza.billing.entity.api.dao.IAccountDAO;
import ru.kwanza.billing.entity.api.dao.IWalletAccountDAO;
import ru.kwanza.billing.entity.api.dao.IWalletDAO;
import ru.kwanza.billing.processor.RequestProcessor;
import ru.kwanza.dbtool.core.UpdateException;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author Vasily Vorobyov
 */
public class WalletAccountIssueOrderProcessor extends RequestProcessor<WalletAccountIssueOrder> {

    @Resource(name = "billing.entity.dao.WalletDAO")
    private IWalletDAO walletDAO;
    @Resource(name = "billing.entity.dao.AccountDAO")
    private IAccountDAO accountDAO;
    @Resource(name = "billing.entity.dao.WalletAccountDAO")
    private IWalletAccountDAO walletAccountDAO;

    @Override
    protected Map<Long, Report> handleRequests(Date processedAt, Collection<WalletAccountIssueOrder> orders) {

        final Map<Long, Report> reports = new HashMap<Long, Report>(orders.size());
        final ArrayList<WalletAccount> walletAccounts = new ArrayList<WalletAccount>(reports.size());

        final Set<Long> walletIds = new HashSet<Long>(orders.size());
        final Set<BigDecimal> accountIds = new HashSet<BigDecimal>(orders.size());

        Iterator<WalletAccountIssueOrder> o = orders.iterator();
        while (o.hasNext()) {
            WalletAccountIssueOrder order = o.next();
            if (null == order.getWalletId() || null == order.getAccountId()) {
                reports.put(order.getId(), new RejectedRequestReport(order.getId(), processedAt, RejectCode.EMPTY_REQUIRED_FIELD));
                o.remove();
            } else if (null != order.getTitle() && order.getTitle().length() > 255) {
                reports.put(order.getId(), new RejectedRequestReport(order.getId(), processedAt, RejectCode.TITLE_TOO_LONG));
                o.remove();
            } else {
                walletIds.add(order.getWalletId());
                accountIds.add(order.getAccountId());
            }
        }

        Map<Long, Wallet> wallets = walletDAO.readByKeys(walletIds);
        Map<BigDecimal, Account> accounts = accountDAO.readByKeys(accountIds);

        for (WalletAccountIssueOrder order: orders) {
            Report report;
            Wallet wallet = wallets.get(order.getWalletId());
            if (null == wallet) {
                report = new RejectedRequestReport(order.getId(), processedAt, RejectCode.WALLET_NOT_FOUND);
            } else {
                Account account = accounts.get(order.getAccountId());
                if (null == account) {
                    report = new RejectedRequestReport(order.getId(), processedAt, RejectCode.ACCOUNT_NOT_FOUND);
                } else {
                    Integer walletAccountCounter = wallet.getWalletAccountCounter() + 1;
                    final WalletAccount walletAccount =
                            walletAccountDAO.prepare(wallet, account, walletAccountCounter, order.getId(), processedAt, order.getTitle());
                    report = new WalletAccountReport(
                            order, processedAt, walletAccount.getId(), walletAccount.getWalletId(),
                            walletAccount.getAccountId(), walletAccount.getCurrencyId(), processedAt, order.getId());
                    walletAccounts.add(walletAccount);
                    wallet.setWalletAccountCounter(walletAccountCounter);
                }
            }
            reports.put(order.getId(), report);
        }

        try {
            walletDAO.update(wallets.values());
        } catch (UpdateException e) {
            throw new RuntimeException("Unable to update Wallets");
        }
        try {
            walletAccountDAO.create(walletAccounts);
        } catch (UpdateException e) {
            throw new RuntimeException("Unable to create WalletAccounts");
        }

        return reports;
    }

    @Override
    protected Map<Long, Report> answerDuplicateRequests(Map<Long, WalletAccountIssueOrder> duplicateRequests) {
        final Map<Long, WalletAccount> walletAccountByOrderIds = walletAccountDAO.readByOrderIds(duplicateRequests.keySet());
        final Map<Long, Report> reports = new HashMap<Long, Report>(duplicateRequests.size());

        for (Map.Entry<Long, WalletAccount> o: walletAccountByOrderIds.entrySet()) {
            WalletAccount walletAccount = o.getValue();
            reports.put(o.getKey(), new WalletAccountReport(duplicateRequests.get(o.getKey()), walletAccount.getCreatedAt(),
                    walletAccount.getId(), walletAccount.getWalletId(), walletAccount.getAccountId(), walletAccount.getCurrencyId(),
                    walletAccount.getCreatedAt(), walletAccount.getOrderId()));
        }

        return reports;
    }
}
