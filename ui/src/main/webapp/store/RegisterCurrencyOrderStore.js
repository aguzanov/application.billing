/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.store.RegisterCurrencyOrderStore', {
    extend: 'Ext.data.Store',
    model: 'Billing.model.RegisterCurrencyOrder',
    autoLoad: true,
    remoteFilter: true,
    remoteSort: false,
    autoSync: false
});