package ru.kwanza.billing.api;

/**
 * @author Dmitry Zhukov
 */
public enum WalletStatus {
    ACTIVE(1, "Active"),
    SUSPENDED(2, "Blocked"),
    CLOSED(3, "Closed"),
    UNDEFINED(-1, "Not defined");

    private int code;
    private String title;

    private WalletStatus(int code, String title) {
        this.code = code;
        this.title = title;
    }

    public int getCode() {
        return code;
    }

    public String getTitle() {
        return title;
    }

    public static WalletStatus findByCode(Integer code) {
        if (null != code) {
            for (WalletStatus o : WalletStatus.values()) {
                if (o.getCode() == code) {
                    return o;
                }
            }
        }
        return UNDEFINED;
    }
}
