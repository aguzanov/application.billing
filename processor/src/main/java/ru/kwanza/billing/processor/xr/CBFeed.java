package ru.kwanza.billing.processor.xr;

import java.math.BigDecimal;
import java.util.*;

/**
 * @author henadiy
 */
public abstract class CBFeed extends Feed {

    private final Integer baseCurrencyCode;

    public CBFeed(Integer baseCurrencyCode) {
        this.baseCurrencyCode = baseCurrencyCode;
    }

    public Integer getBaseCurrencyCode() {
        return baseCurrencyCode;
    }

    @Override
    public Collection<Entry> getAllEntries() {
        List<Entry> result = new LinkedList<Entry>();
        Map<Integer, Entry> fetched = new LinkedHashMap<Integer, Entry>();
        for(Entry e : fetch()) {
            if (null == getRequiredCodes() ||
                    getRequiredCodes().contains(e.getSourceCode()) ||
                    getRequiredCodes().contains(e.getTargetCode())) {

                // maintain the rate of conversion
                fetched.put(e.getSourceCode(), e);

                // added direct (CUR -> BASE) and return (BASE -> CUR) rates of conversion
                result.add(e);
                result.add(e.getReversive());
            }
        }

        // generated cross rates
        Integer[] codes = fetched.keySet().toArray(new Integer[fetched.size()]);
        for(int i = 0; i < (codes.length-1); i++) {
            for(int j = i + 1; j < codes.length; j++) {
                // get courses CUR(I) -> BASE and CUR(j) -> BASE
                Entry ei = fetched.get(codes[i]);
                Entry ej = fetched.get(codes[j]);

                // the resulting cross-courses direct CUR(i) -> CUR(j) and return CUR(j) -> CUR(i)
                Entry e = new Entry(ei.getSourceScale().multiply(ej.getTargetScale()), ei.getSourceCode(),
                        ej.getSourceScale().multiply(ei.getTargetScale()), ej.getSourceCode());
                result.add(e);
                result.add(e.getReversive());
            }
        }

        return result;
    }

    protected Entry buildEntry(String srcCurrencyScale, Integer srcCurrencyCode, String baseCurrencyScale) {
        return new Entry(new BigDecimal(srcCurrencyScale), srcCurrencyCode,
                new BigDecimal(baseCurrencyScale), baseCurrencyCode);
    }
}
