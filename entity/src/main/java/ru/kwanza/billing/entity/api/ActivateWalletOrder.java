package ru.kwanza.billing.entity.api;

import ru.kwanza.console.security.api.AuthEntity;
import ru.kwanza.dbtool.orm.annotations.Entity;

import java.util.Date;

/**
 * @author Dmitry Zhukov
 */
@Entity(table = "activate_wallet_order")
@AuthEntity(name = "ActivateWalletOrder", description = "Order for the transfer wallet to ACTIVE status")
public class ActivateWalletOrder extends WalletOrder {
    public ActivateWalletOrder(Long id, Long sessionRecordId, Long exchangeId, Date processedAt, Long walletId) {
        super(id, sessionRecordId, exchangeId, processedAt, walletId, OrderType.ACTIVATE_WALLET_ORDER);
    }

    @Override
    public void initType() {
        type = OrderType.ACTIVATE_WALLET_ORDER.getCode();
    }
}
