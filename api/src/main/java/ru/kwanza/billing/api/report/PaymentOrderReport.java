package ru.kwanza.billing.api.report;

import com.fasterxml.jackson.annotation.JsonTypeName;

import java.util.Date;

/**
 * @author Vasily Vorobyov
 */
@JsonTypeName(value = "PAYMENT_ORDER_REPORT")
public class PaymentOrderReport extends Report {

    protected PaymentOrderReport() {
    }

    public PaymentOrderReport(Long id, Date processedAt) {
        super(id, processedAt);
    }
}
