package ru.kwanza.billing.api.report;

/**
 * @author Dmitry Zhukov
 */
public enum RejectCode {

    //dzhukov: generic codes for requests and orders, with 1 to 99
    LACKS_OF_AUTHORITY(1, "Insufficient privileges"),
    WALLET_NOT_FOUND(2, "Wallet not found"),
    ACCOUNT_NOT_FOUND(3, "Account not found"),
    CURRENCY_NOT_FOUND(4, "Wrong currency code"),
    EMPTY_REQUIRED_FIELD(5, "Requiried fields"),
    ISSUER_NOT_FOUND(6, "Issuer is not found"),
    WALLET_ACCOUNT_NOT_FOUND(7, "Wallet was not found"),
    ILLEGAL_SIGNATURE(8, "Invalid signature"),
    EXPIRED(9, "Valid time period has expired"),
	NO_ACCESS_OR_ENTITY(10, "There is no access or the specified entity was not found"),

    //dzhukov: the codes of the deviations for the instructions(order), with 100 to 199
    NO_PAYMENT_INSTRUCTIONS(100, "Empty order"),
    UNSUPPORTED_PAYMENT_INSTRUCTION(101, "Unknown payment instruction"),
    ACCOUNT_SUSPENDED(102, "Account operations suspended"),
    MIN_BALANCE_EXCEEDED(103, "Exceeded the minimum allowable account balance"),
    MAX_BALANCE_EXCEEDED(104, "Exceeded the maximum allowable account balance"),
    TITLE_TOO_LONG(105, "Title is too long"),
    ILLEGAL_CURRENCY_ID(106, "Invalid value of currency code"),
    CURRENCY_ALPHA_CODE_TOO_LONG(107, "Currency alpha code is too long"),
    ILLEGAL_ISSUER_BIN(108, "Invalid issuer BIN"),
    NO_ACCOUNTS(109, "Aaccount numbers are empty"),
    IDENTICAL_ACCOUNTS(110, "Same account numbers"),
    ILLEGAL_AMOUNT(111, "Invalid value amount"),
    HAS_RELATED_ACCOUNTS(112, "There are accounts associated with a particular entity"),

    //dzhukov: deviation codes for queries(query), with 200 to 299
    CONVERSION_RATE_NOT_FOUND(200, "The rate of conversion was not found");

    private int code;
    private String title;

    private RejectCode(int code, String title) {
        this.code = code;
        this.title = title;
    }

    public int getCode() {
        return code;
    }

    public String getTitle() {
        return title;
    }

    public static RejectCode findByCode(int code) {
        for (RejectCode o : values()) {
            if (o.code == code) {
                return o;
            }
        }
        throw new IllegalArgumentException("Unknown code: " + code);
    }
}
