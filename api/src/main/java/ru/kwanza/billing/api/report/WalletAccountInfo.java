package ru.kwanza.billing.api.report;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Vasily Vorobyov
 */
public class WalletAccountInfo {

    private BigDecimal id;
    private Date createdAt;
    private Long orderId;
    private BigDecimal accountId;
    private String title;

    protected WalletAccountInfo() {
    }

    public WalletAccountInfo(BigDecimal id, Date createdAt, Long orderId, BigDecimal accountId, String title) {
        this.id = id;
        this.createdAt = createdAt;
        this.orderId = orderId;
        this.accountId = accountId;
        this.title = title;
    }

    public BigDecimal getId() {
        return id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Long getOrderId() {
        return orderId;
    }

    public BigDecimal getAccountId() {
        return accountId;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return "WalletAccountInfo{" +
                "id=" + id +
                ", createdAt=" + createdAt +
                ", orderId=" + orderId +
                ", accountId=" + accountId +
                ", title='" + title + '\'' +
                '}';
    }
}
