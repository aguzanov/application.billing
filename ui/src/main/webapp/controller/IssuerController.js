/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.controller.IssuerController', {
    extend: 'Ext.app.Controller',
    models: ['Billing.model.Issuer'],
    stores: ['Billing.store.IssuerStore'],
    views: [
        'Billing.view.issuer.IssuerList',
        'Billing.view.issuer.IssuerCard'
    ],

    init: function () {
        var me = this;
        this.control({

        });
    }
});