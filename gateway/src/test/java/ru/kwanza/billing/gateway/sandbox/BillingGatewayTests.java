package ru.kwanza.billing.gateway.sandbox;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.glassfish.grizzly.Buffer;
import org.glassfish.grizzly.filterchain.*;
import org.glassfish.grizzly.http.*;
import org.glassfish.grizzly.http.util.Header;
import org.glassfish.grizzly.memory.Buffers;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.kwanza.billing.api.Request;
import ru.kwanza.billing.api.SessionId;
import ru.kwanza.billing.api.order.WalletIssueOrder;
import ru.kwanza.jeda.api.*;
import ru.kwanza.jeda.nio.client.AbstractFilter;
import ru.kwanza.jeda.nio.client.ConnectionConfig;
import ru.kwanza.jeda.nio.client.ITransportEvent;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author Vasily Vorobyov
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:billing-gateway-tests.xml")
@Ignore
public class BillingGatewayTests extends AbstractJUnit4SpringContextTests {

    private final URL url;

    @Resource(name = "client-transport-flow-bus")
    private ISink clientFlowBus;

    private AtomicLong inProgressCounter = new AtomicLong(0);

    public BillingGatewayTests() throws MalformedURLException {
//        url = new URL("http://localhost:10080/process/WalletIssueOrder");
        url = new URL("http://localhost:10080");
    }

    private static class GenerateIdRequest extends AbstractEvent implements ITransportEvent {

        private URL billingEndpoint;
        private BillingAPIRequest request;
        private ISink clientFlowBus;

        private GenerateIdRequest(URL billingEndpoint, BillingAPIRequest request, ISink clientFlowBus) {
            this.billingEndpoint = billingEndpoint;
            this.request = request;
            this.clientFlowBus = clientFlowBus;
        }

        public ConnectionConfig getConnectionConfig() {
            return new ConnectionConfig(
                    new InetSocketAddress(billingEndpoint.getHost(), -1 != billingEndpoint.getPort() ? billingEndpoint.getPort() : 80),
                    true, false, 60000);
        }

        public FilterChain getProcessingFilterChain() {
            FilterChainBuilder builder = FilterChainBuilder.stateless();

            builder.add(new TransportFilter());
            HttpClientFilter httpClientFilter = new HttpClientFilter();
            httpClientFilter.addTransferEncoding(new FixedLengthTransferEncoding());
            builder.add(httpClientFilter);
            builder.add(new GenerateIdResponseHandler(clientFlowBus, request));

            return builder.build();
        }

        public Object getContent() {
            HttpRequestPacket.Builder requestBuilder = HttpRequestPacket.builder();
            requestBuilder.method("POST").uri(billingEndpoint.getPath() + "/generateIds").protocol(Protocol.HTTP_1_1).chunked(false)
                    .contentLength(0)
                    .header(Header.Host, billingEndpoint.getHost() + ":" + billingEndpoint.getPort())
                    .contentType("application/json");
            requestBuilder.header(Header.Connection, "Keep-Alive");

            return HttpContent.builder(requestBuilder.build()).last(true).build();
        }
    }

    private static class GenerateIdResponseHandler extends AbstractFilter {

        private ISink clientFlowBus;
        private BillingAPIRequest request;

        private GenerateIdResponseHandler(ISink clientFlowBus, BillingAPIRequest request) {
            this.clientFlowBus = clientFlowBus;
            this.request = request;
        }

        public NextAction read(FilterChainContext ctx) throws IOException {
            Object message = ctx.getMessage();
            if (message instanceof HttpContent) {
                HttpContent content = (HttpContent) message;

                if (content.isLast()) {
                    String c = content.getContent().toStringContent();
                    System.out.println(c);
                    ObjectMapper mapper = new ObjectMapper();
                    Collection<Long> ids = mapper.readValue(c, new TypeReference<Collection<Long>>() {});
                    request.setRequestId(ids.iterator().next());
                    try {
                        clientFlowBus.tryPut(Collections.singleton(request));
                    } catch (SinkException e) {
                        e.printStackTrace();
                    }
                }

            } else {
                System.out.println("not last");
            }

            return ctx.getStopAction();
        }

        public void handleConnectError(ITransportEvent event, Throwable e) {
            e.printStackTrace();
        }
    }

    private static class ProcessResponseHandler extends AbstractFilter {

        private AtomicLong inProgressCounter;

        private ProcessResponseHandler(AtomicLong inProgressCounter) {
            this.inProgressCounter = inProgressCounter;
        }

        public NextAction read(FilterChainContext ctx) throws IOException {
            inProgressCounter.addAndGet(-1);
            Object message = ctx.getMessage();
            if (message instanceof HttpContent) {
                HttpContent content = (HttpContent) message;

                if (content.isLast()) {
                    System.out.println(content.getContent().toStringContent());
                    releaseConnection(ctx);
                }

            } else {
                System.out.println("not last");
            }

            return ctx.getInvokeAction();
        }

        public void handleConnectError(ITransportEvent event, Throwable e) {
            e.printStackTrace();
        }
    }

    private static class BillingAPIRequest<R extends Request> extends AbstractEvent implements ITransportEvent {

        private URL billingEndpoint;
        private R order;
        private ConnectionConfig config;
        private AtomicLong inProgressCounter;

        private BillingAPIRequest(URL billingEndpoint, R order, AtomicLong inProgressCounter) {
            this.billingEndpoint = billingEndpoint;
            this.order = order;
            this.inProgressCounter = inProgressCounter;
            this.config = new ConnectionConfig(
                    new InetSocketAddress(billingEndpoint.getHost(), -1 != billingEndpoint.getPort() ? billingEndpoint.getPort() : 80),
                    true, false, 60000);
        }

        public void setRequestId(Long id) {
            order.setId(id);
        }

        public ConnectionConfig getConnectionConfig() {
            return config;
        }

        public FilterChain getProcessingFilterChain() {

            FilterChainBuilder builder = FilterChainBuilder.stateless();

            builder.add(new TransportFilter());
            HttpClientFilter httpClientFilter = new HttpClientFilter();
            httpClientFilter.addTransferEncoding(new FixedLengthTransferEncoding());
            builder.add(httpClientFilter);
            builder.add(new ProcessResponseHandler(inProgressCounter));

            return builder.build();
        }

        public Object getContent() {
            ObjectMapper mapper = new ObjectMapper();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            try {
                mapper.writeValue(stream, order);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            Buffer wrap = Buffers.wrap(null, stream.toByteArray());

            HttpRequestPacket.Builder requestBuilder = HttpRequestPacket.builder();

            String requestClassName = order.getClass().getName();
            requestBuilder.method("POST").uri(billingEndpoint.getPath() + "/process/" +
                    requestClassName.substring(requestClassName.lastIndexOf(".") + 1))
                    .protocol(Protocol.HTTP_1_1).chunked(false).contentLength(wrap.capacity())
                    .header(Header.Host, config.getEndpoint().getHostName() + ":" + config.getEndpoint().getPort())
                    .contentType("application/json");
            if (config.isKeepAlive()) {
                requestBuilder.header(Header.Connection, "Keep-Alive");
            } else {
                requestBuilder.header(Header.Connection, "close");
            }

            return HttpContent.builder(requestBuilder.build()).content(wrap).last(true).build();
        }
    }

    @Test
    public void testWalletIssueOrder() throws SinkException {

        List<ITransportEvent> events = new ArrayList<ITransportEvent>();
        inProgressCounter.addAndGet(1);
        WalletIssueOrder walletIssueOrder = new WalletIssueOrder(21508L, new SessionId("SID"), "KWANZA test", "KWANZA test");
        GenerateIdRequest generateIdRequest = new GenerateIdRequest(url, new BillingAPIRequest<WalletIssueOrder>(url, walletIssueOrder, inProgressCounter),
                clientFlowBus);
        events.add(generateIdRequest);

        clientFlowBus.tryPut(events);

        while (0 < inProgressCounter.get()) {
            try {
                Thread.sleep(100L);
            } catch (InterruptedException e) {
            }
        }
    }

}
