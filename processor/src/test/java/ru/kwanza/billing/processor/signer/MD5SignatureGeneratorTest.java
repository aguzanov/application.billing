package ru.kwanza.billing.processor.signer;

import junit.framework.TestCase;
import ru.kwanza.billing.processor.signer.MD5SignatureGenerator;

/**
 * @author henadiy
 */
public class MD5SignatureGeneratorTest extends TestCase{

    public void testGetSignature() {

        MD5SignatureGenerator gen = new MD5SignatureGenerator();

        try {
            gen.getSignature("123".getBytes());
            fail("IllegalArgumentException expected (blank secretKey).");
        }catch (IllegalArgumentException e) {
        }

        try {
            gen.getSignature(null);
            fail("IllegalArgumentException expected (null in data).");
        }catch (IllegalArgumentException e) {
        }

        try {
            gen.getSignature(new byte[0]);
            fail("IllegalArgumentException expected (empty data).");
        }catch (IllegalArgumentException e) {
        }

        gen.setSecretKey("1234567");
        assertEquals("3AA8380532983842C246E66F22B341B7",
                gen.getSignature(new StringBuilder().append("TEST").toString().getBytes()));

    }

}
