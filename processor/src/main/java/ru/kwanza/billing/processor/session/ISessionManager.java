package ru.kwanza.billing.processor.session;

import ru.kwanza.billing.api.Session;
import ru.kwanza.billing.api.SessionId;
import ru.kwanza.billing.api.order.OpenSessionOrder;

import java.util.Collection;
import java.util.Map;

/**
 * @author henadiy
 */
public interface ISessionManager {

	/**
	 * The issuering sessions.
	 *
	 * @param openSessionOrders - the list of instructions for the issuering of the session (including, for example username and password, etc.)
	 * @return table id-orders : session information
	 */
	Map<Long, Session> open(Collection<OpenSessionOrder> openSessionOrders);

	/**
	 * Closing of the sessions
	 *
	 * @param sessionIdsToClose - session IDs, kotoraya should be closed
	 * @return table id-session : session
	 */
	Map<SessionId, Session> close(Collection<SessionId> sessionIdsToClose);

	/**
	 *
	 * @param sessionIds - session IDs
	 * @return table id-session : session
	 */
	Map<SessionId, Session> get(Collection<SessionId> sessionIds);

}
