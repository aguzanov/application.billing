package ru.kwanza.billing.entity.api;

import ru.kwanza.console.security.api.AuthEntity;
import ru.kwanza.console.security.api.AuthField;
import ru.kwanza.dbtool.orm.annotations.Entity;
import ru.kwanza.dbtool.orm.annotations.Field;
import ru.kwanza.dbtool.orm.annotations.ManyToOne;

import java.util.Date;

/**
 * @author Vasily Vorobyov
 */
@Entity(name = "RegisterCurrencyOrder", table = "register_currency_order")
@AuthEntity(name = "RegisterCurrencyOrder", description = "Register currency order")
public class RegisterCurrencyOrder extends Order {

    @Field("currency_id")
    @AuthField(name = "Currency ID")
    private Integer currencyId;

    @Field("alpha_code")
    @AuthField(name = "Alpha code")
    private String alphaCode;

    @Field("title")
    @AuthField(name = "Name")
    private String title;

    @Field("exponent")
    @AuthField(name = "Exponent")
    private Integer exponent;

    @ManyToOne(property = "currencyId")
    private Currency currency;

    public RegisterCurrencyOrder() {
    }

    public RegisterCurrencyOrder(Long id, Long sessionRecordId,Long exchangeId, Date processedAt, Integer currencyId, String alphaCode, String title,
                                 Integer exponent) {
        super(id, sessionRecordId, exchangeId,processedAt,OrderType.REGISTER_CURRENCY);
        this.currencyId = currencyId;
        this.alphaCode = alphaCode;
        this.title = title;
        this.exponent = exponent;
    }

    public Integer getCurrencyId() {
        return currencyId;
    }

    public String getAlphaCode() {
        return alphaCode;
    }

    public String getTitle() {
        return title;
    }

    public Integer getExponent() {
        return exponent;
    }

    public Currency getCurrency() {
        return currency;
    }

    @Override
    public void initType() {
        type = OrderType.REGISTER_CURRENCY.getCode();
    }
}
