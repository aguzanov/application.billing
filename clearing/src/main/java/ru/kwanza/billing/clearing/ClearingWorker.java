package ru.kwanza.billing.clearing;

import ru.kwanza.billing.clearing.dao.IClearingDAO;
import ru.kwanza.billing.clearing.entity.ClearingGroup;
import ru.kwanza.dbtool.core.UpdateException;
import ru.kwanza.worker.api.FixedDelayWorker;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * @author henadiy
 */
public class ClearingWorker extends FixedDelayWorker {

    private IClearingDAO clearingDAO;
    private ClearingCalculator calculator;

    public void setClearingDAO(IClearingDAO clearingDAO) {
        this.clearingDAO = clearingDAO;
    }

    public void setClearingCalculator(ClearingCalculator calculator) {
        this.calculator = calculator;
    }

    @Override
    protected void onFire() {

        Date now = new Date();

        Collection<ClearingGroup> allGroups = clearingDAO.readClearingGroups();
        List<ClearingGroup> clearedGroups = new ArrayList<ClearingGroup>(allGroups.size());

        for(ClearingGroup group : clearingDAO.readClearingGroups()) {
            // required treatment
            if ((group.getLastClearedAt().getTime() + group.getPeriod() * 1000L) <= now.getTime()) {
                calculator.calculate(group);
                clearedGroups.add(group);
            }
        }

        // fix update of groups
        try {
            clearingDAO.updateClearingGroups(clearedGroups);
        }catch (UpdateException e) {
            throw new RuntimeException("Unable to update clearing groups", e);
        }
    }
}
