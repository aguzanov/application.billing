Ext.define('Billing.view.sessionhistory.SessionHistoryList', {

    requires: [
        'Console.grid.FiltersFeature',
        'Console.application.QuickLink',
        'Billing.store.SessionHistoryStore'
    ],
    extend: 'Console.grid.BaseGrid',

    alias: ['widget.session_history_list', 'widget.sessionhistorylist'],
    tabTitle: 'Session history',
    caption: 'Session history',

    model: 'Billing.model.SessionHistory',
    store: 'Billing.store.SessionHistoryStore',
    iconCls: 'i-document-survey',

    constructor: function () {
        var me = this;

        me.columns = [
            {
                xtype: 'quicklinkcolumn',
                text: 'Token',
                dataIndex: 'sessionKey',
                width: 200,
                filter: true
            },
            {
                text: 'User ID',
                dataIndex: 'userId',
                width: 200,
                filter: true
            },
            {
                text: 'Opened at',
                dataIndex: 'openedAt',
                xtype: "datecolumn",
                format: 'd.m.Y H:i:s',
                width: 200,
                filter: true
            },
            {
                text: 'Closed at',
                xtype: "datecolumn",
                dataIndex: 'closedAt',
                format: 'd.m.Y H:i:s',
                width: 200,
                filter: true
            }
        ];

        me.callParent(arguments);
    }
});
