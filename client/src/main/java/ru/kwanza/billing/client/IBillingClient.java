package ru.kwanza.billing.client;

import ru.kwanza.billing.api.Request;
import ru.kwanza.billing.api.report.Report;

import java.util.List;

/**
 * @author henadiy
 */
public interface IBillingClient {

	Long generateRequestId();

	List<Long> generateRequestIds(int count);

	List<Report> processRequests(List<Request> requests);
}
