package ru.kwanza.billing.api.report;

import com.fasterxml.jackson.annotation.JsonTypeName;
import ru.kwanza.billing.api.WalletStatus;
import ru.kwanza.billing.api.order.Order;

import java.util.Collection;
import java.util.Date;

/**
 * @author Vasily Vorobyov
 */
@JsonTypeName(value = "WALLET_REPORT")
public class WalletReport extends Report {

    /**
     * ID released of purse
     */
    private Long walletId;

    private Long orderId;
    private Date createdAt;
    private WalletStatus status;
    private String holderId;
    private String title;
    private Collection<WalletAccountInfo> walletAccountInfos;

    protected WalletReport() {
    }

    public WalletReport(Order order, Date processedAt, Long walletId, Long orderId, Date createdAt, WalletStatus status, String holderId, String title,
                        Collection<WalletAccountInfo> walletAccountInfos) {
        super(order.getId(), processedAt);
        this.walletId = walletId;
        this.orderId = orderId;
        this.createdAt = createdAt;
        this.status = status;
        this.holderId = holderId;
        this.title = title;
        this.walletAccountInfos = walletAccountInfos;
    }

    public Long getWalletId() {
        return walletId;
    }

    public Long getOrderId() {
        return orderId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public WalletStatus getStatus() {
        return status;
    }

    public Collection<WalletAccountInfo> getWalletAccountInfos() {
        return walletAccountInfos;
    }

    public String getHolderId() {
        return holderId;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return "WalletReport{" +
                "walletId=" + walletId +
                ", orderId=" + orderId +
                ", createdAt=" + createdAt +
                ", status=" + status +
                ", holderId='" + holderId + '\'' +
                ", title='" + title + '\'' +
                ", walletAccountInfos=" + walletAccountInfos +
                '}';
    }
}
