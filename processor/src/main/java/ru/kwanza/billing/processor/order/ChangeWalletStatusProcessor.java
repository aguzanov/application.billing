package ru.kwanza.billing.processor.order;

import ru.kwanza.billing.api.WalletStatus;
import ru.kwanza.billing.api.order.ActivateWalletOrder;
import ru.kwanza.billing.api.order.CloseWalletOrder;
import ru.kwanza.billing.api.order.SuspendWalletOrder;
import ru.kwanza.billing.api.order.WalletOrder;
import ru.kwanza.billing.api.report.RejectCode;
import ru.kwanza.billing.api.report.RejectedRequestReport;
import ru.kwanza.billing.api.report.Report;
import ru.kwanza.billing.api.report.WalletChangeStatusReport;
import ru.kwanza.billing.entity.api.Wallet;
import ru.kwanza.billing.entity.api.dao.IWalletDAO;
import ru.kwanza.billing.processor.RequestProcessor;
import ru.kwanza.dbtool.core.UpdateException;

import javax.annotation.Resource;
import java.util.*;

/**
 * @author Dmitry Zhukov
 */
public class ChangeWalletStatusProcessor extends RequestProcessor<WalletOrder> {

    @Resource(name = "billing.entity.dao.WalletDAO")
    private IWalletDAO walletDAO;

    @Override
    protected Map<Long, Report> handleRequests(Date processedAt, Collection<WalletOrder> requests) {
        Map<Long, Report> reports = new HashMap<Long, Report>(requests.size());
        Iterator<WalletOrder> iterator = requests.iterator();

        List<Long> walletIds = new ArrayList<Long>(requests.size());

        while (iterator.hasNext()) {
            WalletOrder order = iterator.next();
            if (null == order.getWalletId()) {
                reports.put(order.getId(), new RejectedRequestReport(order.getId(), processedAt, RejectCode.EMPTY_REQUIRED_FIELD));
                iterator.remove();
            } else {
                walletIds.add(order.getWalletId());
            }
        }

        Map<Long, Wallet> wallets = walletDAO.readByKeys(walletIds);

        List<Wallet> changeWallets = new ArrayList<Wallet>(requests.size());

        for (WalletOrder order : requests) {
            Report report;
            Wallet wallet = null;
            if (order instanceof ActivateWalletOrder) {
                wallet = wallets.get(order.getWalletId());
                if(null != wallet){
                    wallet.setStatus(WalletStatus.ACTIVE);
                }
            } else if (order instanceof SuspendWalletOrder) {
                wallet = wallets.get(order.getWalletId());
                if(null != wallet){
                    wallet.setStatus(WalletStatus.SUSPENDED);
                }
            } else if (order instanceof CloseWalletOrder) {
                wallet = wallets.get(order.getWalletId());
                if(null != wallet){
                    wallet.setStatus(WalletStatus.CLOSED);
                }
            }
            if (null == wallet) {
                report = new RejectedRequestReport(order.getId(), processedAt, RejectCode.WALLET_NOT_FOUND);
            } else {
                changeWallets.add(wallet);
                report = new WalletChangeStatusReport(order.getId(), processedAt, order.getWalletId());
            }
            reports.put(order.getId(), report);
        }
        try {
            walletDAO.update(changeWallets);
        } catch (UpdateException e) {
            throw new RuntimeException("Unable to update Wallets");
        }
        return reports;
    }

    @Override
    protected Map<Long, Report> answerDuplicateRequests(Map<Long, WalletOrder> duplicateRequests) {
        return Collections.emptyMap();
    }
}
