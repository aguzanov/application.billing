package ru.kwanza.billing.controller.valueobjects;

import ru.kwanza.billing.entity.api.Issuer;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Dmitry Zhukov
 */

public class AccountIssueOrderVO extends OrderVO{


    private Integer issuerId;

    private Issuer issuer;

    private BigDecimal accountId;

    public AccountIssueOrderVO(Long id, Long sessionRecordId, Date processedAt, Integer rejectCode, Integer issuerId, Issuer issuer, BigDecimal accountId) {
        super(id, sessionRecordId, processedAt, rejectCode);
        this.issuerId = issuerId;
        this.issuer = issuer;
        this.accountId = accountId;
    }

    public Integer getIssuerId() {
        return issuerId;
    }

    public Issuer getIssuer() {
        return issuer;
    }

    public BigDecimal getAccountId() {
        return accountId;
    }
}
