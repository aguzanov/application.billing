package ru.kwanza.billing.processor.signer;

import org.springframework.util.StringUtils;
import ru.kwanza.billing.api.order.ConversionInstruction;

/**
 * @author henadiy
 */
public class ConversionInstructionSigner extends Signer<ConversionInstruction> {

    @Override
    protected byte[] getData(ConversionInstruction obj) {
        return StringUtils.arrayToCommaDelimitedString(new Object[]{
                obj.getSessionId().getToken(), obj.getIssuedAt().getTime(), obj.getExpiresIn().getTime(),
                obj.getSourceAccountId().getId(), obj.getSourceAccountId().getType(),
                obj.getSourceAmount(), obj.getSourceCurrencyId(), obj.getSourceScale(),
                obj.getTargetAccountId().getId(), obj.getTargetAccountId().getType(),
                obj.getTargetAmount(), obj.getTargetCurrencyId(), obj.getTargetScale()
        }).getBytes();
    }
}
