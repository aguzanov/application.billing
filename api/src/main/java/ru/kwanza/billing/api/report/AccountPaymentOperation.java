package ru.kwanza.billing.api.report;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Allows you to combine AccountDebitRecord and AccountCreditRecord
 *
 * @author Dmitry Zhukov
 */
public class AccountPaymentOperation {
    private BigDecimal id;
    private Long orderId;

    private BigDecimal accountId;
    private Long amount;
    private Long incomingBalance;

    private BigDecimal linkedAccountId;
    private Long linkedAmount;
    private Long linkedIncomingBalance;

    private Date recordedAt;
    private String description;

    protected AccountPaymentOperation() {
    }

    public AccountPaymentOperation(BigDecimal id, Long orderId,
                                   BigDecimal accountId, Long amount, Long incomingBalance,
                                   BigDecimal linkedAccountId, Long linkedAmount, Long linkedIncomingBalance,
                                   Date recordedAt, String description) {
        this.id = id;
        this.orderId = orderId;
        this.accountId = accountId;
        this.amount = amount;
        this.incomingBalance = incomingBalance;
        this.linkedAccountId = linkedAccountId;
        this.linkedAmount = linkedAmount;
        this.linkedIncomingBalance = linkedIncomingBalance;
        this.recordedAt = recordedAt;
        this.description = description;
    }

    public BigDecimal getId() {
        return id;
    }

    public Long getOrderId() {
        return orderId;
    }

    public BigDecimal getAccountId() {
        return accountId;
    }

    public Long getAmount() {
        return amount;
    }

    public Long getIncomingBalance() {
        return incomingBalance;
    }

    public BigDecimal getLinkedAccountId() {
        return linkedAccountId;
    }

    public Long getLinkedAmount() {
        return linkedAmount;
    }

    public Long getLinkedIncomingBalance() {
        return linkedIncomingBalance;
    }

    public Date getRecordedAt() {
        return recordedAt;
    }

    public String getDescription() {
        return description;
    }
}
