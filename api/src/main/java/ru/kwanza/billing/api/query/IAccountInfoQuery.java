package ru.kwanza.billing.api.query;

import java.math.BigDecimal;

/**
 * @author henadiy
 */
public interface IAccountInfoQuery {

	BigDecimal getAccountId();
}
