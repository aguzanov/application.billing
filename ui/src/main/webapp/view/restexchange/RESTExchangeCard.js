/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.view.restexchange.RESTExchangeCard', {
    extend: 'Console.panel.CardPanel',
    requires: ['Billing.model.RESTExchange'],

    alias: ['widget.rest-exchange', 'widget.restexchange'],

    title: 'REST invocation',
    iconCls: 'i-mail-in',

    defaultType: 'fieldset',
    defaults: {
        columnWidth: 1,
        margin: '5 5 4 5',
        defaultType: 'textfield',
        minWidth: 560,
        defaults: {
            labelWidth: 180,
            anchor: '100%'
        }
    },

    layout: {
        type: 'vbox',
        align : 'stretch',
        pack  : 'start'
    },

    model: 'Billing.model.RESTExchange',
    store: 'Billing.store.RESTExchangeStore',

    initComponent: function () {
        var me = this;

        var tabPanel = Ext.create('Ext.TabPanel', {
            xtype: "tabpanel",
            activeTab: 0,
            flex : 2,
            defaults: {
                bodyPadding: 10
            },
            items: [me.createArea('request', "HTTP Request"), me.createArea('response', "HTTP Response")]
        });

        me.items = [
            {
                title: 'Main parameters',
                itemId: 'main',
                flex: 1,
                items: [
                    {
                        fieldLabel: 'ID',
                        itemId: 'id',
                        name: 'id',
                        readOnly: true
                    },
                    {
                        fieldLabel: 'URI',
                        itemId: 'uri',
                        name: 'uri',
                        readOnly: true
                    },
                    {
                        fieldLabel: 'Remote address',
                        itemId: 'remoteAddress',
                        name: 'remoteAddress',
                        readOnly: true
                    },
                    {
                        fieldLabel: 'Status',
                        itemId: 'isSuccess',
                        name: 'isSuccess',
                        renderer: Billing.model.RESTExchange.statusRenderer,
                        readOnly: true
                    }, tabPanel]
            }, tabPanel]

        this.callParent(arguments);
    },

    updateRecord: function (record) {
        this.updateValue(record, 'request');
        this.updateValue(record, 'response');

        this.callParent(arguments)
        this.doLayout();
    },

    updateValue: function (record, field) {
        var me = this, fieldName = String(field),
            area = me[fieldName + "Panel"].down('#' + fieldName),
            headers = me[fieldName + "Panel"].down('#' + fieldName + 'Headers');

        if (area && area.codemirror) {
            this.setCodeMirrorValue(area.codemirror, record.data[fieldName]);
        } else {
            area[fieldName + 'Value'] = record.data[fieldName]
        }

        headers.update(record.data)
    },

    setCodeMirrorValue: function (codemirror, value) {
        codemirror.setValue(value);
        codemirror.setSelection({line: 0, ch: 0}, {line: 0, ch: 0});
        this.doLayout();
    },

    createArea: function (field, caption) {
        var me = this, fieldName = String(field);

        me[fieldName + 'Panel'] = Ext.create('Ext.Panel', {
            title: caption,
            layout: {
                type: 'hbox',
                pack: 'start',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'panel',
                    autoScroll: true,
                    width: 400,
                    itemId: fieldName + 'Headers',
                    cls: 'details',
                    tpl: new Ext.XTemplate(
                        '<tpl>',
                        '<div class="carddetail">',
                        '<legend>Headers</legend>',

                        '<table class="details">{[this.getProperties(Ext.decode(values[this.fieldName]),"",true)]}</table>',
                        '</div>',
                        '</tpl>',
                        {
                            getProperties: function (obj, prefix) {
                                var me = this;
                                var result = '';
                                Ext.Object.each(obj, function (key, value, myself) {
                                    if (Ext.isObject(value)) {
                                        result += me.getProperties(value, prefix + key + '.');
                                    } else if (Ext.isArray(value)) {
                                        for (var i = 0; i < value.length; i++) {
                                            result += me.getProperties(value[i], prefix + key + '[' + i + '].');
                                        }
                                    } else {
                                        result += '<tr><th><label>' + prefix + key + '</label></td><td>' + Ext.util.Format.htmlEncode(value) + '</td></tr>';
                                    }
                                });

                                return result;
                            },

                            fieldName: fieldName + 'Headers'
                        }
                    ),
                    border: false,
                    padding: 10
                },
                {
                    xtype: 'panel',
                    border: false,
                    flex: 1,
                    frame: false,
                    items: [
                        {
                            xtype: 'textarea',
                            border: false,
                            frame: false,
                            itemId: fieldName,
                            layout: 'fit',
                            readOnly: true,
                            listeners: {
                                afterrender: function () {
                                    var textarea = this;
                                    if (!textarea.codemirror) {
                                        textarea.codemirror = CodeMirror.fromTextArea(textarea.getEl().dom,
                                            {
                                                mode: {name: "javascript", json: true},
                                                lineNumbers: true,
                                                lineWrapping: true,
                                                autoClearEmptyLines: true,
                                                readOnly: true
                                            });
                                        var v = textarea[fieldName + 'Value'];
                                        if (v) {
                                            me.setCodeMirrorValue(textarea.codemirror, v)
                                        }
                                    }


                                }
                            }
                        }
                    ],
                    listeners: {
                        show: function () {
                            var me = this.down('textarea');
                            if (me.codemirror) {
                                var size = this.getSize();
                                me.codemirror.setSize(size.width, size.height);
                            }
                        },
                        resize: function () {
                            var me = this.down('textarea');
                            if (me.codemirror) {
                                var size = this.getSize();
                                me.codemirror.setSize(size.width, size.height);
                                me.codemirror.setValue(me.codemirror.getValue());
                            }
                        }
                    }
                }
            ]
        })
        ;

        return me[fieldName + 'Panel']
    }

})



