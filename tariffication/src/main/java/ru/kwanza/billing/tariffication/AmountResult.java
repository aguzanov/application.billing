package ru.kwanza.billing.tariffication;

/**
 * @author henadiy
 */
public class AmountResult<A extends IArguments> extends AbstractResult<A> {

    private final Long amount;

    public AmountResult(Long amount, A arguments, ResultCode code) {
        super(arguments, code);
        this.amount = amount;
    }

    public Long getAmount() {
        return amount;
    }

}
