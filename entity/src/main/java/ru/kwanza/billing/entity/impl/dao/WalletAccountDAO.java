package ru.kwanza.billing.entity.impl.dao;

import ru.kwanza.billing.entity.api.Account;
import ru.kwanza.billing.entity.api.Wallet;
import ru.kwanza.billing.entity.api.WalletAccount;
import ru.kwanza.billing.entity.api.dao.IWalletAccountDAO;
import ru.kwanza.dbtool.core.UpdateException;
import ru.kwanza.dbtool.orm.api.If;
import ru.kwanza.dbtool.orm.api.IEntityManager;
import ru.kwanza.dbtool.orm.api.IQuery;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

/**
 * @author Vasily Vorobyov
 */
public class WalletAccountDAO implements IWalletAccountDAO {

    @Resource(name = "dbtool.IEntityManager")
    private IEntityManager em;

    private IQuery<WalletAccount> queryWalletAccountByOrderId;

    public void init() {
        queryWalletAccountByOrderId = em.queryBuilder(WalletAccount.class).where(If.in("orderId", "orderId")).create();
    }

    public WalletAccount prepare(Wallet wallet, Account account, Integer walletAccountCounter, Long orderId, Date createdAt, String title) {
        return new WalletAccount(wallet, account, walletAccountCounter, orderId, createdAt, title);
    }

    public Collection<WalletAccount> create(Collection<WalletAccount> walletAccounts) throws UpdateException {
        return em.create(WalletAccount.class, walletAccounts);
    }

    public Map<BigDecimal, WalletAccount> readByKeys(Collection<BigDecimal> ids) {
        return em.readMapByKeys(WalletAccount.class, ids);
    }

    public Map<Long, WalletAccount> readByOrderIds(Collection<Long> orderIds) {
        return queryWalletAccountByOrderId.prepare().setParameter("orderId", orderIds).selectMap("orderId");
    }
}
