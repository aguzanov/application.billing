package ru.kwanza.billing.controller;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.kwanza.billing.entity.api.AccountRecord;
import ru.kwanza.console.security.api.AuthOperation;
import ru.kwanza.dbtool.orm.api.IEntityManager;
import ru.kwanza.dbtool.orm.api.If;
import ru.kwanza.dbtool.orm.api.OrderBy;
import ru.kwanza.txn.api.Transactional;
import ru.kwanza.txn.api.TransactionalType;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @author Dmitry Zhukov
 */

@Controller
@RequestMapping("accountrecord")
public class AccountRecordController {

    @Resource(name = "dbtool.IEntityManager")
    private IEntityManager manager;

    @RequestMapping(value = "read", method = RequestMethod.GET)
    @ModelAttribute("data")
    @Transactional(TransactionalType.REQUIRES_NEW)
    @AuthOperation(name = "View accoutn operations", check = "hasPermission('AccountRecord:read')", auditable = false)
    public List<AccountRecord> getList(@RequestParam(value = "start", required = false) Integer start,
                                       @RequestParam(value = "limit", required = false) Integer limit,
                                       @RequestParam(value = "id", required = false) Long id,
                                       @RequestParam(value = "filter.id", required = false) Long filterId,
                                       @RequestParam(value = "filter.accountId", required = false) String accountId,
                                       @RequestParam(value = "filter.orderId", required = false) Long orderId,
                                       @RequestParam(value = "filter.processedAt.from", required = false) @DateTimeFormat(
                                               pattern = "dd.MM.yyyy HH:mm:ss") Date processedAtFrom,
                                       @RequestParam(value = "filter.processedAt.to", required = false) @DateTimeFormat(
                                               pattern = "dd.MM.yyyy HH:mm:ss") Date processedAtTo) {

        return manager.filtering(AccountRecord.class)
                .filter(id != null, If.isEqual("id"), id)
                .filter(filterId != null, If.isEqual("id"), filterId)
                .filter(accountId != null, If.or(If.isEqual("sourceAccountId"), If.isEqual("targetAccountId")), new String[]{accountId, accountId})
                .filter(processedAtFrom != null, If.isGreaterOrEqual("processedAt"), processedAtFrom)
                .filter(processedAtTo != null, If.isLessOrEqual("processedAt"), processedAtTo)
                .filter(orderId != null, If.isEqual("orderId"), orderId)
                .orderBy(OrderBy.DESC("processedAt"))
                .paging(start, limit)
                .selectList();
    }
}
