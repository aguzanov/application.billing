Ext.define('Billing.view.order.DetailOrderList', {
    requires: [
        'Console.grid.FiltersFeature',
        'Console.application.QuickLink'
    ],
    extend: 'Console.grid.BaseGrid',
    alias: ['widget.order-list', 'widget.orderlist'],
    model: 'Billing.model.DetailOrder',
    store: 'Billing.store.DetailOrderStore',

    tabTitle: 'Billing orders',
    caption: 'Billing orders',
    iconCls: 'i-backup-restore',


    constructor: function (config) {
        var me = this;

        me.columns = [
            {
                text: 'ID',
                dataIndex: 'id',
                width: 100,
                renderer: Billing.model.DetailOrder.orderRenderer,
                filter: true
            },
            {
                text: 'Type',
                dataIndex: 'type',
                width: 150,
                filter: {
                    width: 200,
                    labelWidth: 30,
                    showIcons: true,
                    store: {
                        type: 'composite',
                        source: Ext.getStore('Billing.store.lookup.OrderType')
                    }
                },
                renderer: Billing.model.DetailOrder.orderTypeRenderer
            },
            {
                text: 'Session',
                dataIndex: "sessionId",
                renderer: Billing.model.Order.sessionRenderer,
                width: 200,
                filter: false
            },
            {
                text: 'Exchange',
                renderer: Billing.model.Order.exchangeRenderer,
                width: 200,
                filter: false
            },
            {
                xtype: 'datecolumn',
                format: 'd.m.Y H:i:s',
                text: 'Processed at',
                dataIndex: 'processedAt',
                width: 150,
                filter: {
                    allowBlank: true
                }
            },
            {
                text: 'Result',
                dataIndex: 'rejectCode',
                width: 150,
                filter: {
                    showIcons: true,
                    width: 250,
                    labelWidth: 40,
                    store: {
                        type: 'composite',
                        source: Ext.getStore('Billing.store.lookup.RejectCode'),
                        staticData: [
                            {id: -1, title: 'Successfully completed', iconCls: 'i-action-accept'}
                        ]
                    }
                },
                renderer: Billing.model.Order.rejectedCodeRenderer
            }
        ]

        me.callParent(arguments);
    }
});
