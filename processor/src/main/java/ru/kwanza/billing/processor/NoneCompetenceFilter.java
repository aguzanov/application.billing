package ru.kwanza.billing.processor;

import ru.kwanza.billing.api.order.Order;
import ru.kwanza.billing.api.report.RejectedRequestReport;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Map;

/**
 * @author Vasily Vorobyov
 */
public class NoneCompetenceFilter implements ICompetenceFilter<Order> {

    public Map<Long, RejectedRequestReport> filter(Date processedAt, Collection<Order> requests) {
        return Collections.emptyMap();
    }
}
