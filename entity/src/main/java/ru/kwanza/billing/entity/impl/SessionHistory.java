package ru.kwanza.billing.entity.impl;

import ru.kwanza.autokey.api.IAutoKey;
import ru.kwanza.billing.api.Session;
import ru.kwanza.billing.api.SessionId;
import ru.kwanza.billing.entity.api.SessionHistoryRecord;
import ru.kwanza.billing.entity.api.dao.ISessionHistoryDAO;
import ru.kwanza.dbtool.core.UpdateException;
import ru.kwanza.toolbox.Hex;

import javax.annotation.Resource;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

/**
 * @author henadiy
 */
public class SessionHistory {

    private static class SimpleLRUCache<K, V> extends LinkedHashMap<K, V> {
        SimpleLRUCache() {
            super(1, 0.75f, true);
        }

        @Override
        protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
            return size() > 10000;
        }
    }

    private static final Map<SessionId, String> keysCache = Collections.synchronizedMap(new SimpleLRUCache<SessionId, String>());
    private static final Map<String, SessionHistoryRecord> recordsCache = Collections.synchronizedMap(new SimpleLRUCache<String, SessionHistoryRecord>());

    @Resource(name = "autokey.IAutoKey")
    private IAutoKey autoKey;

    private ISessionHistoryDAO sessionHistoryDAO;
    private MessageDigest sha512Digester;
    private String sessionKeyPrefix;

    public void setSessionHistoryDAO(ISessionHistoryDAO sessionHistoryDAO) {
        this.sessionHistoryDAO = sessionHistoryDAO;
    }

    public void setSessionKeyPrefix(String sessionKeyPrefix) {
        this.sessionKeyPrefix = sessionKeyPrefix;
    }

    public Map<SessionId, SessionHistoryRecord> register(Collection<Session> sessions) {
        return updateSessionHistory(sessions, false);
    }

    public Map<SessionId, SessionHistoryRecord> registerClosed(Collection<Session> sessions) {
        return updateSessionHistory(sessions, true);
    }


    public Map<SessionId, SessionHistoryRecord> getHistoryRecords(Collection<SessionId> sessionIds) {
        Map<SessionId, SessionHistoryRecord> res = new HashMap<SessionId, SessionHistoryRecord>(sessionIds.size());

        // prepared by the keys to update
        Map<String, SessionId> sessionKeys = new HashMap<String, SessionId>(sessionIds.size());
        for (SessionId sessionId : sessionIds) {
            String sessionKey = getSessionKey(sessionId);
            // if there is no record in the cache
            if (!recordsCache.containsKey(sessionKey)) {
                sessionKeys.put(sessionKey, sessionId);
            } else {
                res.put(sessionId, recordsCache.get(sessionKey));
            }
        }

        // updated records from the database and update the cache
        Map<String, SessionHistoryRecord> records = sessionHistoryDAO.readBySessionKeys(sessionKeys.keySet());
        recordsCache.putAll(records);

        // prepare the result and fill the list of unregistered sessions
        Set<Session> notRegisteredSessions = new HashSet<Session>();
        for (Map.Entry<String, SessionId> entry : sessionKeys.entrySet()) {
            if (records.containsKey(entry.getKey())) {
                res.put(entry.getValue(), records.get(entry.getKey()));
            } else {
                notRegisteredSessions.add(new Session(entry.getValue(), null, null, Collections.EMPTY_SET));
            }
        }

        // if there is an unknown session for the record
        if (!notRegisteredSessions.isEmpty()) {
            Map<SessionId, SessionHistoryRecord> recordsForNotRegisteredSessions = register(notRegisteredSessions);
            for (SessionHistoryRecord rec : recordsForNotRegisteredSessions.values()) {
                recordsCache.put(rec.getSessionKey(), rec);
            }
            res.putAll(recordsForNotRegisteredSessions);
        }

        return res;
    }

    private Map<SessionId, SessionHistoryRecord> updateSessionHistory(Collection<Session> sessions, boolean closed) {
        Map<SessionId, SessionHistoryRecord> res = new HashMap<SessionId, SessionHistoryRecord>(sessions.size());

        // key generation sessions
        Map<SessionId, String> sessionKeys = new HashMap<SessionId, String>(sessions.size());
        for (Session s : sessions) {
            sessionKeys.put(s.getId(), getSessionKey(s.getId()));
        }

        // search records in database
        Map<String, SessionHistoryRecord> records = sessionHistoryDAO.readBySessionKeys(sessionKeys.values());
        List<SessionHistoryRecord> recordsForCreate = new ArrayList<SessionHistoryRecord>(sessionKeys.size());
        List<SessionHistoryRecord> recordsForUpdate = new ArrayList<SessionHistoryRecord>(sessionKeys.size());

        // current date
        Date now = new Date();

        if (closed) {
            for (Session session : sessions) {
                String sessionKey = sessionKeys.get(session.getId());
                // only process existing closed session
                if (records.containsKey(sessionKey)) {
                    SessionHistoryRecord rec = records.get(sessionKey);
                    if (null == rec.getUserId() && null != session.getUserId()) {
                        rec.setUserId(session.getUserId());
                    }
                    rec.setClosedAt(now);

                    recordsForUpdate.add(rec);
                    res.put(session.getId(), rec);
                }
            }
        } else {
            // processed all sessions
            for (Session session : sessions) {
                String sessionKey = getSessionKey(session.getId());
                SessionHistoryRecord rec = records.get(sessionKey);
                if (null == rec) {
                    rec = new SessionHistoryRecord(
                            autoKey.getNextValue(SessionHistoryRecord.class.getName()), sessionKey, now);
                    recordsForCreate.add(rec);
                } else {
                    recordsForUpdate.add(rec);
                }

                // update the fields of session
                if (null == rec.getExpiresIn() && null != session.getExpiresIn()) {
                    rec.setExpiresIn(session.getExpiresIn());
                }
                if (null == rec.getUserId() && null != session.getUserId()) {
                    rec.setUserId(session.getUserId());
                }

                res.put(session.getId(), rec);
            }
        }

        if (!recordsForCreate.isEmpty()) {
            try {
                sessionHistoryDAO.create(recordsForCreate);
            } catch (UpdateException e) {
                throw new RuntimeException("Unable to create session history records", e);
            }
        }

        if (!recordsForUpdate.isEmpty()) {
            try {
                sessionHistoryDAO.update(recordsForUpdate);
            } catch (UpdateException e) {
                throw new RuntimeException("Unable to update session history records", e);
            }
        }

        return res;
    }

    private String getSessionKey(SessionId sessionId) {
        if (!keysCache.containsKey(sessionId)) {
            keysCache.put(sessionId, sessionId.getToken());
        }
        return keysCache.get(sessionId);
    }

    private MessageDigest getSha512Digester() {
        if (null == sha512Digester) {
            try {
                sha512Digester = MessageDigest.getInstance("SHA-512");
            } catch (NoSuchAlgorithmException e) {
                throw new RuntimeException(e);
            }
        }
        return sha512Digester;
    }

}
