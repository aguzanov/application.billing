package ru.kwanza.billing.entity.api;

import ru.kwanza.console.security.api.AuthEntity;
import ru.kwanza.console.security.api.AuthField;
import ru.kwanza.console.security.api.AuthIdField;
import ru.kwanza.dbtool.orm.annotations.Entity;
import ru.kwanza.dbtool.orm.annotations.Field;
import ru.kwanza.dbtool.orm.annotations.IdField;
import ru.kwanza.dbtool.orm.annotations.VersionField;

import java.io.Serializable;
import java.sql.Types;

/**
 * @author Vasily Vorobyov
 */
@Entity(table = "currency")
@AuthEntity(name = "Currency", description = "Currency")
public class Currency implements Serializable {

    public static final int MAX_ID = 9999;

    @AuthIdField
    @AuthField(name = "ID")
    @IdField(value = "id", type = Types.INTEGER)
    private Integer id;
    
    @VersionField("version")
    private Long version;

    @AuthField(name = "Alpha code")
    @Field("alpha_code")
    private String alphaCode;

    @AuthField(name = "Name")
    @Field("title")
    private String title;

    @AuthField(name = "Exponent")
    @Field("exponent")
    private Integer exponent;

    public Currency() {
    }

    public Currency(Integer id, String alphaCode, String title, Integer exponent) {
        if (!checkId(id)) {
            throw new IllegalArgumentException("id is out of range (0.." + MAX_ID + "]");
        }
        this.id = id;
        this.alphaCode = alphaCode;
        this.title = title;
        this.exponent = exponent;
    }

    public Integer getId() {
        return id;
    }

    public String getIdAsString() {
        return String.format("%04d", id);
    }

    public String getAlphaCode() {
        return alphaCode;
    }

    public String getTitle() {
        return title;
    }

    public Integer getExponent() {
        return exponent;
    }

    public void setAlphaCode(String alphaCode) {
        this.alphaCode = alphaCode;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setExponent(Integer exponent) {
        this.exponent = exponent;
    }

    public static boolean checkId(Integer id) {
        return (0 < id && id <= MAX_ID);
    }

    @Override
    public String toString() {
        return title;
    }
}
