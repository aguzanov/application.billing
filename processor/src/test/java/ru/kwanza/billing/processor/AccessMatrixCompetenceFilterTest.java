package ru.kwanza.billing.processor;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import ru.kwanza.billing.api.Session;
import ru.kwanza.billing.api.SessionId;
import ru.kwanza.billing.api.query.CurrenciesQuery;
import ru.kwanza.billing.api.report.RejectedRequestReport;
import ru.kwanza.billing.processor.session.TestSessionManager;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author henadiy
 */
@RunWith(JUnit4.class)
public class AccessMatrixCompetenceFilterTest {

    class TestImpl extends AccessMatrixCompetenceFilter<CurrenciesQuery> {

        public Map<Long, RejectedRequestReport> filter(Date processedAt, Collection<CurrenciesQuery> requests) {
            return null;
        }
    }

    @Test
    public void testGetSessions() throws Exception {

        TestSessionManager manager = new TestSessionManager();
        manager.putSession(new Session(new SessionId("123"), new Date(), "user123", new HashSet<String>(Arrays.asList("group1"))));
        manager.putSession(new Session(new SessionId("456"), new Date(), "user456", new HashSet<String>(Arrays.asList("group2"))));
        manager.putSession(new Session(new SessionId("678"), new Date(), "user678", new HashSet<String>(Arrays.asList("group1"))));

        TestImpl impl = new TestImpl();
        impl.setSessionManager(manager);

        Map<SessionId, Session> res = impl.getSessions(Arrays.asList(
                new CurrenciesQuery(1L, new SessionId("777")),
                new CurrenciesQuery(2L, new SessionId("123")),
                new CurrenciesQuery(3L, new SessionId("678"))
        ));

        assertEquals(2, res.size());
        assertEquals(new SessionId("123"), res.get(new SessionId("123")).getId());
        assertEquals(new SessionId("678"), res.get(new SessionId("678")).getId());
    }


    @Test
    public void testIsSessionValid() throws Exception {

        Date now = new Date();

        Session s1 = new Session(new SessionId("123"), now, "user123", new HashSet<String>(Arrays.asList("group1")));
        Session s2 = new Session(new SessionId("456"), null, "user456", new HashSet<String>(Arrays.asList("group1")));

        TestImpl impl = new TestImpl();

        assertFalse(impl.isSessionValid(null, now));
        assertFalse(impl.isSessionValid(s1, new Date(now.getTime() + 1000)));
        assertTrue(impl.isSessionValid(s2, new Date(now.getTime() + 1000)));
        assertTrue(impl.isSessionValid(s1, now));
        assertTrue(impl.isSessionValid(s1, new Date(now.getTime() - 1000)));
    }


    @Test
    public void testBuildPermission() throws Exception {

        TestImpl impl = new TestImpl();

        assertEquals("CurrenciesQuery:create",
                impl.buildPermission(new CurrenciesQuery(1L, new SessionId("tok")), "create"));
        assertEquals("CurrenciesQuery:test",
                impl.buildPermission(new CurrenciesQuery(1L, new SessionId("tok")), "test"));

    }
}
