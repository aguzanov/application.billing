package ru.kwanza.billing.api.report;

import com.fasterxml.jackson.annotation.JsonTypeName;

import java.util.Date;

/**
 * @author Dmitry Zhukov
 */
@JsonTypeName(value = "WALLET_CHANGE_STATUS_REPORT")
public class WalletChangeStatusReport extends Report {

    private Long walletId;

    public WalletChangeStatusReport(Long id, Date processedAt, Long walletId) {
        super(id, processedAt);
        this.walletId = walletId;
    }

    public Long getWalletId() {
        return walletId;
    }
}
