package ru.kwanza.billing.processor;

import ru.kwanza.billing.entity.api.dao.IWalletDAO;
import ru.kwanza.billing.entity.api.Wallet;
import ru.kwanza.billing.mock.SyncMaps;
import ru.kwanza.dbtool.core.UpdateException;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Vasily Vorobyov
 */
public class MockWalletDAO implements IWalletDAO {

    private SyncMaps<Long, Wallet> wallets = new SyncMaps<Long, Wallet>(new SyncMaps.KeyExtractor<Long, Wallet>() {
        public Long extractKey(Wallet wallet) {
            return wallet.getId();
        }
    });

    public MockWalletDAO() {
        wallets.addUniqueIndex("orderId", new SyncMaps.IndexExtractor<Wallet>() {
            public Object extractKey(Wallet wallet) {
                return wallet.getOrderId();
            }
        });
    }

    public List<Wallet> readAll() {
        return null;
    }

    public Map<Long, Wallet> readByKeys(Collection<Long> ids) {
        return wallets.readByKey(ids);
    }

    public Map<Long, Wallet> readByOrderIds(Collection<Long> orderIds) {
        return wallets.readByUniqueIndex("orderId", orderIds);
    }

    public Wallet prepare(Integer counter, Long orderId, Date createdAt, String holderId, String title) {
        return new Wallet(counter, orderId, createdAt, holderId, title);
    }

    public Collection<Wallet> create(Collection<Wallet> wallets) throws UpdateException {
        this.wallets.createUpdate(wallets);
        return wallets;
    }

    public void update(Collection<Wallet> wallets) throws UpdateException {
        this.wallets.createUpdate(wallets);
    }
}
