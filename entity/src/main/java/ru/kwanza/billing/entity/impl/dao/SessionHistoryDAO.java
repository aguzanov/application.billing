package ru.kwanza.billing.entity.impl.dao;

import ru.kwanza.autokey.api.IAutoKey;
import ru.kwanza.billing.entity.api.SessionHistoryRecord;
import ru.kwanza.billing.entity.api.dao.ISessionHistoryDAO;
import ru.kwanza.dbtool.core.UpdateException;
import ru.kwanza.dbtool.orm.api.IEntityManager;
import ru.kwanza.dbtool.orm.api.IQuery;
import ru.kwanza.dbtool.orm.api.If;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

/**
 * @author henadiy
 */
public class SessionHistoryDAO implements ISessionHistoryDAO {

    @Resource(name = "dbtool.IEntityManager")
    private IEntityManager em;

    private IQuery<SessionHistoryRecord> queryBySessionIds;

    public void init() {
        queryBySessionIds = em.queryBuilder(SessionHistoryRecord.class).where(If.in("sessionKey", "sessionKeys")).create();
    }

    public Collection<SessionHistoryRecord> create(Collection<SessionHistoryRecord> records) throws UpdateException {
        return em.create(SessionHistoryRecord.class, records);
    }

    public Collection<SessionHistoryRecord> update(Collection<SessionHistoryRecord> records) throws UpdateException {
        return em.update(SessionHistoryRecord.class, records);
    }

    public Map<Long, SessionHistoryRecord> readByIds(Collection<Long> ids) {
        return em.readMapByKeys(SessionHistoryRecord.class, ids);
    }

    public Map<String, SessionHistoryRecord> readBySessionKeys(Collection<String> sessionKeys) {
        return queryBySessionIds.prepare().setParameter("sessionKeys", sessionKeys).selectMap("sessionKey");
    }
}
