/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.view.order.walletissueorder.WalletIssueOrderCard', {
    extend: 'Billing.view.order.OrderCard',

    alias: 'widget.walletissueorderderdetails',

    title: 'Register wallet',
    iconCls: 'i-document-test',

    model: 'Billing.model.WalletIssueOrder',
    store: 'Billing.store.WalletIssueOrderStore',

    constructor: function (config) {
        var me = this;
        me.items = [
            {
                title: 'Additional parameters of the order',
                columnWidth: 1,
                items: [
                    {
                        fieldLabel: 'Wallet',
                        renderer: function (value, meta, record) {
                            var wallet = record.get('wallet');
                            var rejectCode = record.get('rejectCode');
                            if (wallet) {
                                return Console.application.QuickLink.createLink(wallet.title,
                                    'Billing.view.wallet.WalletCard', wallet.id);
                            }
                            return record.get('title');
                        }
                    }
                ]
            }
        ];
        me.callParent(arguments);
        me.items.get('main').columnWidth = 1;
    }
});
