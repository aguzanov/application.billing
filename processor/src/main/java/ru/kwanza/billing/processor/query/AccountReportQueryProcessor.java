package ru.kwanza.billing.processor.query;

import ru.kwanza.billing.api.query.AccountReportQuery;
import ru.kwanza.billing.api.report.AccountReport;
import ru.kwanza.billing.api.report.RejectCode;
import ru.kwanza.billing.api.report.RejectedRequestReport;
import ru.kwanza.billing.api.report.Report;
import ru.kwanza.billing.entity.api.Account;
import ru.kwanza.billing.entity.api.dao.IAccountDAO;
import ru.kwanza.billing.processor.RequestProcessor;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author Vasily Vorobyov
 */
public class AccountReportQueryProcessor extends RequestProcessor<AccountReportQuery> {

    @Resource(name = "billing.entity.dao.AccountDAO")
    private IAccountDAO accountDAO;

    @Override
    protected Map<Long, Report> handleRequests(Date processedAt, Collection<AccountReportQuery> requests) {

        Map<Long, Report> reports = new HashMap<Long, Report>(requests.size());
        Set<BigDecimal> accountIds = new HashSet<BigDecimal>(requests.size());

        for (AccountReportQuery query: requests) {
            if (null == query.getAccountId()) {
                reports.put(query.getId(), new RejectedRequestReport(query.getId(), processedAt, RejectCode.EMPTY_REQUIRED_FIELD));
            } else {
                accountIds.add(query.getAccountId());
            }
        }

        Map<BigDecimal, Account> accounts = accountDAO.readByKeys(accountIds);

        for (AccountReportQuery query: requests) {
            if (null != query.getAccountId()) {
                Account account = accounts.get(query.getAccountId());
                Report report;
                if (null != account) {
                    report = new AccountReport(query.getId(), processedAt, account.getId(), account.getCurrencyId(), account.getCreatedAt(),
                            account.getOrderId(), account.getBalance(), account.getMinBalance(), account.getMaxBalance(),
                            account.getStatus(), account.getLastRecordNumber(), account.getLastRecordAt());
                } else {
                    report = new RejectedRequestReport(query.getId(), processedAt, RejectCode.ACCOUNT_NOT_FOUND);
                }
                reports.put(query.getId(), report);
            }
        }

        return reports;
    }

    @Override
    protected Map<Long, Report> answerDuplicateRequests(Map<Long, AccountReportQuery> duplicateRequests) {
        return handleRequests(new Date(), duplicateRequests.values());
    }
}
