package ru.kwanza.billing.entity.api.id;

import ru.kwanza.billing.api.PaymentToolId;

import java.math.BigDecimal;

/**
 * @author henadiy
 */
public class PaymentToolACEIdBuilder {

	private final String id;
	private final PaymentToolId paymentToolId;
	private final String userId;

	public PaymentToolACEIdBuilder(String id) {
		if (null == id) {
			throw new IllegalArgumentException("Id must not be null");
		}

		String[] elements = id.split(":");
		this.paymentToolId = new PaymentToolId(new BigDecimal(elements[1]),
				"WA".equals(elements[0]) ? PaymentToolId.Type.WALLET_ACCOUNT : PaymentToolId.Type.ACCOUNT);
		this.userId = elements[2];
		this.id = id;
	}

	public PaymentToolACEIdBuilder(PaymentToolId paymentToolId, String userId) {
		this.id = new StringBuilder()
				.append(PaymentToolId.Type.WALLET_ACCOUNT.equals(paymentToolId.getType()) ? "WA" : "AC").append(":")
				.append(paymentToolId.getId()).append(":")
				.append(userId).toString();
		this.paymentToolId = paymentToolId;
		this.userId = userId;
	}

	public String getId() {
		return id;
	}

	public PaymentToolId getPaymentToolId() {
		return paymentToolId;
	}

	public String getUserId() {
		return userId;
	}
}
