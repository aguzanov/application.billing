/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.model.Issuer', {
    extend: 'Ext.data.Model',
    fields: [
        'id',
        'cachedId',
        'version',
        'orderId',
        {
            name: "order",
            persist: false
        },
        'bin',
        'title',
        'accountCounter'

    ],
    entityClassName: 'Issuer',

    detailView: 'Billing.view.issuer.IssuerCard',

    permissions: {
        read: "Issuer:read",
        create: "Issuer:create",
        update: "Issuer:update",
        destroy: "Issuer:delete"
    },

    proxy: {
        type: 'ajax',
        apiUrl: 'billing-controller/issuer/'
    },

    statics: {
        orderRenderer: function (value, meta, record) {
            var order = record.get('order');
            return Console.application.QuickLink.createLink("ID: " + order.id + ", processed at: " + order.processedAt,
                'Billing.view.order.registerissuerorder.RegisterIssuerOrderCard', {recordId: order.id});
        }
    }
});
