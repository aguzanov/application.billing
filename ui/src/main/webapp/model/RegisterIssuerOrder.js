/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.model.RegisterIssuerOrder', {
    extend: 'Billing.model.Order',
    fields: [
        'issuerId',
        {
            name: 'issuer',
            persist: false
        },
        'bin',
        'title'
    ],

    detailView: 'Billing.view.order.registerissuerorder.RegisterIssuerOrderCard',

    permissions: {
        read: "RegisterIssuerOrder:read",
        create: false,
        update: false,
        destroy: false
    },

    proxy: {
        type: 'ajax',
        apiUrl: 'billing-controller/registerissuerorder/'

    },

    statics: {
        issuerRenderer: function (value, meta, record) {
            var status = record.get('rejectCode');
            var issuer = record.get('issuer');
            if (status || !issuer) {
                return record.get('bin');
            }

            return Console.application.QuickLink.createLink(record.get('bin'),
                'Billing.view.issuer.IssuerCard',  {recordId: issuer.id});
        }
    }
});
