package ru.kwanza.billing.api.query;

import com.fasterxml.jackson.annotation.JsonTypeName;
import ru.kwanza.billing.api.RequestType;
import ru.kwanza.billing.api.SessionId;

import java.math.BigDecimal;

/**
 * @author Vasily Vorobyov
 */
@JsonTypeName(value = "Q_ACCOUNT")
public class AccountReportQuery extends Query implements IAccountInfoQuery {

    private BigDecimal accountId;

    public AccountReportQuery() {

    }

    public AccountReportQuery(Long id, SessionId sessionId, BigDecimal accountId) {
        super(id, sessionId);
        this.accountId = accountId;
    }

    public BigDecimal getAccountId() {
        return accountId;
    }

    @Override
    public RequestType getRequestType() {
        return RequestType.Q_ACCOUNT;
    }
}
