package ru.kwanza.billing.processor.signer;

import org.springframework.util.Assert;
import ru.kwanza.toolbox.Hex;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author henadiy
 */
public class MD5SignatureGenerator implements ISignatureGenerator {

    private String secretKey;

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getSignature(byte[] data) {
        Assert.notNull(secretKey, "SecretKey must be defined.");
        Assert.notNull(data, "Data must be defined.");
        Assert.isTrue(data.length > 0, "Data must not be empty.");

        try {
            byte[] secretKeyData = null == secretKey ? new byte[0] : secretKey.getBytes();
            byte[] buf = new byte[data.length + secretKeyData.length];
            System.arraycopy(data, 0, buf, 0, data.length);
            System.arraycopy(secretKeyData, 0, buf, data.length, secretKeyData.length);

            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(buf);
            return Hex.bytesToHex(md.digest());
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException(e);
        }
    }
}
