package ru.kwanza.billing.clearing;

import ru.kwanza.billing.clearing.dao.IClearingDAO;
import ru.kwanza.billing.clearing.entity.ClearingAccount;
import ru.kwanza.billing.clearing.entity.ClearingGroup;
import ru.kwanza.billing.entity.api.AccountRecord;
import ru.kwanza.dbtool.core.UpdateException;

import java.math.BigDecimal;
import java.util.*;

/**
 * @author henadiy
 */
public class MockClearingDAO implements IClearingDAO {

    private Collection<AccountRecord> accountRecords = new LinkedList<AccountRecord>();
	private Map<BigDecimal, ClearingAccount> clearingAccounts = new HashMap<BigDecimal, ClearingAccount>();
    private Map<Long, ClearingGroup> clearingGroups = new HashMap<Long, ClearingGroup>();


	public Collection<ClearingAccount> readClearingGroupAccounts(ClearingGroup clearingGroup) {
		return clearingAccounts.values();
	}


	public Collection<AccountRecord> readAccountRecordsByClearedRecordNumberAndPeriod(
			BigDecimal accountId, Integer recordNumber, Date to) {
		Collection<AccountRecord> res = new LinkedList<AccountRecord>();
		for(AccountRecord r : accountRecords) {
			if (null != r.getSourceAccountId() && null != r.getTargetAccountId() && to.after(r.getProcessedAt()) &&
					(r.getSourceAccountId().equals(accountId) && r.getSourceRecordNumber() > recordNumber ||
							r.getTargetAccountId().equals(accountId) && r.getTargetRecordNumber() > recordNumber)) {
				res.add(r);
			}
		}
		return res;
	}


    public void addAccountRecords(AccountRecord...records) {
        accountRecords.addAll(Arrays.asList(records));
    }


    public Collection<ClearingAccount> createClearingAccounts(Collection<ClearingAccount> clearingAccounts) throws UpdateException {
        for(ClearingAccount a : clearingAccounts) {
            this.clearingAccounts.put(a.getAccountId(), a);
        }
        return clearingAccounts;
    }

    public Collection<ClearingAccount> updateClearingAccounts(Collection<ClearingAccount> clearingAccounts) throws UpdateException {
        for(ClearingAccount a : clearingAccounts) {
            this.clearingAccounts.put(a.getAccountId(), a);
        }
        return clearingAccounts;
    }

    public Map<BigDecimal, ClearingAccount> getClearingAccounts() {
        return clearingAccounts;
    }


    public Collection<ClearingGroup> readClearingGroups() {
        return clearingGroups.values();
    }

    public Collection<ClearingGroup> updateClearingGroups(Collection<ClearingGroup> clearingGroups) throws UpdateException {
        for(ClearingGroup g : clearingGroups) {
            this.clearingGroups.put(g.getId(), g);
        }
        return clearingGroups;
    }

    public Collection<ClearingGroup> createClearingGroups(Collection<ClearingGroup> clearingGroups) throws UpdateException {
        for(ClearingGroup g : clearingGroups) {
            this.clearingGroups.put(g.getId(), g);
        }
        return clearingGroups;
    }
}
