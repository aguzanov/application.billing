Ext.Loader.setConfig({
    enabled: true,
    paths: {
        Console: 'console-ui/js',
        Billing: 'billing-ui'
    }
});

Ext.application({
    requires: [
        'Console.application.ConsolePanel',
        'Console.application.QuickLink',
        'Console.configuration.ConfigurationManager',
        'Console.override.OverrideClasses',
        'Billing.store.LookupStores'
    ],
    name: 'COTT',
    controllers: [
        'Console.menu.MenuController',
        'Console.application.EntityPermissionController',
        'Console.application.HelpLinkController',
        'Console.audit.AuditController',

        'Billing.controller.CurrencyController',
        'Billing.controller.AccountController',
        'Billing.controller.IssuerController',
        'Billing.controller.WalletController',
        'Billing.controller.WalletAccountController',
        'Billing.controller.OrderController'
    ],

    launch: function () {
        Ext.QuickTips.init();
        Console.application.QuickLink.init();

        Ext.create('Ext.container.Viewport', {
            layout: 'fit',
            items: [
                {
                    xtype: 'console',
                    layout: 'fit',
                    applicationTitle: 'Billing system',
                    applicationSubTitle: 'Highload transaction processing'
                }
            ]
        });
    }
});
