package ru.kwanza.billing.tariffication;

/**
 * @author henadiy
 */
public abstract class AbstractResult<A extends IArguments> implements IResult<A> {

    private final ResultCode resultCode;
    private final A arguments;

    public AbstractResult(final A arguments) {
        this(arguments, ResultCode.SUCCESS);
    }

    public AbstractResult(final A arguments, final ResultCode resultCode) {
        this.resultCode = resultCode;
        this.arguments = arguments;
    }

    public ResultCode getResultCode() {
        return resultCode;
    }

    public A getArguments() {
        return arguments;
    }

    public boolean isSuccess() {
        return ResultCode.SUCCESS.equals(getResultCode()) ||
                ResultCode.SUCCESS_WITH_CORRECTIONS.equals(getResultCode());
    }
}
