package ru.kwanza.billing.processor.query;

import ru.kwanza.billing.api.PaymentToolId;
import ru.kwanza.billing.api.order.ConversionInstruction;
import ru.kwanza.billing.api.query.ConversionInstructionQuery;
import ru.kwanza.billing.api.report.ConversionInstructionReport;
import ru.kwanza.billing.api.report.RejectCode;
import ru.kwanza.billing.api.report.RejectedRequestReport;
import ru.kwanza.billing.api.report.Report;
import ru.kwanza.billing.entity.api.ConversionRate;
import ru.kwanza.billing.entity.api.dao.IConversionRateDAO;
import ru.kwanza.billing.entity.api.id.AccountIdBuilder;
import ru.kwanza.billing.entity.api.id.WalletAccountIdBuilder;
import ru.kwanza.billing.processor.RequestProcessor;
import ru.kwanza.billing.processor.signer.Signer;
import ru.kwanza.billing.tariffication.AmountResult;
import ru.kwanza.billing.tariffication.calculator.LinearCalculator;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author henadiy
 */
public class ConversionInstructionQueryProcessor extends RequestProcessor<ConversionInstructionQuery> {

    @Resource(name = "billing.entity.dao.ConversionRateDAO")
    private IConversionRateDAO conversionRateDAO;
    @Resource(name = "billing.processor.signer.ConversionInstructionSigner")
    private Signer<ConversionInstruction> conversionInstructionSigner;


    @Override
    protected Map<Long, Report> handleRequests(Date processedAt, Collection<ConversionInstructionQuery> requests) {

        Map<Long, Report> reports = new HashMap<Long, Report>(requests.size());
        Map<Long, Integer> currencyPairs = new HashMap<Long, Integer>(requests.size());

        // prepare data for sampling
        Iterator<ConversionInstructionQuery> queryIterator = requests.iterator();
        while (queryIterator.hasNext()) {
            ConversionInstructionQuery query = queryIterator.next();

            // add a pair of currencies
            Integer[] currencyIds = new Integer[]{query.getSourceCurrencyId(), query.getTargetCurrencyId()};
            PaymentToolId[] toolIds = new PaymentToolId[]{query.getSourceToolId(), query.getTargetToolId()};
            for(int i = 0; i < toolIds.length; i++) {
                if (null == toolIds[i]) continue;
                if (PaymentToolId.Type.ACCOUNT.equals(toolIds[i].getType())) {
                    currencyIds[i] = new AccountIdBuilder(toolIds[i].getId()).getCurrencyId();
                } else if (PaymentToolId.Type.WALLET_ACCOUNT.equals(toolIds[i].getType())) {
                    currencyIds[i] = new WalletAccountIdBuilder(toolIds[i].getId()).getCurrencyId();
                }
            }

            // check the request
            RejectCode rejectCode = null;
            if (query.getSourceAmount() == query.getTargetAmount() ||
                    null != query.getSourceAmount() && query.getSourceAmount().equals(query.getTargetAmount())) {
                rejectCode = RejectCode.ILLEGAL_AMOUNT;
            } else if (null == currencyIds[0] || null == currencyIds[1]) {
                rejectCode = RejectCode.EMPTY_REQUIRED_FIELD;
            }

            // generated responses for rejected requests
            if (null != rejectCode) {
                reports.put(query.getId(),
                        new RejectedRequestReport(query.getId(), processedAt, rejectCode));
                queryIterator.remove();
                continue;
            }

            // save a couple
            currencyPairs.put(query.getId(), ConversionRate.buildCurrencyPair(currencyIds[0], currencyIds[1]));
        }

        // looking for exchange rates
        Map<Integer, ConversionRate> conversionRates =
                conversionRateDAO.readConversionRates(new HashSet<Integer>(currencyPairs.values()), processedAt);


        // prepare payment instructions for the conversion
        Date expiresIn = getExpiresIn(processedAt);
        for(ConversionInstructionQuery query : requests) {

            ConversionRate rate = conversionRates.get(currencyPairs.get(query.getId()));

            // reject the request if the currency pair not found
            if (null == rate) {
                reports.put(query.getId(),
                        new RejectedRequestReport(query.getId(), processedAt, RejectCode.CONVERSION_RATE_NOT_FOUND));
                continue;
            }

            // calculated amount depending on what value is set
            Long sourceAmount = null, targetAmount = null;
            if (null != query.getSourceAmount()) {
                LinearCalculator calculator =
                        new LinearCalculator(rate.getSourceScale(), rate.getTargetScale(), BigDecimal.ZERO);
                AmountResult<LinearCalculator.Arguments> res = calculator.calculate(
                        new LinearCalculator.Arguments(query.getSourceAmount()));
                if (res.isSuccess()) {
                    sourceAmount = res.getArguments().getVariableValue().longValue();
                    targetAmount = res.getAmount();
                }
            } else if (null != query.getTargetAmount()) {
                LinearCalculator calculator =
                        new LinearCalculator(rate.getTargetScale(), rate.getSourceScale(), BigDecimal.ZERO);
                AmountResult<LinearCalculator.Arguments> res = calculator.calculate(
                        new LinearCalculator.Arguments(query.getTargetAmount()));
                if (res.isSuccess()) {
                    sourceAmount = res.getAmount();
                    targetAmount = res.getArguments().getVariableValue().longValue();
                }
            }

            // create and sign the statement
            ConversionInstruction instruction = conversionInstructionSigner.sign(new ConversionInstruction(
                    query.getSessionId(), processedAt, expiresIn,
                    null == query.getSourceToolId() ? PaymentToolId.BLANK : query.getSourceToolId(),
                    sourceAmount, rate.getSourceCurrencyId(), rate.getSourceScale(),
                    null == query.getTargetToolId() ? PaymentToolId.BLANK : query.getTargetToolId(),
                    targetAmount, rate.getTargetCurrencyId(), rate.getTargetScale()
            ));

            // create a report
            reports.put(query.getId(),
                    new ConversionInstructionReport(query.getId(), processedAt, instruction));
        }

        return reports;
    }

    @Override
    protected Map<Long, Report> answerDuplicateRequests(Map<Long, ConversionInstructionQuery> duplicateRequests) {
        return handleRequests(new Date(), duplicateRequests.values());
    }

    protected Date getExpiresIn(Date current) {
        //TODO: get time from config
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(current);
        calendar.add(Calendar.HOUR_OF_DAY, 1);
        return calendar.getTime();
    }
}
