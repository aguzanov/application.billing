/**
 * @author Dmitry Zhukov
 */

Ext.define('Billing.view.account.AccountCard', {
    extend: 'Console.panel.CardPanel',
    requires: [
        'Billing.store.CurrencyStore',
        'Billing.store.IssuerStore',
        'Billing.store.AccountStatusStore',
        'Billing.view.order.paymentorder.AccountRecordsList'
    ],

    alias: 'widget.accountdetails',

    title: 'Account',
    iconCls: 'i-budget',

    defaultType: 'fieldset',
    defaults: {
        columnWidth: 0.5,
        margin: '5 5 4 5',
        defaultType: 'textfield',
        minWidth: 560,
        defaults: {
            labelWidth: 140,
            anchor: '100%'
        }
    },

    layout: {
        type: 'vbox',
        align: 'stretch',
        pack: 'start'
    },

    buttons: [
        {
            xtype: 'button',
            text: 'Close the account',
            itemId: 'closeAccount',
            disabled: true,
            iconCls: 'i-action-close'
        }
    ],

    model: 'Billing.model.Account',
    store: 'Billing.store.AccountStore',

    initComponent: function () {
        var me = this;
        me.mainPanel = Ext.create("Ext.form.Panel", {
            cardFieldContainer: true,
            layout: {
                type: 'hbox',
                align: 'stretch',
                pack: 'start'
            },

            defaults: {
                columnWidth: 0.5,
                margin: '5 5 4 5',
                defaultType: 'textfield',
                minWidth: 300,
                defaults: {
                    labelWidth: 140,
                    anchor: '100%'
                }
            },

            bodyStyle: 'background-color: #dfe8f6;',
            border: false,
            autoScroll: true,
            header: false,

            items: [
                {
                    xtype: "fieldset",
                    title: 'Main parameters',
                    itemId: 'main',
                    flex: 1,
                    autoScroll: true,
                    bodyStyle: 'background-color: #dfe8f6;',
                    items: [
                        {
                            fieldLabel: 'Account number',
                            itemId: 'accountId',
                            readOnly: true,
                            renderer: function (value, meta, record) {
                                if (record) {
                                    return record.get('id');
                                }
                                return '';

                            }
                        },
                        {
                            fieldLabel: 'Currency',
                            name: 'currencyId',
                            itemId: 'currencyId',
                            xtype: 'selectfield',
                            store: 'Billing.store.CurrencyStore'
                        },
                        {
                            fieldLabel: 'Issuer',
                            name: 'issuerId',
                            itemId: 'issuerId',
                            xtype: 'selectfield',
                            store: 'Billing.store.IssuerStore'
                        },
                        {
                            fieldLabel: 'Balance',
                            name: 'balance',
                            itemId: 'balance',
                            readOnly: true
                        },
                        {
                            xtype: 'infofield',
                            store: 'Billing.store.AccountStatusStore',
                            fieldLabel: 'Status',
                            name: 'statusCode',
                            itemId: 'statusCode',
                            showIcons: true,
                            readOnly: true
                        }
                    ]
                },
                {
                    title: 'Additional options',
                    itemId: 'additional',
                    xtype: "fieldset",
                    flex: 2,
                    items: [
                        {
                            fieldLabel: 'Issue date',
                            name: 'createdAt',
                            itemId: 'createdAt',
                            xtype: 'datefield',
                            format: 'd.m.Y H:i:s.u',
                            readOnly: true
                        },
                        {
                            fieldLabel: 'Issuered by order',
                            name: 'orderId',
                            itemId: 'orderId',
                            valueField: 'orderId',
                            renderer: Billing.model.Account.orderRenderer,
                            readOnly: true
                        },

                        {
                            fieldLabel: 'Min.balance',
                            xtype: 'numberfield',
                            name: 'minBalance',
                            itemId: 'minBalance',
                            renderer: function (value, meta, record) {
                                if (!value) {
                                    return 'Is not set';
                                }
                                return value;
                            },
                            listeners: {
                                change: function (minBalance) {
                                    var maxBalance = this.up('#additional').down('#maxBalance');
                                    maxBalance.setMinValue(minBalance.value);
                                    if (maxBalance.value == null || maxBalance.value < minBalance.value) {
                                        maxBalance.setValue(minBalance.value);
                                    }
                                }
                            }
                        },
                        {
                            fieldLabel: 'Max.balance',
                            xtype: 'numberfield',
                            name: 'maxBalance',
                            itemId: 'maxBalance',
                            renderer: function (value, meta, record) {
                                if (!value) {
                                    return 'Is not set';
                                }
                                return value;
                            }

                        }
                    ]
                }

            ]
        });

        me.items = [
            me.mainPanel,
            {
                xtype: "tabpanel",
                itemId: "accountReports",
                flex: 1,
                activeTab: 0,
                items: [
                    {
                        title: 'Wallets ',
                        itemId: 'walletaccount',
                        xtype: 'walletaccountlist',
                        itemId: 'walletaccountlist',
                        store: Ext.create('Billing.store.WalletAccountStore'),
                        parentOptions: {
                            accountRecord: ''
                        },
                        excludeFields: ['accountId']
                    },
                    {
                        title: 'Payment records',
                        itemId: 'accountrecords',
                        xtype: 'accountrecordslist',
                        store: Ext.create('Billing.store.AccountRecordStore')
                    }
                ]
            }
        ];
        me.callParent(arguments);
    },

    listeners: {
        changemode: function (mode) {
            var me = this, mainPanel = me.mainPanel;
            var createdAt = mainPanel.items.get('additional').items.get('createdAt');
            var orderId = mainPanel.items.get('additional').items.get('orderId');
            var balance = mainPanel.items.get('main').items.get('balance');
            var statusCode = mainPanel.items.get('main').items.get('statusCode');
            var currencyId = mainPanel.items.get('main').items.get('currencyId');
            var issuerId = mainPanel.items.get('main').items.get('issuerId');
            var accountId = mainPanel.items.get('main').items.get('accountId');

            var accountReports = me.items.get('accountReports');

            currencyId.setAllowBlank(mode != 'add');
            currencyId.setReadOnly(mode != 'add');
            issuerId.setAllowBlank(mode != 'add');
            issuerId.setReadOnly(mode != 'add');

            createdAt.hidden = mode == 'add';
            orderId.hidden = mode == 'add';
            balance.hidden = mode == 'add';
            statusCode.hidden = mode == 'add';
            accountId.hidden = mode == 'add';

            accountReports.hidden = mode == 'add';
        }
    },

    updateRecord: function (record) {
        var me = this,
            isClosed = record && (record.get('statusCode') == 3 || record.get('statusCode') == -1);
        if (record) {
            me.title = 'Account ' + record.get('id');
        }
        me.callParent(arguments);
        me.down('#closeAccount').setDisabled(record == null || isClosed);
        me.down('#edit').setDisabled(record == null || isClosed);

        if (record) {
            var walletaccountlist = me.down('#walletaccountlist');
            walletaccountlist.store.addFilter([new Ext.util.Filter({
                id: 'accountId',
                property: 'accountId',
                value: this.record.get('id')
            })], false);

            walletaccountlist.parentOptions.accountRecord = record;

            var accountrecords = me.down('#accountrecords');

            accountrecords.store.addFilter([new Ext.util.Filter({
                id: 'accountId',
                property: 'accountId',
                value: this.record.get('id')
            })], false);
        }
    }

});
