package ru.kwanza.billing.entity;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author Vasily Vorobyov
 */
public class SimpleTest {

    public static void main(String[] args) {
        new ClassPathXmlApplicationContext("classpath:billing-entity-test.xml");
    }
}
