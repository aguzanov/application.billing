/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.store.AccountRecordStore', {
    extend: 'Ext.data.Store',
    model: 'Billing.model.AccountRecord',
    autoLoad: true,
    remoteFilter: true,
    remoteSort: false,
    autoSync: false
});