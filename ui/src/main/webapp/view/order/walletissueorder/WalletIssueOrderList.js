/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.view.order.walletissueorder.WalletIssueOrderList', {
    extend: 'Billing.view.order.OrderList',

    requires: [
        'Billing.store.WalletIssueOrderStore'
    ],

    alias: ['widget.wallet-issue-order-list', 'widget.walletissueorderlist'],
    tabTitle: 'Register wallets orders',
    caption: 'Register wallets orders',

    model: 'Billing.model.WalletIssueOrder',
    store: 'Billing.store.WalletIssueOrderStore',
    iconCls: 'i-document-test',

    constructor: function () {
        var me = this;

        me.columns = [
            {
                text: 'Wallet',
                width: 200,
                renderer: function (value, meta, record) {
                    var wallet = record.get('wallet');
                    var rejectCode = record.get('rejectCode');
                    if (wallet) {
                        return Console.application.QuickLink.createLink(wallet.title,
                            'Billing.view.wallet.WalletCard', wallet.id);
                    }
                    return record.get('title');
                }
            }
        ];

        me.callParent(arguments);
    }
});
