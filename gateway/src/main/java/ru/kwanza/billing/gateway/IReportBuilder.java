package ru.kwanza.billing.gateway;

import ru.kwanza.billing.api.report.Report;

import java.io.IOException;

/**
 * @author Vasily Vorobyov
 */
public interface IReportBuilder {

    String getMimeType();

    String getEncoding();

    public byte[] build(Report... reports) throws IOException;
}
