package ru.kwanza.billing.entity.api;

import ru.kwanza.console.security.api.AuthEntity;
import ru.kwanza.console.security.api.AuthField;
import ru.kwanza.console.security.api.AuthIdField;
import ru.kwanza.dbtool.orm.annotations.Entity;
import ru.kwanza.dbtool.orm.annotations.Field;
import ru.kwanza.dbtool.orm.annotations.IdField;

import java.io.Serializable;
import java.util.Date;

/**
 * @author henadiy
 */
@Entity(table = "session_history_record")
@AuthEntity(name="SessionHistory", description = "Session")
public class SessionHistoryRecord implements Serializable {

    @IdField(value="id")
    @AuthIdField
    @AuthField(name="ID")
    private Long id;

    @Field("session_key")
    @AuthField(name = "token")
    private String sessionKey;

    @Field("opened_at")
    @AuthField(name = "Opened at")
    private Date openedAt;

    @Field("closed_at")
    @AuthField(name = "Closed at")
    private Date closedAt;

    @Field("expires_in")
    @AuthField(name = "Expired at")
    private Date expiresIn;

    @Field("user_id")
    @AuthField(name = "UserId")
    private String userId;


    protected SessionHistoryRecord() {

    }

    public SessionHistoryRecord(Long id, String sessionKey, Date openedAt) {
        this.id = id;
        this.sessionKey = sessionKey;
        this.openedAt = openedAt;
    }

    public Long getId() {
        return id;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public Date getOpenedAt() {
        return openedAt;
    }

    public Date getClosedAt() {
        return closedAt;
    }

    public void setClosedAt(Date closedAt) {
        this.closedAt = closedAt;
    }

    public Date getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(Date expiresIn) {
        this.expiresIn = expiresIn;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
