package ru.kwanza.billing.entity.api;

import ru.kwanza.billing.api.report.RejectedRequestReport;
import ru.kwanza.billing.api.report.Report;

import java.util.Collection;
import java.util.Map;

/**
 * The Registrar of the results of processing orders
 *
 * @author Vasily Vorobyov
 */
public interface IReportRegistrator<R extends Report> {

    void registerReports(Collection<R> reports);

    Map<Long, RejectedRequestReport> searchRejectedOrderReports(Collection<Long> requestIds);

    Collection<R> readOrderReports(Collection<Long> ids);
}
