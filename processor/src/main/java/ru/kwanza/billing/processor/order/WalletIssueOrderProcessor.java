package ru.kwanza.billing.processor.order;

import ru.kwanza.autokey.api.AutoKeyValueSequence;
import ru.kwanza.billing.api.report.Report;
import ru.kwanza.billing.api.order.WalletIssueOrder;
import ru.kwanza.billing.api.report.WalletAccountInfo;
import ru.kwanza.billing.api.report.WalletReport;
import ru.kwanza.billing.entity.api.Wallet;
import ru.kwanza.billing.entity.api.dao.IWalletDAO;
import ru.kwanza.billing.processor.RequestProcessor;
import ru.kwanza.dbtool.core.UpdateException;

import javax.annotation.Resource;
import java.util.*;

/**
 * @author Vasily Vorobyov
 */
public class WalletIssueOrderProcessor extends RequestProcessor<WalletIssueOrder> {

    @Resource(name = "billing.entity.dao.WalletDAO")
    private IWalletDAO walletDAO;

    @Override
    protected Map<Long, Report> handleRequests(Date processedAt, Collection<WalletIssueOrder> requests) {

        HashMap<Long, Report> orderReports = new HashMap<Long, Report>(requests.size());
        Collection<Wallet> wallets = new ArrayList<Wallet>(requests.size());

        AutoKeyValueSequence seq = autoKey.getValueSequence(Wallet.class.getName(), requests.size());

        for (WalletIssueOrder o : requests) {
            final Wallet wallet = walletDAO.prepare((int)(seq.next() % (long)1E10), o.getId(), processedAt, o.getHolderId(), o.getTitle());
            wallets.add(wallet);
            orderReports.put(o.getId(), new WalletReport(o, processedAt, wallet.getId(), o.getId(), processedAt, wallet.getStatus(), wallet.getHolderId(), wallet.getTitle(),
                    Collections.<WalletAccountInfo>emptyList()));
        }

        try {
            walletDAO.create(wallets);
        } catch (UpdateException e) {
            throw new RuntimeException(e);
        }

        return orderReports;
    }

    @Override
    protected Map<Long, Report> answerDuplicateRequests(Map<Long, WalletIssueOrder> duplicateRequests) {

        Map<Long, Wallet> walletByOrderIds = walletDAO.readByOrderIds(duplicateRequests.keySet());

        Map<Long, Report> answers = new HashMap<Long, Report>(walletByOrderIds.size());

        for (Map.Entry<Long, WalletIssueOrder> orderById : duplicateRequests.entrySet()) {
            Wallet wallet = walletByOrderIds.get(orderById.getKey());
            if (null != wallet) {
                WalletIssueOrder order = orderById.getValue();
                answers.put(orderById.getKey(),
                        new WalletReport(order, wallet.getCreatedAt(), wallet.getId(), order.getId(), wallet.getCreatedAt(),
                                wallet.getStatus(), wallet.getHolderId(), wallet.getTitle(), Collections.<WalletAccountInfo>emptyList()));
            }
        }

        return answers;
    }
}
