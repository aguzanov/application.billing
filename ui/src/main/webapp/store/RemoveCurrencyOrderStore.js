/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.store.RemoveCurrencyOrderStore', {
    extend: 'Ext.data.Store',
    model: 'Billing.model.RemoveCurrencyOrder',
    autoLoad: true,
    remoteFilter: true,
    remoteSort: false,
    autoSync: false
});
