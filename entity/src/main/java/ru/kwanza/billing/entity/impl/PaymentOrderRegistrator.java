package ru.kwanza.billing.entity.impl;

import ru.kwanza.billing.api.order.PaymentOrder;

import java.util.Date;

/**
 * @author Vasily Vorobyov
 */
public class PaymentOrderRegistrator extends RequestRegistrator<
        PaymentOrder, ru.kwanza.billing.entity.api.PaymentOrder> {

    public PaymentOrderRegistrator() {
        super(ru.kwanza.billing.entity.api.PaymentOrder.class);
    }

    @Override
    protected ru.kwanza.billing.entity.api.PaymentOrder createEntity(PaymentOrder apiOrder, Date processedAt, Long sessionRecordId) {
        return new ru.kwanza.billing.entity.api.PaymentOrder(
                apiOrder.getId(), sessionRecordId, apiOrder.getExchangeId(), processedAt, apiOrder.getPaymentInstructions().length,
                apiOrder.getDescription());
    }

    @Override
    protected Long getEntityId(ru.kwanza.billing.entity.api.PaymentOrder entity) {
        return entity.getId();
    }
}
