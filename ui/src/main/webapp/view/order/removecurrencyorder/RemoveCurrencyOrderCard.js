/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.view.order.removecurrencyorder.RemoveCurrencyOrderCard', {
    extend: 'Billing.view.order.OrderCard',

    alias: 'widget.removecurrencyorderdetails',

    title: 'Removing currency',
    iconCls: 'i-content',

    model: 'Billing.model.RemoveCurrencyOrder',
    store: 'Billing.store.RemoveCurrencyOrderStore',

    constructor: function (config) {
        var me = this;
        me.items = [
            {
                title: 'Additional parameters of the order',
                columnWidth: 1,
                items: [
                    {
                        fieldLabel: 'Currency code',
                        name: 'currencyId'
                    }
                ]
            }
        ];
        me.callParent(arguments);
        me.items.get('main').columnWidth = 1;
    }
});
