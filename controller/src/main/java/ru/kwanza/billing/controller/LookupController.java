package ru.kwanza.billing.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.kwanza.billing.api.AccountStatus;
import ru.kwanza.billing.api.WalletAccountStatus;
import ru.kwanza.billing.api.WalletStatus;
import ru.kwanza.billing.api.report.RejectCode;
import ru.kwanza.billing.entity.api.OrderType;
import ru.kwanza.dbtool.orm.api.IEntityManager;

import javax.annotation.Resource;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("lookups")
public class LookupController {
    @Resource(name = "dbtool.IEntityManager")
    private IEntityManager manager;

    public class LookupItem {
        private long id;
        private String code;
        private String title;

        public LookupItem(long id, String code, String title) {
            this.id = id;
            this.title = title;
            this.code = code;
        }

        public long getId() {
            return id;
        }

        public String getCode() {
            return code;
        }

        public String getTitle() {
            return title;
        }

    }

    public class ConfigItem {
        private String name;
        private String value;

        public ConfigItem(String name, String value) {
            this.name = name;
            this.value = value;
        }
    }

    static Map<String, Class> enumMap = new HashMap<String, Class>();

    static {
        registerEnum(WalletAccountStatus.class);
        registerEnum(WalletStatus.class);
        registerEnum(AccountStatus.class);
        registerEnum(RejectCode.class);
        registerEnum(OrderType.class);

    }

    private static void registerEnum(Class clazz) {
        enumMap.put(clazz.getSimpleName(), clazz);
    }

    private static void registerEnum(Class clazz, String name) {
        enumMap.put(name, clazz);
    }

    private List<LookupItem> getEnum(@RequestParam("name") String name)
            throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {

        List<LookupItem> list = new ArrayList<LookupItem>();
        Class aClass = enumMap.get(name);
        Method methodCode = aClass.getDeclaredMethod("getCode");
        Method methodTitle = aClass.getDeclaredMethod("getTitle");
        for (Object item : aClass.getEnumConstants()) {
            list.add(new LookupItem(
                    methodCode.getReturnType().equals(long.class) ? (Long) methodCode.invoke(item) : (Integer) methodCode.invoke(item),
                    item.toString(), (String) methodTitle.invoke(item)));
        }

        return list;
    }

    @RequestMapping(value = "map/public", method = RequestMethod.GET)
    @ModelAttribute("data")
    public Map<String, List<LookupItem>> getMap() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {

        Map<String, List<LookupItem>> result = new HashMap<String, List<LookupItem>>();
        for (String className : enumMap.keySet()) {
            result.put(className, getEnum(className));
        }

        return result;
    }

}
