package ru.kwanza.billing.api.order;

import ru.kwanza.billing.api.Request;
import ru.kwanza.billing.api.SessionId;

/**
 * Order
 *
 * From the point of view of business logic, a request to perform an atomic operation, which must be executed completely or not executed at all.
 * The ID is generated on demand according to certain rules.
 *
 * @author Vasily Vorobyov
 */
public abstract class Order extends Request {

    protected Order() {

    }

    protected Order(Long id, SessionId sessionId) {
        super(id, sessionId);
    }

}
