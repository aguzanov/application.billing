package ru.kwanza.billing.entity.api;

import ru.kwanza.console.security.api.AuthEntity;
import ru.kwanza.console.security.api.AuthField;
import ru.kwanza.dbtool.orm.annotations.Entity;
import ru.kwanza.dbtool.orm.annotations.Field;
import ru.kwanza.dbtool.orm.annotations.OneToMany;

import java.util.Collection;
import java.util.Date;

/**
 * @author Vasily Vorobyov
 */
@Entity(table = "payment_order")
@AuthEntity(name = "PaymentOrder", description = "Payment order")
public class PaymentOrder extends Order {

    @Field("instruction_count")
    @AuthField(name = "Number of instructions")
    private Integer instructionCount;

    @Field("description")
    @AuthField(name = "Description")
    private String description;

    @OneToMany(relationClass = PaymentInstruction.class, relationProperty = "orderId")
    private Collection<PaymentInstruction> paymentInstructions;

    @OneToMany(relationClass = AccountRecord.class, relationProperty = "orderId")
    private Collection<AccountRecord> accountRecords;

    public PaymentOrder() {
    }

    public PaymentOrder(Long id, Long sessionRecordId,Long exchangeId, Date processedAt, Integer instructionCount, String description) {
        super(id, sessionRecordId,exchangeId, processedAt, OrderType.PAYMENT_ORDER);
        this.instructionCount = instructionCount;
        this.description = description;
    }

    public Integer getInstructionCount() {
        return instructionCount;
    }

    public String getDescription() {
        return description;
    }

    public Collection<PaymentInstruction> getPaymentInstructions() {
        return paymentInstructions;
    }

    public Collection<AccountRecord> getAccountRecords() {
        return accountRecords;
    }

    @Override
    public void initType() {
        type = OrderType.PAYMENT_ORDER.getCode();
    }
}
