package ru.kwanza.billing.entity.api;

import ru.kwanza.console.security.api.AuthEntity;
import ru.kwanza.dbtool.orm.annotations.Entity;

import java.util.Date;

/**
 * @author Dmitry Zhukov
 */
@Entity(table = "close_wallet_order")
@AuthEntity(name = "CloseWalletOrder", description = "Order for transfer wallet to CLOSED status")
public class CloseWalletOrder extends WalletOrder {
    public CloseWalletOrder(Long id, Long sessionRecordId, Long exchangeId, Date processedAt, Long walletId) {
        super(id, sessionRecordId, exchangeId, processedAt, walletId, OrderType.CLOSE_WALLET_ORDER);
    }

    @Override
    public void initType() {
        type = OrderType.CLOSE_WALLET_ORDER.getCode();
    }
}
