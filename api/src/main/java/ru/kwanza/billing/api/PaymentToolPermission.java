package ru.kwanza.billing.api;

import java.util.ArrayList;
import java.util.List;

/**
 * @author henadiy
 */
public enum PaymentToolPermission {

	READ(0x01, "read"), PAYMENT(0x02, "payment"), CONVERSION(0x04, "conversion"),
        WITHDRAWAL(0x08, "withdrawal"), DEPOSIT(0x10, "deposit");

	private final int mask;
    private final String code;

	private PaymentToolPermission(int mask, String code) {
		this.mask = mask;
        this.code = code;
	}

	public int mask() {
		return mask;
	}

    public String code() {return code;}

    public boolean check(int value) {
        return 0 != (mask & value);
    }

	public static boolean check(int value, PaymentToolPermission...permissions) {
		for(PaymentToolPermission p : permissions) {
			if (0 == (value & p.mask)) return false;
		}
		return true;
	}

	public static int pack(PaymentToolPermission...permissions) {
		int res = 0;
		for(PaymentToolPermission p : permissions) {
			res |= p.mask;
		}
		return res;
	}

	public static PaymentToolPermission[] unpack(int mask) {
		List<PaymentToolPermission> res =
				new ArrayList<PaymentToolPermission>(PaymentToolPermission.values().length);
		for(PaymentToolPermission p : PaymentToolPermission.values()) {
			if (p.check(mask)) res.add(p);
		}
		return res.toArray(new PaymentToolPermission[res.size()]);
	}

}
