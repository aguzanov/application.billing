package ru.kwanza.billing.controller.valueobjects;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Dmitry Zhukov
 */
public class WalletAccountIssueOrderVO extends OrderVO {

    private Long walletId;

    private BigDecimal accountId;

    private String title;

    public WalletAccountIssueOrderVO(Long id, Long sessionRecordId, Date processedAt, Integer rejectCode, Long walletId, BigDecimal accountId,
                                     String title) {
        super(id, sessionRecordId, processedAt, rejectCode);
        this.walletId = walletId;
        this.accountId = accountId;
        this.title = title;
    }

    public Long getWalletId() {
        return walletId;
    }

    public BigDecimal getAccountId() {
        return accountId;
    }

    public String getTitle() {
        return title;
    }
}
