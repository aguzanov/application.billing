/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.model.RegisterCurrencyOrder', {
    extend: 'Billing.model.Order',
    fields: [
        'currencyId',
        'alphaCode',
        {
            name: 'currency',
            persist: false
        },
        'title',
        'exponent'
    ],

    detailView: 'Billing.view.order.registercurrencyorder.RegisterCurrencyOrderCard',

    permissions: {
        read: "RegisterCurrencyOrder:read",
        create: false,
        update: false,
        destroy: false
    },

    proxy: {
        type: 'ajax',
        apiUrl: 'billing-controller/registercurrencyorder/'

    },

    statics: {
        currencyRenderer: function (value, meta, record) {
            var status = record.get('rejectCode');
            var currency = record.get('currency');
            if (status || !currency) {
                return record.get('currencyId');
            }
            return Console.application.QuickLink.createLink(record.get('currencyId'),
                'Billing.view.currency.CurrencyCard', value);
        }
    }
});
