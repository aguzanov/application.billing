package ru.kwanza.billing.entity.api;

import ru.kwanza.billing.entity.api.id.WalletAccountIdBuilder;
import ru.kwanza.console.security.api.AuthEntity;
import ru.kwanza.console.security.api.AuthField;
import ru.kwanza.console.security.api.AuthIdField;
import ru.kwanza.dbtool.orm.annotations.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Types;
import java.util.Date;

/**
 * @author Vasily Vorobyov
 */
@Entity(table = "wallet_account")
@AuthEntity(name = "WalletAccount", description = "Wallet account")
public class WalletAccount implements Serializable {

    private WalletAccountIdBuilder idBuilder;

    @IdField(value = "id", type = Types.DECIMAL)
    @AuthIdField
    @AuthField(name = "ID")

    private BigDecimal id;

    @VersionField("version")
    private Long version;


    @Field("account_id")
    @AuthField(name = "Account number")
    private BigDecimal accountId;

    @Field("created_at")
    @AuthField(name = "Created at")
    private Date createdAt;

    @Field("order_id")
    @AuthField(name = "ID of the order")
    private Long orderId;

    @Field("title")
    @AuthField(name = "Name")
    private String title;

    @ManyToOne(property = "orderId")
    private WalletAccountIssueOrder order;

    public WalletAccount() {
    }

    public WalletAccount(Wallet wallet, Account account, Integer walletAccountCounter, Long orderId, Date createdAt, String title) {
        this.idBuilder = new WalletAccountIdBuilder(wallet.getId(), account.getCurrencyId(), walletAccountCounter);
        this.id = idBuilder.getId();
        this.accountId = account.getId();
        this.orderId = orderId;
        this.createdAt = createdAt;
        this.title = title;
    }

    public BigDecimal getId() {
        return id;
    }

    public Long getWalletId() {
        return getIdBuilder().getWalletId();
    }

    public BigDecimal getAccountId() {
        return accountId;
    }

    public Integer getCurrencyId() {
        return getIdBuilder().getCurrencyId();
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Long getOrderId() {
        return orderId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    protected WalletAccountIdBuilder getIdBuilder() {
        if (null == idBuilder) {
            idBuilder = new WalletAccountIdBuilder(getId());
        }
        return idBuilder;
    }

    
}
