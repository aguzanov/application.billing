package ru.kwanza.billing.processor.xr;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import ru.kwanza.billing.processor.util.XmlUtils;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Tape courses CBR
 * Read more about the format:
 *      http://www.cbr.ru/scripts/Root.asp?PrtId=SXML
 *
 * @author henadiy
 */
public class CBRFFeed extends CBFeed {

    protected static final TimeZone timeZone = TimeZone.getTimeZone("Europe/Moscow");
    protected static final SimpleDateFormat DATE_FORMAT;
    static {
        DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");
        DATE_FORMAT.setTimeZone(timeZone);
    }

    public CBRFFeed() {
        super(643);
    }

    @Override
    public void setDate(Date date) {
        Calendar cal = Calendar.getInstance(timeZone);
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        super.setDate(cal.getTime());
    }

    @Override
    public Collection<Entry> fetch() {
        return fetchURL(buildUrl());
    }

    @Override
    protected Collection<Entry> parse(InputStream is) {
        List<Entry> res = new LinkedList<Entry>();
        try {
            Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(is);
            doc.getDocumentElement().normalize();

            NodeList currencies = doc.getDocumentElement().getElementsByTagName("Valute");
            for(int i = 0; i < currencies.getLength(); i++) {
                if (Node.ELEMENT_NODE != currencies.item(i).getNodeType()) {
                    continue;
                }
                Map<String, Element> childElements = XmlUtils.getChildElementsMap(currencies.item(i));
                Entry entry = buildEntry(
                        XmlUtils.getNodeText(childElements.get("Nominal"), "0"),
                        Integer.valueOf(XmlUtils.getNodeText(childElements.get("NumCode"), "0")),
                        XmlUtils.getNodeText(childElements.get("Value"), "0").replace(",", ".")
                );
                if (entry.isValid()) {
                    res.add(entry);
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return res;
    }

    protected String buildUrl() {
        return new StringBuilder()
                .append("http://www.cbr.ru/scripts/XML_daily.asp?date_req=")
                .append(DATE_FORMAT.format(getDate()))
                .toString();
    }


}
