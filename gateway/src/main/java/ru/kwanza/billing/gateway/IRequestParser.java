package ru.kwanza.billing.gateway;

import ru.kwanza.billing.api.Request;

import java.io.IOException;

/**
 * @author Vasily Vorobyov
 */
public interface IRequestParser<R extends Request> {

    R[] parse(String content) throws IOException;

}
