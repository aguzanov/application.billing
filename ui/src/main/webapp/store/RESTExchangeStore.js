Ext.define('Billing.store.RESTExchangeStore',{
    extend: 'Ext.data.Store',
    model: 'Billing.model.RESTExchange',
    autoLoad: true,
    remoteFilter: true,
    remoteSort: false,
    autoSync: false
});
