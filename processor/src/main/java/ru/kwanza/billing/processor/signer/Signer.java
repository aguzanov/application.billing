package ru.kwanza.billing.processor.signer;

import ru.kwanza.billing.api.ISigned;

/**
 * @author henadiy
 */
public abstract class Signer<T extends ISigned> {

    private ISignatureGenerator signatureGenerator;

    public void setSignatureGenerator(ISignatureGenerator signatureGenerator) {
        this.signatureGenerator = signatureGenerator;
    }

    protected ISignatureGenerator getSignatureGenerator() {
        return signatureGenerator;
    }

    public T sign(T obj) {
        obj.sign(signatureGenerator.getSignature(getData(obj)));
        return obj;
    }

    public boolean check(T obj) {
        return signatureGenerator.getSignature(getData(obj)).equals(obj.getSignature());
    }

    protected abstract byte[] getData(T obj);

}
