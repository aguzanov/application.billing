package ru.kwanza.billing.controller;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.kwanza.billing.api.order.ActivateWalletOrder;
import ru.kwanza.billing.api.order.CloseWalletOrder;
import ru.kwanza.billing.api.order.SuspendWalletOrder;
import ru.kwanza.billing.api.order.WalletIssueOrder;
import ru.kwanza.billing.api.report.RejectedRequestReport;
import ru.kwanza.billing.api.report.Report;
import ru.kwanza.billing.api.report.WalletReport;
import ru.kwanza.billing.controller.billing.BillingHelper;
import ru.kwanza.billing.controller.exceptions.WalletException;
import ru.kwanza.billing.entity.api.Wallet;
import ru.kwanza.console.core.IMerger;
import ru.kwanza.console.security.api.AuthOperation;
import ru.kwanza.console.security.api.EntityAuditor;
import ru.kwanza.dbtool.core.UpdateException;
import ru.kwanza.dbtool.orm.api.IEntityManager;
import ru.kwanza.dbtool.orm.api.If;
import ru.kwanza.dbtool.orm.api.OrderBy;
import ru.kwanza.txn.api.Transactional;
import ru.kwanza.txn.api.TransactionalType;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author Dmitry Zhukov
 */

@Controller
@RequestMapping("wallet")
public class WalletController {
    @Resource(name = "dbtool.IEntityManager")
    private IEntityManager manager;

    @Resource(name = "console.IMerger")
    private IMerger merger;

    @Resource(name = "billing.BillingHelper")
    private BillingHelper billingHelper;


    @RequestMapping(value = "read", method = RequestMethod.GET)
    @ModelAttribute("data")
    @Transactional(TransactionalType.REQUIRES_NEW)
    @AuthOperation(name = "View wallets", check = "hasPermission('Wallet:read')", auditable = false)
    public List<Wallet> getList(@RequestParam(value = "start", required = false) Integer start,
                                @RequestParam(value = "limit", required = false) Integer limit,
                                @RequestParam(value = "id", required = false) BigDecimal id,
                                @RequestParam(value = "filter.id", required = false) BigDecimal filterId,
                                @RequestParam(value = "filter.createdAt.from", required = false) @DateTimeFormat(
                                        pattern = "dd.MM.yyyy HH:mm:ss") Date createdAtFrom,
                                @RequestParam(value = "filter.createdAt.to", required = false) @DateTimeFormat(
                                        pattern = "dd.MM.yyyy HH:mm:ss") Date createdAtTo,
                                @RequestParam(value = "filter.holderId", required = false) String holderId,
                                @RequestParam(value = "filter.statusCode", required = false) Integer statusCode,
                                @RequestParam(value = "filter.walletAccountCounter", required = false) Integer walletAccountCounter) {
        return manager.filtering(Wallet.class).filter(id != null, If.isEqual("id"), id).filter(filterId != null, If.isEqual("id"), filterId)
                .filter(createdAtFrom != null, If.isGreaterOrEqual("createdAt"), createdAtFrom)
                .filter(createdAtTo != null, If.isLessOrEqual("createdAt"), createdAtTo)
                .filter(holderId != null, If.like("holderId"), "%" + (holderId == null ? holderId : holderId.toUpperCase()) + "%")
                .filter(statusCode != null, If.isEqual("statusCode"), statusCode)
                .filter(walletAccountCounter != null, If.isEqual("walletAccountCounter"), walletAccountCounter).paging(start, limit)
                .join("order")
                .orderBy(OrderBy.DESC("id")).selectList();
    }

    @RequestMapping(value = "create", method = RequestMethod.POST)
    @ModelAttribute("data")
    @Transactional(TransactionalType.REQUIRES_NEW)
    @AuthOperation(name = "Create wallet", check = "hasPermission('Wallet:create')")
    public Wallet create(@RequestBody Wallet data, HttpServletRequest request) throws WalletException {

        Report report = billingHelper.processRequest(
                new WalletIssueOrder(billingHelper.generateRequestId(), billingHelper.getSessionId(request),
                        data.getHolderId(), data.getTitle())
        );
        if (report instanceof RejectedRequestReport) {
            throw new WalletException.WalletCreateException(((RejectedRequestReport) report).getRejectCode());
        }

        final Wallet wallet = manager.readByKey(Wallet.class, ((WalletReport) report).getWalletId());
        manager.fetch(wallet,"order");
        return EntityAuditor.logCreate(wallet);
    }

    @RequestMapping(value = "update", method = RequestMethod.POST)
    @ModelAttribute("data")
    @Transactional(TransactionalType.REQUIRES_NEW)
    @AuthOperation(name = "Update wallet", check = "hasPermission('Wallet:update')")
    public Wallet update(@RequestBody Wallet data) throws WalletException, UpdateException {

        //todo aguzanov   why not update order

        Wallet wallet = manager.readByKey(Wallet.class, data.getId());

        if (wallet == null) {
            throw new WalletException.WalletNotFoundException();
        }



        EntityAuditor.logBeforeUpdate(wallet);
        merger.merge(wallet, data);
        manager.fetch(wallet,"order");
        return EntityAuditor.logAfterUpdate(manager.update(wallet));
    }

    @RequestMapping(value = "activate", method = RequestMethod.POST)
    @ModelAttribute("data")
    @Transactional(value = TransactionalType.REQUIRES_NEW, applicationExceptions = WalletException.class)
    @AuthOperation(name = "Activation wallet", check = "hasPermission('Wallet:activate')")
    public Wallet activate(@RequestParam(value = "id", required = true) Long id, HttpServletRequest request)
            throws UpdateException, WalletException {


        Wallet wallet = manager.readByKey(Wallet.class, id);

        if (wallet == null) {
            throw new WalletException.WalletNotFoundException();
        }


        Report report = billingHelper.processRequest(
                new ActivateWalletOrder(billingHelper.generateRequestId(), billingHelper.getSessionId(request),
                        wallet.getId())
        );
        if (report instanceof RejectedRequestReport) {
            throw new WalletException.WalletCloseException(((RejectedRequestReport) report).getRejectCode());
        }

        wallet = manager.readByKey(Wallet.class, id);
        manager.fetch(wallet,"order");
        return EntityAuditor.logDelete(wallet);

    }

    @RequestMapping(value = "suspend", method = RequestMethod.POST)
    @ModelAttribute("data")
    @Transactional(value = TransactionalType.REQUIRES_NEW, applicationExceptions = WalletException.class)
    @AuthOperation(name = "Lock wallet", check = "hasPermission('Wallet:suspend')")
    public Wallet suspend(@RequestParam(value = "id", required = true) Long id, HttpServletRequest request)
            throws UpdateException, WalletException {

        Wallet wallet = manager.readByKey(Wallet.class, id);

        if (wallet == null) {
            throw new WalletException.WalletNotFoundException();
        }

        Report report = billingHelper.processRequest(
                new SuspendWalletOrder(billingHelper.generateRequestId(), billingHelper.getSessionId(request),
                        wallet.getId())
        );

        if (report instanceof RejectedRequestReport) {
            throw new WalletException.WalletCloseException(((RejectedRequestReport) report).getRejectCode());
        }

        wallet = manager.readByKey(Wallet.class, id);
        manager.fetch(wallet,"order");
        return EntityAuditor.logDelete(wallet);

    }

    @RequestMapping(value = "close", method = RequestMethod.POST)
    @ModelAttribute("data")
    @Transactional(value = TransactionalType.REQUIRES_NEW, applicationExceptions = WalletException.class)
    @AuthOperation(name = "Close wallet", check = "hasPermission('Wallet:delete')")
    public Wallet close(@RequestParam(value = "id", required = true) Long id, HttpServletRequest request)
            throws UpdateException, WalletException {

        Wallet wallet = manager.readByKey(Wallet.class, id);

        if (wallet == null) {
            throw new WalletException.WalletNotFoundException();
        }

        Report report = billingHelper.processRequest(
                new CloseWalletOrder(billingHelper.generateRequestId(), billingHelper.getSessionId(request),
                        wallet.getId())
        );

        if (report instanceof RejectedRequestReport) {
            throw new WalletException.WalletCloseException(((RejectedRequestReport) report).getRejectCode());
        }

        wallet = manager.readByKey(Wallet.class, id);
        manager.fetch(wallet,"order");
        return EntityAuditor.logDelete(wallet);

    }

}


