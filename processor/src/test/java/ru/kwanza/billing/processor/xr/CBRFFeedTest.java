package ru.kwanza.billing.processor.xr;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.ByteArrayInputStream;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.TimeZone;

import static org.junit.Assert.*;

/**
 * @author henadiy
 */
@RunWith(JUnit4.class)
public class CBRFFeedTest {


    class TestImpl extends CBRFFeed {
        TestImpl(Date d) {
            setDate(d);
        }
    }

    public static final byte[] xml = new StringBuilder()
            .append("<?xml version=\"1.0\" encoding=\"windows-1251\" ?>")
            .append("<ValCurs Date=\"14/05/2014\" name=\"Foreign Currency Market\">")
            .append("<Valute ID=\"R01090\">")
            .append("   <NumCode>974</NumCode>")
            .append("   <CharCode>BYR</CharCode>")
            .append("   <Nominal>10000</Nominal>")
            .append("   <Name>Belarusian rubles</Name>")
            .append("   <Value>34,7399</Value>")
            .append("</Valute>")
            .append("<Valute ID=\"R01235\">")
            .append("   <NumCode>840</NumCode>")
            .append("   <CharCode>USD</CharCode>")
            .append("   <Nominal>1</Nominal>")
            .append("   <Name>The US dollar</Name>")
            .append("   <Value>34,8789</Value>")
            .append("</Valute>")
            .append("<Valute ID=\"R01239\">")
            .append("   <NumCode>978</NumCode>")
            .append("   <CharCode>EUR</CharCode>")
            .append("   <Nominal>1</Nominal>")
            .append("   <Name>Euro</Name>")
            .append("   <Value>48,0073</Value>")
            .append("</Valute>")
            .append("<Valute ID=\"R01239\">")
            .append("   <NumCode>000</NumCode>")
            .append("   <CharCode>XXX</CharCode>")
            .append("   <Name>Euro</Name>")
            .append("   <Value>00,0000</Value>")
            .append("</Valute>")
            .append("<Test ID=\"R01234\">")
            .append("   <NumCode>123</NumCode>")
            .append("   <CharCode>XXX</CharCode>")
            .append("   <Nominal>1</Nominal>")
            .append("   <Name>XXX</Name>")
            .append("   <Value>00,0000</Value>")
            .append("</Test>")
            .append("</ValCurs>")
            .toString().getBytes(Charset.forName("CP1251"));

    @Test
    public void testParse() throws Exception {

        Collection<Feed.Entry> entries = new TestImpl(new Date()).parse(new ByteArrayInputStream(xml));
        assertEquals(3, entries.size());

        Feed.Entry[] entryArray = entries.toArray(new Feed.Entry[entries.size()]);

        assertEquals(new BigDecimal("10000"), entryArray[0].getSourceScale());
        assertEquals(Integer.valueOf(974), entryArray[0].getSourceCode());
        assertEquals(new BigDecimal("34.7399"), entryArray[0].getTargetScale());
        assertEquals(Integer.valueOf(643), entryArray[0].getTargetCode());

        assertEquals(new BigDecimal("1"), entryArray[1].getSourceScale());
        assertEquals(Integer.valueOf(840), entryArray[1].getSourceCode());
        assertEquals(new BigDecimal("34.8789"), entryArray[1].getTargetScale());
        assertEquals(Integer.valueOf(643), entryArray[1].getTargetCode());

        assertEquals(new BigDecimal("1"), entryArray[2].getSourceScale());
        assertEquals(Integer.valueOf(978), entryArray[2].getSourceCode());
        assertEquals(new BigDecimal("48.0073"), entryArray[2].getTargetScale());
        assertEquals(Integer.valueOf(643), entryArray[2].getTargetCode());

    }

    @Test
    public void testSetDate() throws Exception {

        CBRFFeed feed = new CBRFFeed();

        // GMT-8 (-7) -> MSK-12 (-11)
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("America/Ensenada"));
        cal.set(2014, Calendar.MAY, 13, 13, 35, 00);
        feed.setDate(cal.getTime());
        cal = Calendar.getInstance(TimeZone.getTimeZone("Europe/Moscow"));
        cal.set(2014, Calendar.MAY, 14, 0, 0, 0);
        cal.set(Calendar.MILLISECOND, 0);
        assertEquals(cal.getTime(), feed.getDate());

        cal = Calendar.getInstance(TimeZone.getTimeZone("America/Ensenada"));
        cal.set(2014, Calendar.MAY, 13, 12, 35, 0);
        feed.setDate(cal.getTime());
        cal = Calendar.getInstance(TimeZone.getTimeZone("Europe/Moscow"));
        cal.set(2014, Calendar.MAY, 13, 0, 0, 0);
        cal.set(Calendar.MILLISECOND, 0);
        assertEquals(cal.getTime(), feed.getDate());

        // GMT+12 (+13) -> MSK+8 (+9)
        cal = Calendar.getInstance(TimeZone.getTimeZone("Antarctica/South_Pole"));
        cal.set(2014, Calendar.MAY, 15, 7, 10, 15);
        feed.setDate(cal.getTime());
        cal = Calendar.getInstance(TimeZone.getTimeZone("Europe/Moscow"));
        cal.set(2014, Calendar.MAY, 14, 0, 0, 0);
        cal.set(Calendar.MILLISECOND, 0);
        assertEquals(cal.getTime(), feed.getDate());

        cal = Calendar.getInstance(TimeZone.getTimeZone("Antarctica/South_Pole"));
        cal.set(2014, Calendar.MAY, 15, 8, 10, 15);
        feed.setDate(cal.getTime());
        cal = Calendar.getInstance(TimeZone.getTimeZone("Europe/Moscow"));
        cal.set(2014, Calendar.MAY, 15, 0, 0, 0);
        cal.set(Calendar.MILLISECOND, 0);
        assertEquals(cal.getTime(), feed.getDate());

        // GMT+4 (+4) -> MSK+0 (+0)
        cal = Calendar.getInstance(TimeZone.getTimeZone("Europe/Moscow"));
        cal.set(2014, Calendar.MAY, 14, 23, 59, 59);
        feed.setDate(cal.getTime());
        cal = Calendar.getInstance(TimeZone.getTimeZone("Europe/Moscow"));
        cal.set(2014, Calendar.MAY, 14, 0, 0, 0);
        cal.set(Calendar.MILLISECOND, 0);
        assertEquals(cal.getTime(), feed.getDate());

        cal = Calendar.getInstance(TimeZone.getTimeZone("Europe/Moscow"));
        cal.set(2014, Calendar.MAY, 15, 0, 0, 0);
        feed.setDate(cal.getTime());
        cal = Calendar.getInstance(TimeZone.getTimeZone("Europe/Moscow"));
        cal.set(2014, Calendar.MAY, 15, 0, 0, 0);
        cal.set(Calendar.MILLISECOND, 0);
        assertEquals(cal.getTime(), feed.getDate());

    }

    @Test
    public void testBuildUrl() throws Exception {

        // GMT-8 (-7) -> MSK-12 (-11)
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("America/Ensenada"));
        cal.set(2014, Calendar.MAY, 13, 13, 35, 00);
        assertEquals("http://www.cbr.ru/scripts/XML_daily.asp?date_req=14/05/2014",
                new TestImpl(cal.getTime()).buildUrl());
        cal.set(2014, Calendar.MAY, 13, 12, 35, 00);
        assertEquals("http://www.cbr.ru/scripts/XML_daily.asp?date_req=13/05/2014",
                new TestImpl(cal.getTime()).buildUrl());


        // GMT+12 (+13) -> MSK+8 (+9)
        cal = Calendar.getInstance(TimeZone.getTimeZone("Antarctica/South_Pole"));
        cal.set(2014, Calendar.MAY, 15, 7, 10, 15);
        assertEquals("http://www.cbr.ru/scripts/XML_daily.asp?date_req=14/05/2014",
                new TestImpl(cal.getTime()).buildUrl());
        cal.set(2014, Calendar.MAY, 15, 8, 10, 15);
        assertEquals("http://www.cbr.ru/scripts/XML_daily.asp?date_req=15/05/2014",
                new TestImpl(cal.getTime()).buildUrl());

        // GMT+4 (+4) -> MSK+0 (+0)
        cal = Calendar.getInstance(TimeZone.getTimeZone("Europe/Moscow"));
        cal.set(2014, Calendar.MAY, 14, 23, 59, 59);
        assertEquals("http://www.cbr.ru/scripts/XML_daily.asp?date_req=14/05/2014",
                new TestImpl(cal.getTime()).buildUrl());
        cal.set(2014, Calendar.MAY, 15, 0, 0, 0);
        assertEquals("http://www.cbr.ru/scripts/XML_daily.asp?date_req=15/05/2014",
                new TestImpl(cal.getTime()).buildUrl());

    }


}
