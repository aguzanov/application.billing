package ru.kwanza.billing.api.order;

import com.fasterxml.jackson.annotation.JsonTypeName;
import ru.kwanza.billing.api.RequestType;

import java.util.HashMap;
import java.util.Map;

/**
 * @author henadiy
 */
@JsonTypeName(value = "O_SESSION_OPEN")
public class OpenSessionOrder extends Order {

    public static final String ARG_USERNAME = "username";
    public static final String ARG_PASSWORD = "password";
    public static final String ARG_TOKEN = "token";
    public static final String ARG_VALIDATION_FACTORS = "validationFactors";

    private final Map<String, Object> arguments = new HashMap<String, Object>();

	protected OpenSessionOrder() {
	}

	public OpenSessionOrder(Long id) {
		super(id,  null);
	}

	public OpenSessionOrder(Long id, Map<String, Object> arguments) {
		super(id, null);
		this.arguments.putAll(arguments);
	}	

	public void addArgument(String name, Object value) {
		this.arguments.put(name, value);
	}

	public Map<String, Object> getArguments() {
		return arguments;
	}

	@Override
	public RequestType getRequestType() {
		return RequestType.O_SESSION_OPEN;
	}
}
