/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.view.order.paymentorder.PaymentInstructionsList', {
    extend: 'Console.grid.BaseGrid',

    alias: 'widget.paymentinstructionslist',

    updateInterval: 0,

    permissions:{
        read: true,
        create: false,
        update: false,
        destroy: false
    },

    columns: [
        {
            text: 'ID',
            dataIndex: 'id',
            width: 200
        },
        {
            text: 'indexInOrder',
            dataIndex: 'indexInOrder',
            width: 200
        },
        {
            text: 'sourceAccountId',
            dataIndex: 'sourceAccountId',
            width: 200
        },
        {
            text: 'sourceWalletAccountId',
            dataIndex: 'sourceWalletAccountId',
            width: 200
        },
        {
            text: 'sourceAmount',
            dataIndex: 'sourceAmount',
            width: 200
        },
        {
            text: 'targetAccountId',
            dataIndex: 'targetAccountId',
            width: 200
        },
        {
            text: 'targetWalletAccountId',
            dataIndex: 'targetWalletAccountId',
            width: 200
        },
        {
            text: 'targetAmount',
            dataIndex: 'targetAmount',
            width: 200
        },
        {
            text: 'recordedAt',
            dataIndex: 'recordedAt',
            width: 200
        },
        {
            text: 'description',
            dataIndex: 'description',
            width: 200
        }

    ]
});