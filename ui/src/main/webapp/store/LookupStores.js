/**
 * A helper class that initiates the loading of enumerations (data containing a numeric code, code string, description text)
 * and creates data sources, to work with them. Data sources are created with the ID type 'TicketService.store.lookup.' + the name of the enumeration class.
 *
 * Data transfers loads once in one request from the server.
 * Expects the response object, each field of which is the name of the enumeration class,
 * and the value is an object containing the following fields:
 *
 * - **id** - numeric code
 *
 * - **code** - string code
 *
 * - **title** - text description
 *
 * Trying to compare classes icon records, string code, Provides methods for searching records and renderers for the data sources
 */
Ext.define('Billing.store.LookupStores', {
    requires: ['Console.security.SecurityManager'],
    singleton: true,

    /**
     * @property
     * Contains a list of classes matching the string codes of the icons.
     * Searching for icon class begins in the property corresponding to the name of the enumeration class, if not found there in the property DEFAULT
     * @readonly
     */
    icons: {
        DEFAULT: {
            ACTIVE: 'i-msg-success',
            BLOCKED: 'i-msg-blocked',
            DELETED: 'i-action-delete',
            INFO: 'i-msg-info',
            WARN: 'i-msg-warning',
            ERROR: 'i-msg-info-red',
            SUCCESS: 'i-msg-success',
            FAIL: 'i-action-close',
            NO_ACTIVE: 'i-msg-prohibit'
        }
        ,
        RejectCode: {
            LACKS_OF_AUTHORITY: 'i-msg-blocked',
            WALLET_NOT_FOUND: 'i-msg-blocked',
            ACCOUNT_NOT_FOUND: 'i-msg-blocked',
            CURRENCY_NOT_FOUND: 'i-msg-blocked',
            EMPTY_REQUIRED_FIELD: 'i-msg-blocked',
            ISSUER_NOT_FOUND: 'i-msg-blocked',
            WALLET_ACCOUNT_NOT_FOUND: 'i-msg-blocked',
            NO_PAYMENT_INSTRUCTIONS: 'i-msg-blocked',
            UNSUPPORTED_PAYMENT_INSTRUCTION: 'i-msg-blocked',
            ACCOUNT_SUSPENDED: 'i-msg-blocked',
            MIN_BALANCE_EXCEEDED: 'i-msg-blocked',
            MAX_BALANCE_EXCEEDED: 'i-msg-blocked',
            TITLE_TOO_LONG: 'i-msg-blocked',
            ILLEGAL_CURRENCY_ID: 'i-msg-blocked',
            CURRENCY_ALPHA_CODE_TOO_LONG: 'i-msg-blocked',
            ILLEGAL_ISSUER_BIN: 'i-msg-blocked'
        },

        OrderType: {
            REGISTER_ACCOUNT: 'i-document-order',
            REGISTER_WALLET: 'i-document-test',
            REGISTER_WALLET_ACCOUNT: 'i-checklist',
            REGISTER_CURRENCY: 'i-document-report',
            REGISTER_ISSUER: 'i-document-survey',
            PAYMENT_ORDER: 'i-document-report',
            SUSPEND_WALLET_ORDER: 'i-action-delete',
            ACTIVATE_WALLET_ORDER: 'i-msg-success',
            CLOSE_WALLET_ORDER: 'i-action-close',
            PERMISSION_ORDER: 'i-msg-info',
            REMOVE_CURRENCY_ORDER: 'i-content'

        }
    },

    constructor: function () {
        this.loadData();
    },

    loadData: function () {
        var me = this;

        if (me.loaded || !Console.security.SecurityManager.authenticated) {
            return;
        }

        Ext.Ajax.request({
            scope: me,
            async: false,
            url: 'billing-controller/lookups/map/public',
            success: function (response) {
                var stores = Ext.decode(response.responseText).data;

                Ext.iterate(stores, function (name, data) {
                    var hasIcons = false,
                        fields = ['id', 'code', 'title'],
                        icons = me.icons[name] || me.icons.DEFAULT;

                    data.forEach(function (item) {
                        var iconCls = icons[item.code];
                        if (iconCls) {
                            item.iconCls = iconCls;
                            hasIcons = true;
                        }
                    });

                    hasIcons && fields.push('iconCls');
                    Ext.create('Ext.data.Store', {
                        fields: fields,
                        storeId: 'Billing.store.lookup.' + name,
                        data: data
                    });
                });

                me.loaded = true;
                Console.security.SecurityManager.un('userInfoChange', me.loadData, me);
            },
            failure: function () {
                Console.security.SecurityManager.on('userInfoChange', me.loadData, me);
            }
        });
    },

    /**
     * Returns the data source for the specified enumeration. Use of this method should be avoided
     * Instead of using this method, you should get the source by ID using Ext.getStore
     * The ID is based on the rule 'Billing.store.lookup.' + the name of the enumeration class
     * @param {String} name the name of the enumeration class
     * @returns {Ext.data.Store} the data source
     */
    getStore: function (/*String*/ name) {
        console.warn("method Billing.store.LookupStores.getStore('%s') is deprecated, use Ext.getStore('Billing.store.lookup.%s') instead", name, name);
        return Ext.getStore('Billing.store.lookup.' + name);
    },

    /**
     * Searches the class of the enum entry by numeric code, and returns its text description
     * @param {String} name the name of the enumeration class
     * @param {Number} code numeric code
     * @returns {String/Number} text description of the numeric code passed or, if a record with this ID was not found.
     */
    resolveTitle: function (name, code) {
        var me = this,
            store = Ext.getStore('Billing.store.lookup.' + name),
            record = store.getById(code);

        return record && record.get('title') || code;
    },

    /**
     * Searches the class of the enum entry by numeric code, and returns the icon class
     * @param {String} name the name of the enumeration class
     * @param {Number} code numeric code
     * @returns {String} a text description or an empty string if the record with this ID was not found.
     */
    resolveIconCls: function (name, code) {
        var me = this,
            store = Ext.getStore('Billing.store.lookup.' + name),
            record = store.getById(code);

        return record && record.get('iconCls') || '';
    },

    /**
     * The function returns the renderer for a field whose values are elements of an enumeration
     * @param {String} name the name of the enumeration class
     * @returns {Function} function renderer (shows a text description for the code)
     */
    getRenderer: function (/*String*/ name) {
        return function (value) {
            return TicketService.store.LookupStores.resolveTitle(name, value);
        }
    }
});

Ext.apply(Ext.util.Format, {
    lookup: function (value, name) {
        return TicketService.store.LookupStores.resolveTitle(name, value);
    }
});
