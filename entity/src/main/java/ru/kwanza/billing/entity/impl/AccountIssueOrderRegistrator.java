package ru.kwanza.billing.entity.impl;

import ru.kwanza.billing.api.order.AccountIssueOrder;

import java.util.*;

/**
 * @author Vasily Vorobyov
 */
public class AccountIssueOrderRegistrator extends RequestRegistrator<
        AccountIssueOrder, ru.kwanza.billing.entity.api.AccountIssueOrder> {

    public AccountIssueOrderRegistrator() {
        super(ru.kwanza.billing.entity.api.AccountIssueOrder.class);
    }

    @Override
    protected ru.kwanza.billing.entity.api.AccountIssueOrder createEntity(AccountIssueOrder apiOrder, Date processedAt, Long sessionRecordId) {
        return new ru.kwanza.billing.entity.api.AccountIssueOrder(
                apiOrder.getId(), sessionRecordId,apiOrder.getExchangeId(), processedAt, apiOrder.getIssuerId(), apiOrder.getCurrencyId());
    }

    @Override
    protected Long getEntityId(ru.kwanza.billing.entity.api.AccountIssueOrder entity) {
        return entity.getId();
    }
}
