package ru.kwanza.billing.processor;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import ru.kwanza.billing.api.Session;
import ru.kwanza.billing.api.SessionId;
import ru.kwanza.billing.api.query.CurrenciesQuery;
import ru.kwanza.billing.api.report.RejectedRequestReport;
import ru.kwanza.billing.processor.session.TestSessionManager;

import java.util.*;

import static org.junit.Assert.assertEquals;

/**
 * @author henadiy
 */
@RunWith(JUnit4.class)
public class GeneralCompetenceFilterTest {

    @Test
    public void testFilter() throws Exception {

        Date now = new Date();

        MockAccessMatrix accessMatrix = new MockAccessMatrix();
        accessMatrix.addRolePermissions("group2", "CurrenciesQuery:create");


        TestSessionManager manager = new TestSessionManager();
        manager.putSession(new Session(new SessionId("123"), new Date(now.getTime() + 1000),
                "user123", new HashSet<String>(Arrays.asList("group2", "group20"))));
        manager.putSession(new Session(new SessionId("456"), now, "user456", new HashSet<String>(Arrays.asList("group3"))));
        manager.putSession(new Session(new SessionId("789"), new Date(now.getTime() - 1000),
                "user789", new HashSet<String>(Arrays.asList("group2"))));


        GeneralCompetenceFilter<CurrenciesQuery> filter = new GeneralCompetenceFilter<CurrenciesQuery>();
        filter.setSessionManager(manager);
        filter.setAccessMatrix(accessMatrix);


        ArrayList<CurrenciesQuery> requests = new ArrayList<CurrenciesQuery>(Arrays.asList(
                new CurrenciesQuery(1L, new SessionId("123")),
                new CurrenciesQuery(2L, new SessionId("456")),
                new CurrenciesQuery(3L, new SessionId("789")),
                new CurrenciesQuery(4L, new SessionId("000"))
        ));

        Map<Long, RejectedRequestReport> res = filter.filter(now, requests);

        assertEquals(3, res.size());
        assertEquals(new HashSet<Long>(Arrays.asList(2L, 3L, 4L)),	res.keySet());
    }

}
