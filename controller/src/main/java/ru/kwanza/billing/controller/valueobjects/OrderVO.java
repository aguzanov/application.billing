package ru.kwanza.billing.controller.valueobjects;

import java.util.Date;

/**
 * @author Dmitry Zhukov
 */
public abstract class OrderVO {

    private Long id;

    private Long sessionRecordId;

    private Date processedAt;

    private Integer rejectCode;

    protected OrderVO(Long id, Long sessionRecordId, Date processedAt, Integer rejectCode) {
        this.id = id;
        this.sessionRecordId = sessionRecordId;
        this.processedAt = processedAt;
        this.rejectCode = rejectCode;
    }

    public Long getId() {
        return id;
    }

    public Long getSessionRecordId() {
        return sessionRecordId;
    }

    public Date getProcessedAt() {
        return processedAt;
    }

    public Integer getRejectCode() {
        return rejectCode;
    }
}
