package ru.kwanza.billing.entity.api.id;

import ru.kwanza.billing.entity.api.CheckDigitUtil;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @author henadiy
 */
public class WalletAccountIdBuilder {

    public static final BigDecimal CONST_1E10 = BigDecimal.valueOf(10).pow(10);
    public static final BigDecimal CONST_1E6 = BigDecimal.valueOf(10).pow(6);

    private final BigDecimal id;
    private final Long walletId;
    private final Integer currencyId;

    public WalletAccountIdBuilder(BigDecimal id) {
        if (null == id) {
            throw new IllegalArgumentException("Id must not be null");
        }
        this.id = id;
        this.walletId = id.divide(CONST_1E10, RoundingMode.DOWN).longValue() * 10 +
                CheckDigitUtil.calculate(BigDecimal.valueOf(id.divide(CONST_1E10, RoundingMode.DOWN).longValue()));
        this.currencyId = id.divide(CONST_1E6, RoundingMode.DOWN).remainder(BigDecimal.valueOf(10000)).intValue();
    }

    public WalletAccountIdBuilder(Long walletId, Integer currency, Integer walletAccountCounter) {
        BigDecimal id = BigDecimal.valueOf(walletId / 10).multiply(CONST_1E10);
        id = id.add(BigDecimal.valueOf(currency).multiply(CONST_1E6));
        id = id.add(BigDecimal.valueOf(10L * walletAccountCounter));
        id = id.add(BigDecimal.valueOf(CheckDigitUtil.calculate(id.divide(BigDecimal.valueOf(10)))));

        this.id = id;
        this.walletId = walletId;
        this.currencyId = currency;
    }

    public BigDecimal getId() {
        return id;
    }

    public Long getWalletId() {
        return walletId;
    }

    public Integer getCurrencyId() {
        return currencyId;
    }
}
