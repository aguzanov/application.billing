package ru.kwanza.billing.clearing.entity;

import ru.kwanza.dbtool.orm.annotations.Entity;
import ru.kwanza.dbtool.orm.annotations.Field;
import ru.kwanza.dbtool.orm.annotations.IdField;

import java.io.Serializable;
import java.util.Date;

/**
 * @author henadiy
 */
@Entity(name = "ClearingGroup", table = "clearing_group")
public class ClearingGroup implements Serializable {

	@IdField("id")
	private Long id;

	@Field("period")
	private Integer period;

	@Field("currency_id")
	private Integer currencyId;

    @Field("last_cleared_at")
    private Date lastClearedAt;


	protected ClearingGroup() {
	}

    public ClearingGroup(Long id, Integer period, Integer currencyId, Date lastClearedAt) {
        this.id = id;
        this.period = period;
        this.currencyId = currencyId;
        this.lastClearedAt = lastClearedAt;
    }

    public Long getId() {
        return id;
    }

    public Integer getPeriod() {
        return period;
    }

    public Integer getCurrencyId() {
        return currencyId;
    }

    public Date getLastClearedAt() {
        return lastClearedAt;
    }

    public void setLastClearedAt(Date lastClearedAt) {
        this.lastClearedAt = lastClearedAt;
    }

}
