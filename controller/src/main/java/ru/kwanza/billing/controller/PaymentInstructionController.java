package ru.kwanza.billing.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.kwanza.billing.entity.api.PaymentInstruction;
import ru.kwanza.console.security.api.AuthOperation;
import ru.kwanza.dbtool.orm.api.IEntityManager;
import ru.kwanza.dbtool.orm.api.If;
import ru.kwanza.dbtool.orm.api.OrderBy;
import ru.kwanza.txn.api.Transactional;
import ru.kwanza.txn.api.TransactionalType;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Dmitry Zhukov
 */
@Controller
@RequestMapping("paymentinstruction")
public class PaymentInstructionController {

    @Resource(name = "dbtool.IEntityManager")
    private IEntityManager manager;

    @RequestMapping(value = "read", method = RequestMethod.GET)
    @ModelAttribute("data")
    @Transactional(TransactionalType.REQUIRES_NEW)
    @AuthOperation(name = "View payment instructions", check = "hasPermission('PaymentInstruction:read')", auditable = false)
    public List<PaymentInstruction> getList(@RequestParam(value = "start", required = false, defaultValue = "0") Integer start,
                                       @RequestParam(value = "limit", required = false, defaultValue = "25") Integer limit,
                                       @RequestParam(value = "id", required = false) Long id,
                                       @RequestParam(value = "filter.id", required = false) Long filterId,
                                       @RequestParam(value = "filter.orderId", required = false) Long orderId) {

        return manager.filtering(PaymentInstruction.class)
                .filter(id != null, If.isEqual("id"), id)
                .filter(filterId != null, If.isEqual("id"), filterId)
                .filter(orderId != null, If.isEqual("orderId"), orderId)
                .orderBy(OrderBy.ASC("recordedAt"))
                .paging(start,limit)
                .selectList();
    }
}
