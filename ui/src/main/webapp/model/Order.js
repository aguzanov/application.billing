/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.model.Order', {
    extend: 'Ext.data.Model',
    fields: [
        'id',
        'cachedId',
        {name: 'type'},
        {name: 'sessionId', mapping: 'sessionRecordId'},
        'exchangeId',
        {
            name: 'processedAt',
            type: 'date',
            dateFormat: 'd.m.Y H:i:s.u'
        },
        {name: 'rejectedReport', persist: false},
        {
            name: 'rejectCode',
            convert: function (v, r) {
                if (r.get('rejectedReport')) {
                    return r.get('rejectedReport').rejectCode;
                }
                return '';
            }
        }
    ],

    associations: [
        {
            type: 'hasOne',
            associationKey: 'sessionHistoryRecord',
            model: 'Billing.model.SessionHistory',
            foreignKey: 'sessionRecordId',
            getterName: 'getSessionHistoryRecord'
        },
        {
            type: 'hasOne',
            associationKey: 'exchange',
            model: 'Billing.model.RESTExchange',
            foreignKey: 'exchangeId',
            getterName: 'getExchange'
        }
    ],

    statics: {
        rejectedCodeRenderer: function (value, meta, record) {
            meta.iconCls = value && value != '' ? 'i-msg-blocked' : 'i-action-accept';
            if (value && value != '') {
                var state = Ext.getStore('Billing.store.lookup.RejectCode').getById(value);
                return state && state.title != '' ? state.get('title') : 'Successfully completed';
            }
            return 'Successfully completed';
        },

        sessionRenderer: function (value, metaData, record) {
            var session = record.getSessionHistoryRecord();
            return session ? Console.application.QuickLink.createLink(session.get('sessionKey'),
                'Billing.view.sessionhistory.SessionHistoryCard', {recordId: session.getId()}) : value;
        },

        exchangeRenderer: function (value, metaData, record) {
            var exchange = record.getExchange();
            return exchange ? Console.application.QuickLink.createLink(exchange.get('uri'),
                'Billing.view.restexchange.RESTExchangeCard', {recordId: exchange.getId()}) : value;
        }
    }
});
