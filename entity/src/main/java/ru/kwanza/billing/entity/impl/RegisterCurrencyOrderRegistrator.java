package ru.kwanza.billing.entity.impl;

import ru.kwanza.billing.api.order.RegisterCurrencyOrder;

import java.util.Date;

/**
 * @author Vasily Vorobyov
 */
public class RegisterCurrencyOrderRegistrator
        extends RequestRegistrator<RegisterCurrencyOrder, ru.kwanza.billing.entity.api.RegisterCurrencyOrder> {

    public RegisterCurrencyOrderRegistrator() {
        super(ru.kwanza.billing.entity.api.RegisterCurrencyOrder.class);
    }

    @Override
    protected ru.kwanza.billing.entity.api.RegisterCurrencyOrder createEntity(RegisterCurrencyOrder order, Date processedAt, Long sessionRecordId) {
        return new ru.kwanza.billing.entity.api.RegisterCurrencyOrder(order.getId(), sessionRecordId,order.getExchangeId(), processedAt, order.getCurencyId(),
                order.getAlphaCode(), order.getTitle(), order.getExponent());
    }

    @Override
    protected Long getEntityId(ru.kwanza.billing.entity.api.RegisterCurrencyOrder entity) {
        return entity.getId();
    }
}
