/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.model.WalletAccountIssueOrder', {
    extend: 'Billing.model.Order',
    fields: [
        'walletId',
        'accountId',
        'title'
    ],

    detailView: 'Billing.view.order.walletaccountissueorder.WalletAccountIssueOrderCard',

    permissions: {
        read: "WalletAccountIssueOrder:read",
        create: false,
        update: false,
        destroy: false
    },

    proxy: {
        type: 'ajax',
        apiUrl: 'billing-controller/walletaccountissueorder/'

    }
});