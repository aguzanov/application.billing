package ru.kwanza.billing.processor.order;

import ru.kwanza.billing.api.order.RegisterIssuerOrder;
import ru.kwanza.billing.api.report.IssuerReport;
import ru.kwanza.billing.api.report.RejectCode;
import ru.kwanza.billing.api.report.RejectedRequestReport;
import ru.kwanza.billing.api.report.Report;
import ru.kwanza.billing.entity.api.Issuer;
import ru.kwanza.billing.entity.api.dao.IIssuerDAO;
import ru.kwanza.billing.processor.RequestProcessor;
import ru.kwanza.dbtool.core.UpdateException;

import javax.annotation.Resource;
import java.util.*;

/**
 * @author Vasily Vorobyov
 */
public class RegisterIssuerOrderProcessor extends RequestProcessor<RegisterIssuerOrder> {

    @Resource(name = "billing.entity.dao.IssuerDAO")
    private IIssuerDAO issuerDAO;

    @Override
    protected Map<Long, Report> handleRequests(Date processedAt, Collection<RegisterIssuerOrder> requests) {
        final Map<Long, Report> reports = new HashMap<Long, Report>(requests.size());
        final Set<Integer> issuerIds = new HashSet<Integer>();

        Iterator<RegisterIssuerOrder> orderIt = requests.iterator();

        while (orderIt.hasNext()) {
            RegisterIssuerOrder order = orderIt.next();
            RejectCode rejectCode = null;
            if (null == order.getIssuerBin() || null == order.getTitle()) {
                rejectCode = RejectCode.EMPTY_REQUIRED_FIELD;
            } else if (order.getIssuerBin() <= 0 || order.getIssuerBin() > 999999) {
                rejectCode = RejectCode.ILLEGAL_ISSUER_BIN;
            } else if (null != order.getIssuerId()) {
                issuerIds.add(order.getIssuerId());
            }
            if (null != rejectCode) {
                reports.put(order.getId(), new RejectedRequestReport(order.getId(), processedAt, rejectCode));
                orderIt.remove();
            }
        }

        Map<Integer, Issuer> issuers = issuerDAO.readByKeys(issuerIds);

        List<Issuer> created = new ArrayList<Issuer>(requests.size() - issuerIds.size());
        for (RegisterIssuerOrder order: requests) {
            Issuer issuer = null;
            if (null != order.getIssuerId()) {
                issuer = issuers.get(order.getIssuerId());
                if (null != issuer) {
                    issuer.setBin(order.getIssuerBin());
                    issuer.setTitle(order.getTitle());
                }
            }
            if (null == issuer) {
                issuer = issuerDAO.prepare(order.getId(), order.getIssuerBin(), order.getTitle());
                created.add(issuer);
            }
            reports.put(order.getId(), new IssuerReport(order.getId(), processedAt,
                    issuer.getId(), issuer.getOrderId(), issuer.getBin(), issuer.getTitle(), issuer.getAccountCounter()));
        }

        try {
            if (!created.isEmpty()) {
                issuerDAO.create(created);
            }
            if (!issuers.isEmpty()) {
                issuerDAO.update(issuers.values());
            }
        } catch (UpdateException e) {
            throw new RuntimeException("Unable to create/update Issuers", e);
        }

        return reports;
    }

    @Override
    protected Map<Long, Report> answerDuplicateRequests(Map<Long, RegisterIssuerOrder> duplicateRequests) {
        // todo
        throw new UnsupportedOperationException("Implement me");
    }
}
