package ru.kwanza.billing.api.order;

import com.fasterxml.jackson.annotation.JsonTypeName;
import ru.kwanza.billing.api.RequestType;
import ru.kwanza.billing.api.SessionId;

/**
 * @author Vasily Vorobyov
 */
@JsonTypeName(value = "O_ACCOUNT_ISSUE")
public class AccountIssueOrder extends Order {

    private Integer issuerId;
    private Integer currencyId;
    private Long minBalance;
    private Long maxBalance;

    protected AccountIssueOrder() {
    }

    public AccountIssueOrder(Long id, SessionId sessionId, Integer issuerId, Integer currencyId, Long minBalance, Long maxBalance) {
        super(id, sessionId);
        this.currencyId = currencyId;
        this.issuerId = issuerId;
        this.minBalance = minBalance;
        this.maxBalance = maxBalance;
    }

    @Override
    public RequestType getRequestType() {
        return RequestType.O_ACCOUNT_ISSUE;
    }

    public Integer getCurrencyId() {
        return currencyId;
    }

    public Integer getIssuerId() {
        return issuerId;
    }

    public Long getMinBalance() {
        return minBalance;
    }

    public Long getMaxBalance() {
        return maxBalance;
    }
}
