/**
 * @author Dmitry Zhukov
 */

Ext.define('Billing.store.AccountStore',{
    extend: 'Ext.data.Store',
    model: 'Billing.model.Account',
    autoLoad: true,
    remoteFilter: true,
    remoteSort: false,
    autoSync: false
});