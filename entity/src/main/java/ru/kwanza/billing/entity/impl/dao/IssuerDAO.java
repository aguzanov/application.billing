package ru.kwanza.billing.entity.impl.dao;

import ru.kwanza.autokey.api.IAutoKey;
import ru.kwanza.billing.entity.api.Currency;
import ru.kwanza.billing.entity.api.Issuer;
import ru.kwanza.billing.entity.api.dao.IIssuerDAO;
import ru.kwanza.dbtool.core.UpdateException;
import ru.kwanza.dbtool.orm.api.IEntityManager;
import ru.kwanza.dbtool.orm.api.IQuery;
import ru.kwanza.dbtool.orm.api.If;
import ru.kwanza.dbtool.orm.api.OrderBy;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author Vasily Vorobyov
 */
public class IssuerDAO implements IIssuerDAO {

    @Resource(name = "dbtool.IEntityManager")
    private IEntityManager em;
    @Resource(name = "autokey.IAutoKey")
    protected IAutoKey autoKey;

    private IQuery<Issuer> queryAllIssuers;

    public void init() {
        queryAllIssuers = em.queryBuilder(Issuer.class).orderBy(OrderBy.ASC("id")).create();
    }

    public List<Issuer> readAll() {
        return queryAllIssuers.prepare().selectList();
    }

    public Issuer prepare(Long orderId, Integer bin, String title) {
        return new Issuer(autoKey.getNextValue(Issuer.class.getName()).intValue(), orderId, bin, title);
    }

    public Collection<Issuer> create(Collection<Issuer> issuers) throws UpdateException {
        return em.create(Issuer.class, issuers);
    }

    public Map<Integer, Issuer> readByKeys(Collection<Integer> ids) {
        return em.readMapByKeys(Issuer.class, ids, "id");
    }

    public void update(Collection<Issuer> issuers) throws UpdateException {
        if (null != issuers && !issuers.isEmpty()) {
            em.update(Issuer.class, issuers);
        }
    }
}
