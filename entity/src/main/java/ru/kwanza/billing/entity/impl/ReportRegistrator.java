package ru.kwanza.billing.entity.impl;

import ru.kwanza.billing.api.report.*;
import ru.kwanza.billing.entity.api.IReportRegistrator;
import ru.kwanza.billing.entity.api.RejectedReport;
import ru.kwanza.dbtool.orm.api.EntityUpdateException;
import ru.kwanza.dbtool.orm.api.IEntityBatcher;
import ru.kwanza.dbtool.orm.api.IEntityManager;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.Map;
import java.util.HashMap;

/**
 * @author Vasily Vorobyov
 */
public abstract class ReportRegistrator<API_ORDER_REPORT extends Report> implements IReportRegistrator<API_ORDER_REPORT> {

    @Resource(name = "dbtool.IEntityManager")
    protected IEntityManager em;

    public void registerReports(Collection<API_ORDER_REPORT> reports) {

        final IEntityBatcher entityBatcher = em.createEntityBatcher();

        for (API_ORDER_REPORT apiOrderReport : reports) {
            if (apiOrderReport instanceof RejectedRequestReport) {
                entityBatcher.create(convertRejectedOrderReport((RejectedRequestReport) apiOrderReport));
            } else {
                final Object entityOrderReport = convertOrderReport(apiOrderReport);
                if (null != entityOrderReport) {
                    entityBatcher.create(entityOrderReport);
                }
            }
        }

        try {
            entityBatcher.flush();
        } catch (EntityUpdateException e) {
            throw new RuntimeException("Unable to store order reports", e);
        }
    }

    public Map<Long, RejectedRequestReport> searchRejectedOrderReports(Collection<Long> requestIds) {
        Collection<RejectedReport> entityOrderReports = em.readByKeys(RejectedReport.class, requestIds);
        HashMap<Long, RejectedRequestReport> apiOrderReports =
                new HashMap<Long, RejectedRequestReport>(entityOrderReports.size());

        for (RejectedReport entityOrderReport: entityOrderReports) {
            apiOrderReports.put(entityOrderReport.getId(), convertRejectedOrderReport(entityOrderReport));
        }

        return apiOrderReports;
    }

    private RejectedReport convertRejectedOrderReport(RejectedRequestReport apiOrderReport) {
        return new RejectedReport(apiOrderReport.getId(), apiOrderReport.getProcessedAt(), apiOrderReport.getRejectCode().getCode());
    }

    private RejectedRequestReport convertRejectedOrderReport(RejectedReport entityOrderReport) {
        return new RejectedRequestReport(
                entityOrderReport.getId(), entityOrderReport.getProcessedAt(), RejectCode.findByCode(entityOrderReport.getRejectCode()));
    }

    protected abstract <ENTITY_ORDER_REPORT> ENTITY_ORDER_REPORT convertOrderReport(API_ORDER_REPORT apiOrderReport);
}
