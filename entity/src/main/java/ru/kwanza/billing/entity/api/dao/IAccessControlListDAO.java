package ru.kwanza.billing.entity.api.dao;

import ru.kwanza.billing.api.PaymentToolId;
import ru.kwanza.billing.entity.api.PaymentToolACE;
import ru.kwanza.dbtool.core.UpdateException;

import java.util.Collection;
import java.util.Map;

/**
 * @author henadiy
 */
public interface IAccessControlListDAO {

	PaymentToolACE preparePaymentToolACE(PaymentToolId paymentToolId, String userId, Integer permissions);

	Map<String, PaymentToolACE> readPaymentToolACEByIds(Collection<String> ids);

	Collection<PaymentToolACE> createPaymentToolACE(Collection<PaymentToolACE> aces) throws UpdateException;

	Collection<PaymentToolACE> updatePaymentToolACE(Collection<PaymentToolACE> aces) throws UpdateException;

    Collection<PaymentToolACE> deletePaymentToolACE(Collection<PaymentToolACE> aces) throws UpdateException;

}
