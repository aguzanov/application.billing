package ru.kwanza.billing.api.report;

import java.util.Date;

/**
 * @author Dmitry Zhukov
 */
public class RemoveCurrenciesReport extends CurrenciesReport {
    public RemoveCurrenciesReport() {
    }

    public RemoveCurrenciesReport(Long id, Date processedAt, Currency[] currencies) {
        super(id, processedAt, currencies);
    }
}
