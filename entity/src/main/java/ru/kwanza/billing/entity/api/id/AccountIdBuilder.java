package ru.kwanza.billing.entity.api.id;

import ru.kwanza.billing.entity.api.CheckDigitUtil;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;

/**
 * @author henadiy
 */
public class AccountIdBuilder {
    public static final BigDecimal CONST_1E19 = BigDecimal.valueOf(10).pow(19);
    public static final BigDecimal CONST_1E15 = BigDecimal.valueOf(10).pow(15);
    public static final BigDecimal CONST_1E11 = BigDecimal.valueOf(10).pow(11);
    public static final BigDecimal CONST_1E4 = BigDecimal.valueOf(10).pow(4);

    private final BigDecimal id;
    private final Integer bin;
    private final Integer currencyId;

    public AccountIdBuilder(BigDecimal id) {
        if (null == id) {
            throw new IllegalArgumentException("Id must not be null");
        }
        this.id = id;
        this.currencyId = id.divide(CONST_1E15, RoundingMode.DOWN).remainder(CONST_1E4).intValue();
        this.bin = id.divide(CONST_1E19, RoundingMode.DOWN).intValue();
    }

    public AccountIdBuilder(Integer bin, Integer currencyId, Integer accountCounter) {
        Calendar ts = Calendar.getInstance();
        BigDecimal id = BigDecimal.valueOf(bin % 1000000);
        id = id.multiply(CONST_1E19);
        id = id.add(BigDecimal.valueOf(currencyId % 10000).multiply(CONST_1E15));
        id = id.add(BigDecimal.valueOf((ts.get(Calendar.YEAR) % 100) * 100 + ts.get(Calendar.WEEK_OF_YEAR)).multiply(CONST_1E11));
        id = id.add(BigDecimal.valueOf(10L * accountCounter));
        id = id.add(BigDecimal.valueOf(CheckDigitUtil.calculate(id)));
        this.id = id;
        this.currencyId = currencyId;
        this.bin = bin;
    }

    public BigDecimal getId() {
        return id;
    }

    public Integer getBin() {
        return bin;
    }

    public Integer getCurrencyId() {
        return currencyId;
    }
}
