package ru.kwanza.billing.controller.valueobjects;

import java.util.Date;

/**
 * @author Dmitry Zhukov
 */
public class RegisterCurrencyOrderVO extends OrderVO {

    private Integer currencyId;

    private String alphaCode;

    private String title;

    private Integer exponent;

    public RegisterCurrencyOrderVO(Long id, Long sessionRecordId, Date processedAt, Integer rejectCode, Integer currencyId, String alphaCode, String title,
                                   Integer exponent) {
        super(id, sessionRecordId, processedAt, rejectCode);
        this.currencyId = currencyId;
        this.alphaCode = alphaCode;
        this.title = title;
        this.exponent = exponent;
    }

    public Integer getCurrencyId() {
        return currencyId;
    }

    public String getAlphaCode() {
        return alphaCode;
    }

    public String getTitle() {
        return title;
    }

    public Integer getExponent() {
        return exponent;
    }
}
