/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.view.order.registercurrencyorder.RegisterCurrencyOrderList', {
    extend: 'Billing.view.order.OrderList',

    requires: [
        'Billing.store.RegisterCurrencyOrderStore'
    ],

    alias: ['widget.register-currency-order-list', 'widget.registercurrencyorderlist'],
    tabTitle: 'Creating and editing orders',
    caption: 'Creating and editing orders',

    model: 'Billing.model.RegisterCurrencyOrder',
    store: 'Billing.store.RegisterCurrencyOrderStore',
    iconCls: 'i-document-report',

    constructor: function () {
        var me = this;

        me.columns = [
            {
                text: 'Order params',
                columns: [
                    {
                        text: 'Code',
                        dataIndex: 'currencyId',
                        width: 60,
                        filter: true,
                        renderer: Billing.model.RegisterCurrencyOrder.currencyRenderer
                    },
                    {
                        text: 'Alpha code',
                        dataIndex: 'alphaCode',
                        width: 60,
                        filter: true,
                    },
                    {
                        text: 'Name',
                        dataIndex: 'title',
                        width: 60,
                        filter: true
                    },
                    {
                        text: 'Exponent',
                        dataIndex: 'exponent',
                        width: 60
                    }

                ]
            }
        ];

        me.callParent(arguments);
    }
});
