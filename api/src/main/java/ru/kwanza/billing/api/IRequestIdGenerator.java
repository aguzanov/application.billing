package ru.kwanza.billing.api;

import java.util.Collection;

/**
 * @author Vasily Vorobyov
 */
public interface IRequestIdGenerator {

    Collection<Long> generateIds(int count);
}
