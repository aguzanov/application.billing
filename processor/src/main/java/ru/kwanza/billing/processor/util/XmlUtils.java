package ru.kwanza.billing.processor.util;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author henadiy
 */
public class XmlUtils {
	public static String getNodeText(Node node, String textIfBlank) {
		String res = null;
		if (null != node) {
			res = node.getTextContent();
		}
		return (null != res && res.length() > 0) ? new String(res.toString()) : textIfBlank;
	}

	public static Map<String, Element> getChildElementsMap(Node element) {
		Map<String, Element> result = new LinkedHashMap<String, Element>();
		if (null != element) {
			NodeList childNodes = element.getChildNodes();
			for(int i = 0; i < childNodes.getLength(); i++) {
				Node node = childNodes.item(i);
				if (Node.ELEMENT_NODE == node.getNodeType()) {
					result.put(new String(node.getNodeName()), (Element)node);
				}
			}
		}
		return result;
	}
}
