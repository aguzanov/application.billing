package ru.kwanza.billing.processor;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import ru.kwanza.billing.api.PaymentToolId;
import ru.kwanza.billing.api.PaymentToolPermission;
import ru.kwanza.billing.api.SessionId;
import ru.kwanza.billing.api.order.ConversionInstruction;
import ru.kwanza.billing.api.order.PaymentInstruction;
import ru.kwanza.billing.api.order.PaymentOrder;
import ru.kwanza.billing.api.query.AccountReportQuery;
import ru.kwanza.billing.api.report.RejectCode;
import ru.kwanza.billing.api.report.RejectedRequestReport;
import ru.kwanza.billing.entity.api.PaymentToolACE;
import ru.kwanza.billing.entity.api.dao.IAccessControlListDAO;
import ru.kwanza.billing.api.Session;
import ru.kwanza.billing.processor.session.TestSessionManager;
import ru.kwanza.dbtool.core.UpdateException;

import java.math.BigDecimal;
import java.util.*;

import static org.junit.Assert.*;

/**
 * @author henadiy
 */
@RunWith(JUnit4.class)
public class AccountAccessCompetenceFilterTest {

	class ACLDAOImpl implements IAccessControlListDAO {

		private Map<String, PaymentToolACE> paymentToolACEs = new HashMap<String, PaymentToolACE>();

		public PaymentToolACE preparePaymentToolACE(PaymentToolId paymentToolId, String userId, Integer permissions) {
			return null;
		}

		public Map<String, PaymentToolACE> readPaymentToolACEByIds(Collection<String> ids) {
			Map<String, PaymentToolACE> res = new HashMap<String, PaymentToolACE>();
			for(String id : ids) {
				if (paymentToolACEs.containsKey(id)) {
					res.put(id, paymentToolACEs.get(id));
				}
			}
			return res;
		}

		public Collection<PaymentToolACE> createPaymentToolACE(Collection<PaymentToolACE> aces) throws UpdateException {
			return null;
		}

		public Collection<PaymentToolACE> updatePaymentToolACE(Collection<PaymentToolACE> aces) throws UpdateException {
			return null;
		}

        public Collection<PaymentToolACE> deletePaymentToolACE(Collection<PaymentToolACE> aces) throws UpdateException {
            return null;
        }

        public void addPaymentToolACE(PaymentToolACE ace) {
			paymentToolACEs.put(ace.getId(), ace);
		}

	}

	@Test
	public void testQuerySuccess() throws Exception {

		Date now = new Date();

		TestSessionManager manager = new TestSessionManager();
		manager.putSession(new Session(new SessionId("123"), new Date(now.getTime() + 1000),
				"user123", new HashSet<String>(Arrays.asList("group1", "group2"))));
		manager.putSession(new Session(new SessionId("456"), new Date(now.getTime() + 1000),
				"user456", new HashSet<String>(Arrays.asList("group3"))));
		manager.putSession(new Session(new SessionId("789"), new Date(now.getTime() + 1000),
				"user789", new HashSet<String>(Arrays.asList("group3"))));
		manager.putSession(new Session(new SessionId("000"), new Date(now.getTime() + 1000),
				"user000", new HashSet<String>(Arrays.asList("group3"))));



        MockAccessMatrix accessMatrix = new MockAccessMatrix();
        accessMatrix.addRolePermissions("group2", "AccountReportQuery:read");

        ACLDAOImpl dao = new ACLDAOImpl();
		dao.addPaymentToolACE(new PaymentToolACE(
				new PaymentToolId(new BigDecimal("1234567"), PaymentToolId.Type.ACCOUNT),
				"user456", PaymentToolPermission.pack(PaymentToolPermission.READ)));
		dao.addPaymentToolACE(new PaymentToolACE(
				new PaymentToolId(new BigDecimal("1234567"), PaymentToolId.Type.ACCOUNT),
				"user789", PaymentToolPermission.pack(PaymentToolPermission.PAYMENT)));


		AccountAccessCompetenceFilter<AccountReportQuery> filter = new AccountAccessCompetenceFilter<AccountReportQuery>();
		filter.setSessionManager(manager);
		filter.setAccessControlListDAO(dao);
        filter.setAccessMatrix(accessMatrix);


        ArrayList<AccountReportQuery> requests = new ArrayList<AccountReportQuery>(
                Arrays.asList(
                        new AccountReportQuery(1L, new SessionId("123"), new BigDecimal("1234567")),
                        new AccountReportQuery(2L, new SessionId("456"), new BigDecimal("1234567")),
                        new AccountReportQuery(3L, new SessionId("789"), new BigDecimal("1234567")),
                        new AccountReportQuery(4L, new SessionId("000"), new BigDecimal("1234567"))
                )
        );

		Map<Long, RejectedRequestReport> res = filter.filter(now, requests);

		assertEquals(2, res.size());
		assertEquals(RejectCode.LACKS_OF_AUTHORITY, res.get(3L).getRejectCode());
		assertEquals(RejectCode.NO_ACCESS_OR_ENTITY, res.get(4L).getRejectCode());
	}

	@Test
	public void testOrderSuccess() throws Exception {

		Date now = new Date();

		TestSessionManager manager = new TestSessionManager();
		manager.putSession(new Session(new SessionId("123"), new Date(now.getTime() + 1000),
				"user123", new HashSet<String>(Arrays.asList("group1", "group2"))));
		manager.putSession(new Session(new SessionId("456"), new Date(now.getTime() + 1000),
				"user456", new HashSet<String>(Arrays.asList("group3"))));
		manager.putSession(new Session(new SessionId("789"), new Date(now.getTime() + 1000),
				"user789", new HashSet<String>(Arrays.asList("group3"))));
		manager.putSession(new Session(new SessionId("000"), new Date(now.getTime() + 1000),
				"user000", new HashSet<String>(Arrays.asList("group3"))));


        MockAccessMatrix accessMatrix = new MockAccessMatrix();
        accessMatrix.addRolePermissions("group2",
                "PaymentOrder:payment", "PaymentOrder:conversion", "PaymentOrder:deposit", "PaymentOrder:withdrawal");
        accessMatrix.addRolePermissions("group3",
                "PaymentOrder:payment");


		ACLDAOImpl dao = new ACLDAOImpl();
		dao.addPaymentToolACE(new PaymentToolACE(
				new PaymentToolId(new BigDecimal("1234567"), PaymentToolId.Type.ACCOUNT),
				"user456", PaymentToolPermission.pack(
					PaymentToolPermission.PAYMENT, PaymentToolPermission.DEPOSIT, PaymentToolPermission.WITHDRAWAL)));
		dao.addPaymentToolACE(new PaymentToolACE(
				new PaymentToolId(new BigDecimal("11223344"), PaymentToolId.Type.ACCOUNT),
				"user456", PaymentToolPermission.pack(PaymentToolPermission.PAYMENT, PaymentToolPermission.CONVERSION)));
		dao.addPaymentToolACE(new PaymentToolACE(
				new PaymentToolId(new BigDecimal("1111111"), PaymentToolId.Type.WALLET_ACCOUNT),
				"user456", PaymentToolPermission.pack(PaymentToolPermission.PAYMENT)));

		dao.addPaymentToolACE(new PaymentToolACE(
				new PaymentToolId(new BigDecimal("1234567"), PaymentToolId.Type.ACCOUNT),
				"user789", PaymentToolPermission.pack(PaymentToolPermission.PAYMENT, PaymentToolPermission.CONVERSION)));
		dao.addPaymentToolACE(new PaymentToolACE(
				new PaymentToolId(new BigDecimal("1111111"), PaymentToolId.Type.WALLET_ACCOUNT),
				"user789", PaymentToolPermission.pack(PaymentToolPermission.READ)));


		AccountAccessCompetenceFilter<PaymentOrder> filter = new AccountAccessCompetenceFilter<PaymentOrder>();
		filter.setSessionManager(manager);
		filter.setAccessControlListDAO(dao);
        filter.setAccessMatrix(accessMatrix);

        ArrayList<PaymentOrder> requests = new ArrayList<PaymentOrder>(Arrays.asList(
                new PaymentOrder(1L, new SessionId("123"), "", new PaymentInstruction[]{
                        new PaymentInstruction(
                                new PaymentToolId(new BigDecimal("1234567"), PaymentToolId.Type.ACCOUNT),
                                new PaymentToolId(new BigDecimal("7654321"), PaymentToolId.Type.ACCOUNT), 10L),
                        new PaymentInstruction(
                                new PaymentToolId(new BigDecimal("22222222"), PaymentToolId.Type.WALLET_ACCOUNT),
                                new PaymentToolId(new BigDecimal("33333333"), PaymentToolId.Type.WALLET_ACCOUNT), 10L),
                        new ConversionInstruction(new SessionId("123"), new Date(114, 3, 12, 15, 35, 22), new Date(114, 3, 12, 16, 35, 22),
                                new PaymentToolId(new BigDecimal("11223344"), PaymentToolId.Type.ACCOUNT), 10L, 123, BigDecimal.ONE,
                                new PaymentToolId(new BigDecimal("44332211"), PaymentToolId.Type.ACCOUNT), 1L, 123, BigDecimal.TEN
                        ),
                        new PaymentInstruction(
                                new PaymentToolId(new BigDecimal("1234567"), PaymentToolId.Type.ACCOUNT),
                                PaymentToolId.BLANK, 10L),
                        new PaymentInstruction(
                                PaymentToolId.BLANK,
                                new PaymentToolId(new BigDecimal("1234567"), PaymentToolId.Type.ACCOUNT), 10L),
                }),

                new PaymentOrder(2L, new SessionId("456"), "", new PaymentInstruction[]{
                        new PaymentInstruction(
                                new PaymentToolId(new BigDecimal("1234567"), PaymentToolId.Type.ACCOUNT),
                                new PaymentToolId(new BigDecimal("7654321"), PaymentToolId.Type.ACCOUNT), 10L),
                        new PaymentInstruction(
                                new PaymentToolId(new BigDecimal("1111111"), PaymentToolId.Type.WALLET_ACCOUNT),
                                new PaymentToolId(new BigDecimal("0000000"), PaymentToolId.Type.WALLET_ACCOUNT), 10L),
                        new ConversionInstruction(new SessionId("456"), new Date(114, 3, 12, 15, 35, 22), new Date(114, 3, 12, 16, 35, 22),
                                new PaymentToolId(new BigDecimal("11223344"), PaymentToolId.Type.ACCOUNT), 10L, 123, BigDecimal.ONE,
                                new PaymentToolId(new BigDecimal("44332211"), PaymentToolId.Type.ACCOUNT), 1L, 123, BigDecimal.TEN
                        ),
                        new PaymentInstruction(
                                new PaymentToolId(new BigDecimal("1234567"), PaymentToolId.Type.ACCOUNT),
                                PaymentToolId.BLANK, 10L),
                        new PaymentInstruction(
                                PaymentToolId.BLANK,
                                new PaymentToolId(new BigDecimal("1234567"), PaymentToolId.Type.ACCOUNT), 10L),
                }),

                new PaymentOrder(3L, new SessionId("789"), "", new PaymentInstruction[]{
                        new PaymentInstruction(
                                new PaymentToolId(new BigDecimal("1234567"), PaymentToolId.Type.ACCOUNT),
                                new PaymentToolId(new BigDecimal("7654321"), PaymentToolId.Type.ACCOUNT), 10L),
                        new PaymentInstruction(
                                new PaymentToolId(new BigDecimal("1111111"), PaymentToolId.Type.WALLET_ACCOUNT),
                                new PaymentToolId(new BigDecimal("0000000"), PaymentToolId.Type.WALLET_ACCOUNT), 10L),
                        new ConversionInstruction(new SessionId("789"), new Date(114, 3, 12, 15, 35, 22), new Date(114, 3, 12, 16, 35, 22),
                                new PaymentToolId(new BigDecimal("1234567"), PaymentToolId.Type.ACCOUNT), 10L, 123, BigDecimal.ONE,
                                new PaymentToolId(new BigDecimal("7654321"), PaymentToolId.Type.ACCOUNT), 1L, 123, BigDecimal.TEN
                        ),
                        new PaymentInstruction(
                                new PaymentToolId(new BigDecimal("1234567"), PaymentToolId.Type.ACCOUNT),
                                PaymentToolId.BLANK, 10L),
                        new PaymentInstruction(
                                PaymentToolId.BLANK,
                                new PaymentToolId(new BigDecimal("1234567"), PaymentToolId.Type.ACCOUNT), 10L),
                }),

                new PaymentOrder(4L, new SessionId("000"), "", new PaymentInstruction[]{
                        new PaymentInstruction(
                                new PaymentToolId(new BigDecimal("1234567"), PaymentToolId.Type.ACCOUNT),
                                new PaymentToolId(new BigDecimal("7654321"), PaymentToolId.Type.ACCOUNT), 10L),
                        new PaymentInstruction(
                                new PaymentToolId(new BigDecimal("1111111"), PaymentToolId.Type.WALLET_ACCOUNT),
                                new PaymentToolId(new BigDecimal("0000000"), PaymentToolId.Type.WALLET_ACCOUNT), 10L),
                        new ConversionInstruction(new SessionId("000"), new Date(114, 3, 12, 15, 35, 22), new Date(114, 3, 12, 16, 35, 22),
                                new PaymentToolId(new BigDecimal("1234567"), PaymentToolId.Type.ACCOUNT), 10L, 123, BigDecimal.ONE,
                                new PaymentToolId(new BigDecimal("7654321"), PaymentToolId.Type.ACCOUNT), 1L, 123, BigDecimal.TEN
                        ),
                        new PaymentInstruction(
                                new PaymentToolId(new BigDecimal("1234567"), PaymentToolId.Type.ACCOUNT),
                                PaymentToolId.BLANK, 10L),
                        new PaymentInstruction(
                                PaymentToolId.BLANK,
                                new PaymentToolId(new BigDecimal("1234567"), PaymentToolId.Type.ACCOUNT), 10L),
                })

        ));

		Map<Long, RejectedRequestReport> res = filter.filter(now, requests);

		assertEquals(2, res.size());
		assertEquals(RejectCode.LACKS_OF_AUTHORITY, res.get(3L).getRejectCode());
		assertEquals(RejectCode.NO_ACCESS_OR_ENTITY, res.get(4L).getRejectCode());

	}
}
