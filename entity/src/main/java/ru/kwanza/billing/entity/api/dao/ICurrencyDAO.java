package ru.kwanza.billing.entity.api.dao;

import ru.kwanza.billing.entity.api.Currency;
import ru.kwanza.dbtool.core.UpdateException;

import java.util.Collection;
import java.util.Map;

/**
 * @author Vasily Vorobyov
 */
public interface ICurrencyDAO {

    Map<Integer, Currency> readByKeys(Collection<Integer> ids);

    Map<Integer, Currency> readAll();

    Currency prepare(Integer id, String alphaCode, Integer exponent, String title);

    Collection<Currency> create(Collection<Currency> currencies) throws UpdateException;

    Collection<Currency> update(Collection<Currency> currencies) throws UpdateException;

    Collection<Currency> delete(Collection<Currency> currencies) throws UpdateException;
}
