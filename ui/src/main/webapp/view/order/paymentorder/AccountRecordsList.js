/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.view.order.paymentorder.AccountRecordsList', {
    extend: 'Console.grid.BaseGrid',

    alias: 'widget.accountrecordslist',

    updateInterval: 0,

    permissions: {
        read: "AccountRecord:read",
        create: false,
        update: false,
        destroy: false
    },

    tabTitle: 'Account operations list',
    caption: 'Account operations list',
    iconCls: 'i-money',

    columns: [
        {
            text: 'ID',
            dataIndex: 'id',
            width: 118
        },
        {
            xtype: 'datecolumn',
            format: 'd.m.Y H:i:s.u',
            text: 'Processing date',
            dataIndex: 'processedAt',
            width: 150,
            filter: {
                allowBlank: true
            }
        },
        {
            text: 'Sender',
            columns: [
                {
                    xtype: 'quicklinkcolumn',
                    detailView: 'Billing.view.account.AccountCard',
                    valueField: 'sourceAccountId',
                    text: 'Account',
                    dataIndex: 'sourceAccountId',
                    width: 145
                },
                {
                    text: 'Balance',
                    dataIndex: 'sourceIncomingBalance',
                    width: 130
                },
                {
                    text: 'Amount',
                    dataIndex: 'sourceAmount',
                    width: 130
                }
            ]
        },
        {
            text: 'Receiver',
            columns: [
                {
                    xtype: 'quicklinkcolumn',
                    detailView: 'Billing.view.account.AccountCard',
                    valueField: 'targetAccountId',
                    text: 'Account',
                    dataIndex: 'targetAccountId',
                    width: 145
                },
                {
                    text: 'Balance',
                    dataIndex: 'targetIncomingBalance',
                    width: 130
                },
                {
                    text: 'Amount',
                    dataIndex: 'targetAmount',
                    width: 130
                }
            ]
        },
        {
            text: 'Description',
            dataIndex: 'description',
            width: 333
        }
    ]
});
