package ru.kwanza.billing.entity.impl.dao;

import ru.kwanza.billing.entity.api.AccountRecord;
import ru.kwanza.billing.entity.api.ConversionInstruction;
import ru.kwanza.billing.entity.api.PaymentInstruction;
import ru.kwanza.billing.entity.api.RejectedPaymentInstruction;
import ru.kwanza.billing.entity.api.dao.IPaymentInstructionDAO;
import ru.kwanza.dbtool.core.UpdateException;
import ru.kwanza.dbtool.orm.api.*;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;

/**
 * @author Vasily Vorobyov
 */
public class PaymentInstructionDAO implements IPaymentInstructionDAO {

    @Resource(name = "dbtool.IEntityManager")
    private IEntityManager em;


    public PaymentInstruction preparePaymentInstruction(BigDecimal id, Long orderId, Integer indexInOrder,
                                                        BigDecimal sourceAccountId, BigDecimal sourceWalletAccountId, Long sourceAmount,
                                                        BigDecimal targetAccountId, BigDecimal targetWalletAccountId, Long targetAmount,
                                                        Date recordedAt, String description) {
        return new PaymentInstruction(id, orderId, indexInOrder,
                sourceAccountId, sourceWalletAccountId, sourceAmount,
                targetAccountId, targetWalletAccountId, targetAmount,
                recordedAt, description);
    }

    public Collection<PaymentInstruction> createPaymentInstructions(Collection<PaymentInstruction> instructions) throws UpdateException {
        return em.create(PaymentInstruction.class, instructions);
    }

    public ConversionInstruction prepareConversionInstruction(BigDecimal id,
                                                              Date issuedAt, Date expiresIn,
                                                              Integer sourceCurrencyId, BigDecimal sourceScale,
                                                              Integer targetCurrencyId, BigDecimal targetScale) {
        return new ConversionInstruction(id, issuedAt, expiresIn,
                sourceCurrencyId, sourceScale, targetCurrencyId, targetScale);
    }

    public Collection<ConversionInstruction> createConversionInstructions(Collection<ConversionInstruction> instructions) throws UpdateException {
        return em.create(ConversionInstruction.class, instructions);
    }

    public RejectedPaymentInstruction prepareRejectedPaymentInstruction(
            BigDecimal id, Long sourceIncomingBalance, Long targetIncomingBalance) {
        return new RejectedPaymentInstruction(id, sourceIncomingBalance, targetIncomingBalance);
    }

    public Collection<RejectedPaymentInstruction> createRejectedPaymentInstructions(
            Collection<RejectedPaymentInstruction> rejectedPaymentInstructions) throws UpdateException {
        return em.create(RejectedPaymentInstruction.class, rejectedPaymentInstructions);
    }

    public AccountRecord prepareAccountRecord(BigDecimal id, Long orderId,
                                              BigDecimal sourceAccountId, Integer sourceRecordNumber, Long sourceAmount, Long sourceIncomingBalance,
                                              BigDecimal targetAccountId, Integer targetRecordNumber, Long targetAmount, Long targetIncomingBalance,
                                              Date processedAt, String description) {
        return new AccountRecord(id, orderId,
                sourceAccountId, sourceRecordNumber, sourceAmount, sourceIncomingBalance,
                targetAccountId, targetRecordNumber, targetAmount, targetIncomingBalance,
                processedAt, description);
    }

    public Collection<AccountRecord> createAccountRecords(Collection<AccountRecord> records) throws UpdateException {
        return em.create(AccountRecord.class, records);
    }


    public Collection<AccountRecord> selectDepositRecordsByAccountId(Collection<BigDecimal> ids, Date recorderedAfter, Date recorderedBefore) {
        if(ids.isEmpty()){
            return Collections.emptyList();
        }
        return em.filtering(AccountRecord.class).filter(
                new Filter(!ids.isEmpty(), If.in("targetAccountId"), ids),
                new Filter(recorderedAfter != null, If.isGreaterOrEqual("recordedAt"), recorderedAfter),
                new Filter(recorderedBefore != null, If.isLessOrEqual("recordedAt"), recorderedBefore)
        ).orderBy(OrderBy.ASC("recordNumber")).selectList();
    }

    public Collection<AccountRecord> selectWithdrawalRecordsByAccountId(Collection<BigDecimal> ids, Date recorderedAfter, Date recorderedBefore) {
        if(ids.isEmpty()){
            return Collections.emptyList();
        }
        return em.filtering(AccountRecord.class).filter(
                new Filter(!ids.isEmpty(), If.in("sourceAccountId"), ids),
                new Filter(recorderedAfter != null, If.isGreaterOrEqual("recordedAt"), recorderedAfter),
                new Filter(recorderedBefore != null, If.isLessOrEqual("recordedAt"), recorderedBefore)
        ).orderBy(OrderBy.ASC("recordNumber")).selectList();
    }

}
