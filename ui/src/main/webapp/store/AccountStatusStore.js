/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.store.AccountStatusStore', {
    extend: 'Ext.data.Store',
    fields: ['id', 'title', 'iconCls'],

    data: [
        {"id": 1, "title": "Active", iconCls: 'i-action-accept'},
        {"id": 2, "title": "Blocked", iconCls: 'i-action-close'},
        {"id": 3, "title": "Closed", iconCls: 'i-action-close'},
        {"id": -1, "title": "Not defined", iconCls: 'i-msg-question'}
    ],
    statics: {
        renderer: function (id, meta) {
            var record = Ext.StoreManager.get('Billing.store.AccountStatusStore').getById(id);

            meta.iconCls = record.get('iconCls');

            return record.get('title');
        }
    }
});