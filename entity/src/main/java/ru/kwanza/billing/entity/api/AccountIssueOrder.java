package ru.kwanza.billing.entity.api;

import ru.kwanza.console.security.api.AuthEntity;
import ru.kwanza.console.security.api.AuthField;
import ru.kwanza.dbtool.orm.annotations.Association;
import ru.kwanza.dbtool.orm.annotations.Entity;
import ru.kwanza.dbtool.orm.annotations.Field;
import ru.kwanza.dbtool.orm.annotations.ManyToOne;

import javax.mail.search.OrTerm;
import java.util.Date;

/**
 * @author Vasily Vorobyov
 */
@Entity(table = "account_issue_order")
@AuthEntity(name = "AccountIssueOrder", description = "Issue and modification account order")
public class AccountIssueOrder extends Order {

    @Field("issuer_id")
    @AuthField(name = "Issuer")
    private Integer issuerId;

    @Field("currency_id")
    @AuthField(name = "Currency")
    private Integer currencyId;

    @ManyToOne(property = "issuerId")
    private Issuer issuer;

    @Association(property = "id", relationProperty = "orderId")
    private Account account;

    @Association(property = "currencyId", relationProperty = "id")
    private Currency currency;

    public AccountIssueOrder() {
    }

    @Override
    public void initType() {
        type = OrderType.REGISTER_ACCOUNT.getCode();
    }

    public AccountIssueOrder(Long id, Long sessionRecordId, Long exchangeId, Date processedAt, Integer issuerId, Integer currencyId) {
        super(id, sessionRecordId, exchangeId, processedAt, OrderType.REGISTER_ACCOUNT);
        this.issuerId = issuerId;
        this.currencyId = currencyId;
    }

    public Integer getIssuerId() {
        return issuerId;
    }

    public Issuer getIssuer() {
        return issuer;
    }

    public Account getAccount() {
        return account;
    }

    public Integer getCurrencyId() {
        return currencyId;
    }

    public Currency getCurrency() {
        return currency;
    }


}
