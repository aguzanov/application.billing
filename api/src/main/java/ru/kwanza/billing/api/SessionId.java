package ru.kwanza.billing.api;

import com.fasterxml.jackson.annotation.JsonTypeName;

import java.util.HashMap;
import java.util.Map;

/**
 * @author henadiy
 */
@JsonTypeName(value = "SESSION_ID")
public class SessionId {

    public static final String DETAIL_VALIDATION_FACTORS = "validationFactors";

    private String token;
    private Map<String, Object> details = new HashMap<String, Object>();

    protected SessionId() {
    }

    public SessionId(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public Map<String, Object> getDetails() {
        return details;
    }

    public void putDetail(String key, Object value) {
        details.put(key, value);
    }

    @Override
    public boolean equals(Object obj) {
        return this == obj || (obj instanceof SessionId) && getToken().equals(((SessionId) obj).getToken());
    }

    @Override
    public int hashCode() {
        return getToken().hashCode();
    }

    @Override
    public String toString() {
        return new StringBuilder("SessionId{")
                .append("token=").append(getToken()).append(", ")
                .append("details=").append(getDetails())
                .append("}").toString();
    }
}
