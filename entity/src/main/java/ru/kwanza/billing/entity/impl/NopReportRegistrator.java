package ru.kwanza.billing.entity.impl;

import ru.kwanza.billing.api.report.Report;

import java.util.Collection;

/**
 * @author Vasily Vorobyov
 */
public class NopReportRegistrator extends ReportRegistrator<Report> {

    @Override
    protected <ENTITY_ORDER_REPORT> ENTITY_ORDER_REPORT convertOrderReport(Report orderReport) {
        return null;
    }

    public Collection<Report> readOrderReports(Collection<Long> ids) {
        return null;
    }
}
