package ru.kwanza.billing.processor.order;

import ru.kwanza.billing.api.Session;
import ru.kwanza.billing.api.SessionId;
import ru.kwanza.billing.api.order.CloseSessionOrder;
import ru.kwanza.billing.api.report.RejectCode;
import ru.kwanza.billing.api.report.RejectedRequestReport;
import ru.kwanza.billing.api.report.Report;
import ru.kwanza.billing.api.report.SessionReport;
import ru.kwanza.billing.processor.RequestProcessor;
import ru.kwanza.billing.processor.session.ISessionManager;

import java.util.*;

/**
 * @author henadiy
 */
public class CloseSessionOrderProcessor extends RequestProcessor<CloseSessionOrder> {

	private ISessionManager sessionManager;

	public void setSessionManager(ISessionManager sessionManager) {
		this.sessionManager = sessionManager;
	}

	@Override
	protected Map<Long, Report> handleRequests(Date processedAt, Collection<CloseSessionOrder> requests) {
		HashMap<Long, Report> orderReports = new HashMap<Long, Report>(requests.size());

		Set<SessionId> sessionIds = new HashSet<SessionId>(requests.size());
		for(CloseSessionOrder order : requests) {
			sessionIds.add(order.getSessionId());
		}

		Map<SessionId, Session> closedSessions = sessionManager.close(sessionIds);
		for(CloseSessionOrder order : requests) {
			Session session = closedSessions.get(order.getSessionId());
			if (null != session) {
				orderReports.put(order.getId(),
						new SessionReport(order.getId(), processedAt, session));
			} else {
				orderReports.put(order.getId(),
						new RejectedRequestReport(order.getId(), processedAt, RejectCode.LACKS_OF_AUTHORITY));
			}
		}

		return orderReports;
	}

	@Override
	protected Map<Long, Report> answerDuplicateRequests(Map<Long, CloseSessionOrder> duplicateRequests) {
		return Collections.emptyMap();
	}
}
