/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.model.AccountIssueOrder', {
    extend: 'Billing.model.Order',
    fields:[
        'issuerId',
        {
            name: 'issuer',
            persist: false
        },
        {
            name: 'issuerTitle',
            persist: false,
            convert:function(v, r){
                var issuer = r.get('issuer');
                return issuer ? issuer.title : '';
            }
        },
        {name: 'account', persist: false},
        {name: 'accountId', convert:function(v,r){
            var account = r.get('account');
            if(account){
                return account.id;
            }
            return '';
        }},
        {name: 'currency', persist: false}
    ],

    entityClassName: 'AccountIssueOrder',

    detailView: 'Billing.view.order.accountissueorder.AccountIssueOrderCard',

    permissions: {
        read: "AccountIssueOrder:read",
        create: false,
        update: false,
        destroy: false
    },

    proxy: {
        type: 'ajax',
        apiUrl: 'billing-controller/accountissueorder/'

    }
});