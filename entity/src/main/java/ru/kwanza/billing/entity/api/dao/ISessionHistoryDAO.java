package ru.kwanza.billing.entity.api.dao;

import ru.kwanza.billing.entity.api.SessionHistoryRecord;
import ru.kwanza.dbtool.core.UpdateException;

import java.util.Collection;
import java.util.Date;
import java.util.Map;

/**
 * @author henadiy
 */
public interface ISessionHistoryDAO {

    Collection<SessionHistoryRecord> create(Collection<SessionHistoryRecord> records) throws UpdateException;

    Collection<SessionHistoryRecord> update(Collection<SessionHistoryRecord> records) throws UpdateException;

    Map<String, SessionHistoryRecord> readBySessionKeys(Collection<String> sessionKeys);

    Map<Long, SessionHistoryRecord> readByIds(Collection<Long> ids);
}
