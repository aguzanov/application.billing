package ru.kwanza.billing.processor.session;

import ru.kwanza.billing.api.Session;
import ru.kwanza.billing.api.SessionId;
import ru.kwanza.billing.api.order.OpenSessionOrder;
import ru.kwanza.billing.entity.impl.SessionHistory;
import ru.kwanza.toolbox.Base64;

import javax.annotation.Resource;
import java.net.URLEncoder;
import java.util.*;

/**
 * @author henadiy
 */
public class NopSessionManager implements ISessionManager {

    @Resource(name = "billing.entity.SessionHistory")
    private SessionHistory history;

    public Map<Long, Session> open(Collection<OpenSessionOrder> openSessionOrders) {
        Map<Long, Session> infoMap = new HashMap<Long, Session>(openSessionOrders.size());
        for (OpenSessionOrder order : openSessionOrders) {
            SessionId sid;
            if (null != order.getArguments() && order.getArguments().get(OpenSessionOrder.ARG_TOKEN) != null) {
                sid = new SessionId(String.valueOf(order.getArguments().get(OpenSessionOrder.ARG_TOKEN)));
                } else {
                StringBuilder sessionId = new StringBuilder("id=").append(order.getId().toString()).append("&");
                try {
                    for (Map.Entry<String, Object> e : order.getArguments().entrySet()) {
                        if(e.getValue()!=null) {
                            sessionId.append(URLEncoder.encode(e.getKey(), "UTF-8")).append("=").append(URLEncoder.encode(e.getValue().toString(), "UTF-8")).append("&");
                        }
                    }
                } catch (final Exception e) {
                    throw new RuntimeException(e);
                }

                sessionId.append("ts").append(System.currentTimeMillis());

                sid = new SessionId(Base64.encodeBytes(sessionId.toString().getBytes()));
            }
            final Session value = new Session(sid, null, order.getUserData().getUsername(), Collections.EMPTY_SET);
            infoMap.put(order.getId(), value);
        }
        history.register(infoMap.values());
        return infoMap;
    }


    public Map<SessionId, Session> close(Collection<SessionId> sessionIdsToClose) {
        Map<SessionId, Session> infoMap = new HashMap<SessionId, Session>(sessionIdsToClose.size());
        for (SessionId sid : sessionIdsToClose) {
            infoMap.put(sid, new Session(sid, new Date(), sid.getToken(), Collections.EMPTY_SET));
        }
        history.registerClosed(infoMap.values());
        return infoMap;
    }

    public Map<SessionId, Session> get(Collection<SessionId> sessionIds) {
        Map<SessionId, Session> infoMap = new HashMap<SessionId, Session>(sessionIds.size());
        for (SessionId sid : sessionIds) {
            infoMap.put(sid, new Session(sid, null, sid.getToken(), Collections.EMPTY_SET));
        }
        history.register(infoMap.values());
        return infoMap;
    }

}
