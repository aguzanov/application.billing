package ru.kwanza.billing.controller;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.kwanza.billing.api.AccountStatus;
import ru.kwanza.billing.api.order.AccountIssueOrder;
import ru.kwanza.billing.api.report.AccountReport;
import ru.kwanza.billing.api.report.RejectedRequestReport;
import ru.kwanza.billing.api.report.Report;
import ru.kwanza.billing.controller.billing.BillingHelper;
import ru.kwanza.billing.controller.exceptions.AccountException;
import ru.kwanza.billing.controller.valueobjects.AccountVO;
import ru.kwanza.billing.entity.api.Account;
import ru.kwanza.console.core.IMerger;
import ru.kwanza.console.security.api.AuthOperation;
import ru.kwanza.console.security.api.EntityAuditor;
import ru.kwanza.dbtool.core.UpdateException;
import ru.kwanza.dbtool.orm.api.IEntityManager;
import ru.kwanza.dbtool.orm.api.If;
import ru.kwanza.dbtool.orm.api.OrderBy;
import ru.kwanza.txn.api.Transactional;
import ru.kwanza.txn.api.TransactionalType;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author Dmitry Zhukov
 */

@Controller
@RequestMapping("account")
public class AccountController {

    @Resource(name = "dbtool.IEntityManager")
    private IEntityManager manager;

    @Resource(name = "console.IMerger")
    private IMerger merger;

    @Resource(name = "billing.BillingHelper")
    private BillingHelper billingHelper;


    @RequestMapping(value = "read", method = RequestMethod.GET)
    @ModelAttribute("data")
    @Transactional(TransactionalType.REQUIRES_NEW)
    @AuthOperation(name = "View accounts", check = "hasPermission('Account:read')", auditable = false)
    public List<Account> getList(@RequestParam(value = "start", required = false) Integer start,
                                   @RequestParam(value = "limit", required = false) Integer limit,
                                   @RequestParam(value = "id", required = false) BigDecimal id,
                                   @RequestParam(value = "filter.id", required = false) BigDecimal filterId,
                                   @RequestParam(value = "filter.issuerId", required = false) Long issuerId,
                                   @RequestParam(value = "filter.currencyId", required = false) Long currencyId,
                                   @RequestParam(value = "filter.createdAt.from", required = false) @DateTimeFormat(
                                           pattern = "dd.MM.yyyy HH:mm:ss") Date createdAtFrom,
                                   @RequestParam(value = "filter.createdAt.to", required = false) @DateTimeFormat(
                                           pattern = "dd.MM.yyyy HH:mm:ss") Date createdAtTo,
                                   @RequestParam(value = "filter.statusCode", required = false) Integer statusCode) {

        return manager.filtering(Account.class).filter(id != null, If.isEqual("id"), id)
                .filter(filterId != null, If.isEqual("id"), filterId)
                .filter(issuerId != null, If.isEqual("order.issuerId"), issuerId)
                .filter(currencyId != null, If.isEqual("order.currencyId"), currencyId)
                .filter(createdAtFrom != null, If.isGreaterOrEqual("createdAt"), createdAtFrom)
                .filter(createdAtTo != null, If.isLessOrEqual("createdAt"), createdAtTo)
                .filter(statusCode != null, If.isEqual("statusCode"), statusCode).paging(start, limit)
                .join("order{issuer,currency}")
                .orderBy(OrderBy.DESC("createdAt")).selectList();
    }

    @RequestMapping(value = "create", method = RequestMethod.POST)
    @ModelAttribute("data")
    @Transactional(TransactionalType.REQUIRES_NEW)
    @AuthOperation(name = "Creaate account", check = "hasPermission('Account:create')")
    public Account create(@RequestBody AccountVO data, HttpServletRequest request)
            throws IllegalAccessException, UpdateException, AccountException {
        Report report = billingHelper.processRequest(
                new AccountIssueOrder(billingHelper.generateRequestId(), billingHelper.getSessionId(request),
                        data.getIssuerId(), data.getCurrencyId(), data.getMinBalance(), data.getMaxBalance())
        );

        if (report instanceof RejectedRequestReport) {
            throw new AccountException.AccountCreateException(((RejectedRequestReport) report).getRejectCode());
        }

        final Account result = manager.readByKey(Account.class, ((AccountReport) report).getAccountId());
        manager.fetch(result,"order{issuer,currency}");
        return EntityAuditor.logCreate(result);
    }

    @RequestMapping(value = "update", method = RequestMethod.POST)
    @ModelAttribute("data")
    @Transactional(TransactionalType.REQUIRES_NEW)
    @AuthOperation(name = "Update account", check = "hasPermission('Account:update')")
    public Account update(@RequestBody Account data) throws UpdateException, IllegalAccessException, AccountException {

        //todo aguzanov why not billing api reguqest?

        Account account = manager.readByKey(Account.class, data.getId());

        if (account == null) {
            throw new AccountException.AccountNotFoundException();
        }
        EntityAuditor.logBeforeUpdate(account);


        merger.merge(account, data);
        manager.fetch(account,"order{issuer,currency}");
        //dzhukov: repacking is needed for references to the Issuer and currency of the account
        return EntityAuditor.logAfterUpdate(manager.update(account));
    }

    @RequestMapping(value = "close", method = RequestMethod.POST)
    @ModelAttribute("data")
    @Transactional(TransactionalType.REQUIRES_NEW)
    @AuthOperation(name = "Close account", check = "hasPermission('Account:delete')")
    public Account destroy(@RequestParam(value = "id", required = false) BigDecimal id)
            throws UpdateException, IllegalAccessException, AccountException {

        //todo aguzanov why not RemoveAccountOrder

        Account result = manager.readByKey(Account.class, id);

        if (result == null) {
            throw new AccountException.AccountNotFoundException();
        }

        result.setStatus(AccountStatus.CLOSED);

        return EntityAuditor.logDelete(manager.update(result));
    }
}
