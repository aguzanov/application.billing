package ru.kwanza.billing.tariffication;

/**
 * @author henadiy
 */
public interface ICalculator<A extends IArguments, R extends IResult<A>> {

    R calculate(A arguments);

}
