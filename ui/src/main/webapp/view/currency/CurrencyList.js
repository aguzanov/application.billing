/**
 * @author Dmitry Zhukov
 */

Ext.define('Billing.view.currency.CurrencyList', {
    requires: [
        'Console.grid.FiltersFeature',
        'Console.application.QuickLink',
        'Billing.store.CurrencyStore'
    ],
    extend: 'Console.grid.BaseGrid',
    alias: ['widget.currency-list', 'widget.currencylist'],
    tabTitle: 'Currencies',
    caption: 'Currencies',

    model: 'Billing.model.Currency',
    store: 'Billing.store.CurrencyStore',
    iconCls: 'i-money',

    columns: [
        {
            xtype: 'quicklinkcolumn',
            text: 'Currency code',
            dataIndex: 'id',
            width: 200,
            filter: true
        },
        {
            text: 'Alpha code',
            dataIndex: 'alphaCode',
            width: 200,
            filter: true
        },
        {
            xtype: 'quicklinkcolumn',
            text: 'Name',
            dataIndex: 'title',
            width: 200,
            filter: true


        },
        {
            text: 'Exponent',
            dataIndex: 'exponent',
            width: 200
        }
    ]
});
