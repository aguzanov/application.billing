/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.store.PaymentInstructionStore', {
    extend: 'Ext.data.Store',
    model: 'Billing.model.PaymentInstruction',
    autoLoad: true,
    remoteFilter: true,
    remoteSort: false,
    autoSync: false
});