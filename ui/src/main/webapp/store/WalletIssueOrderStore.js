/**
/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.store.WalletIssueOrderStore', {
    extend: 'Ext.data.Store',
    model: 'Billing.model.WalletIssueOrder',
    autoLoad: true,
    remoteFilter: true,
    remoteSort: false,
    autoSync: false
});