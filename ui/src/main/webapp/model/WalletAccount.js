/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.model.WalletAccount', {
    extend: 'Ext.data.Model',
    fields: [
        'id',
        'cachedId',
        'version',
        {name: 'createdAt', type: 'date', dateFormat: 'd.m.Y H:i:s.u'},
        {name: 'order', persist: false},
        'orderId',
        'title',
        {
            name: 'accountTitle',
            persist: false,
            convert: function (v, r) {
                var order = r.get('order');
                var account = order ? order.account : undefined
                return account ? account.id : '';
            }
        },
        {
            name: 'accountId',
            convert: function (v, r) {
                var order = r.get('order');
                var account = order ? order.issuer : undefined
                return account ? account.id : v;
            }
        },
        {
            name: 'walletTitle',
            persist: false,
            convert: function (v, r) {
                var order = r.get('order');
                var wallet = order ? order.wallet : undefined
                return wallet ? wallet.title : '';
            }
        },
        {
            name: 'walletId',
            convert: function (v, r) {
                var order = r.get('order');
                var wallet = order ? order.wallet : undefined
                return wallet ? wallet.id : v;
            }
        },

    ],

    detailView: 'Billing.view.walletaccount.WalletAccountCard',

    permissions: {
        read: "WalletAccount:read",
        create: "WalletAccount:create",
        update: false,
        destroy: "WalletAccount:delete"
    },

    proxy: {
        type: 'ajax',
        apiUrl: 'billing-controller/walletaccount/'
    },

    statics: {
        orderRenderer: function (value, meta, record) {
            var order = record.get('order');
            return Console.application.QuickLink.createLink("ID: " + order.id + ", processed at: " + order.processedAt,
                'Billing.view.order.walletaccountissueorder.WalletAccountIssueOrderCard', {recordId: order.id});
        }
    }
});