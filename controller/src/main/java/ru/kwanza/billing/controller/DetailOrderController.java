package ru.kwanza.billing.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.kwanza.billing.entity.api.*;
import ru.kwanza.console.security.api.AuthOperation;
import ru.kwanza.dbtool.orm.api.Filter;
import ru.kwanza.dbtool.orm.api.IEntityManager;
import ru.kwanza.dbtool.orm.api.If;
import ru.kwanza.dbtool.orm.api.OrderBy;
import ru.kwanza.txn.api.Transactional;
import ru.kwanza.txn.api.TransactionalType;

import javax.annotation.Resource;
import java.util.*;

/**
 * @author Dmitry Zhukov
 */

@Controller
@RequestMapping("detailorder")
public class DetailOrderController {

    @Autowired
    private AccountIssueOrderController accountIssueOrderController;
    @Autowired
    private PaymentsController paymentsController;
    @Autowired
    private RegisterCurrencyOrderController registerCurrencyOrderController;
    @Autowired
    private RegisterIssuerOrderController registerIssuerOrderController;
    @Autowired
    private RemoveCurrencyOrderController removeCurrencyOrderControllerController;
    @Autowired
    private WalletIssueOrderController walletIssueOrderController;
    @Autowired
    private WalletAccountIssueOrderController walletAccountIssueOrderController;


    @RequestMapping(value = "read", method = RequestMethod.GET)
    @ModelAttribute("data")
    @Transactional(TransactionalType.REQUIRES_NEW)
    public List<Order> getList(@RequestParam(value = "start", required = false) Integer start,
                               @RequestParam(value = "limit", required = false) Integer limit,
                               @RequestParam(value = "id", required = false) Long id,
                               @RequestParam(value = "filter.id", required = false) Long filterId,
                               @RequestParam(value = "filter.type", required = false) Long type,
                               @RequestParam(value = "filter.sessionId", required = false) String sessionId,
                               @RequestParam(value = "filter.processedAt.from", required = false) @DateTimeFormat(
                                       pattern = "dd.MM.yyyy HH:mm:ss") Date processedAtFrom,
                               @RequestParam(value = "filter.processedAt.to", required = false) @DateTimeFormat(
                                       pattern = "dd.MM.yyyy HH:mm:ss") Date processedAtTo,
                               @RequestParam(value = "filter.rejectCode", required = false) Integer rejectCode) {


        final ArrayList<Order> orders = new ArrayList<Order>();

        if (type == null || type == OrderType.REGISTER_ACCOUNT.getCode()) {
            orders.addAll(accountIssueOrderController.getList(0, limit + start, id, filterId, sessionId, processedAtFrom, processedAtTo, null, rejectCode));
        }

        if (type == null || type == OrderType.PAYMENT_ORDER.getCode()) {
            orders.addAll(paymentsController.getList(0, limit + start, id, filterId, sessionId, processedAtFrom, processedAtTo, rejectCode));
        }

        if (type == null || type == OrderType.REGISTER_CURRENCY.getCode()) {
            orders.addAll(registerCurrencyOrderController.getList(0, limit + start, id, filterId, sessionId, processedAtFrom, processedAtTo, null, null, null, rejectCode));
        }

        if (type == null || type == OrderType.REGISTER_ISSUER.getCode()) {
            orders.addAll(registerIssuerOrderController.getList(0, limit + start, id, filterId, sessionId, processedAtFrom, processedAtTo, null, null, rejectCode));
        }

        if (type == null || type == OrderType.REMOVE_CURRENCY_ORDER.getCode()) {
            orders.addAll(removeCurrencyOrderControllerController.getList(0, limit + start, id, filterId, sessionId, processedAtFrom, processedAtTo, null, rejectCode));
        }

        if (type == null || type == OrderType.REGISTER_WALLET.getCode()) {
            orders.addAll(walletIssueOrderController.getList(0, limit + start, id, filterId, sessionId, processedAtFrom, processedAtTo, rejectCode));
        }

        if (type == null || type == OrderType.REGISTER_WALLET_ACCOUNT.getCode()) {
            orders.addAll(walletAccountIssueOrderController.getList(0, limit + start, id, filterId, sessionId, processedAtFrom, processedAtTo, null, null, null, rejectCode));
        }
        
        Collections.sort(orders, new Comparator<Order>() {
            public int compare(Order o1, Order o2) {
                return o2.getProcessedAt().compareTo(o1.getProcessedAt());
            }
        });

        final List<Order> result = orders.subList(Math.min(start, orders.size()), Math.min(start + limit, orders.size()));
        for (Order order : result) {
            order.initType();
        }
        return result;
    }
}
