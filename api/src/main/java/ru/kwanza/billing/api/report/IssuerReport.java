package ru.kwanza.billing.api.report;

import com.fasterxml.jackson.annotation.JsonTypeName;

import java.util.Date;

/**
 * @author Vasily Vorobyov
 */
@JsonTypeName(value = "ISSUER_REPORT")
public class IssuerReport extends Report {

    private Integer issuerId;
    private Long orderId;
    private Integer bin;
    private String title;
    private Integer accountCounter;

    protected IssuerReport() {
    }

    public IssuerReport(Long id, Date processedAt, Integer issuerId, Long orderId, Integer bin, String title, Integer accountCounter) {
        super(id, processedAt);
        this.issuerId = issuerId;
        this.orderId = orderId;
        this.bin = bin;
        this.title = title;
        this.accountCounter = accountCounter;
    }

    public Integer getIssuerId() {
        return issuerId;
    }

    public Long getOrderId() {
        return orderId;
    }

    public Integer getBin() {
        return bin;
    }

    public String getTitle() {
        return title;
    }

    public Integer getAccountCounter() {
        return accountCounter;
    }
}
