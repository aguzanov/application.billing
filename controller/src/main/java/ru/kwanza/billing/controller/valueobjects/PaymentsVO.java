package ru.kwanza.billing.controller.valueobjects;

import ru.kwanza.billing.entity.api.Account;
import ru.kwanza.billing.entity.api.PaymentOrder;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Dmitry Zhukov
 * The class is a value object for classes AccountRecord, RejectRecord, PaymentInstruction and PaymentOrder
 */
public class PaymentsVO {
    private BigDecimal id;

    private Long orderId;

    private BigDecimal sourceAccountId;

    private Integer sourceRecordNumber;

    private Long sourceAmount;

    private Long sourceIncomingBalance;

    private BigDecimal targetAccountId;

    private Integer targetRecordNumber;

    private Long targetAmount;

    private Long targetIncomingBalance;

    private Date processedAt;

    private String description;

    private PaymentOrder paymentOrder;

    private Account sourceAccount;

    private Account targetAccount;

    private Integer rejectCode;

    public PaymentsVO(BigDecimal id, Long orderId, BigDecimal sourceAccountId, Integer sourceRecordNumber, Long sourceAmount,
                      Long sourceIncomingBalance, BigDecimal targetAccountId, Integer targetRecordNumber, Long targetAmount,
                      Long targetIncomingBalance, Date processedAt, String description, PaymentOrder paymentOrder, Account sourceAccount,
                      Account targetAccount, Integer rejectCode) {
        this.id = id;
        this.orderId = orderId;
        this.sourceAccountId = sourceAccountId;
        this.sourceRecordNumber = sourceRecordNumber;
        this.sourceAmount = sourceAmount;
        this.sourceIncomingBalance = sourceIncomingBalance;
        this.targetAccountId = targetAccountId;
        this.targetRecordNumber = targetRecordNumber;
        this.targetAmount = targetAmount;
        this.targetIncomingBalance = targetIncomingBalance;
        this.processedAt = processedAt;
        this.description = description;
        this.paymentOrder = paymentOrder;
        this.sourceAccount = sourceAccount;
        this.targetAccount = targetAccount;
        this.rejectCode = rejectCode;
    }

    public BigDecimal getId() {
        return id;
    }

    public Long getOrderId() {
        return orderId;
    }

    public BigDecimal getSourceAccountId() {
        return sourceAccountId;
    }

    public Integer getSourceRecordNumber() {
        return sourceRecordNumber;
    }

    public Long getSourceAmount() {
        return sourceAmount;
    }

    public Long getSourceIncomingBalance() {
        return sourceIncomingBalance;
    }

    public BigDecimal getTargetAccountId() {
        return targetAccountId;
    }

    public Integer getTargetRecordNumber() {
        return targetRecordNumber;
    }

    public Long getTargetAmount() {
        return targetAmount;
    }

    public Long getTargetIncomingBalance() {
        return targetIncomingBalance;
    }

    public Date getProcessedAt() {
        return processedAt;
    }

    public String getDescription() {
        return description;
    }

    public PaymentOrder getPaymentOrder() {
        return paymentOrder;
    }

    public Account getSourceAccount() {
        return sourceAccount;
    }

    public Account getTargetAccount() {
        return targetAccount;
    }

    public Integer getRejectCode() {
        return rejectCode;
    }
}
