/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.controller.WalletController', {
    extend: 'Ext.app.Controller',
    models: [
        'Billing.model.Wallet'
    ],
    stores: [
        'Billing.store.WalletStore',
        'Billing.store.WalletStatusStore'
    ],
    views: [
        'Billing.view.wallet.WalletCard',
        'Billing.view.wallet.WalletList'
    ],

    init: function () {
        var me = this;
        this.control({
            'walletdetails #activateWallet': {
                click: me.activateWalletFromCard
            },

            'walletlist #activateWallet': {
                click: me.activateWallet
            },
            'walletdetails #suspendWallet': {
                click: me.suspendWalletFromCard
            },

            'walletlist #suspendWallet': {
                click: me.suspendWallet
            },
            'walletdetails #closeWallet': {
                click: me.closeWalletFromCard
            },

            'walletlist #closeWallet': {
                click: me.closeWallet
            },
        });
    },

    activateWallet: function (button) {
        var walletlist = button.up('walletlist'),
            record = walletlist.getSelectionModel().getSelection()[0];
        this.doChangeWalletStatus(record, button, 'activate', 'activate', 'The Wallet successfully activated.', 'Failed to activate your wallet.');

    },

    activateWalletFromCard: function (button) {
        var walletdetails = button.up('walletdetails'),
            record = walletdetails.record;
        this.doChangeWalletStatus(record, button, 'activate', 'activate', 'The Wallet successfully activated.', 'Failed to activate your wallet.');

    },

    suspendWallet: function (button) {
        var walletlist = button.up('walletlist'),
            record = walletlist.getSelectionModel().getSelection()[0];
        this.doChangeWalletStatus(record, button, 'suspend', 'block', 'The wallet was successfully blocked.', 'Failed to lock the wallet.');

    },

    suspendWalletFromCard: function (button) {
        var walletdetails = button.up('walletdetails'),
            record = walletdetails.record;
        this.doChangeWalletStatus(record, button, 'suspend', 'block', 'The wallet was successfully blocked.', 'Failed to lock the wallet.');

    },

    closeWallet: function (button) {
        var walletlist = button.up('walletlist'),
            record = walletlist.getSelectionModel().getSelection()[0];
        this.doChangeWalletStatus(record, button, 'close', 'close', 'The wallet is closed successfully.', 'Failed to close the purse.');

    },

    closeWalletFromCard: function (button) {
        var walletdetails = button.up('walletdetails'),
            record = walletdetails.record;
        this.doChangeWalletStatus(record, button, 'close', 'close', 'The wallet is closed successfully.', 'Failed to close the purse.');

    },

    doChangeWalletStatus: function (record, button, actionUrl, action, successMessage, failureMessage) {
        var me = this;
        me.onRemove({
            accept: function () {
                var store = Ext.getStore('Billing.store.WalletStore');
                Ext.Ajax.request(
                    {
                        url: 'billing-controller/wallet/' + actionUrl,
                        method: "POST",
                        async: true,
                        params: {
                            id: record.get('id')
                        },
                        success: function (response) {
                            var result = Ext.decode(response.responseText);
                            if (result.success) {

                                store.reload();
                                Ext.MessageBox.show({
                                    title: 'Success',
                                    msg: successMessage,
                                    buttons: Ext.Msg.OK,
                                    closable: false,
                                    icon: Ext.Msg.INFO
                                });
                            } else {
                                Ext.MessageBox.show({
                                    title: 'Error:' + result.messageTitle,
                                    msg: failureMessage + ' ' + result.message,
                                    buttons: Ext.Msg.OK,
                                    closable: false,
                                    icon: Ext.Msg.ERROR
                                });
                            }
                        },
                        failure: function (response) {
                            button.up('.window').close();
                            Ext.MessageBox.show({
                                title: 'Error:' + result.messageTitle,
                                msg: failureMessage + ' ' + result.message,
                                buttons: Ext.Msg.OK,
                                closable: false,
                                icon: Ext.Msg.ERROR
                            });
                        }
                    });

            },
            reject: function () {
            }
        }, record, action);
    },

    onRemove: function (handler, record, action) {
        handler.wait = true;
        var me = this;

        Ext.Msg.show(
            {title: 'Confirmation',
                msg: 'Indeed ' + action + ' wallet?',
                width: 300,
                buttons: Ext.Msg.YESNO,
                buttonText: {
                    yes: 'Yes',
                    no: 'No'
                },
                fn: function (buttonId) {
                    if ('yes' === buttonId) {
                        handler.accept();
                    } else {
                        handler.reject();
                    }
                },
                icon: Ext.Msg.QUESTION
            });
    }


});
