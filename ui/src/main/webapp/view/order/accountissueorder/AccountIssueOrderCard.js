/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.view.order.accountissueorder.AccountIssueOrderCard', {
    extend: 'Billing.view.order.OrderCard',

    alias: 'widget.accountissueorderdetails',

    title: 'Register account',
    iconCls: 'i-document-order',

    model: 'Billing.model.AccountIssueOrder',
    store: 'Billing.store.AccountIssueOrderStore',

    constructor: function (config) {
        var me = this;
        me.items = [
            {
                title: 'Additional parameters of the order',
                columnWidth: 1,
                items: [
                    {
                        xtype:'infofield',
                        valueField: 'issuerId',
                        detailView: 'Billing.view.issuer.IssuerCard',
                        fieldLabel: 'Issuer',
                        name: 'issuerTitle'
                    },
                    {
                        xtype:'infofield',
                        valueField: 'accountId',
                        detailView: 'Billing.view.account.AccountCard',
                        fieldLabel: 'Account',
                        name: 'accountId'
                    }
                ]
            }
        ];
        me.callParent(arguments);
        me.items.get('main').columnWidth = 1;
    }
});
