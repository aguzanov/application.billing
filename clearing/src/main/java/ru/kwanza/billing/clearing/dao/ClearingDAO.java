package ru.kwanza.billing.clearing.dao;

import ru.kwanza.billing.clearing.entity.ClearingAccount;
import ru.kwanza.billing.clearing.entity.ClearingGroup;
import ru.kwanza.billing.entity.api.AccountRecord;
import ru.kwanza.dbtool.core.UpdateException;
import ru.kwanza.dbtool.orm.api.IEntityManager;
import ru.kwanza.dbtool.orm.api.IQuery;
import ru.kwanza.dbtool.orm.api.If;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

/**
 * @author henadiy
 */
public class ClearingDAO implements IClearingDAO {

    @Resource(name = "dbtool.IEntityManager")
    private IEntityManager em;

	private IQuery<ClearingAccount> queryClearingAccountsByGroupId;
    private IQuery<ClearingGroup> queryClearingGroups;
    private IQuery<AccountRecord> queryAccountRecordsByAccountIdAndNumber;

    public void init() {
		queryClearingAccountsByGroupId = em.queryBuilder(ClearingAccount.class)
				.join("clearingMember")
				.where(If.isEqual("clearingGroupId"))
				.create();
        queryClearingGroups = em.queryBuilder(ClearingGroup.class).create();
		queryAccountRecordsByAccountIdAndNumber = em.queryBuilder(AccountRecord.class)
				.where(
					If.and(
							If.or(
									If.and(
											If.isEqual("sourceAccountId"),
											If.isGreater("sourceRecordNumber"),
											If.isNotNull("targetAccountId")
									),
									If.and(
											If.isEqual("targetAccountId"),
											If.isGreater("targetRecordNumber"),
											If.isNotNull("sourceAccountId")
									)
							),
							If.isLessOrEqual("processedAt")
					)
				).create();
    }

    public Collection<ClearingAccount> updateClearingAccounts(Collection<ClearingAccount> clearingAccounts) throws UpdateException {
        return em.create(ClearingAccount.class, clearingAccounts);
    }

    public Collection<ClearingAccount> createClearingAccounts(Collection<ClearingAccount> clearingAccounts) throws UpdateException {
        return em.update(ClearingAccount.class, clearingAccounts);
    }

    public Collection<ClearingAccount> readClearingGroupAccounts(ClearingGroup clearingGroup) {
		return queryClearingAccountsByGroupId.prepare().setParameter("clearingGroupId", clearingGroup.getId()).selectList();
	}

	public Collection<AccountRecord> readAccountRecordsByClearedRecordNumberAndPeriod(
			BigDecimal accountId, Integer recordNumber, Date to) {
		return queryAccountRecordsByAccountIdAndNumber.prepare()
				.setParameter("sourceAccountId", accountId).setParameter("targetAccountId", accountId)
				.setParameter("sourceRecordNumber", recordNumber).setParameter("targetRecordNumber", recordNumber)
				.setParameter("processedAt", to)
				.selectList();
	}

    public Collection<ClearingGroup> readClearingGroups() {
        return queryClearingGroups.prepare().selectList();
    }

    public Collection<ClearingGroup> createClearingGroups(Collection<ClearingGroup> clearingGroups) throws UpdateException {
        return em.create(ClearingGroup.class, clearingGroups);
    }

    public Collection<ClearingGroup> updateClearingGroups(Collection<ClearingGroup> clearingGroups) throws UpdateException {
        return em.update(ClearingGroup.class, clearingGroups);
    }
}
