package ru.kwanza.billing.api.query;

import com.fasterxml.jackson.annotation.JsonTypeName;
import ru.kwanza.billing.api.PaymentToolId;
import ru.kwanza.billing.api.RequestType;
import ru.kwanza.billing.api.SessionId;

/**
 * @author henadiy
 */
@JsonTypeName(value = "Q_CONVERSION_INSTRUCTION")
public class ConversionInstructionQuery extends Query {

    private PaymentToolId sourceToolId;
    private PaymentToolId targetToolId;
    private Integer sourceCurrencyId;
    private Integer targetCurrencyId;
    private Long sourceAmount;
    private Long targetAmount;


    protected ConversionInstructionQuery() {

    }

    public ConversionInstructionQuery(Long id, SessionId sessionId, PaymentToolId sourceToolId, Long sourceAmount,
                                      PaymentToolId targetToolId, Long targetAmount) {
        super(id, sessionId);
        this.sourceToolId = sourceToolId;
        this.targetToolId = targetToolId;
        this.sourceCurrencyId = null;
        this.targetCurrencyId = null;
        this.sourceAmount = sourceAmount;
        this.targetAmount = targetAmount;
    }

    public ConversionInstructionQuery(Long id, SessionId sessionId, Integer sourceCurrencyId, Long sourceAmount,
                                      PaymentToolId targetToolId, Long targetAmount) {
        super(id, sessionId);
        this.sourceToolId = PaymentToolId.BLANK;
        this.targetToolId = targetToolId;
        this.sourceCurrencyId = sourceCurrencyId;
        this.targetCurrencyId = null;
        this.sourceAmount = sourceAmount;
        this.targetAmount = targetAmount;
    }

    public ConversionInstructionQuery(Long id, SessionId sessionId, PaymentToolId sourceToolId, Long sourceAmount,
                                      Integer targetCurrencyId, Long targetAmount) {
        super(id, sessionId);
        this.sourceToolId = sourceToolId;
        this.targetToolId = PaymentToolId.BLANK;
        this.sourceCurrencyId = null;
        this.targetCurrencyId = targetCurrencyId;
        this.sourceAmount = sourceAmount;
        this.targetAmount = targetAmount;
    }


    public PaymentToolId getSourceToolId() {
        return sourceToolId;
    }

    public PaymentToolId getTargetToolId() {
        return targetToolId;
    }

    public Integer getSourceCurrencyId() {
        return sourceCurrencyId;
    }

    public Integer getTargetCurrencyId() {
        return targetCurrencyId;
    }

    public Long getSourceAmount() {
        return sourceAmount;
    }

    public Long getTargetAmount() {
        return targetAmount;
    }

    @Override
    public RequestType getRequestType() {
        return RequestType.Q_CONVERSION_INSTRUCTION;
    }
}
