package ru.kwanza.billing.processor;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.kwanza.billing.api.IRequestProcessor;
import ru.kwanza.billing.api.SessionId;
import ru.kwanza.billing.api.order.WalletIssueOrder;
import ru.kwanza.billing.api.report.Report;
import ru.kwanza.dbtool.core.UpdateException;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

/**
 * @author Dmitry Zhukov
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:order-processors-test.xml")
public class GenerateWalletsTest extends AbstractJUnit4SpringContextTests {

    @Resource(name = "billing.processor.WalletIssueOrderProcessor")
    private IRequestProcessor<WalletIssueOrder> walletIssueProcessor;

    @Test
    public void testSuccess() throws UpdateException {
        final int WALLETS_COUNT = 10000;

        Collection<Long> walletOrderIds = walletIssueProcessor.generateRequestIds(WALLETS_COUNT);

        ArrayList<WalletIssueOrder> walletIssueOrders = new ArrayList<WalletIssueOrder>(walletOrderIds.size());
        for (Long id: walletOrderIds) {
            walletIssueOrders.add(new WalletIssueOrder(id, new SessionId("SID"), "Wallet#" + id, "Group №" + id));
        }
        Map<Long, Report> walletReports = walletIssueProcessor.process(walletIssueOrders);
    }
}
