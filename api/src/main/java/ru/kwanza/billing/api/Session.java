package ru.kwanza.billing.api;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeName;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * @author henadiy
 */
@JsonTypeName(value = "SESSION")
public class Session {

	private SessionId id;
	private Date expiresIn;
	private String userId;
	private Set<String> groupIds;

    protected Session(){

    }

	public Session(SessionId id, Date expiresIn, String userId, Set<String> groupIds) {
		this.id = id;
		this.expiresIn = expiresIn;
		this.userId = userId;
		this.groupIds = groupIds;
	}

	public SessionId getId() {
		return id;
	}

	public Date getExpiresIn() {
		return expiresIn;
	}

	public String getUserId() {
		return userId;
	}

	public Set<String> getGroupIds() {
		return groupIds;
	}

    @JsonIgnore
    public boolean isValid() {
        return null == getExpiresIn() || getExpiresIn().after(new Date());
    }
}
