package ru.kwanza.billing.gateway;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import ru.kwanza.billing.api.report.Report;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;

/**
 * @author Vasily Vorobyov
 */
public class JSONReportBuilder implements IReportBuilder {

    private static ObjectWriter writer;

    static {
        writer = new ObjectMapper()
                .configure(JsonGenerator.Feature.WRITE_BIGDECIMAL_AS_PLAIN, true)
                .configure(JsonGenerator.Feature.WRITE_NUMBERS_AS_STRINGS, true)
                .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
                .setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'"))
                .writer();
    }

    public String getMimeType() {
        return "application/json";
    }

    public String getEncoding() {
        return "utf-8";
    }

    public byte[] build(Report... reports) throws IOException {

        final ByteArrayOutputStream stream = new ByteArrayOutputStream();
        if (reports.length > 1) {
            writer.writeValue(stream, reports);
        } else {
            writer.writeValue(stream, reports[0]);
        }

        return stream.toByteArray();
    }
}
