package ru.kwanza.billing.api;

import org.junit.Test;
import ru.kwanza.billing.api.order.Order;
import ru.kwanza.billing.api.order.WalletIssueOrder;
import ru.kwanza.billing.processor.order.WalletIssueOrderProcessor;

import static org.junit.Assert.assertNotNull;

/**
 * @author Vasily Vorobyov
 */
public class FactoryTest {

    @Test
    public void test() {
        IRequestProcessorFactory f = new IRequestProcessorFactory() {
            public <T extends Request> IRequestProcessor<T> getRequestProcessor(Class<T> orderClass) {
                if (WalletIssueOrder.class.isAssignableFrom(orderClass)) {
                    return (IRequestProcessor<T>) new WalletIssueOrderProcessor();
                }
                throw new IllegalArgumentException(orderClass.getName());
            }
        };

        IRequestProcessor<WalletIssueOrder> p = f.getRequestProcessor(WalletIssueOrder.class);
        assertNotNull(p);
    }

}
