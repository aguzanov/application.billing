package ru.kwanza.billing.processor.query;

import ru.kwanza.billing.api.query.AccountStatementQuery;
import ru.kwanza.billing.api.report.*;
import ru.kwanza.billing.api.report.Report;
import ru.kwanza.billing.entity.api.*;
import ru.kwanza.billing.entity.api.dao.IAccountDAO;
import ru.kwanza.billing.entity.api.dao.IPaymentInstructionDAO;
import ru.kwanza.billing.processor.RequestProcessor;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author Dmitry Zhukov
 */
public class AccountStatementQueryProcessor extends RequestProcessor<AccountStatementQuery> {

    @Resource(name = "billing.entity.dao.PaymentInstructionDAO")
    private IPaymentInstructionDAO paymentInstructionDAO;

    @Resource(name = "billing.entity.dao.AccountDAO")
    private IAccountDAO accountDAO;

    @Override
    protected Map<Long, Report> handleRequests(Date processedAt, Collection<AccountStatementQuery> queries) {

        Map<Long, Report> outReports = new HashMap<Long, Report>(queries.size());
        Date minDateInterval = new Date(), maxDateInterval = new Date(0);
        List<BigDecimal> accountIds = new ArrayList<BigDecimal>(queries.size());

        //dzhukov: get the date intervals and the list id accounts
        Iterator<AccountStatementQuery> queryIterator = queries.iterator();
        while (queryIterator.hasNext()) {
            AccountStatementQuery query = queryIterator.next();
            Date recorderedAfter = query.getRecorderedAfter();
            Date recorderedBefore = query.getRecorderedBefore();
            if (recorderedAfter == null || recorderedBefore == null) {
                outReports.put(query.getId(),
                        new RejectedRequestReport(query.getId(), processedAt, RejectCode.EMPTY_REQUIRED_FIELD));
                queryIterator.remove();
                continue;
            }
            if (minDateInterval.after(recorderedAfter)) {
                minDateInterval = recorderedAfter;
            }
            if (maxDateInterval.before(recorderedBefore)) {
                maxDateInterval = recorderedBefore;
            }
            if (!accountIds.contains(query.getAccountId())) {
                accountIds.add(query.getAccountId());
            }
        }

        //dzhukov: check up all accounts in the database
        Map<BigDecimal, Account> accountsFromBase = accountDAO.readByKeys(accountIds);
        Collection<BigDecimal> accountIdsFromBase = accountsFromBase.keySet();
        //dzhukov: see if there's any extra subtraction of collections
        List<BigDecimal> notInBaseAccounts = new ArrayList<BigDecimal>(accountIds);
        notInBaseAccounts.removeAll(accountIdsFromBase);

        //dzhukov: getting the debits and credits of the big "bundle"
        Collection<AccountRecord> depositRecords =
                paymentInstructionDAO.selectDepositRecordsByAccountId(accountIds, minDateInterval, maxDateInterval);
        Collection<AccountRecord> withdrawalRecords =
                paymentInstructionDAO.selectWithdrawalRecordsByAccountId(accountIds, minDateInterval, maxDateInterval);

        //dzhukov: processing the debits and credits and divide large bundles into an ordered structure
        //key - the account number value - record values

        Map<BigDecimal, Collection<AccountRecord>> recordsByAccountId =
                new HashMap<BigDecimal, Collection<AccountRecord>>();
        recordsByAccountId.putAll(aggregateRecords(depositRecords, true));
        recordsByAccountId.putAll(aggregateRecords(withdrawalRecords, false));

        //dzhukov: we break down the records as per requests
        for (AccountStatementQuery query : queries) {
            //dzhukov: if you request a non-existent accounts is rejected sozdaem report
            if (!notInBaseAccounts.isEmpty() && notInBaseAccounts.contains(query.getAccountId())) {
                outReports.put(query.getId(),
                        new RejectedRequestReport(query.getId(), processedAt, RejectCode.ACCOUNT_NOT_FOUND));
                continue;
            }

            Collection<AccountRecord> records = recordsByAccountId.get(query.getAccountId());
            List<AccountRecord> currentQueryRecords = new ArrayList<AccountRecord>();

            if (records != null) {
                for (AccountRecord record : records) {
                    if ((query.getRecorderedAfter() == null || query.getRecorderedAfter().before(record.getProcessedAt())) && (
                            query.getRecorderedBefore() == null || query.getRecorderedBefore().after(record.getProcessedAt()))) {
                        currentQueryRecords.add(record);
                    }
                }
            }
            Integer currencyId = accountsFromBase.get(query.getAccountId()).getCurrencyId();
            outReports.put(query.getId(),
                    generateQueryReport(currentQueryRecords, query, currencyId, processedAt));
        }
        return outReports;
    }

    @Override
    protected Map<Long, Report> answerDuplicateRequests(Map<Long, AccountStatementQuery> duplicateRequests) {
        final Date processedAt = new Date();
        return handleRequests(processedAt, duplicateRequests.values());
    }

    private Report generateQueryReport(List<AccountRecord> queryInstructions, AccountStatementQuery query, Integer currencyId,
                                       Date currentProcessingTime) {
        Date recordedAfter = query.getRecorderedAfter();
        Date recordedBefore = query.getRecorderedBefore();
        //dzhukov: if the records are not sent the report with an empty collection
        if (queryInstructions == null || queryInstructions.isEmpty()) {
            return new AccountStatementReport(query.getId(), currentProcessingTime, query.getAccountId(), currencyId, recordedAfter,
                    recordedBefore, null);
        }
        //dzhukov: sort the records in the collection operations
        Collections.sort(queryInstructions, new Comparator<AccountRecord>() {
            public int compare(AccountRecord o1, AccountRecord o2) {
                return o1.getId().subtract(o2.getId()).intValue();
            }
        });

        //dzhukov: create a collection we need for your report
        Collection<AccountPaymentOperation> queries = new ArrayList<AccountPaymentOperation>();
        for (AccountRecord rec : queryInstructions) {
            queries.add(new AccountPaymentOperation(rec.getId(), rec.getOrderId(),
                    query.getAccountId(),
                    query.getAccountId().equals(rec.getSourceAccountId()) ? rec.getSourceAmount() : rec.getTargetAmount(),
                    query.getAccountId().equals(rec.getSourceAccountId()) ?
                            rec.getSourceIncomingBalance() : rec.getTargetIncomingBalance(),
                    query.getAccountId().equals(rec.getSourceAccountId()) ?
                            rec.getTargetAccountId() : rec.getSourceAccountId(),
                    query.getAccountId().equals(rec.getSourceAccountId()) ?
                            rec.getTargetAmount() : rec.getSourceAmount(),
                    query.getAccountId().equals(rec.getSourceAccountId()) ?
                            rec.getTargetIncomingBalance() : rec.getSourceIncomingBalance(),
                    rec.getProcessedAt(), rec.getDescription()));
        }

        return new AccountStatementReport(query.getId(), currentProcessingTime, query.getAccountId(), currencyId, recordedAfter,
                recordedBefore, queries.toArray(new AccountPaymentOperation[queries.size()]));
    }

    private Map<BigDecimal, Collection<AccountRecord>> aggregateRecords(
            Collection<? extends AccountRecord> records, boolean deposit) {
        Map<BigDecimal, Collection<AccountRecord>> outRecords =
                new HashMap<BigDecimal, Collection<AccountRecord>>();
        for (AccountRecord rec : records) {

            BigDecimal accountId = deposit ? rec.getTargetAccountId() : rec.getSourceAccountId();
            Collection<AccountRecord> currentRecordValue = outRecords.get(accountId);
            if (currentRecordValue != null) {
                currentRecordValue.add(rec);
            } else {
                Collection<AccountRecord> collection = new ArrayList<AccountRecord>();
                collection.add(rec);
                outRecords.put(accountId, collection);
            }
        }
        return outRecords;
    }

}
