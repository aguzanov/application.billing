package ru.kwanza.billing.controller.exceptions;

import ru.kwanza.billing.api.report.RejectCode;

/**
 * @author Dmitry Zhukov
 */
public class CurrencyException extends Exception {

    public CurrencyException(String message) {
        super(message);
    }

    public static class DuplicateCurrencyException extends CurrencyException {
        public DuplicateCurrencyException() {
            super("Currency with this ID is already exist");
        }
    }

    public static class CurrencyCreateException extends CurrencyException {
        public CurrencyCreateException(RejectCode rejectCode) {
            super("You cannot create currency. " + rejectCode.getTitle());
        }
    }

    public static class CurrencyNotFoundException extends CurrencyException {

        public CurrencyNotFoundException() {
            super("Currency doesn't exist.");
        }
    }

    public static class CurrencyUpdateException extends CurrencyException {
        public CurrencyUpdateException(RejectCode rejectCode) {
            super("You cannot update the currency. " + rejectCode.getTitle());
        }
    }

    public static class AccountsAlreadyExistException extends CurrencyException {
        public AccountsAlreadyExistException() {
            super("You cannot delete  currency. There are accounts of this currency.");
        }
    }

    public static class CurrencyDeleteException extends CurrencyException {
        public CurrencyDeleteException(RejectCode rejectCode) {
            super("You cannot delete a currency. " + rejectCode.getTitle());
        }
    }
}
