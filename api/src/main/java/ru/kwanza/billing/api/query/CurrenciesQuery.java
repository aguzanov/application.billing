package ru.kwanza.billing.api.query;

import com.fasterxml.jackson.annotation.JsonTypeName;
import ru.kwanza.billing.api.RequestType;
import ru.kwanza.billing.api.SessionId;

/**
 * @author Vasily Vorobyov
 */
@JsonTypeName(value = "Q_CURRENCIES")
public class CurrenciesQuery extends Query {

    protected CurrenciesQuery() {
    }

    public CurrenciesQuery(Long id, SessionId sessionId) {
        super(id, sessionId);
    }

    @Override
    public RequestType getRequestType() {
        return RequestType.Q_CURRENCIES;
    }
}
