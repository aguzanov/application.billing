Ext.define('Billing.store.DetailOrderStore', {
    extend: 'Ext.data.Store',
    model: 'Billing.model.DetailOrder',
    autoLoad: true,
    remoteFilter: true,
    remoteSort: false,
    autoSync: false
});
