Ext.define('Billing.view.restexchange.RESTExchangeList', {
    requires: [
        'Console.grid.FiltersFeature',
        'Console.application.QuickLink',
        'Billing.store.RESTExchangeStore'
    ],
    extend: 'Console.grid.BaseGrid',
    alias: ['widget.rest-exchange-list', 'widget.restexchangelist'],
    tabTitle: 'REST API Journal',
    caption: 'REST API Journal',

    model: 'Billing.model.RESTExchange',
    store: 'Billing.store.RESTExchangeStore',
    iconCls: 'i-documents',

    constructor: function () {
        var me = this;

        me.columns = [
            {
                xtype: 'quicklinkcolumn',
                text: 'ID',
                dataIndex: 'id',
                width: 80
            },
            {
                xtype: 'quicklinkcolumn',
                text: 'URI',
                dataIndex: 'uri',
                width: 200,
                filter: true
            },
            {
                text: 'Remote address',
                dataIndex: 'remoteAddress',
                width: 200
            },
            {
                xtype: 'datecolumn',
                format: 'd.m.Y H:i:s',
                text: 'Timestamp',
                dataIndex: 'ts',
                width: 200,
                filter: true
            },
            {
                xtype: 'storecolumn', text: 'Result', dataIndex: 'isSuccess', width: 150, filter: true, showIcons: true,
                store: {
                    xtype: 'store',
                    fields: ['id', 'title', 'iconCls'],
                    data: [
                        {id: true, title: "Successful", iconCls: 'i-action-accept'},
                        {id: false, title: "Failed", iconCls: 'i-action-close'}
                    ]
                }
            }
        ];

        me.callParent(arguments);
    }
});
