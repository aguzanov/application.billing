/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.view.order.accountissueorder.AccountIssueOrderList', {

    extend: 'Billing.view.order.OrderList',

    requires: [
        'Billing.store.AccountIssueOrderStore'
    ],

    alias: ['widget.account-issue-order-list', 'widget.accountissueorderlist'],
    tabTitle: 'Register accounts orders',
    caption: 'Register accounts orders ',

    model: 'Billing.model.AccountIssueOrder',
    store: 'Billing.store.AccountIssueOrderStore',
    iconCls: 'i-document-order',

    constructor: function () {
        var me = this;

        me.columns = [
            {
                text: 'Issuer',
                dataIndex: 'issuerId',
                displayField: "issuerTitle",
                valueField: 'issuerId',
                width: 200,
                filter: {
                    store: 'Billing.store.IssuerStore'
                },
                xtype: 'quicklinkcolumn',
                detailView: 'Billing.view.issuer.IssuerCard'
            },
            {
                xtype: 'quicklinkcolumn',
                detailView: 'Billing.view.account.AccountCard',
                valueField: 'accountId',
                text: 'Account',
                dataIndex: 'accountId',
                width: 200

            }
        ];

        me.callParent(arguments);
    }
});
