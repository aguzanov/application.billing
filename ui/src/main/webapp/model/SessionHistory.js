/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.model.SessionHistory', {
    extend: 'Ext.data.Model',
    fields: [
        'id',
        'sessionKey',
        'userId',
        {
            name: 'openedAt',
            type: 'date',
            dateFormat: 'd.m.Y H:i:s.u'
        },
        {
            name: 'closedAt',
            type: 'date',
            dateFormat: 'd.m.Y H:i:s.u'
        }
    ],
    entityClassName: 'SessionHistory',

    detailView: 'Billing.view.sessionhistory.SessionHistoryCard',

    permissions: {
        read: "SessionHistory:read",
        create: false,
        update: false,
        destroy: false
    },

    proxy: {
        type: 'ajax',
        apiUrl: 'billing-controller/sessionhistory/'
    }
});
