/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.store.WalletAccountIssueOrderStore', {
    extend: 'Ext.data.Store',
    model: 'Billing.model.WalletAccountIssueOrder',
    autoLoad: true,
    remoteFilter: true,
    remoteSort: false,
    autoSync: false
});