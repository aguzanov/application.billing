package ru.kwanza.billing.processor.signer;

/**
 * @author henadiy
 */
public class NopSignatureGenerator implements ISignatureGenerator {
    public String getSignature(byte[] data) {
        return new String(data);
    }
}
