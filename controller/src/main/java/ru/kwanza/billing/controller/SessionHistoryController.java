package ru.kwanza.billing.controller;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.kwanza.billing.entity.api.SessionHistoryRecord;
import ru.kwanza.console.security.api.AuthOperation;
import ru.kwanza.dbtool.orm.api.IEntityManager;
import ru.kwanza.dbtool.orm.api.If;
import ru.kwanza.dbtool.orm.api.OrderBy;
import ru.kwanza.txn.api.Transactional;
import ru.kwanza.txn.api.TransactionalType;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @author Dmitry Zhukov
 */

@Controller
@RequestMapping("sessionhistory")
public class SessionHistoryController {
    @Resource(name = "dbtool.IEntityManager")
    private IEntityManager manager;

    @RequestMapping(value = "read", method = RequestMethod.GET)
    @ModelAttribute("data")
    @Transactional(TransactionalType.REQUIRES_NEW)
    @AuthOperation(name = "View session history", check = "hasPermission('SessionHistory:read')", auditable = false)
    public List<SessionHistoryRecord> getList(@RequestParam(value = "start", required = false) Integer start,
                                              @RequestParam(value = "limit", required = false) Integer limit,
                                              @RequestParam(value = "id", required = false) Long id,
                                              @RequestParam(value = "filter.token", required = false) String sessionId,
                                              @RequestParam(value = "filter.userId", required = false) String userId,
                                              @RequestParam(value = "filter.openedAt.from", required = false) @DateTimeFormat(pattern = "dd.MM.yyyy HH:mm:ss") Date openedAtFrom,
                                              @RequestParam(value = "filter.openedAt.to", required = false) @DateTimeFormat(pattern = "dd.MM.yyyy HH:mm:ss") Date openedAtTo,
                                              @RequestParam(value = "filter.closedAt.from", required = false) @DateTimeFormat(pattern = "dd.MM.yyyy HH:mm:ss") Date closedAtFrom,
                                              @RequestParam(value = "filter.closedAt.to", required = false) @DateTimeFormat(pattern = "dd.MM.yyyy HH:mm:ss") Date closedAtTo) {


        return manager.filtering(SessionHistoryRecord.class)
                .filter(id != null, If.isEqual("id"), id)
                .filter(openedAtFrom != null, If.isGreaterOrEqual("openedAt"), openedAtFrom)
                .filter(openedAtTo != null, If.isLessOrEqual("openedAt"), openedAtTo)
                .filter(closedAtFrom != null, If.isGreaterOrEqual("closedAt"), closedAtFrom)
                .filter(closedAtTo != null, If.isLessOrEqual("closedAt"), closedAtTo)
                .orderBy(OrderBy.DESC("openedAt"))
                .paging(start, limit)
                .selectList();
    }
}
