
Ext.define('Billing.model.DetailOrder', {
    extend: 'Billing.model.Order',
    fields: [
        {
            name: "orderType",
            persist: false,
            convert: function (v, record) {
                return Billing.model.DetailOrder.metadata[record.get('type')].title;
            }
        }
    ],

    proxy: {
        type: 'ajax',
        apiUrl: 'billing-controller/detailorder/'
    },

    permissions: {
        read: true,
        create: false,
        update: false,
        destroy: false
    },

    statics: {

        metadata: {},

        registerMetaData: function (code, title, detailView) {
            return this.metadata[code] = {code: code, title: title, detailView: detailView}
        },

        orderRenderer: function (value, meta, record) {
            var id = record.get('id');
            return Console.application.QuickLink.createLink(id, Billing.model.DetailOrder.metadata[record.get('type')].detailView, {recordId: id})
        },

        orderTypeRenderer: function (value, meta, record) {
            if (value && value != '') {
                var state = Ext.getStore('Billing.store.lookup.OrderType').getById(value);
                if (state) {
                    meta.iconCls = state.get('iconCls');
                }
                return state && state.title != '' ? state.get('title') : 'Unknown';
            }
            return 'Unknown';
        }
    }
});
