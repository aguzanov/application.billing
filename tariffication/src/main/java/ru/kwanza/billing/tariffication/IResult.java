package ru.kwanza.billing.tariffication;

/**
 * @author henadiy
 */
public interface IResult<A extends IArguments> {

    ResultCode getResultCode();

    A getArguments();

}
