package ru.kwanza.billing.entity.impl;

import ru.kwanza.billing.api.order.RegisterIssuerOrder;

import java.util.Date;

/**
 * @author Vasily Vorobyov
 */
public class RegisterIssuerOrderRegistrator extends RequestRegistrator<
        RegisterIssuerOrder, ru.kwanza.billing.entity.api.RegisterIssuerOrder> {

    public RegisterIssuerOrderRegistrator() {
        super(ru.kwanza.billing.entity.api.RegisterIssuerOrder.class);
    }

    @Override
    protected ru.kwanza.billing.entity.api.RegisterIssuerOrder createEntity(RegisterIssuerOrder apiOrder, Date processedAt, Long sessionRecordId) {
        return new ru.kwanza.billing.entity.api.RegisterIssuerOrder(
                apiOrder.getId(), sessionRecordId, apiOrder.getExchangeId(), processedAt, apiOrder.getIssuerId(), apiOrder.getIssuerBin(), apiOrder.getTitle());
    }

    @Override
    protected Long getEntityId(ru.kwanza.billing.entity.api.RegisterIssuerOrder entity) {
        return entity.getId();
    }
}
