package ru.kwanza.billing.entity.api;

import ru.kwanza.console.security.api.AuthEntity;
import ru.kwanza.dbtool.orm.annotations.Association;
import ru.kwanza.dbtool.orm.annotations.Entity;

import java.util.Date;

/**
 * @author Vasily Vorobyov
 */
@Entity(table = "wallet_issue_order")
@AuthEntity(name = "WalletIssueOrder", description = "Open wallet order")
public class WalletIssueOrder extends Order {

    @Association(property = "id", relationProperty = "orderId")
    private Wallet wallet;

    public WalletIssueOrder() {
    }

    public WalletIssueOrder(Long id, Long exchangeId, Long sessionRecordId, Date processedAt) {
        super(id, sessionRecordId, exchangeId, processedAt, OrderType.REGISTER_WALLET);
    }

    public Wallet getWallet() {
        return wallet;
    }

    @Override
    public void initType() {
        type = OrderType.REGISTER_WALLET.getCode();
    }
}
