package ru.kwanza.billing.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.kwanza.billing.api.order.RegisterCurrencyOrder;
import ru.kwanza.billing.api.order.RemoveCurrencyOrder;
import ru.kwanza.billing.api.report.RejectedRequestReport;
import ru.kwanza.billing.api.report.Report;
import ru.kwanza.billing.controller.billing.BillingHelper;
import ru.kwanza.billing.controller.exceptions.CurrencyException;
import ru.kwanza.billing.entity.api.Currency;
import ru.kwanza.console.core.IMerger;
import ru.kwanza.console.security.api.AuthOperation;
import ru.kwanza.console.security.api.EntityAuditor;
import ru.kwanza.dbtool.core.UpdateException;
import ru.kwanza.dbtool.orm.api.IEntityManager;
import ru.kwanza.dbtool.orm.api.If;
import ru.kwanza.dbtool.orm.api.OrderBy;
import ru.kwanza.txn.api.Transactional;
import ru.kwanza.txn.api.TransactionalType;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Dmitry Zhukov
 */

@Controller
@RequestMapping("currency")
public class CurrencyController {

    @Resource(name = "dbtool.IEntityManager")
    private IEntityManager manager;

    @Resource(name = "console.IMerger")
    private IMerger merger;

    @Resource(name = "billing.BillingHelper")
    private BillingHelper billingHelper;


    @RequestMapping(value = "read", method = RequestMethod.GET)
    @ModelAttribute("data")
    @Transactional(TransactionalType.REQUIRES_NEW)
    @AuthOperation(name = "View currencies", check = "hasPermission('Currency:read')", auditable = false)
    public List<Currency> getList(@RequestParam(value = "id", required = false) Integer id,
                                  @RequestParam(value = "filter.id", required = false) Integer filterId,
                                  @RequestParam(value = "filter.alphaCode", required = false) String alphaCode,
                                  @RequestParam(value = "filter.title", required = false) String title) {

        return manager.filtering(Currency.class)
                .filter(id != null, If.isEqual("id"), id)
                .filter(filterId != null, If.isEqual("id"), filterId)
                .filter(alphaCode != null, If.like("alphaCode"), "%" + (alphaCode == null ? alphaCode : alphaCode.toUpperCase()) + "%")
                .filter(title != null, If.like("title"), "%" + (title == null ? title : title.toUpperCase()) + "%")
                .orderBy(OrderBy.ASC("id"))
                .selectList();
    }

    @RequestMapping(value = "create", method = RequestMethod.POST)
    @ModelAttribute("data")
    @Transactional(TransactionalType.REQUIRES_NEW)
    @AuthOperation(name = "Create currency", check = "hasPermission('Currency:create')")
    public Currency create(@RequestBody Currency data, HttpServletRequest request)
            throws IllegalAccessException, UpdateException, CurrencyException {

        Currency chechCurrency = manager.readByKey(Currency.class, data.getId());
        if (chechCurrency != null) {
            throw new CurrencyException.DuplicateCurrencyException();
        }


        Report report = billingHelper.processRequest(
                new RegisterCurrencyOrder(billingHelper.generateRequestId(), billingHelper.getSessionId(request),
                        data.getId(), data.getExponent(), data.getAlphaCode(), data.getTitle())
        );

        if (report instanceof RejectedRequestReport) {
            throw new CurrencyException.CurrencyCreateException(((RejectedRequestReport) report).getRejectCode());
        }
        return EntityAuditor.logCreate(manager.readByKey(Currency.class, data.getId()));
    }

    @RequestMapping(value = "update", method = RequestMethod.POST)
    @ModelAttribute("data")
    @Transactional(TransactionalType.REQUIRES_NEW)
    @AuthOperation(name = "Update currency", check = "hasPermission('Currency:update')")
    public Currency update(@RequestBody Currency data, HttpServletRequest request)
            throws UpdateException, IllegalAccessException, CurrencyException {

        Currency currency = manager.readByKey(Currency.class, data.getId());

        if (currency == null) {
            throw new CurrencyException.CurrencyNotFoundException();
        }

        EntityAuditor.logBeforeUpdate(currency);


        Report report = billingHelper.processRequest(
                new RegisterCurrencyOrder(billingHelper.generateRequestId(), billingHelper.getSessionId(request),
                        data.getId(), data.getExponent(), data.getAlphaCode(), data.getTitle())
        );

        if (report instanceof RejectedRequestReport) {
            throw new CurrencyException.CurrencyUpdateException(((RejectedRequestReport) report).getRejectCode());
        }

        merger.merge(currency, data);

        return EntityAuditor.logAfterUpdate(currency);
    }

    @RequestMapping(value = "destroy", method = RequestMethod.POST)
    @ModelAttribute("data")
    @Transactional(value = TransactionalType.REQUIRES_NEW, applicationExceptions = CurrencyException.class)
    @AuthOperation(name = "Delete currency", check = "hasPermission('Currency:delete')")
    public Currency destroy(@RequestBody Currency data, HttpServletRequest request)
            throws UpdateException, IllegalAccessException, CurrencyException {


        Report report = billingHelper.processRequest(
                new RemoveCurrencyOrder(billingHelper.generateRequestId(), billingHelper.getSessionId(request),
                        data.getId())
        );

        if (report instanceof RejectedRequestReport) {
            throw new CurrencyException.CurrencyDeleteException(((RejectedRequestReport) report).getRejectCode());
        }

        return EntityAuditor.logDelete(data);
    }
}
