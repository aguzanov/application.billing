package ru.kwanza.billing.entity.api;

import ru.kwanza.console.security.api.AuthField;
import ru.kwanza.dbtool.orm.annotations.Field;

import java.util.Date;

/**
 * @author Vasily Vorobyov
 */
public abstract class WalletOrder extends Order {

    @Field("wallet_id")
    @AuthField(name = "Wallet")
    private Long walletId;

    protected WalletOrder() {
    }

    protected WalletOrder(Long id, Long sessionRecordId, Long exchangeId, Date processedAt, Long walletId, OrderType orderType) {
        super(id, sessionRecordId, exchangeId, processedAt,orderType);
        this.walletId = walletId;
    }

    public Long getWalletId() {
        return walletId;
    }
}
