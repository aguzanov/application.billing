package ru.kwanza.billing.api.order;

import com.fasterxml.jackson.annotation.JsonTypeName;
import ru.kwanza.billing.api.PaymentToolId;
import ru.kwanza.billing.api.PaymentToolPermission;
import ru.kwanza.billing.api.RequestType;
import ru.kwanza.billing.api.SessionId;

/**
 * @author henadiy
 */
@JsonTypeName(value = "O_PAYMENT_TOOL_GRANT_PERMISSIONS")
public class PaymentToolPermissionsOrder extends Order {

    private PaymentToolId paymentToolId;
    private String userId;
    private PaymentToolPermission[] permissions;

    public PaymentToolPermissionsOrder() {
    }

    public PaymentToolPermissionsOrder(Long id, SessionId sessionId,
                                       PaymentToolId paymentToolId, String userId, PaymentToolPermission... permissions) {
        super(id, sessionId);
        this.paymentToolId = paymentToolId;
        this.userId = userId;
        this.permissions = permissions;
    }

    public PaymentToolId getPaymentToolId() {
        return paymentToolId;
    }

    public String getUserId() {
        return userId;
    }

    public PaymentToolPermission[] getPermissions() {
        return permissions;
    }

    @Override
    public RequestType getRequestType() {
        return RequestType.O_PAYMENT_TOOL_GRANT_PERMISSIONS;
    }
}
