package ru.kwanza.billing.entity.api;

import ru.kwanza.console.security.api.AuthEntity;
import ru.kwanza.console.security.api.AuthField;
import ru.kwanza.dbtool.orm.annotations.Entity;
import ru.kwanza.dbtool.orm.annotations.Field;
import ru.kwanza.dbtool.orm.annotations.ManyToOne;

import java.util.Date;

/**
 * @author Dmitry Zhukov
 */
@Entity(table = "remove_currency_order")
@AuthEntity(name = "RemoveCurrencyOrder", description = "Remove currency order")
public class RemoveCurrencyOrder extends Order {

    @Field("currency_id")
    @AuthField(name = "Currency ID ")
    private Integer currencyId;

    @ManyToOne(property = "currencyId")
    private Currency currency;


    public RemoveCurrencyOrder() {
    }

    public RemoveCurrencyOrder(Long id, Long sessionRecordId, Long exchangeId, Date processedAt, Integer currencyId) {
        super(id, sessionRecordId, exchangeId, processedAt, OrderType.REMOVE_CURRENCY_ORDER);
        this.currencyId = currencyId;
    }

    public Integer getCurrencyId() {
        return currencyId;
    }

    @Override
    public void initType() {
        type = OrderType.REMOVE_CURRENCY_ORDER.getCode();
    }
}
