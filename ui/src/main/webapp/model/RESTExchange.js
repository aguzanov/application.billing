/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.model.RESTExchange', {
    extend: 'Ext.data.Model',
    fields: [
        'id',
        'uri',
        'remoteAddress',
        'isSuccess',
        'requestHeaders',
        'request',
        'responseHeaders',
        'response',
        {
            name: 'ts',
            type: 'date',
            dateFormat: 'd.m.Y H:i:s.u'
        }
    ],

    entityClassName: 'RESTExchange',

    detailView: 'Billing.view.restexchange.RESTExchangeCard',

    permissions: {
        read: "RESTExchange:read",
        create: false,
        update: false,
        destroy: false
    },

    proxy: {
        type: 'ajax',
        apiUrl: 'billing-controller/restexchange/'
    },

    statics: {
        statusRenderer: function (value, meta, record) {
            if (record.data.isSuccess != true) {
                meta.iconCls = 'i-action-close';
                return "Not successful"
            } else {
                meta.iconCls = 'i-action-accept';
                return "Successful";
            }
        }
    }
});
