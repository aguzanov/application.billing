/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.store.AccountIssueOrderStore', {
    extend: 'Ext.data.Store',
    model: 'Billing.model.AccountIssueOrder',
    autoLoad: true,
    remoteFilter: true,
    remoteSort: false,
    autoSync: false
});
