package ru.kwanza.billing.entity.impl;

import ru.kwanza.billing.api.Request;
import ru.kwanza.billing.entity.api.IRequestRegistrator;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Map;

/**
 * @author Vasily Vorobyov
 */
public class NopRequestRegistrator implements IRequestRegistrator<Request> {

    public Map<Long, Request> registerRequests(Collection<Request> requests, Date processedAt) {
        return Collections.emptyMap();
    }
}
