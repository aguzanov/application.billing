package ru.kwanza.billing.entity.api;

import ru.kwanza.dbtool.orm.annotations.Entity;
import ru.kwanza.dbtool.orm.annotations.Field;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author henadiy
 */
@Entity(table = "payment_tool_permissions_order")
public class PaymentToolPermissionsOrder extends Order{


    @Field("user_id")
    private String userId;

    @Field("permissions")
    private Integer permissions;

    @Field("account_id")
    private BigDecimal accountId;

    @Field("wallet_account_id")
    private BigDecimal walletAccountId;


    protected PaymentToolPermissionsOrder() {
    }

    public PaymentToolPermissionsOrder(Long id, Long sessionRecordId, Long exchangeId, Date processedAt, String userId, Integer permissions,
                                       BigDecimal accountId, BigDecimal walletAccountId) {
        super(id, sessionRecordId,exchangeId, processedAt,OrderType.PERMISSION_ORDER);
        this.userId = userId;
        this.permissions = permissions;
        this.accountId = accountId;
        this.walletAccountId = walletAccountId;
    }

    public String getUserId() {
        return userId;
    }

    public Integer getPermissions() {
        return permissions;
    }

    public BigDecimal getAccountId() {
        return accountId;
    }

    public BigDecimal getWalletAccountId() {
        return walletAccountId;
    }

    @Override
    public void initType() {
        type = OrderType.PERMISSION_ORDER.getCode();
    }
}
