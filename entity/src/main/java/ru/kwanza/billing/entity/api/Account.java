package ru.kwanza.billing.entity.api;

import ru.kwanza.billing.api.AccountStatus;
import ru.kwanza.billing.entity.api.id.AccountIdBuilder;
import ru.kwanza.console.core.ReadOnly;
import ru.kwanza.console.security.api.AuthEntity;
import ru.kwanza.console.security.api.AuthField;
import ru.kwanza.console.security.api.AuthIdField;
import ru.kwanza.dbtool.orm.annotations.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Types;
import java.util.Date;

/**
 * @author Vasily Vorobyov
 */
@Entity(table = "account")
@AuthEntity(name = "Account", description = "Account")
public class Account implements Serializable {

    private AccountIdBuilder idBuilder;

    @IdField(value = "id", type = Types.DECIMAL)
    @AuthIdField
    @AuthField(name = "Account number")
    private BigDecimal id;

    @VersionField("version")
    private Long version;

    @AuthField(name = "Open date")
    @Field("created_at")
    @ReadOnly
    private Date createdAt;

    @AuthField(name = "ID of the order")
    @Field("order_id")
    private Long orderId;

    @AuthField(name = "Current balance")
    @Field("balance")
    private Long balance;

    @AuthField(name = "Minimum balance")
    @Field("min_balance")
    private Long minBalance;

    @AuthField(name = "Maximum balance")
    @Field("max_balance")
    private Long maxBalance;

   @AuthField(name = "Last time of balance changes")
    @Field("last_record_at")
    private Date lastRecordAt;

    @AuthField(name = "Sequence number of the last record on the account")
    @Field("last_record_number")
    private Integer lastRecordNumber;

    @AuthField(name = "ID the last entry on the account")
    @Field("last_record_id")
    private BigDecimal lastRecordId;

    @AuthField(name = "Sum of the last balance changes")
    @Field("last_record_amount")
    private Long lastRecordAmount;

    @AuthField(name = "Status")
    @Field("status_code")
    private Integer statusCode;

    @ManyToOne(property = "orderId")
    private AccountIssueOrder order;

    public Account() {
    }

    public Account(Date createdAt, Issuer issuer, Currency currency, Integer accountCounter, Long orderId, Long minBalance, Long maxBalance) {
        this.idBuilder = new AccountIdBuilder(issuer.getId(), currency.getId(), accountCounter);
        this.id = idBuilder.getId();
        this.createdAt = createdAt;
        this.orderId = orderId;
        this.balance = 0L;
        this.minBalance = minBalance;
        this.maxBalance = maxBalance;
        this.statusCode = AccountStatus.ACTIVE.getCode();
        this.lastRecordNumber = 0;
        this.lastRecordId = null;
        this.lastRecordAmount = 0L;
        this.lastRecordAt = null;
    }

    public BigDecimal getId() {
        return id;
    }

    public String getIdAsString() {
        return String.format("%025s", id.toString());
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Integer getCurrencyId() {
        return getIdBuilder().getCurrencyId();
    }

    public Integer getIssuerId() {
        return getIdBuilder().getBin();
    }

    public Long getOrderId() {
        return orderId;
    }

    public Long getBalance() {
        return balance;
    }

    public void setBalance(Long balance) {
        this.balance = balance;
    }

    public Long getVersion() {
        return version;
    }

    public Integer getLastRecordNumber() {
        return lastRecordNumber;
    }

    public Long getMinBalance() {
        return minBalance;
    }

    public void setMinBalance(Long minBalance) {
        this.minBalance = minBalance;
    }

    public Long getMaxBalance() {
        return maxBalance;
    }

    public void setMaxBalance(Long maxBalance) {
        this.maxBalance = maxBalance;
    }

    public Date getLastRecordAt() {
        return lastRecordAt;
    }

    public void setLastRecordAt(Date lastRecordAt) {
        this.lastRecordAt = lastRecordAt;
    }

    public BigDecimal getLastRecordId() {
        return lastRecordId;
    }

    public void setLastRecordId(BigDecimal lastRecordId) {
        this.lastRecordId = lastRecordId;
    }

    public Long getLastRecordAmount() {
        return lastRecordAmount;
    }

    public void setLastRecordAmount(Long lastRecordAmount) {
        this.lastRecordAmount = lastRecordAmount;
    }

    public Integer nextRecordNumber() {
        return ++lastRecordNumber;
    }

    public AccountStatus getStatus() {
        return AccountStatus.findByCode(this.statusCode);
    }

    public void setStatus(AccountStatus status) {
        this.statusCode = status.getCode();
    }

    public AccountIssueOrder getOrder() {
        return order;
    }

    public static BigDecimal calculateLowerLimit(Integer issuerBin) {
        BigDecimal id = BigDecimal.valueOf(issuerBin % 1000000);
        id = id.multiply(AccountIdBuilder.CONST_1E19);
        return id;
    }

    public static BigDecimal calculateUpperLimit(Integer issuerBin) {
        BigDecimal id = BigDecimal.valueOf(issuerBin % 1000000);
        id = id.multiply(AccountIdBuilder.CONST_1E19);
        id = id.add(AccountIdBuilder.CONST_1E19);
        return id;
    }

    public static BigDecimal calculateLowerLimit(Integer issuerBin, Integer currencyId) {
        BigDecimal id = BigDecimal.valueOf(issuerBin % 1000000);
        id = id.multiply(AccountIdBuilder.CONST_1E19);
        id = id.add(BigDecimal.valueOf(currencyId % 10000).multiply(AccountIdBuilder.CONST_1E15));
        return id;
    }

    public static BigDecimal calculateUpperLimit(Integer issuerBin, Integer currencyId) {
        BigDecimal id = BigDecimal.valueOf(issuerBin % 1000000);
        id = id.multiply(AccountIdBuilder.CONST_1E19);
        id = id.add(BigDecimal.valueOf(currencyId % 10000).multiply(AccountIdBuilder.CONST_1E15));
        id = id.add(AccountIdBuilder.CONST_1E15);
        return id;
    }
    
    protected AccountIdBuilder getIdBuilder() {
        if (null == idBuilder) {
            idBuilder = new AccountIdBuilder(getId());
        }
        return idBuilder;
    }

}
