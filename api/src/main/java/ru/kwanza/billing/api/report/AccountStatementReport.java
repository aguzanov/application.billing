package ru.kwanza.billing.api.report;

import com.fasterxml.jackson.annotation.JsonTypeName;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Dmitry Zhukov
 */
@JsonTypeName(value = "ACCOUNT_STATEMENT_REPORT")
public class AccountStatementReport extends Report {
    private BigDecimal accountId;
    private Integer currencyId;
    private Date recordedAfter;
    private Date recordedBefore;
    private AccountPaymentOperation[] accountPaymentOperations;

    protected AccountStatementReport() {
    }

    public AccountStatementReport(Long id, Date processedAt, BigDecimal accountId, Integer currencyId, Date recordedAfter,
                                  Date recordedBefore, AccountPaymentOperation[] accountPaymentOperations) {
        super(id, processedAt);
        this.accountId = accountId;
        this.currencyId = currencyId;
        this.recordedAfter = recordedAfter;
        this.recordedBefore = recordedBefore;
        this.accountPaymentOperations = accountPaymentOperations;

    }

    public BigDecimal getAccountId() {
        return accountId;
    }

    public Integer getCurrencyId() {
        return currencyId;
    }

    public Date getRecordedAfter() {
        return recordedAfter;
    }

    public Date getRecordedBefore() {
        return recordedBefore;
    }

    public AccountPaymentOperation[] getAccountPaymentOperations() {
        return accountPaymentOperations;
    }

}
