package ru.kwanza.billing.api;

/**
 *
 * @author Vasily Vorobyov
 */
public interface IRequestProcessorFactory {

    <R extends Request> IRequestProcessor<R> getRequestProcessor(Class<R> requestClass);

}
