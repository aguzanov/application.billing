package ru.kwanza.billing.controller.valueobjects;

import ru.kwanza.billing.entity.api.Issuer;

import java.util.Date;

/**
 * @author Dmitry Zhukov
 */
public class RegisterIssuerOrderVO extends OrderVO {

    private Integer issuerId;

    private Issuer issuer;

    private Integer bin;

    private String title;

    public RegisterIssuerOrderVO(Long id, Long sessionRecordId, Date processedAt, Integer rejectCode, Integer issuerId, Issuer issuer, Integer bin, String title) {
        super(id, sessionRecordId, processedAt, rejectCode);
        this.issuerId = issuerId;
        this.issuer = issuer;
        this.bin = bin;
        this.title = title;
    }

    public Integer getIssuerId() {
        return issuerId;
    }

    public Issuer getIssuer() {
        return issuer;
    }

    public Integer getBin() {
        return bin;
    }

    public String getTitle() {
        return title;
    }
}
