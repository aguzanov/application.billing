package ru.kwanza.billing.entity.impl.dao;

import ru.kwanza.billing.entity.api.dao.IWalletDAO;
import ru.kwanza.billing.entity.api.Wallet;
import ru.kwanza.dbtool.core.UpdateException;
import ru.kwanza.dbtool.orm.api.If;
import ru.kwanza.dbtool.orm.api.IEntityManager;
import ru.kwanza.dbtool.orm.api.IQuery;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Vasily Vorobyov
 */
public class WalletDAO implements IWalletDAO {

    @Resource(name = "dbtool.IEntityManager")
    private IEntityManager em;
    private IQuery<Wallet> queryWalletsByOrderIds;
    private IQuery<Wallet> queryReadAll;

    public void init() {
        queryWalletsByOrderIds = em.queryBuilder(Wallet.class).where(If.in("orderId", "orderIds")).create();
        queryReadAll = em.queryBuilder(Wallet.class).create();
    }

    public List<Wallet> readAll() {
        return queryReadAll.prepare().selectList();
    }

    public Map<Long, Wallet> readByKeys(Collection<Long> ids) {
        return em.readMapByKeys(Wallet.class, ids);
    }

    public Map<Long, Wallet> readByOrderIds(Collection<Long> orderIds) {
        return queryWalletsByOrderIds.prepare().setParameter("orderIds", orderIds).selectMap("orderId");
    }

    public Wallet prepare(Integer counter, Long orderId, Date createdAt, String holderId, String title) {
        return new Wallet(counter, orderId, createdAt, holderId, title);
    }

    public Collection<Wallet> create(Collection<Wallet> wallets) throws UpdateException {
        return em.create(Wallet.class, wallets);
    }

    public void update(Collection<Wallet> wallets) throws UpdateException {
        em.update(Wallet.class, wallets);
    }
}
