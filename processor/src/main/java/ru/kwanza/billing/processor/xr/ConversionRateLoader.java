package ru.kwanza.billing.processor.xr;

import ru.kwanza.billing.entity.api.ConversionRate;
import ru.kwanza.billing.entity.api.dao.IConversionRateDAO;
import ru.kwanza.billing.entity.api.dao.ICurrencyDAO;
import ru.kwanza.dbtool.core.UpdateException;
import ru.kwanza.worker.api.FixedDelayWorker;

import java.util.*;

/**
 * @author henadiy
 */
public class ConversionRateLoader extends FixedDelayWorker {

    private static final long DAY = 1000L * 3600L * 24L;

    private Feed feed;
    private ICurrencyDAO currencyDAO;
    private IConversionRateDAO conversionRateDAO;

    public void setCurrencyDAO(ICurrencyDAO currencyDAO) {
        this.currencyDAO = currencyDAO;
    }

    public void setConversionRateDAO(IConversionRateDAO conversionRateDAO) {
        this.conversionRateDAO = conversionRateDAO;
    }

    public void setFeed(Feed feed) {
        this.feed = feed;
    }

    @Override
    protected void onFire() {
        Set<Integer> currencyPairs = new HashSet<Integer>();

        // initialize the tape and get a pair
        feed.setDate(new Date());
        feed.setRequiredCodes(getCurrencyPairsForUpdate(currencyPairs));

        // find courses to update
        Collection<ConversionRate> rates = getConversionRatesForUpdate(currencyPairs);

        // updated courses
        try {
            conversionRateDAO.create(rates);
        } catch (UpdateException e) {
            throw new RuntimeException(e);
        }
    }

    protected List<ConversionRate> getConversionRatesForUpdate(Set<Integer> currencyPairs) {
        List<ConversionRate> res = new ArrayList<ConversionRate>(currencyPairs.size());
        Collection<Feed.Entry> entries = feed.getAllEntries();

        for(Feed.Entry entry : entries) {
            if (currencyPairs.contains(
                    ConversionRate.buildCurrencyPair(entry.getSourceCode(), entry.getTargetCode()))) {
                res.add(conversionRateDAO.prepare(null, feed.getDate(), null,
                        entry.getSourceCode(), entry.getTargetCode(),
                        entry.getSourceScale(), entry.getTargetScale()));
            }
        }

        return res;
    }

    protected Set<Integer> getCurrencyPairsForUpdate( Set<Integer> currencyPairs) {
        Set<Integer> currencyIds = new LinkedHashSet<Integer>();

        // get the currency pair from the store for the date (feed.date - feed.date + 1.day - 1ms)
        // todo: write a method that returns only the pairs
        Set<Integer> existingCurrencyPairs = conversionRateDAO.readByDateRange(feed.getDate(),
                        new Date(feed.getDate().getTime() + DAY - 1L)).keySet();

        // read all currencies and sized according read all currencies (and sort by id)
        // todo: add in ICurrencyDAO a method that returns all the IDs of the currencies readAllCurrencyIds
        List<Integer> allCurrencyIds = new ArrayList<Integer>(currencyDAO.readAll().keySet());
        Collections.sort(allCurrencyIds);

        // prepare a list of currency pairs in need of updating
        for(int i = 0; i < allCurrencyIds.size()-1; i++) {
            // skip virtual currency
            if (allCurrencyIds.get(i) >= 1000) {
                break;
            }

            // build recombination
            for(int j = i+1; j < allCurrencyIds.size(); j++) {
                // skip virtual currency
                if (allCurrencyIds.get(j) >= 1000) {
                    break;
                }

                // generated pair
                Integer[] pairs = new Integer[]{
                        ConversionRate.buildCurrencyPair(allCurrencyIds.get(i), allCurrencyIds.get(j)),
                        ConversionRate.buildCurrencyPair(allCurrencyIds.get(j), allCurrencyIds.get(i))
                };
                // if pair not in the database, add eё in the list to update
                for(Integer pair : pairs) {
                    if (!existingCurrencyPairs.contains(pair)) {
                        currencyPairs.add(pair);
                        currencyIds.add(allCurrencyIds.get(i));
                        currencyIds.add(allCurrencyIds.get(j));
                    }
                }
            }
        }

        return currencyIds;
    }

}
