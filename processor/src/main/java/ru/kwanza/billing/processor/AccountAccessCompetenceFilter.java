package ru.kwanza.billing.processor;

import ru.kwanza.billing.api.*;
import ru.kwanza.billing.api.order.ConversionInstruction;
import ru.kwanza.billing.api.order.PaymentInstruction;
import ru.kwanza.billing.api.order.PaymentOrder;
import ru.kwanza.billing.api.query.IAccountInfoQuery;
import ru.kwanza.billing.api.report.RejectCode;
import ru.kwanza.billing.api.report.RejectedRequestReport;
import ru.kwanza.billing.entity.api.PaymentToolACE;
import ru.kwanza.billing.entity.api.dao.IAccessControlListDAO;
import ru.kwanza.billing.entity.api.id.PaymentToolACEIdBuilder;

import java.util.*;

/**
 * @author henadiy
 */
public class AccountAccessCompetenceFilter<R extends Request> extends AccessMatrixCompetenceFilter<R> {

    private IAccessControlListDAO aclDao;

    public void setAccessControlListDAO(IAccessControlListDAO dao) {
        this.aclDao = dao;
    }


    public Map<Long, RejectedRequestReport> filter(Date processedAt, Collection<R> requests) {

        // the results of the access check
        Map<Long, RejectedRequestReport> res = new HashMap<Long, RejectedRequestReport>(requests.size());

        // get the list of sessions
        Map<SessionId, Session> sessions = getSessions(requests);

        // the list of request ID to verify the access list
        Set<Long> reqIdsRequiresACLCheck = new HashSet<Long>(requests.size());

        // the list of records of access control to check
        Set<String> paymentToolACEIds = new HashSet<String>(requests.size());

        // check access by groups
        Iterator<R> it = requests.iterator();
        while (it.hasNext()) {
            R req = it.next();

            // obtain and review session
            Session session = sessions.get(req.getSessionId());
            if (!isSessionValid(session, processedAt)) {
                res.put(req.getId(),
                        new RejectedRequestReport(req.getId(), processedAt, RejectCode.LACKS_OF_AUTHORITY));
                it.remove();
                continue;
            }

            // count the mask the user's authority based on its groups
            int grpPerm = getGroupPermissionsMask(req, session);

            // hope the powers necessary to perform the operation
            int reqPerm = getFullRequestPermissions(req);

            // if the user does not have sufficient group rights will check ACL
            if (reqPerm != (reqPerm & grpPerm)) {
                reqIdsRequiresACLCheck.add(req.getId());
                collectPaymentToolACEIds(req, session.getUserId(), paymentToolACEIds);
            }
        }

        // read the record access control
        Map<String, PaymentToolACE> paymentToolACEs = aclDao.readPaymentToolACEByIds(paymentToolACEIds);

        // verification of access by the records access control
        it = requests.iterator();
        while (it.hasNext()) {
            R req = it.next();

            // skip requests do not require verification
            if (!reqIdsRequiresACLCheck.contains(req.getId())) continue;

            // get session data
            Session session = sessions.get(req.getSessionId());

            RejectCode rejectCode = null;
            if (req instanceof PaymentOrder) {
                // for instructions scan payment instructions and check the user permissions
                for(PaymentInstruction pi : ((PaymentOrder)req).getPaymentInstructions()) {
                    // hope the powers necessary to execute
                    int piPerm = getPaymentInstructionPermissions(pi);
                    // looking for the credentials of the user in the access list
                    PaymentToolACE ace = paymentToolACEs.get(getPaymentInstructionPaymentToolACEId(pi, session.getUserId()));
                    if (null == ace) {
                        rejectCode = RejectCode.NO_ACCESS_OR_ENTITY;
                    } else if (piPerm != (piPerm & ace.getPermissions())) {
                        rejectCode = RejectCode.LACKS_OF_AUTHORITY;
                    }
                    if (null != rejectCode) break;
                }
            } else if (req instanceof IAccountInfoQuery) {
                // looking for user rights in the access list
                PaymentToolACE ace = paymentToolACEs.get(getAccountInfoQueryPaymentToolId(req, session.getUserId()));
                if (null == ace) {
                    rejectCode = RejectCode.NO_ACCESS_OR_ENTITY;
                } else if (!PaymentToolPermission.check(ace.getPermissions(), PaymentToolPermission.READ)) {
                    rejectCode = RejectCode.LACKS_OF_AUTHORITY;
                }
            } else {
                // neponyat what's the request
                rejectCode = RejectCode.LACKS_OF_AUTHORITY;
            }

            // if the error code is specified, reject the request
            if (null != rejectCode) {
                res.put(req.getId(), new RejectedRequestReport(req.getId(), processedAt, rejectCode));
                it.remove();
            }
        }

        return res;
    }

    protected int getGroupPermissionsMask(R req, Session session) {

        // define the prefix for the codes allocation of authority
        String permissionPrefix = buildPermission(req, "");

        // collect codes credentials for the session
        Set<String> permCodes = new HashSet<String>(PaymentToolPermission.values().length);
        for(String perm : getAccessMatrix().getPermissions(session.getGroupIds())) {
            if (!perm.startsWith(permissionPrefix)) continue;
            permCodes.add(perm.substring(permissionPrefix.length()));
        }

        int mask = 0;

        // the generated mask powers for the session
        for(PaymentToolPermission p : PaymentToolPermission.values()) {
            if (permCodes.contains(p.code())) mask |= p.mask();
        }

        return mask;
    }

    protected int getFullRequestPermissions(R req) {
        int res = 0;

        if (req instanceof PaymentOrder) {
            for(PaymentInstruction pi : ((PaymentOrder)req).getPaymentInstructions()) {
                res |= getPaymentInstructionPermissions(pi);
            }
        } else {
            res = PaymentToolPermission.READ.mask();
        }
        return res;
    }

    protected void collectPaymentToolACEIds(R req, String userId, Set<String> paymentToolACEIds) {
        if (req instanceof PaymentOrder) {
            PaymentOrder pay = (PaymentOrder)req;
            for(PaymentInstruction pi : pay.getPaymentInstructions()) {
                paymentToolACEIds.add(getPaymentInstructionPaymentToolACEId(pi, userId));
            }
        } else if (req instanceof IAccountInfoQuery) {
            paymentToolACEIds.add(getAccountInfoQueryPaymentToolId(req, userId));
        }
    }

    protected int getPaymentInstructionPermissions(PaymentInstruction pi) {
        int res = 0;
        if (null == pi.getSourceAccountId() || PaymentToolId.BLANK.equals(pi.getSourceAccountId())) {
            // if no source is specified is the external replenishment
            res |= PaymentToolPermission.DEPOSIT.mask();
        } else if (null == pi.getTargetAccountId() || PaymentToolId.BLANK.equals(pi.getTargetAccountId())) {
            // if the receiver is not specified is an external debit
            res |= PaymentToolPermission.WITHDRAWAL.mask();
        } else {
            // the payment
            res |= PaymentToolPermission.PAYMENT.mask();
            // or operation of conversion
            if (pi instanceof ConversionInstruction) {
                res |= PaymentToolPermission.CONVERSION.mask();
            }
        }
        return res;
    }


    protected String getPaymentInstructionPaymentToolACEId(PaymentInstruction pi, String userId) {
        if (null == pi.getSourceAccountId() || PaymentToolId.BLANK.equals(pi.getSourceAccountId())) {
            return new PaymentToolACEIdBuilder(pi.getTargetAccountId(), userId).getId();
        } else {
            return new PaymentToolACEIdBuilder(pi.getSourceAccountId(), userId).getId();
        }
    }

    protected String getAccountInfoQueryPaymentToolId(Request req, String userId) {
        return new PaymentToolACEIdBuilder(
                new PaymentToolId(((IAccountInfoQuery)req).getAccountId(), PaymentToolId.Type.ACCOUNT), userId).getId();
    }

}
