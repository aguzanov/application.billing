/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.view.order.registercurrencyorder.RegisterCurrencyOrderCard', {
    extend: 'Billing.view.order.OrderCard',

    alias: 'widget.registercurrencyorderdetails',

    title: 'Currency order',
    iconCls: 'i-document-report',

    model: 'Billing.model.RegisterCurrencyOrder',
    store: 'Billing.store.RegisterCurrencyOrderStore',

    constructor: function (config) {
        var me = this;
        me.items = [
            {
                title: 'Additional parameters of the order',
                columnWidth: 1,
                items: [
                    {
                        fieldLabel: 'Currency',
                        renderer: function (value, meta, record) {
                            var status = record.get('rejectCode');
                            if (status) {
                                return record.get('title');
                            }
                            return Console.application.QuickLink.createLink(record.get('title'),
                                'Billing.view.currency.CurrencyCard', record.get('currencyId'));
                        }
                    },
                    {
                        fieldLabel: 'Currency code',
                        name: 'currencyId'
                    },
                    {
                        fieldLabel: 'Alpha code',
                        name: 'alphaCode'
                    },
                    {
                        fieldLabel: 'Name',
                        name: 'title'
                    },
                    {
                        fieldLabel: 'Exponent',
                        name: 'exponent'
                    }
                ]
            }
        ];
        me.callParent(arguments);
        me.items.get('main').columnWidth = 1;
    }
});
