package ru.kwanza.billing.controller.billing;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import ru.kwanza.billing.api.SessionId;
import ru.kwanza.billing.api.order.OpenSessionOrder;
import ru.kwanza.console.security.ConsoleHandler;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author henadiy
 */
@Component
public class BillingAuthenticationHandler implements ConsoleHandler {

    @Resource(name = "billing.BillingHelper")
    private BillingHelper billingHelper;
    public void onAuthenticationSuccess(
            HttpServletRequest request,
            HttpServletResponse httpServletResponse,
            Authentication authentication) throws IOException, ServletException {

        // fill in the request arguments session
        Map<String, Object> sessionArgs = new HashMap<String, Object>();
        final SessionId sessionId = billingHelper.getSessionId(request);
        sessionArgs.put(OpenSessionOrder.ARG_TOKEN, sessionId == null ? null : sessionId.getToken());
        System.out.println("Session args: " + sessionArgs);
        billingHelper.login(sessionArgs, request);

    }

    public void logout(
            HttpServletRequest request,
            HttpServletResponse httpServletResponse,
            Authentication authentication) {

        // try closing the session to the billing system
        billingHelper.logout(request);
    }
}
