package ru.kwanza.billing.processor.xr;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author henadiy
 */
@RunWith(JUnit4.class)
public class CBFeedTest {

    class TestCBFeed extends CBFeed {

        TestCBFeed() {
            super(643);
        }

        @Override
        public Collection<Entry> fetch() {
            return parse(null);
        }

        @Override
        protected Collection<Entry> parse(InputStream is) {
            return Arrays.asList(
                    buildEntry("10000", 974, "34.7399"),
                    buildEntry("1", 840, "34.8789"),
                    buildEntry("1", 978, "48.0073")
            );
        }
    }

    @Test
    public void testGetAllEntries() throws Exception {

        TestCBFeed feed = new TestCBFeed();
        List<Feed.Entry> entries = (List<Feed.Entry>)feed.getAllEntries();

        assertEquals(12, entries.size());

        assertEquals(feed.buildEntry("10000", 974, "34.7399"), entries.get(0));
        assertEquals(feed.buildEntry("10000", 974, "34.7399").getReversive(), entries.get(1));

        assertEquals(feed.buildEntry("1", 840, "34.8789"), entries.get(2));
        assertEquals(feed.buildEntry("1", 840, "34.8789").getReversive(), entries.get(3));

        assertEquals(feed.buildEntry("1", 978, "48.0073"), entries.get(4));
        assertEquals(feed.buildEntry("1", 978, "48.0073").getReversive(), entries.get(5));

        Feed.Entry e = new Feed.Entry(
                new BigDecimal("10000").multiply(new BigDecimal("34.8789")), 974,
                new BigDecimal("1").multiply(new BigDecimal("34.7399")), 840);
        assertEquals(e, entries.get(6));
        assertEquals(e.getReversive(), entries.get(7));

        e = new Feed.Entry(
                new BigDecimal("10000").multiply(new BigDecimal("48.0073")), 974,
                new BigDecimal("1").multiply(new BigDecimal("34.7399")), 978);
        assertEquals(e, entries.get(8));
        assertEquals(e.getReversive(), entries.get(9));

        e = new Feed.Entry(
                new BigDecimal("1").multiply(new BigDecimal("48.0073")), 840,
                new BigDecimal("1").multiply(new BigDecimal("34.8789")), 978);
        assertEquals(e, entries.get(10));
        assertEquals(e.getReversive(), entries.get(11));

        feed = new TestCBFeed();
        feed.setRequiredCodes(new HashSet<Integer>(Arrays.asList(974)));
        entries = (List<Feed.Entry>)feed.getAllEntries();

        assertEquals(2, entries.size());

        assertEquals(feed.buildEntry("10000", 974, "34.7399"), entries.get(0));
        assertEquals(feed.buildEntry("10000", 974, "34.7399").getReversive(), entries.get(1));

    }
}
