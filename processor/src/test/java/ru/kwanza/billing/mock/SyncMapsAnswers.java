package ru.kwanza.billing.mock;
import org.easymock.EasyMock;
import org.easymock.IAnswer;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author Vasily Vorobyov
 */
public class SyncMapsAnswers<K, V> {

    private SyncMaps<K, V> syncMaps;

    public SyncMapsAnswers(SyncMaps<K, V> syncMaps) {
        this.syncMaps = syncMaps;
    }

    public IAnswer<Collection<V>> createUpdateCollection() {
        return new IAnswer<Collection<V>>() {
            @SuppressWarnings("unchecked")
            public Collection<V> answer() throws Throwable {
                return syncMaps.createUpdate((Collection<V>) EasyMock.getCurrentArguments()[0]);
            }
        };
    }

    public IAnswer<Collection<V>> deleteCollection() {
        return new IAnswer<Collection<V>>() {
            @SuppressWarnings("unchecked")
            public Collection<V> answer() throws Throwable {
                return syncMaps.delete((Collection<V>) EasyMock.getCurrentArguments()[0]);
            }
        };
    }

    public IAnswer<Collection<V>> readByKeyCollection() {
        return new IAnswer<Collection<V>>() {
            @SuppressWarnings("unchecked")
            public Collection<V> answer() throws Throwable {
                Map<K, V> kvMap = syncMaps.readByKey((Collection<K>) EasyMock.getCurrentArguments()[0]);
                return null != kvMap ? kvMap.values() : null;
            }
        };
    }

    public IAnswer<Map<K, V>> readMapByKeyCollection() {
        return new IAnswer<Map<K, V>>() {
            @SuppressWarnings("unchecked")
            public Map<K, V> answer() throws Throwable {
                return syncMaps.readByKey((Collection<K>) EasyMock.getCurrentArguments()[0]);
            }
        };
    }

    public IAnswer<Collection<V>> readByNonUniqueIndex(final String indexName) {
        return new IAnswer<Collection<V>>() {
            public Collection<V> answer() throws Throwable {
                return syncMaps.readByNonUniqueIndex(indexName, EasyMock.getCurrentArguments()[0]);
            }
        };
    }

    public IAnswer<Collection<V>> readByNonUniqueIndexCollection(final String indexName) {
        return new IAnswer<Collection<V>>() {
            @SuppressWarnings("unchecked")
            public Collection<V> answer() throws Throwable {
                Map<Object, V> vMap = syncMaps.readByNonUniqueIndex(indexName, (Collection) EasyMock.getCurrentArguments()[0]);
                return null != vMap ? vMap.values() : null;
            }
        };
    }

    public <I> IAnswer<Map<I, List<V>>> readMapListByNonUniqueIndexCollection(final String indexName) {
        return new IAnswer<Map<I, List<V>>>() {
            @SuppressWarnings("unchecked")
            public Map<I, List<V>> answer() throws Throwable {
                return syncMaps.readByNonUniqueIndex(indexName, (Collection<I>) EasyMock.getCurrentArguments()[0]);
            }
        };
    }

    public <I> IAnswer<Map<I, V>> readByUniqueIndexCollection(final String indexName) {
        return new IAnswer<Map<I, V>>() {
            @SuppressWarnings("unchecked")
            public Map<I, V> answer() throws Throwable {
                return syncMaps.readByUniqueIndex(indexName, (Collection<I>)EasyMock.getCurrentArguments()[0]);
            }
        };
    }
}
