/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.model.WalletIssueOrder', {
    extend: 'Billing.model.Order',
    fields: [
        {
            name: 'wallet',
            persist: false
        }
    ],

    detailView: 'Billing.view.order.walletissueorder.WalletIssueOrderCard',

    permissions: {
        read: "WalletIssueOrder:read",
        create: false,
        update: false,
        destroy: false
    },

    proxy: {
        type: 'ajax',
        apiUrl: 'billing-controller/walletissueorder/'

    }
});