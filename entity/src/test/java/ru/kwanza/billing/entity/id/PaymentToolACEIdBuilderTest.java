package ru.kwanza.billing.entity.id;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import ru.kwanza.billing.api.PaymentToolId;
import ru.kwanza.billing.entity.api.id.PaymentToolACEIdBuilder;
import ru.kwanza.billing.entity.api.id.WalletAccountIdBuilder;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

/**
 * @author henadiy
 */
@RunWith(JUnit4.class)
public class PaymentToolACEIdBuilderTest {

    @Test
    public void testPack() throws Exception {

        PaymentToolACEIdBuilder b = new PaymentToolACEIdBuilder(
                new PaymentToolId(new BigDecimal("1"), PaymentToolId.Type.ACCOUNT), "user");
        assertEquals(new BigDecimal("1"), b.getPaymentToolId().getId());
        assertEquals(PaymentToolId.Type.ACCOUNT, b.getPaymentToolId().getType());
        assertEquals("user", b.getUserId());
        assertEquals("AC:1:user", b.getId());

        b = new PaymentToolACEIdBuilder(
                new PaymentToolId(new BigDecimal("2"), PaymentToolId.Type.WALLET_ACCOUNT), "user");
        assertEquals(new BigDecimal("2"), b.getPaymentToolId().getId());
        assertEquals(PaymentToolId.Type.WALLET_ACCOUNT, b.getPaymentToolId().getType());
        assertEquals("user", b.getUserId());
        assertEquals("WA:2:user", b.getId());

    }

    @Test
    public void testUnpack() throws Exception {

        PaymentToolACEIdBuilder b = new PaymentToolACEIdBuilder("AC:1:user");
        assertEquals(new BigDecimal("1"), b.getPaymentToolId().getId());
        assertEquals(PaymentToolId.Type.ACCOUNT, b.getPaymentToolId().getType());
        assertEquals("user", b.getUserId());
        assertEquals("AC:1:user", b.getId());

        b = new PaymentToolACEIdBuilder("WA:2:user");
        assertEquals(new BigDecimal("2"), b.getPaymentToolId().getId());
        assertEquals(PaymentToolId.Type.WALLET_ACCOUNT, b.getPaymentToolId().getType());
        assertEquals("user", b.getUserId());
        assertEquals("WA:2:user", b.getId());

    }
}
