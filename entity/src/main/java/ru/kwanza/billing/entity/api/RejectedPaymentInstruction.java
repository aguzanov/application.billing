package ru.kwanza.billing.entity.api;

import ru.kwanza.dbtool.orm.annotations.Entity;
import ru.kwanza.dbtool.orm.annotations.Field;
import ru.kwanza.dbtool.orm.annotations.IdField;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Vasily Vorobyov
 */
@Entity(table = "rejected_payment_instruction")
public class RejectedPaymentInstruction implements Serializable {

    @IdField("id")
    protected BigDecimal id;

    @Field("source_incoming_balance")
    protected Long sourceIncomingBalance;
    @Field("target_incoming_balance")
    protected Long targetIncomingBalance;

    protected RejectedPaymentInstruction() {
    }

    public RejectedPaymentInstruction(BigDecimal id, Long sourceIncomingBalance, Long targetIncomingBalance) {
        this.id = id;
        this.sourceIncomingBalance = sourceIncomingBalance;
        this.targetIncomingBalance = targetIncomingBalance;
    }

    public BigDecimal getId() {
        return id;
    }

    public Long getSourceIncomingBalance() {
        return sourceIncomingBalance;
    }

    public Long getTargetIncomingBalance() {
        return targetIncomingBalance;
    }
}
