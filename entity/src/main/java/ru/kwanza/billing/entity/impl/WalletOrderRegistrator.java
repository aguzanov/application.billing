package ru.kwanza.billing.entity.impl;

import ru.kwanza.billing.api.order.ActivateWalletOrder;
import ru.kwanza.billing.api.order.SuspendWalletOrder;
import ru.kwanza.billing.api.order.WalletOrder;

import java.util.Date;

/**
 * @author Dmitry Zhukov
 */
public class WalletOrderRegistrator extends RequestRegistrator<WalletOrder, ru.kwanza.billing.entity.api.WalletOrder> {

    public WalletOrderRegistrator() {
        super(ru.kwanza.billing.entity.api.WalletOrder.class);
    }

    @Override
    protected ru.kwanza.billing.entity.api.WalletOrder createEntity(WalletOrder walletOrder, Date processedAt, Long sessionRecordId) {
        if (walletOrder instanceof ActivateWalletOrder) {
            return new ru.kwanza.billing.entity.api.ActivateWalletOrder(walletOrder.getId(), sessionRecordId, walletOrder.getExchangeId(),
                    processedAt, walletOrder.getWalletId());
        } else if (walletOrder instanceof SuspendWalletOrder) {
            return new ru.kwanza.billing.entity.api.SuspendWalletOrder(walletOrder.getId(), sessionRecordId, walletOrder.getExchangeId(),
                    processedAt, walletOrder.getWalletId());
        } else {
            //dzhukov: walletOrder instanceof CloseWalletOrder
            return new ru.kwanza.billing.entity.api.CloseWalletOrder(walletOrder.getId(), sessionRecordId, walletOrder.getExchangeId(),
                    processedAt, walletOrder.getWalletId());
        }
    }

    @Override
    protected Long getEntityId(ru.kwanza.billing.entity.api.WalletOrder walletOrder) {
        return walletOrder.getId();
    }
}
