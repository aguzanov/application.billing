/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.view.order.paymentorder.PaymentOrderCard', {

    extend: 'Console.panel.CardPanel',

    alias: 'widget.paymentordercard',

    title: 'Payment order',
    iconCls: 'i-document-license',

    model: 'Billing.model.PaymentOrder',
    store: 'Billing.store.PaymentOrderStore',

    defaultType: 'fieldset',
    defaults: {
        columnWidth: 0.5,
        margin: '5 5 4 5',
        defaultType: 'textfield',
        minWidth: 560,
        defaults: {
            labelWidth: 140,
            anchor: '100%'
        }
    },

    layout: {
        type: 'vbox',
        align: 'stretch',
        pack: 'start'
    },


    initComponent: function () {
        var me = this;
        me.mainPanel = Ext.create("Ext.form.Panel", {
            cardFieldContainer: true,
            layout: {
                type: 'hbox',
                align: 'stretch',
                pack: 'start'
            },

            defaults: {
                columnWidth: 0.5,
                margin: '5 5 4 5',
                defaultType: 'textfield',
                minWidth: 300,
                defaults: {
                    labelWidth: 140,
                    anchor: '100%'
                }
            },

            bodyStyle: 'background-color: #dfe8f6;',
            border: false,
            autoScroll: true,
            header: false,

            items: [
                {
                    title: 'Main parameters',
                    itemId: 'main',
                    xtype: "fieldset",
                    flex: 2,
                    autoScroll: true,
                    bodyStyle: 'background-color: #dfe8f6;',
                    items: [
                        {
                            fieldLabel: 'ID',
                            name: 'id'
                        },
                        {
                            fieldLabel: 'Session',
                            name: 'sessionId',
                            renderer: Billing.model.Order.sessionRenderer
                        },
                        {
                            fieldLabel: 'Exchange',
                            name: 'exchangeId',
                            renderer: Billing.model.Order.exchangeRenderer
                        },
                        {
                            xtype: 'datefield',
                            format: 'd.m.Y H:i:s',
                            fieldLabel: 'Processed at',
                            name: 'processedAt'
                        },
                        {
                            fieldLabel: 'Result',
                            name: 'rejectCode',
                            renderer: Billing.model.Order.rejectedCodeRenderer
                        }
                    ]
                },
                {
                    title: 'Additional parameters of the order',
                    xtype: "fieldset",
                    flex: 1,
                    items: [
                        {
                            fieldLabel: 'Number of instructions',
                            name: 'instructionCount'
                        },
                        {
                            fieldLabel: 'Description',
                            name: 'description'
                        }
                    ]
                }

            ]
        });


        me.items = [
            me.mainPanel,
            {
                xtype: "tabpanel",
                itemId: "orderReports",
                flex: 1,
                activeTab: 0,
                items: [
                    {
                        title: 'Payment instructions',
                        xtype: 'grid',
                        layout: 'auto',
                        store: Ext.getStore('empty-store'),
                        itemId: 'paymentInstructions',
                        columns: [
                            {
                                text: 'ID',
                                dataIndex: 'id',
                                width: 60
                            },
                            {
                                text: 'Sender',
                                columns: [
                                    {
                                        text: 'Type',
                                        width: 100,
                                        renderer: function (value, meta, record) {
                                            if (record.get('sourceAccountId')) {
                                                meta.iconCls = "i-budget";
                                                return 'Accounti';
                                            } else if (record.get('sourceWalletAccountId')) {
                                                meta.iconCls = "i-briefcase";
                                                return 'The wallet';
                                            } else {
                                                return "";
                                            }
                                        }
                                    },
                                    {
                                        text: 'Number',
                                        width: 164,
                                        renderer: function (value, meta, record) {
                                            if (record.get('sourceAccountId')) {
                                                return Console.application.QuickLink.createLink(record.get('sourceAccountId'),
                                                    'Billing.view.account.AccountCard', record.get('sourceAccountId'));
                                            } else if (record.get('sourceWalletAccountId')) {
                                                return Console.application.QuickLink.createLink(record.get('sourceWalletAccountId'),
                                                    'Billing.view.walletaccount.WalletAccountCard', record.get('sourceWalletAccountId'));
                                            } else {
                                                return "";
                                            }
                                        }
                                    },
                                    {
                                        text: 'Amount',
                                        dataIndex: 'sourceAmount',
                                        width: 80
                                    }
                                ]
                            },
                            {
                                text: 'Receiver',
                                columns: [
                                    {
                                        text: 'Type',
                                        width: 120,
                                        renderer: function (value, meta, record) {
                                            if (record.get('targetAccountId')) {
                                                meta.iconCls = "i-budget";
                                                return 'Account';
                                            } else if (record.get('targetWalletAccountId')) {
                                                meta.iconCls = "i-briefcase";
                                                return 'Wallet';
                                            } else {
                                                return "";
                                            }
                                        }
                                    },
                                    {
                                        text: 'Number',
                                        width: 164,
                                        renderer: function (value, meta, record) {
                                            if (record.get('targetAccountId')) {
                                                return Console.application.QuickLink.createLink(record.get('targetAccountId'),
                                                    'Billing.view.account.AccountCard', record.get('targetAccountId'));
                                            } else if (record.get('targetWalletAccountId')) {
                                                return Console.application.QuickLink.createLink(record.get('targetWalletAccountId'),
                                                    'Billing.view.walletaccount.WalletAccountCard', record.get('targetWalletAccountId'));
                                            } else {
                                                return "";
                                            }
                                        }
                                    },
                                    {
                                        text: 'Amount',
                                        dataIndex: 'targetAmount',
                                        width: 80
                                    }
                                ]
                            },

                            {
                                text: 'Description',
                                dataIndex: 'description',
                                width: 200
                            }
                        ]
                    },
                    {

                        xtype: 'grid',
                        layout: 'auto',
                        title: 'Records',
                        itemId: 'accountRecords',
                        store: Ext.getStore('empty-store'),
                        columns: [
                            {
                                text: 'ID',
                                dataIndex: 'id',
                                width: 60
                            },
                            {
                                text: 'Sender',
                                columns: [
                                    {
                                        xtype: 'quicklinkcolumn',
                                        detailView: 'Billing.view.account.AccountCard',
                                        valueField: 'sourceAccountId',
                                        text: 'Account',
                                        dataIndex: 'sourceAccountId',
                                        width: 145
                                    },
                                    {
                                        text: 'Balance',
                                        dataIndex: 'sourceIncomingBalance',
                                        width: 100
                                    },
                                    {
                                        text: 'Amount',
                                        dataIndex: 'sourceAmount',
                                        width: 100
                                    }
                                ]
                            },
                            {
                                text: 'Receiver',
                                columns: [
                                    {
                                        xtype: 'quicklinkcolumn',
                                        detailView: 'Billing.view.account.AccountCard',
                                        valueField: 'targetAccountId',
                                        text: 'Account',
                                        dataIndex: 'targetAccountId',
                                        width: 145
                                    },
                                    {
                                        text: 'Balance',
                                        dataIndex: 'targetIncomingBalance',
                                        width: 100
                                    },
                                    {
                                        text: 'Amount',
                                        dataIndex: 'targetAmount',
                                        width: 100
                                    }
                                ]
                            },
                            {
                                text: 'Description',
                                dataIndex: 'description',
                                width: 200
                            }
                        ]


                    }
                ]
            }
        ];

        me.callParent(arguments);
    },

    updateRecord: function (record) {
        var me = this;
        me.callParent(arguments);

        if (record) {
            var paymentInstructions = me.down('#paymentInstructions');
            var accountRecords = me.down('#accountRecords');
            var accountRecordsStore = Ext.create('Billing.store.AccountRecordStore', {
                autoLoad: true,
                filters: [
                    new Ext.util.Filter({property: 'orderId', value: record.get('id')})
                ]
            });
            accountRecordsStore.on('load', me.accountRecordsLoad, me, {});
            var paymentInstructionsStore = Ext.create('Billing.store.PaymentInstructionStore', {
                autoLoad: true,
                filters: [
                    new Ext.util.Filter({property: 'orderId', value: record.get('id')})
                ]
            });
            paymentInstructionsStore.on('load', me.paymentInstructionsLoad, me, {});
        }

    },

    accountRecordsLoad: function (store) {
        var me = this;
        var accountRecords = me.down('#accountRecords');
        if (accountRecords) {
            accountRecords.reconfigure(store);
        }
    },

    paymentInstructionsLoad: function (store) {
        var me = this;
        var paymentInstructions = me.down('#paymentInstructions');
        if (paymentInstructions) {
            paymentInstructions.reconfigure(store);
        }
    }
});
