package ru.kwanza.billing.entity.api;

import ru.kwanza.dbtool.orm.annotations.Entity;
import ru.kwanza.dbtool.orm.annotations.Field;
import ru.kwanza.dbtool.orm.annotations.IdField;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author henadiy
 */
@Entity(name = "ConversionRate", table = "conversion_rate")
public class ConversionRate implements Serializable {

    @IdField("id")
    private Long id;

    @Field("currency_pair")
    private Integer currencyPair;

    @Field("effective_from")
    private Date effectiveFrom;

    @Field("effective_to")
    private Date effectiveTo;

    @Field("source_scale")
    private BigDecimal sourceScale;

    @Field("target_scale")
    private BigDecimal targetScale;

    public ConversionRate() {
    }

    public ConversionRate(Long id, Date effectiveFrom, Date effectiveTo,
                          Integer sourceCurrencyId, Integer targetCurrencyId,
                          BigDecimal sourceScale, BigDecimal targetScale) {
        this.id = id;
        this.effectiveFrom = effectiveFrom;
        this.effectiveTo = effectiveTo;
        this.currencyPair = buildCurrencyPair(sourceCurrencyId, targetCurrencyId);
        this.sourceScale = sourceScale;
        this.targetScale = targetScale;
    }

    public Long getId() {
        return id;
    }

    public Date getEffectiveFrom() {
        return effectiveFrom;
    }

    public Date getEffectiveTo() {
        return effectiveTo;
    }

    public Integer getSourceCurrencyId() {
        return currencyPair / 10000;
    }

    public Integer getTargetCurrencyId() {
        return currencyPair % 10000;
    }

    public BigDecimal getSourceScale() {
        return sourceScale;
    }

    public BigDecimal getTargetScale() {
        return targetScale;
    }


    public Integer getCurrencyPair() {
        return currencyPair;
    }

    public static Integer buildCurrencyPair(Integer srcCurrencyId, Integer targCurrencyId) {
        return (srcCurrencyId % 10000) * 10000 + targCurrencyId % 10000;
    }

    public static Integer[] splitCurrencyPair(Integer currencyPair) {
        return new Integer[]{(currencyPair / 10000) % 10000, (currencyPair % 10000)};
    }
}
