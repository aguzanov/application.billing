package ru.kwanza.billing.processor;

import ru.kwanza.billing.api.Request;
import ru.kwanza.billing.api.report.RejectedRequestReport;

import java.util.Collection;
import java.util.Date;
import java.util.Map;

/**
 * Filter incoming calls
 * <p/>
 * Designed to test the rights of the Issuer to issue requests
 *
 * @author Vasily Vorobyov
 */
public interface ICompetenceFilter<R extends Request> {

    /**
     * Filters incoming list of references, leaving only those that can be executed from the point of view of rights availability
     *
     *
     * @param processedAt    timestamp handling
     * @param requests the package of requests, the output of the method contains only allowed to execute the order
     * @return list of rejected applications with the reasons
     */
    Map<Long, RejectedRequestReport> filter(Date processedAt, Collection<R> requests);
}
