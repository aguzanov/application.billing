package ru.kwanza.billing.api.order;

import com.fasterxml.jackson.annotation.JsonTypeName;
import org.springframework.util.Assert;
import ru.kwanza.billing.api.ISigned;
import ru.kwanza.billing.api.PaymentToolId;
import ru.kwanza.billing.api.SessionId;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author henadiy
 */
@JsonTypeName(value = "CONVERSION_INSTRUCTION")
public class ConversionInstruction extends PaymentInstruction implements ISigned {

    private String signature;
    private SessionId sessionId;
    private Date issuedAt;
    private Date expiresIn;
    private Integer sourceCurrencyId;
    private Integer targetCurrencyId;
    private BigDecimal sourceScale;
    private BigDecimal targetScale;

    protected ConversionInstruction(){
    }

    public ConversionInstruction(SessionId sessionId, Date issuedAt, Date expiresIn,
            PaymentToolId sourceAccountId, Long sourceAmount, Integer sourceCurrencyId, BigDecimal sourceScale,
            PaymentToolId targetAccountId, Long targetAmount, Integer targetCurrencyId, BigDecimal targetScale) {
        super(sourceAccountId, sourceAmount, targetAccountId, targetAmount);
        this.sessionId = sessionId;
        this.issuedAt = issuedAt;
        this.expiresIn = expiresIn;
        this.sourceCurrencyId = sourceCurrencyId;
        this.targetCurrencyId = targetCurrencyId;
        this.sourceScale = sourceScale;
        this.targetScale = targetScale;
    }


    public SessionId getSessionId() {
        return sessionId;
    }

    public Date getIssuedAt() {
        return issuedAt;
    }

    public Date getExpiresIn() {
        return expiresIn;
    }

    public Integer getSourceCurrencyId() {
        return sourceCurrencyId;
    }

    public Integer getTargetCurrencyId() {
        return targetCurrencyId;
    }

    public BigDecimal getSourceScale() {
        return sourceScale;
    }

    public BigDecimal getTargetScale() {
        return targetScale;
    }

    public String getSignature() {
        return signature;
    }

    public void sign(String signature) {
        Assert.isNull(this.signature, "Already signed");
        this.signature = signature;
    }
}
