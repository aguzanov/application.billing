package ru.kwanza.billing.controller.exceptions;

import ru.kwanza.billing.api.report.RejectCode;

/**
 * @author Dmitry Zhukov
 */
public class WalletException extends Exception {

    public WalletException(String message) {
        super(message);
    }

    public static class WalletCreateException extends WalletException {

        public WalletCreateException(RejectCode rejectCode) {
            super("It is impossible to create the wallet. " + rejectCode.getTitle());
        }
    }

    public static class WalletNotFoundException extends WalletException {

        public WalletNotFoundException() {
            super("Wallet doesn't exist");
        }
    }

    public static class WalletCloseException extends WalletException{
        public WalletCloseException(RejectCode rejectCode) {
            super("It's impossible to close wallet: " + rejectCode.getTitle());
        }
    }
}
