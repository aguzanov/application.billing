package ru.kwanza.billing.entity.api;

import ru.kwanza.console.security.api.AuthEntity;
import ru.kwanza.console.security.api.AuthField;
import ru.kwanza.console.security.api.AuthRefField;
import ru.kwanza.dbtool.orm.annotations.Association;
import ru.kwanza.dbtool.orm.annotations.Entity;
import ru.kwanza.dbtool.orm.annotations.Field;

import java.util.Date;

/**
 * @author Vasily Vorobyov
 */
@Entity(table = "register_issuer_order")
@AuthEntity(name = "RegisterIssuerOrder", description = "Create issuer order")
public class RegisterIssuerOrder extends Order {

    @Field("issuer_id")
    @AuthRefField(name = "Issuer")
    private Integer issuerId;

    @Field("bin")
    @AuthField(name = "Bank ID")
    private Integer bin;

    @Field("title")
    @AuthField(name = "Name")
    private String title;

    @Association(property = "bin" , relationProperty = "bin")
    private Issuer issuer;

    public RegisterIssuerOrder() {
    }

    public RegisterIssuerOrder(Long id, Long sessionRecordId, Long exchangeId, Date processedAt, Integer issuerId, Integer bin, String title) {
        super(id, sessionRecordId, exchangeId, processedAt, OrderType.REGISTER_ISSUER);
        this.issuerId = issuerId;
        this.bin = bin;
        this.title = title;
    }

    public Integer getIssuerId() {
        return issuerId;
    }

    public Integer getBin() {
        return bin;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public void initType() {
        type = OrderType.REGISTER_ISSUER.getCode();
    }
}
