/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.view.order.OrderCard', {
    extend: 'Console.panel.CardPanel',
    requires: [],

    defaultType: 'fieldset',
    defaults: {
        columnWidth: 0.5,
        margin: '5 5 4 5',
        defaultType: 'textfield',
        minWidth: 560,
        defaults: {
            labelWidth: 140,
            anchor: '100%'
        }
    },

    constructor: function (config) {
        var me = this;
        me.items = [
            {
                title: 'Main parameters',
                itemId: 'main',
                items: [
                    {
                        fieldLabel: 'ID',
                        name: 'id'
                    },
                    {
                        fieldLabel: 'Session',
                        name: 'sessionId',
                        renderer: Billing.model.Order.sessionRenderer
                    },
                    {
                        fieldLabel: 'Exchange',
                        name: 'exchangeId',
                        renderer: Billing.model.Order.exchangeRenderer
                    },
                    {
                        xtype: 'datefield',
                        format: 'd.m.Y H:i:s',
                        fieldLabel: 'Processed at',
                        name: 'processedAt'
                    },
                    {
                        fieldLabel: 'Result',
                        name: 'rejectCode',
                        renderer:Billing.model.Order.rejectedCodeRenderer
                    }
                ]
            }
        ].concat(this.items || []);
        me.callParent(arguments);
    }
});
