/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.view.issuer.IssuerCard', {
    extend: 'Console.panel.CardPanel',
    requires: [],

    alias: 'widget.issuerdetails',

    title: 'Issuer details',
    iconCls: 'i-person-male',

    defaultType: 'fieldset',
    defaults: {
        columnWidth: 1,
        margin: '5 5 4 5',
        defaultType: 'textfield',
        minWidth: 560,
        defaults: {
            labelWidth: 180,
            anchor: '100%'
        }
    },

    model: 'Billing.model.Issuer',
    store: 'Billing.store.IssuerStore',

    items:[
        {
            title: 'Main parameters',
            itemId: 'main',
            items: [
                {
                    fieldLabel: 'ID',
                    itemId: 'id',
                    name: 'id',
                    readOnly: true
                },
                {
                    fieldLabel: 'Order',
                    itemId: 'orderId',
                    name: 'orderId',
                    xtype: 'infofield',
                    valueField: 'orderId',
                    renderer: Billing.model.Issuer.orderRenderer,
                    readOnly: true
                },
                {
                    fieldLabel: 'Bank ID',
                    itemId: 'bin',
                    name: 'bin',
                    maxLength: 6,
                    allowBlank: false
                },
                {
                    fieldLabel: 'Name',
                    itemId: 'title',
                    name: 'title',
                    allowBlank: false,
                    maxLength: 255
                },
                {
                    fieldLabel: 'Accounts',
                    itemId: 'accountCounter',
                    name: 'accountCounter',
                    readOnly: true
                }
            ]
        }
    ],

    listeners: {
        changemode: function (mode) {
            var me = this;
            var id = me.items.get('main').items.get('id');
            var orderId = me.items.get('main').items.get('orderId');
            var accountCounter = me.items.get('main').items.get('accountCounter');


            id.hidden = mode == 'add';
            orderId.hidden = mode == 'add';
            accountCounter.hidden = mode == 'add';

        }
    }
});
