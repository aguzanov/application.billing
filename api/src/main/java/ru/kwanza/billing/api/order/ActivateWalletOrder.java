package ru.kwanza.billing.api.order;

import com.fasterxml.jackson.annotation.JsonTypeName;
import ru.kwanza.billing.api.RequestType;
import ru.kwanza.billing.api.SessionId;

/**
 * @author Dmitry Zhukov
 */
@JsonTypeName(value = "O_WALLET_ACTIVATE")
public class ActivateWalletOrder extends WalletOrder {

    public ActivateWalletOrder(Long id, SessionId sessionId, Long walletId) {
        super(id, sessionId, walletId);
    }

    @Override
    public RequestType getRequestType() {
        return RequestType.O_WALLET_ACTIVATE;
    }
}
