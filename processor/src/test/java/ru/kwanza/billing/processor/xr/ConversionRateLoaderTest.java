package ru.kwanza.billing.processor.xr;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import ru.kwanza.billing.entity.api.ConversionRate;
import ru.kwanza.billing.entity.api.Currency;
import ru.kwanza.billing.entity.api.dao.IConversionRateDAO;
import ru.kwanza.billing.entity.api.dao.ICurrencyDAO;
import ru.kwanza.dbtool.core.UpdateException;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.*;

import static org.junit.Assert.*;

/**
 * @author henadiy
 */
@RunWith(JUnit4.class)
public class ConversionRateLoaderTest {

    class TestCurrencyDAO implements ICurrencyDAO {

        public Map<Integer, Currency> readByKeys(Collection<Integer> ids) {
            return null;
        }

        public Map<Integer, Currency> readAll() {
            Map<Integer, Currency> res = new LinkedHashMap<Integer, Currency>();
            res.put(974, null);     // BYR
            res.put(840, null);     // USD
            res.put(978, null);     // EUR
            res.put(643, null);     // RUB
            res.put(52, null);      // BBD :) Barbados Dollar
            res.put(36, null);      // AUD
            res.put(2520, null);    // CUSTOM1
            res.put(5560, null);    // CUSTOM2
            return res;
        }

        public Currency prepare(Integer id, String alphaCode, Integer exponent, String title) {
            return null;
        }

        public Collection<Currency> create(Collection<Currency> currencies) throws UpdateException {
            return null;
        }

        public Collection<Currency> update(Collection<Currency> currencies) throws UpdateException {
            return null;
        }

        public Collection<Currency> delete(Collection<Currency> currencies) throws UpdateException {
            return null;
        }
    }

    class TestConversionRateDAO implements IConversionRateDAO {

        public ConversionRate prepare(Long id, Date effectiveFrom, Date effectiveTo, Integer sourceCurrencyId, Integer targetCurrencyId, BigDecimal sourceScale, BigDecimal targetScale) {
            return new ConversionRate(id, effectiveFrom, effectiveTo,
                    sourceCurrencyId, targetCurrencyId, sourceScale, targetScale);
        }

        public Map<Integer, ConversionRate> readConversionRates(Collection<Integer> currencyPairs, Date date) {
            return null;
        }

        public Map<Integer, ConversionRate> readByDateRange(Date fromDate, Date toDate) {
            Map<Integer, ConversionRate> res = new LinkedHashMap<Integer, ConversionRate>();
            res.put(9780036, null);     // EUR->AUD
            res.put(360978, null);      // AUD->EUR
            res.put(9740036, null);     // BYR->AUD
            res.put(360974, null);      // AUD->BYR
            res.put(8400036, null);     // USD->AUD
            res.put(360840, null);      // AUD->USD
            res.put(6430036, null);     // RUB->AUD
            res.put(360643, null);      // AUD->RUB
            res.put(520036, null);      // BBD->AUD
            res.put(360052, null);      // AUD->BBD
            res.put(9740840, null);     // BYR->USD
            res.put(6430978, null);     // RUB->EUR
            res.put(25200052, null);    // CUSTOM1->BBD
            res.put(8405560, null);     // USD->CUSTOM2
            return res;
        }

        public Collection<ConversionRate> create(Collection<ConversionRate> rates) throws UpdateException {
            return null;
        }
    }

    class TestCBFeed extends CBFeed {

        TestCBFeed() {
            super(643);
        }

        @Override
        public Collection<Entry> fetch() {
            return parse(null);
        }

        @Override
        protected Collection<Entry> parse(InputStream is) {
            return Arrays.asList(
                    buildEntry("10000", 974, "34.7399"),
                    buildEntry("1", 840, "34.8789"),
                    buildEntry("1", 978, "48.0073")
            );
        }
    }

    class TestConversionRateLoader extends ConversionRateLoader {
    }

    @Test
    public void testGetCurrencyPairsForUpdate() throws Exception {
        TestConversionRateLoader loader = new TestConversionRateLoader();
        loader.setCurrencyDAO(new TestCurrencyDAO());
        loader.setConversionRateDAO(new TestConversionRateDAO());
        loader.setFeed(new TestCBFeed());

        Set<Integer> pairs = new LinkedHashSet<Integer>();
        Set<Integer> ids = loader.getCurrencyPairsForUpdate(pairs);


        // 36 52 643 840 974 978
        assertArrayEquals(new Integer[]{
            /*360052, 520036, 360643, 6430036, 360840, 8400036, 360974, 9740036, 360978, 9780036,*/
            520643, 6430052, 520840, 8400052, 520974, 9740052, 520978, 9780052,
            6430840, 8400643, 6430974, 9740643, /*6430978,*/ 9780643,
            8400974, /*9740840,*/ 8400978, 9780840,
            9740978, 9780974
        }, pairs.toArray());

        assertArrayEquals(new Integer[]{
            52, 643, 840, 974, 978
        }, ids.toArray());
    }

    @Test
    public void testGetConversionRatesForUpdate() throws Exception {


        TestConversionRateLoader loader = new TestConversionRateLoader();
        loader.setCurrencyDAO(new TestCurrencyDAO());
        loader.setConversionRateDAO(new TestConversionRateDAO());

        Feed feed = new TestCBFeed();
        feed.setDate(new Date());
        loader.setFeed(feed);

        // load pairs
        Set<Integer> pairs = new LinkedHashSet<Integer>();
        feed.setRequiredCodes(loader.getCurrencyPairsForUpdate(pairs));

        // load rates
        List<ConversionRate> rates = loader.getConversionRatesForUpdate(pairs);
        assertEquals(10, rates.size());

        List<Integer> resultPairs = new ArrayList<Integer>(10);
        for(ConversionRate r : rates) {
            resultPairs.add(r.getCurrencyPair());
        }

        // 974 840 978
        assertArrayEquals(new Integer[]{
           9740643, 6430974, 8400643, 6430840, 9780643, /* 6430978, */
           /*9740840,*/ 8400974, 9740978, 9780974,
           8400978, 9780840
        }, resultPairs.toArray());

    }
}
