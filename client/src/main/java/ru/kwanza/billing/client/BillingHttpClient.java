package ru.kwanza.billing.client;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RequestCallback;
import org.springframework.web.client.ResponseExtractor;
import org.springframework.web.client.RestTemplate;
import ru.kwanza.billing.api.PaymentToolId;
import ru.kwanza.billing.api.Request;
import ru.kwanza.billing.api.SessionId;
import ru.kwanza.billing.api.order.*;
import ru.kwanza.billing.api.report.AccountReport;
import ru.kwanza.billing.api.report.Report;
import ru.kwanza.billing.api.report.SessionReport;
import ru.kwanza.billing.api.report.WalletReport;
import ru.kwanza.console.demo.DemoRegistry;
import ru.kwanza.console.demo.DemoUser;
import ru.kwanza.toolbox.Base64;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author Vasily Vorobyov
 */
public class BillingHttpClient implements IBillingClient {

    private static ObjectMapper mapper = new ObjectMapper()
            .configure(JsonGenerator.Feature.WRITE_BIGDECIMAL_AS_PLAIN, true)
            .configure(JsonGenerator.Feature.WRITE_NUMBERS_AS_STRINGS, true)
            .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
            .setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'"));

    private String basicUrl;
    private RestTemplate restTemplate = new RestTemplate(getFactory());
    private DemoRegistry demo;


    public SimpleClientHttpRequestFactory getFactory() {
        final SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
        factory.setReadTimeout(30000);
        factory.setConnectTimeout(30000);
        return factory;
    }


    public void setBasicUrl(String basicUrl) {
        this.basicUrl = basicUrl;
    }

    protected String getBasicUrl() {
        return basicUrl;
    }

    public Long generateRequestId() {
        Collection<Long> ids = generateRequestIds(1);
        return ids.iterator().next();
    }

    public List<Long> generateRequestIds(final int count) {
        try {
            return restTemplate.execute(new URI(String.format("%s/generateIds", getBasicUrl())), HttpMethod.POST,
                    new RequestCallback() {
                        public void doWithRequest(ClientHttpRequest request) throws IOException {
                            final DemoUser currentUser = demo.getCurrentUser();
                            System.out.println(currentUser);
                            if (currentUser != null) {
                                HttpHeaders headers = request.getHeaders();
                                headers.add("demo-user", currentUser.getName());
                                headers.add("demo-user-password", currentUser.getPassword());
                            }
                            request.getBody().write(String.valueOf("{\"count\":\"" + count + "\"}").getBytes("UTF-8"));
                        }
                    }, new ResponseExtractor<List<Long>>() {
                        public List<Long> extractData(ClientHttpResponse response) throws IOException {
                            return mapper.readValue(response.getBody(),
                                    mapper.getTypeFactory().constructCollectionType(ArrayList.class, Long.class));
                        }
                    }
            );
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    public List<Report> processRequests(List<Request> requests) {
        final String className = requests.get(0).getClass().getName();
        int dotPoint = className.lastIndexOf('.');
        try {
            return restTemplate.execute(
                    new URI(String.format("%s/process/%s", getBasicUrl(),
                            (dotPoint > 0 ? className.substring(dotPoint + 1) : className))),
                    HttpMethod.POST,
                    new RequestCallbackImpl(requests), new JSONResponseExtractor());
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }


    private class RequestCallbackImpl implements RequestCallback {

        private final MediaType JSON_MEDIA_TYPE = new MediaType("application", "json", Charset.forName("utf-8"));

        private List<Request> requests;

        private RequestCallbackImpl(List<Request> requests) {
            this.requests = requests;
        }

        public void doWithRequest(ClientHttpRequest httpRequest) throws IOException {
            HttpHeaders headers = httpRequest.getHeaders();
            OutputStream outputStream = httpRequest.getBody();
            headers.setContentType(JSON_MEDIA_TYPE);
            final DemoUser currentUser = demo.getCurrentUser();
            System.out.println(currentUser);
            if (currentUser != null) {
                headers.add("demo-user", currentUser.getName());
                headers.add("demo-user-password", currentUser.getPassword());
            }

            if (requests.size() > 1) {
                mapper.writeValue(outputStream, requests);
            } else {
                mapper.writeValue(outputStream, requests.get(0));
            }
        }
    }


    private static class JSONResponseExtractor implements ResponseExtractor<List<Report>> {

        public List<Report> extractData(ClientHttpResponse response) throws IOException {

            final ObjectReader objReader = mapper.reader();
            final JsonNode jsonNode = objReader.readTree(response.getBody());
            if (jsonNode.isArray()) {
                return objReader.readValue(jsonNode.traverse(), mapper.getTypeFactory()
                        .constructCollectionType(ArrayList.class, Report.class));
            } else {
                List<Report> res = new ArrayList<Report>(1);
                res.add(objReader.readValue(jsonNode.traverse(), Report.class));
                return res;
            }
        }
    }


    public DemoRegistry getDemo() {
        return demo;
    }

    public void setDemo(DemoRegistry demo) {
        this.demo = demo;
    }

    public static void main(String[] args) throws IOException {
        final BillingHttpClient client = new BillingHttpClient();
        client.setBasicUrl("http://localhost:10080");
        final DemoRegistry demoRegistry = new DemoRegistry();
        demoRegistry.setWorkingDir("/share/prj/demo/demo.ttp/stand/billing/tomcat/billing");
        client.setDemo(demoRegistry);
        demoRegistry.init();
        demoRegistry.setCurrentUser(demoRegistry.loadUserByUsername("admin"));
        final Long id = client.generateRequestId();
        final List<Report> reports = client.processRequests(Collections.<Request>singletonList(new OpenSessionOrder(id, Collections.<String, Object>emptyMap())));
        SessionReport sessionReport = (SessionReport) reports.get(0);

        Map<Integer, List<BigDecimal>> accounts = new HashMap<Integer, List<BigDecimal>>();


        createWalletAccounts(client, sessionReport.getSession().getId(), new int[]{826, 840, 978}, 3, 20, "Mr User(BAA)-", accounts);
        createWalletAccounts(client, sessionReport.getSession().getId(), new int[]{826, 840, 978}, 4, 20, "Mr User(DBA)-", accounts);
        createWalletAccounts(client, sessionReport.getSession().getId(), new int[]{826, 840, 978}, 5, 20, "Mr User(BEA))-", accounts);


        firstDeposit(client, sessionReport.getSession().getId(), accounts, 10000);
        makePayments(client, sessionReport.getSession().getId(), accounts, 50l, 500l, 20);
    }

    public static void firstDeposit(BillingHttpClient client, SessionId id, Map<Integer, List<BigDecimal>> accounts, long amount) {
        final Random random = new Random();
        for (List<BigDecimal> list : accounts.values()) {
            for (BigDecimal accountId : list) {
                client.processRequests(Collections.<Request>singletonList(new PaymentOrder(client.generateRequestId(), id, "Initial deposit",
                        getPaymentInstructions(amount, random, accountId)
                )));
            }
        }
    }

    private static PaymentInstruction[] getPaymentInstructions(long amount, Random random, BigDecimal accountId) {
        return new PaymentInstruction[]{
                new PaymentInstruction(new PaymentToolId(accountId, PaymentToolId.Type.ACCOUNT), amount + (long) (amount * random.nextDouble()))
        };
    }


    public static void makePayments(BillingHttpClient client, SessionId id, Map<Integer, List<BigDecimal>> accounts, long fromAmount, long toAmount, int iterationCount) {
        final Random random = new Random();
        for (int i = 0; i < iterationCount; i++) {
            for (List<BigDecimal> list : accounts.values()) {
                for (BigDecimal accountId : list) {
                    final BigDecimal accountIdTo = chooseAccount(random, list,accountId);
                    long amount = fromAmount + (long) ((toAmount - fromAmount) * random.nextDouble());
                    client.processRequests(Collections.<Request>singletonList(new PaymentOrder(client.generateRequestId(), id, "Transfer of " + new Date(),
                            getPaymentInstructions(accountId, accountIdTo, amount)
                    )));
                }
            }

        }
    }
    private static BigDecimal chooseAccount(Random random, List<BigDecimal> list, BigDecimal account) {
        BigDecimal bigDecimal = null;
        while (true) {
            bigDecimal = list.get(random.nextInt(list.size() - 1));
            if (!bigDecimal.equals(account)) break;
        }


        return bigDecimal;
    }
    private static PaymentInstruction[] getPaymentInstructions(BigDecimal accountId, BigDecimal accountIdTo, long amount) {
        return new PaymentInstruction[]{
                new PaymentInstruction(new PaymentToolId(accountId, PaymentToolId.Type.ACCOUNT), new PaymentToolId(accountIdTo, PaymentToolId.Type.ACCOUNT), amount)
        };
    }


    private static void createWalletAccounts(BillingHttpClient client, SessionId id, int[] currencyIds,
                                             int issuerId, int count, String userTitle,
                                             Map<Integer, List<BigDecimal>> accounts) {
        for (int i = 0; i < count; i++) {
            ArrayList<BigDecimal> accountIds = new ArrayList<BigDecimal>();
            final Random random = new Random();
            for (int currencyId : currencyIds) {
                final List<Report> reports = client.processRequests(Collections.<Request>singletonList(new AccountIssueOrder(client.generateRequestId(), id, issuerId, currencyId, 0l, 10000000l)));
                AccountReport report = (AccountReport) reports.get(0);
                accountIds.add(report.getAccountId());
                addAccount(currencyId, report.getAccountId(), accounts);
            }

            final byte[] bytes = new byte[10];
            random.nextBytes(bytes);
            final List<Report> reports = client.processRequests(Collections.<Request>singletonList(new WalletIssueOrder(client.generateRequestId(), id, Base64.encodeBytes(bytes) + String.valueOf(System.currentTimeMillis()), userTitle + i)));
            WalletReport walletReport = (WalletReport) reports.get(0);

            for (BigDecimal accountId : accountIds) {
                final List<Report> reports1 = client.processRequests(Collections.<Request>singletonList(new WalletAccountIssueOrder(client.generateRequestId(), id, walletReport.getWalletId(), accountId, "Wallet of " + userTitle + i)));
            }
        }

    }

    private static void addAccount(int currencyId, BigDecimal account, Map<Integer, List<BigDecimal>> map) {
        List<BigDecimal> list = map.get(currencyId);
        if (list == null) {
            list = new ArrayList<BigDecimal>();
            map.put(currencyId, list);
        }

        list.add(account);

    }
}
