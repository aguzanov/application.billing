package ru.kwanza.billing.clearing;

import ru.kwanza.billing.api.PaymentToolId;
import ru.kwanza.billing.api.Request;
import ru.kwanza.billing.api.order.OpenSessionOrder;
import ru.kwanza.billing.api.order.PaymentInstruction;
import ru.kwanza.billing.api.order.PaymentOrder;
import ru.kwanza.billing.api.report.RejectedRequestReport;
import ru.kwanza.billing.api.report.Report;
import ru.kwanza.billing.api.report.SessionReport;
import ru.kwanza.billing.clearing.dao.IClearingDAO;
import ru.kwanza.billing.clearing.entity.ClearingAccount;
import ru.kwanza.billing.clearing.entity.ClearingGroup;
import ru.kwanza.billing.client.IBillingClient;
import ru.kwanza.billing.entity.api.AccountRecord;
import ru.kwanza.dbtool.core.UpdateException;

import java.math.BigDecimal;
import java.util.*;

/**
 * @author henadiy
 */
public class ClearingCalculator {

	private IClearingDAO clearingDAO;
	private IBillingClient billingClient;
	private Map<String, Object> sessionArguments;

	public void setClearingDAO(IClearingDAO clearingDAO) {
		this.clearingDAO = clearingDAO;
	}

	public void setBillingClient(IBillingClient billingClient) {
		this.billingClient = billingClient;
	}

	public void setSessionArguments(Map<String, Object> sessionArguments) {
		this.sessionArguments = sessionArguments;
	}

	public void calculate(ClearingGroup group) {

		// Calculated end date of the clearing period
		Date clearingPeriodToDate = getClearingPeriodEndDate(group);

		// find all accounts for the clearing
		Collection<ClearingAccount> clearingAccounts = clearingDAO.readClearingGroupAccounts(group);

		// select the IDs of the accounts
		Set<BigDecimal> accountIds = new HashSet<BigDecimal>(clearingAccounts.size());
		for(ClearingAccount a : clearingAccounts) {
			accountIds.add(a.getAccountId());
		}

		// Total net position for the cor. accounts
		Map<BigDecimal, Long> nettoAccountPositions = new HashMap<BigDecimal, Long>();

		// obrabatyval account
		for(ClearingAccount cla : clearingAccounts) {
			long nettoPosition = 0l;

			// get the number of the last processed record
			int recordNumber = null == cla.getLastClearedRecordNumber() ? -1 : cla.getLastClearedRecordNumber();

			// get the list of operations
			Collection<AccountRecord> accountRecords = clearingDAO.readAccountRecordsByClearedRecordNumberAndPeriod(
					cla.getAccountId(), recordNumber, clearingPeriodToDate);

			// processed transactions
			for(AccountRecord ar : accountRecords) {
				long delta = 0;
				int recNum = 0;

				// if the replenishment reduced the net position otherwise uvelichivat
				if (cla.getAccountId().equals(ar.getTargetAccountId())) {
					delta -= ar.getTargetAmount();
					recNum = ar.getTargetRecordNumber();
				} else {
					delta += ar.getSourceAmount();
					recNum = ar.getSourceRecordNumber();
				}

				// change total net position
				nettoPosition += delta;

				// save room recordings
				recordNumber = Math.max(recordNumber, recNum);
			}

			// obtained by taking into account net positions and the recorded current net position
			BigDecimal nettoAccountId = cla.getClearingMember().getNettoAccountId();
			nettoAccountPositions.put(nettoAccountId, nettoPosition +
					(nettoAccountPositions.containsKey(nettoAccountId) ? nettoAccountPositions.get(nettoAccountId) : 0L));

			// save room recordings
            cla.setLastClearedRecordNumber(recordNumber);
		}

		// attempt to fix the results of the clearing
		if (persistNettoPositions(group, nettoAccountPositions)) {
			// record processed records
            try {
                clearingDAO.updateClearingAccounts(clearingAccounts);
            }catch (UpdateException e) {
                throw new RuntimeException("Unable to update clearing accounts", e);
            }

            // updated the date of the last clearing
            group.setLastClearedAt(clearingPeriodToDate);
		}
	}

	protected boolean persistNettoPositions(ClearingGroup group, Map<BigDecimal, Long> nettoAccountPositions) {

		Report report = null;

		// preparation of IDs for the query
		List<Long> requestIds = billingClient.generateRequestIds(2);

		// the session establishment
		report = billingClient.processRequests(
				Arrays.asList((Request)new OpenSessionOrder(requestIds.get(0), sessionArguments))).get(0);
		if (report instanceof RejectedRequestReport) {
			throw new RuntimeException(String.format("Unable to open clearing session, error code %s",
					((RejectedRequestReport)report).getRejectCode()));
		}


		// preparation of payment instructions of net positions
		List<PaymentInstruction> paymentInstructions = new ArrayList<PaymentInstruction>(nettoAccountPositions.size());
		for(Map.Entry<BigDecimal, Long> entry : nettoAccountPositions.entrySet()) {
			paymentInstructions.add(new PaymentInstruction(
					new PaymentToolId(entry.getKey(), PaymentToolId.Type.ACCOUNT), entry.getValue()));
		}

		// registration nesto positions
		report = billingClient.processRequests(Arrays.asList((Request)new PaymentOrder(
				requestIds.get(1), ((SessionReport)report).getSession().getId(), "Registering clearing results",
				paymentInstructions.toArray(new PaymentInstruction[paymentInstructions.size()])
		))).get(0);
		if (report instanceof RejectedRequestReport) {
			throw new RuntimeException(String.format("Unable to register clearing results for group %s, error code %s",
					group.getId(), ((RejectedRequestReport)report).getRejectCode()));
		}

		return true;
	}

	protected Date getClearingPeriodEndDate(ClearingGroup group) {
		long clearingPeriodMillis = group.getPeriod() * 1000L;
		return new Date(
				(long)Math.floor(System.currentTimeMillis() / (double)clearingPeriodMillis) * clearingPeriodMillis);
	}

}
