package ru.kwanza.billing.api.order;

import com.fasterxml.jackson.annotation.JsonTypeName;
import ru.kwanza.billing.api.RequestType;
import ru.kwanza.billing.api.SessionId;

/**
 * @author Vasily Vorobyov
 */
@JsonTypeName(value = "O_CURRENCY_REGISTER")
public class RegisterCurrencyOrder extends Order {

    private Integer curencyId;
    private Integer exponent;
    private String alphaCode;
    private String title;

    protected RegisterCurrencyOrder() {
    }

    public RegisterCurrencyOrder(Long id, SessionId sessionId, Integer curencyId, Integer exponent, String alphaCode, String title) {
        super(id, sessionId);
        this.curencyId = curencyId;
        this.exponent = exponent;
        this.alphaCode = alphaCode;
        this.title = title;
    }

    @Override
    public RequestType getRequestType() {
        return RequestType.O_CURRENCY_REGISTER;
    }

    public Integer getCurencyId() {
        return curencyId;
    }

    public Integer getExponent() {
        return exponent;
    }

    public String getAlphaCode() {
        return alphaCode;
    }

    public String getTitle() {
        return title;
    }
}
