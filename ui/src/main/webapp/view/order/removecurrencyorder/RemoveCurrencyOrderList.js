/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.view.order.removecurrencyorder.RemoveCurrencyOrderList', {
    extend: 'Billing.view.order.OrderList',

    requires: [
        'Billing.store.RemoveCurrencyOrderStore'
    ],

    alias: ['widget.remove-currency-order-list', 'widget.removecurrencyorderlist'],
    tabTitle: 'Removing currencies orders',
    caption: 'Removing currencies orders',

    model: 'Billing.model.RemoveCurrencyOrder',
    store: 'Billing.store.RemoveCurrencyOrderStore',
    iconCls: 'i-content',

    constructor: function () {
        var me = this;

        me.columns = [
            {
                text: 'Currency code',
                dataIndex: 'currencyId',
                renderer: Billing.model.RemoveCurrencyOrder.currencyRenderer,
                width: 200,
                filter: true
            }
        ];

        me.callParent(arguments);
    }
});
