package ru.kwanza.billing.api.order;

import ru.kwanza.billing.api.SessionId;

import java.util.Date;

/**
 * @author Vasily Vorobyov
 */
public abstract class WalletOrder extends Order {

    private Long walletId;

    protected WalletOrder() {
    }

    protected WalletOrder(Long id, SessionId sessionId, Long walletId) {
        super(id, sessionId);
        this.walletId = walletId;
    }

    public Long getWalletId() {
        return walletId;
    }

}
