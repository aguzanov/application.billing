package ru.kwanza.billing.entity.api.dao;

import ru.kwanza.billing.entity.api.ConversionRate;
import ru.kwanza.dbtool.core.UpdateException;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

/**
 * @author henadiy
 */
public interface IConversionRateDAO {

    ConversionRate prepare(Long id, Date effectiveFrom, Date effectiveTo,
                                         Integer sourceCurrencyId, Integer targetCurrencyId,
                                         BigDecimal sourceScale, BigDecimal targetScale);

    Collection<ConversionRate> create(Collection<ConversionRate> conversionRates) throws UpdateException;

    Map<Integer, ConversionRate> readByDateRange(Date fromDate, Date toDate);

    Map<Integer, ConversionRate> readConversionRates(Collection<Integer> currencyPairs, Date date);
}
