package ru.kwanza.billing.entity.api;

import ru.kwanza.billing.api.Request;
import ru.kwanza.dbtool.orm.api.EntityUpdateException;

import java.util.Collection;
import java.util.Date;
import java.util.Map;

/**
 * @author Vasily Vorobyov
 */
public interface IRequestRegistrator<API_REQUEST extends Request> {

    /**
     * Accepts orders in terms billing.api, converts the stored object billinng.entity.api
     * and logs ascending orders.
     *
     * Upon detection of instructions, which is already registered, returns a list of them in terms of billing.api.
     *
     *
     * @param exchangeId
     * @param requests       requests to register
     * @param processedAt    the actual processing of orders
     * @return  MEP duplicate requests by ID or an empty map if none found
     * @throws EntityUpdateException
     */
    Map<Long, API_REQUEST> registerRequests(Collection<API_REQUEST> requests, Date processedAt);
}
