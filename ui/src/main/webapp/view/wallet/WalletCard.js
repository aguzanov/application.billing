/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.view.wallet.WalletCard', {
    extend: 'Console.panel.CardPanel',
    requires: [
        'Billing.store.WalletStatusStore',
        'Billing.store.WalletAccountStore',
        'Billing.view.walletaccount.WalletAccountList'
    ],

    alias: 'widget.walletdetails',

    title: 'Wallet',
    iconCls: 'i-briefcase',

    layout: {
        type: 'vbox',
        align: 'stretch',
        pack: 'start'
    },

    defaultType: 'fieldset',
    defaults: {
        columnWidth: 0.5,
        margin: '5 5 4 5',
        defaultType: 'textfield',
        minWidth: 560,
        defaults: {
            labelWidth: 140,
            anchor: '100%'
        }
    },

    model: 'Billing.model.Wallet',

    buttons: [
        {
            xtype: 'button',
            text: 'Activate',
            itemId: 'activateWallet',
            iconCls: 'i-action-accept',
            disabled: true,
            permission: 'Wallet:activate'
        },
        {
            xtype: 'button',
            text: 'Block',
            itemId: 'suspendWallet',
            iconCls: 'i-msg-blocked',
            disabled: true,
            permission: 'Wallet:suspend'
        },
        {
            xtype: 'button',
            text: 'Close',
            itemId: 'closeWallet',
            iconCls: 'i-action-delete',
            disabled: true,
            permission: 'Wallet:close'
        }
    ],

    listeners: {
        changemode: function (mode) {
            var me = this, mainPanel = me.mainPanel;
            var createdAt = mainPanel.items.get('additional').items.get('createdAt');
            var orderId = mainPanel.items.get('additional').items.get('orderId');
            var statusCode = mainPanel.items.get('main').items.get('statusCode');
            var walletId = mainPanel.items.get('main').items.get('walletId');
            var walletReport = me.items.get('walletReport');

            createdAt.hidden = mode == 'add';
            orderId.hidden = mode == 'add';
            statusCode.hidden = mode == 'add';
            walletReport.hidden = mode == 'add';
        }
    },

    initComponent: function () {
        var me = this;
        me.mainPanel = Ext.create("Ext.form.Panel", {
            cardFieldContainer: true,
            layout: {
                type: 'hbox',
                align: 'stretch',
                pack: 'start'
            },

            defaults: {
                columnWidth: 0.5,
                margin: '5 5 4 5',
                defaultType: 'textfield',
                minWidth: 300,
                defaults: {
                    labelWidth: 140,
                    anchor: '100%'
                }
            },

            bodyStyle: 'background-color: #dfe8f6;',
            border: false,
            autoScroll: true,
            header: false,

            items: [
                {
                    title: 'Main parameters',
                    itemId: 'main',
                    xtype: "fieldset",
                    height: 126,
                    items: [
                        {
                            fieldLabel: 'The number of purse',
                            itemId: 'walletId',
                            readOnly: true,
                            renderer: function (value, meta, record) {
                                if (record) {
                                    return record.get('id');
                                }
                                return '';

                            }
                        },
                        {
                            fieldLabel: 'Holder ID',
                            name: 'holderId',
                            itemId: 'holderId',
                            maxLength: 255,
                            allowBlank: false
                        },
                        {
                            fieldLabel: 'Name',
                            name: 'title',
                            itemId: 'title',
                            maxLength: 255,
                            allowBlank: false
                        },
                        {
                            xtype: 'selectfield',
                            store: 'Billing.store.WalletStatusStore',
                            fieldLabel: 'Status',
                            name: 'statusCode',
                            itemId: 'statusCode',
                            showIcons: true,
                            readOnly: true
                        }
                    ]
                },
                {
                    title: 'For more information',
                    itemId: 'additional',
                    xtype: "fieldset",
                    height: 126,
                    items: [
                        {
                            fieldLabel: 'Issue date',
                            name: 'createdAt',
                            itemId: 'createdAt',
                            xtype: 'datefield',
                            format: 'd.m.Y H:i:s.u',
                            readOnly: true
                        },
                        {
                            fieldLabel: 'Order',
                            name: 'orderId',
                            itemId: 'orderId',
                            xtype: 'infofield',
                            valueField: 'orderId',
                            renderer: Billing.model.Wallet.orderRenderer,
                            readOnly: true
                        },
                    ]
                }
            ]
        });

        me.items = [
            me.mainPanel,
            {
                xtype: "tabpanel",
                itemId: "walletReport",
                flex: 1,
                activeTab: 0,
                items: [
                    {
                        title: 'Accounts ',
                        itemId: 'walletaccountlist',
                        xtype: 'walletaccountlist',
                        store: Ext.create('Billing.store.WalletAccountStore'),
                        parentOptions: {
                            walletRecord: ''
                        },
                        excludeFields: ['walletId']
                    }
                ]
            }
        ];
        me.callParent(arguments);
    },


    updateRecord: function (record) {
        var me = this;

        if (record) {
            me.title = 'Wallet ' + record.get('id');
        }

        me.callParent(arguments);

        var isClosed = record && (record.get('statusCode') == 3 || record.get('statusCode') == -1);
        var isSuspended = record && (record.get('statusCode') == 2 || record.get('statusCode') == -1);
        var isActivated = record && (record.get('statusCode') == 1 || record.get('statusCode') == -1);
        me.down('#activateWallet').setDisabled(record == null || isActivated);
        me.down('#suspendWallet').setDisabled(record == null || isSuspended || isClosed);
        me.down('#closeWallet').setDisabled(record == null || isClosed);
        me.down('#edit').setDisabled(record == null || isClosed);


        if (record) {
            var walletaccountlist = me.down('#walletaccountlist');
            walletaccountlist.store.addFilter([new Ext.util.Filter({
                id: 'walletId',
                property: 'walletId',
                value: this.record.get('id')
            })], false);

            walletaccountlist.parentOptions.walletRecord = record;
        }

    }
});
