package ru.kwanza.billing.api;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.*;

/**
 * @author henadiy
 */
@RunWith(JUnit4.class)
public class PaymentToolPermissionTest {

    @Test
    public void testCheck() throws Exception {
        assertTrue(PaymentToolPermission.READ.check(PaymentToolPermission.pack(
                PaymentToolPermission.READ, PaymentToolPermission.PAYMENT)));
        assertFalse(PaymentToolPermission.READ.check(PaymentToolPermission.pack(
                PaymentToolPermission.WITHDRAWAL, PaymentToolPermission.DEPOSIT)));

        assertTrue(
                PaymentToolPermission.check(PaymentToolPermission.READ.mask() | PaymentToolPermission.DEPOSIT.mask(),
                PaymentToolPermission.READ, PaymentToolPermission.DEPOSIT)
        );
        assertFalse(
                PaymentToolPermission.check(PaymentToolPermission.READ.mask() | PaymentToolPermission.DEPOSIT.mask(),
                        PaymentToolPermission.READ, PaymentToolPermission.DEPOSIT, PaymentToolPermission.WITHDRAWAL)
        );
    }

    @Test
    public void testPack() throws Exception {

        assertEquals(PaymentToolPermission.READ.mask() | PaymentToolPermission.PAYMENT.mask(),
                PaymentToolPermission.pack(PaymentToolPermission.READ, PaymentToolPermission.PAYMENT));

    }

    @Test
    public void testUnpack() throws Exception {

        assertArrayEquals(new PaymentToolPermission[]{PaymentToolPermission.READ, PaymentToolPermission.PAYMENT},
                PaymentToolPermission.unpack(PaymentToolPermission.READ.mask() | PaymentToolPermission.PAYMENT.mask()));

    }
}
