/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.view.walletaccount.WalletAccountCard', {
    extend: 'Console.panel.CardPanel',
    requires: [
    ],

    alias: 'widget.walletaccountdetails',

    title: "Wallet account details",
    iconCls: 'i-briefcase',

    defaultType: 'fieldset',
    defaults: {
        columnWidth: 0.5,
        margin: '5 5 4 5',
        defaultType: 'textfield',
        minWidth: 560,
        defaults: {
            labelWidth: 140,
            anchor: '100%'
        }
    },

    model: 'Billing.model.WalletAccount',

    initComponent: function () {
        var me = this;
        me.items = [
            {
                title: 'Main parameters',
                itemId: 'main',
                items: [
                    {
                        fieldLabel: 'Issue date',
                        name: 'createdAt',
                        itemId: 'createdAt',
                        xtype: 'datefield',
                        format: 'd.m.Y H:i:s'
                    },
                    {
                        fieldLabel: 'Order',
                        name: 'orderId',
                        itemId: 'orderId',
                        xtype: 'infofield',
                        valueField: 'orderId',
                        detailView: 'Billing.view.order.walletaccountissueorder.WalletAccountIssueOrderCard'
                    },
                    {
                        fieldLabel: 'Account',
                        name: 'accountId',
                        itemId: 'accountId',
                        xtype: "selectfield",
                        store: 'Billing.store.AccountStore',
                        detailView: 'Billing.view.AccountCard'
                    },
                    {
                        fieldLabel: 'Wallet',
                        name: 'walletId',
                        itemId: 'walletId',
                        xtype: "selectfield",
                        store: 'Billing.store.WalletStore',
                        detailView: 'Billing.view.wallet.WalletStore'
                    },
                    {
                        fieldLabel: 'Name',
                        name: 'title',
                        itemId: 'title'
                    }
                ]
            }
        ];

        me.callParent(arguments);
    },

    updateRecord: function (record) {
        var me = this;
        me.callParent(arguments);
        console.log(record);
    },

    listeners: {
        changemode: function (mode) {
            var me = this;
            var createdAt = me.items.get('main').items.get('createdAt');
            var orderId = me.items.get('main').items.get('orderId');
            var walletId = me.items.get('main').items.get('walletId');
            var accountId = me.items.get('main').items.get('accountId');
            var parentOptions = me.quickLink.options.parentOptions;

            createdAt.hidden = mode == 'add';
            orderId.hidden = mode == 'add';
            me.title = mode == 'add' ? "Add account order" : "Wallet account";

            if (parentOptions && parentOptions.walletRecord && mode == 'add') {
                walletId.readOnly = true;
                walletId.setValue(parentOptions.walletRecord.get('id'));
            }

            if (parentOptions && parentOptions.accountRecord && mode == 'add') {
                accountId.readOnly = true;
                accountId.setValue(parentOptions.accountRecord.get('id'));
            }

            createdAt.setDisabled(mode != 'view');
            orderId.setDisabled(mode != 'view');
        }
    }
});
