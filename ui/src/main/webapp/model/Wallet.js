/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.model.Wallet', {
    extend: 'Ext.data.Model',
    fields: [
        'id',
        'cachedId',
        'version',
        {name: 'createdAt', type: 'date', dateFormat: 'd.m.Y H:i:s.u'},
        {name: "order", persist: false},
        'orderId',
        'holderId',
        'title',
        'statusCode',
        'walletAccountCounter'

    ],
    entityClassName: 'Wallet',

    detailView: 'Billing.view.wallet.WalletCard',

    permissions: {
        read: "Wallet:read",
        create: "Wallet:create",
        update: "Wallet:update",
        destroy: false
    },

    proxy: {
        type: 'ajax',
        apiUrl: 'billing-controller/wallet/'
    },

    statics: {
        orderRenderer: function (value, meta, record) {
            var order = record.get('order');
            return Console.application.QuickLink.createLink("ID: " + order.id + ", processed at: " + order.processedAt,
                'Billing.view.order.walletissueorder.WalletIssueOrderCard', {recordId: order.id});
        }
    }
});
