package ru.kwanza.billing.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.kwanza.autokey.api.AutoKeyValueSequence;
import ru.kwanza.autokey.api.IAutoKey;
import ru.kwanza.billing.api.IRequestProcessor;
import ru.kwanza.billing.api.Request;
import ru.kwanza.billing.api.report.Report;
import ru.kwanza.billing.entity.api.IRequestRegistrator;
import ru.kwanza.billing.entity.api.IReportRegistrator;
import ru.kwanza.txn.api.Transactional;
import ru.kwanza.txn.api.TransactionalType;

import javax.annotation.Resource;
import java.util.*;

/**
 * Basic processor instructions
 * <p/>
 * Manages the processing of orders:
 * 1. Identify repeated requests
 * 2. Registers orders
 * 3. Discards from processing unlawful orders
 * 4. Delegates the handling to the heir
 * 5. Registers rejected orders
 * 6. Delegates the formation of responses to repeated queries
 *
 * @author Vasily Vorobyov
 */
public abstract class RequestProcessor<R extends Request> implements IRequestProcessor<R> {

    private static final Logger logger = LoggerFactory.getLogger(RequestProcessor.class);

    @Resource(name = "autokey.IAutoKey")
    protected IAutoKey autoKey;
    @Resource(name = "billing.processor.ICompetenceFilter")
    private ICompetenceFilter<R> competenceFilter;

    private IRequestRegistrator<R> requestRegistrator;
    private IReportRegistrator<Report> reportRegistrator;

    protected RequestProcessor() {
    }

    public void setRequestRegistrator(IRequestRegistrator<R> requestRegistrator) {
        this.requestRegistrator = requestRegistrator;
    }

    public void setReportRegistrator(IReportRegistrator<Report> reportRegistrator) {
        this.reportRegistrator = reportRegistrator;
    }

    @Transactional(TransactionalType.REQUIRED)
    public Collection<Long> generateRequestIds(int count) {
        // todo - Mix the date in the beginning
        AutoKeyValueSequence seq = autoKey.getValueSequence(Request.class.getName(), count);
        List<Long> orderIds = new ArrayList<Long>(count);
        while (seq.hasNext()) {
            orderIds.add(seq.next());
        }
        return orderIds;
    }

    @Transactional(TransactionalType.REQUIRED)
    public Map<Long, Report> process(Collection<R> requests) {

        long t0 = System.currentTimeMillis(), t1, t2, t3, t4, t;

        final Date processedAt = new Date();

        Map<Long, R> duplicateRequests = requestRegistrator.registerRequests(requests, processedAt);

        t1 = System.currentTimeMillis();

        HashMap<Long, Report> reports = new HashMap<Long, Report>(requests.size());

        reports.putAll(competenceFilter.filter(processedAt, requests));

        t2 = System.currentTimeMillis();

        if (!requests.isEmpty()) {
            reports.putAll(handleRequests(processedAt, requests));
        }

        t3 = System.currentTimeMillis();

        reportRegistrator.registerReports(reports.values());

        t4 = System.currentTimeMillis();

        if (!duplicateRequests.isEmpty()) {

            Map<Long, Report> duplicateReports = answerDuplicateRequests(duplicateRequests);
            if (duplicateReports.size() < duplicateRequests.size()) {
                for (Map.Entry<Long, Report> duplicateOrderReport : duplicateReports.entrySet()) {
                    reports.put(duplicateOrderReport.getKey(), duplicateOrderReport.getValue());
                    duplicateRequests.remove(duplicateOrderReport.getKey());
                }
            }

            if (!duplicateRequests.isEmpty()) {
                reports.putAll(reportRegistrator.searchRejectedOrderReports(duplicateRequests.keySet()));
            }
        }

        t = System.currentTimeMillis();

        System.out.format("process: registerRequests = %dms, competenceFilter = %dms, "
                + "handleRequests = %dms, registerReports = %dms, answerDuplicateRequests = %dms\n", t1 - t0, t2 - t1, t3 - t2, t4 - t3,
                t - t4);

        return reports;
    }

    /**
     * The execution of requests
     *
     *
     * @param processedAt
     * @param requests the package of requests admitted to the performance
     * @return a package of reports on the processing of
     */
    protected abstract Map<Long, Report> handleRequests(Date processedAt, Collection<R> requests);

    /**
     * The formation of processing results obtained by running the queries
     *
     * @param duplicateRequests package requests
     * @return a package of reports on the processing of
     */
    protected abstract Map<Long, Report> answerDuplicateRequests(Map<Long, R> duplicateRequests);
}
