package ru.kwanza.billing.api;

/**
 * @author Dmitry Zhukov
 */
public enum AccountStatus {
    ACTIVE(1, "Active"),
    SUSPENDED(2, "Blocked"),
    CLOSED(3, "Closed"),
    UNDEFINED(-1, "Not defined");

    private int code;
    private String title;

    private AccountStatus(int code, String title) {
        this.code = code;
        this.title = title;
    }

    public int getCode() {
        return code;
    }

    public String getTitle() {
        return title;
    }

    public static AccountStatus findByCode(Integer code) {
        if (null != code) {
            for (AccountStatus o: AccountStatus.values()) {
                if (o.getCode() == code) {
                    return o;
                }
            }
        }
        return UNDEFINED;
    }

}
