/**
 * @author Dmitry Zhukov
 */
Ext.define('Billing.view.order.OrderList', {
    requires: [
        'Console.grid.FiltersFeature',
        'Console.application.QuickLink'
    ],
    extend: 'Console.grid.BaseGrid',

    constructor: function (config) {
        var me = this;

        me.columns = [
            {
                xtype: 'quicklinkcolumn',
                text: 'ID',
                dataIndex: 'id',
                width: 60,
                filter: true
            },
            {
                text: 'Session',
                renderer: Billing.model.Order.sessionRenderer,
                width: 200,
                filter: false
            },
            {
                text: 'Exchange',
                renderer: Billing.model.Order.exchangeRenderer,
                width: 200,
                filter: false
            },
            {
                xtype: 'datecolumn',
                format: 'd.m.Y H:i:s',
                text: 'Processed at',
                dataIndex: 'processedAt',
                width: 150,
                filter: {
                    allowBlank: true
                }
            }
        ].concat(this.columns || [])
            .concat([
                {
                    text: 'Result',
                    dataIndex: 'rejectCode',
                    width: 150,
                    filter: {
                        showIcons: true,
                        width: 250,
                        labelWidth: 40,
                        store: {
                            type: 'composite',
                            source: Ext.getStore('Billing.store.lookup.RejectCode'),
                            staticData: [
                                {id: -1, title: 'Successfully completed', iconCls: 'i-action-accept'}
                            ]
                        }
                    },
                    renderer: Billing.model.Order.rejectedCodeRenderer
                }
            ]);

        me.callParent(arguments);
    }
});
